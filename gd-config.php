<?php

define( 'GD_VIP', '198.71.233.15' );
define( 'GD_RESELLER', 1 );
define( 'GD_ASAP_KEY', '5409fe1663dc7845cb0049ff59b673fb' );
define( 'GD_STAGING_SITE', false );
define( 'GD_EASY_MODE', true );
define( 'GD_SITE_CREATED', 1462844696 );
define( 'GD_ACCOUNT_UID', '9ea7ce36-4f24-4fb3-bfeb-b83bc5f13528' );
define( 'GD_SITE_TOKEN', 'abdac5d7-44d6-4231-a8e0-5f43e5efb419' );
define( 'GD_TEMP_DOMAIN', '8f0.134.myftpupload.com' );
define( 'GD_CDN_ENABLED', FALSE );

// Newrelic tracking
if ( function_exists( 'newrelic_set_appname' ) ) {
	newrelic_set_appname( '9ea7ce36-4f24-4fb3-bfeb-b83bc5f13528;' . ini_get( 'newrelic.appname' ) );
}

/**
 * Is this is a mobile client?  Can be used by batcache.
 * @return array
 */
function is_mobile_user_agent() {
	return array(
	       "mobile_browser"             => !in_array( $_SERVER['HTTP_X_UA_DEVICE'], array( 'bot', 'pc' ) ),
	       "mobile_browser_tablet"      => false !== strpos( $_SERVER['HTTP_X_UA_DEVICE'], 'tablet-' ),
	       "mobile_browser_smartphones" => in_array( $_SERVER['HTTP_X_UA_DEVICE'], array( 'mobile-iphone', 'mobile-smartphone', 'mobile-firefoxos', 'mobile-generic' ) ),
	       "mobile_browser_android"     => false !== strpos( $_SERVER['HTTP_X_UA_DEVICE'], 'android' )
	);
}


