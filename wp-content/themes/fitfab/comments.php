<?php
/**
 * Fitfab - Comments
 *
 * This is comments list template
 *
 * @package fitfab
 * @subpackage fitfab
 * @since fitfab 1.0.0
 */
if (!empty($_SERVER ['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER ['SCRIPT_FILENAME']))
    die('Please do not load this page directly. Thanks!');

if (post_password_required()) {
    echo '<p class="nocomments">' . esc_html__('This post is password protected. Enter the password to view comments.', 'fitfab') . '</p>';
    return;
}
function wpb_move_comment_field_to_bottom( $fields ) {
$comment_field = $fields['comment'];
unset( $fields['comment'] );
$fields['comment'] = $comment_field;
return $fields; 
}

add_filter( 'comment_form_fields', 'wpb_move_comment_field_to_bottom' );
?>
<div id="comments">
    <?php if (have_comments()) : ?>
        <div class="blog-detail-comment comment-reply">
            <h2 class="bh2"><?php esc_html_e('recent comments', 'fitfab') ?></h2>
            <h2 class="recent comments section-heading"><span><?php comments_number(esc_html__('No Comments', 'fitfab'), esc_html__('1 Comment', 'fitfab'), esc_html__('% Comments', 'fitfab')); ?></span></h2>
            <?php
            if (get_option('page_comments')) :
                $comment_pages = paginate_comments_links('echo=0');
                if ($comment_pages) : ?>
                    <div class="commentnavi pagination"><?php echo esc_html($comment_pages); ?></div>
                <?php endif;
            endif;
            ?><div class="commentlist"><?php
            wp_list_comments(array(
                'callback' => 'fitfab_comment',
                'style' => 'div',
                'type' => 'comment',
                'short_ping' => true,
                'avatar_size' => 120
            ));
            ?>
            </div>
            <?php
            if (get_option('page_comments')) :
                $comment_pages = paginate_comments_links('echo=0');
                if ($comment_pages) : ?>
                    <div class="commentnavi pagination"><?php echo esc_html($comment_pages); ?></div>';
                <?php endif;
            endif;
            ?>
        </div>
        <!-- #comments -->

    <!-- #primary -->
<?php else : // this is displayed if there are no comments so far  ?>
    <!-- #primary -->
    <?php if ('open' == $post->comment_status) : ?>
    <!--<p>comments are open, but there are no comments. </p>-->
    <?php else : // comments are closed  ?>
        <!--<p> comments are closed.</p>-->
    <?php endif; ?>
<?php endif; ?>
<?php
global $aria_req;
$fields = array(
    'author' => '<div class="form-group"><label for="name">' . esc_html__('Your Name (required)', 'fitfab') . '</label><input type="text" class="form-control" id="author" name="author" value="' . esc_attr($commenter ['comment_author']) . '" ' . $aria_req . ' placeholder="' . esc_html__('name', 'fitfab') . '"></div>',
    'email' => '<div class="form-group"><label for="email">' . esc_html__('Your Emai (required)', 'fitfab') . '</label><input type="text" class="form-control" id="email" name="email" value="' . esc_attr($commenter ['comment_author_email']) . '" ' . $aria_req . ' placeholder="' . esc_html__('email', 'fitfab') . '"></div>',
    'url' => '<div class="form-group"><label for="subject">' . esc_html__('Subject', 'fitfab') . '</label><input type="text" class="form-control" id="url" name="url" value="' . esc_attr($commenter ['comment_author_url']) . '" ' . $aria_req . ' placeholder="' . esc_html__('subject', 'fitfab') . '"></div>'
);

$comments_args = array(
    'id_form' => 'commentform',
    'id_submit' => 'submit-comment',
    'title_reply' => '<h2 class="bh2">' . esc_html__('Leave a comment', 'fitfab') . '</h2>',
    'title_reply_to' => esc_html__('Leave a Reply to %s', 'fitfab'),
    'cancel_reply_link' => esc_html__('Cancel Reply', 'fitfab'),
    'label_submit' => esc_html__('Submit', 'fitfab'),
    'comment_field' => '<div class="form-group"><label for="msg">' . esc_html__('Your message', 'fitfab') . '</label><textarea id="comment" name="comment" rows="1" cols="1" class="text-area" placeholder="' . esc_html__('Message', 'fitfab') . '" data-validation="required"></textarea></div>',
    'must_log_in' => '<p class="must-log-in">' . sprintf(wp_kses('You must be <a href="%s">logged in</a> to post a comment.','fitfab'), wp_login_url(apply_filters('the_permalink', get_permalink()))) . '</p>',
    'logged_in_as' => '<p class="logged-in-as">' . sprintf(wp_kses('Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>','fitfab'), admin_url('profile.php'), $user_identity, wp_logout_url(apply_filters('the_permalink', get_permalink()))) . '</p>',
    'comment_notes_after' => '',
    'comment_notes_before' => '',
    'fields' => $fields
);
comment_form($comments_args); ?>
    </div>

