<?php
/**
 * FitFab - oue schedule shortcode
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates
 * @since fitfab 1.0.0
 */
extract(shortcode_atts(array(
    'title' => '',
    'sub_title' => '',
    'description' => '',
    ), $atts));
	if(!empty($title) || !empty($sub_title) || !empty($description)) :
	
?>
<section class="schedule-section fit-fab-schedule-layout-one">
    <div class="clearfix">
        <div class="schedule-info">
            <?php if (!empty($title)) : ?>
                <h2><span><?php echo esc_html($title); ?></span> <?php echo esc_html($sub_title); ?></h2>
            <?php endif; ?>
            <?php if (!empty($description)) : ?>
                <p><?php echo esc_html($description); ?></p>
            <?php endif; ?>
        </div>
        <?php $day_arr = array('monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'); ?>
        <div class="schedule-slider">
            <div id="owl-schedule" class="owl-carousel">
                <?php
		$all_posts = array();
                foreach ($day_arr as $day_arrs) :
                    $args = array(
                        'meta_key' => 'fitfab_class_day',
                        'meta_value' => $day_arrs,
                        'post_type' => 'class',
                        'post_status' => 'publish',
			'post__not_in' => $all_posts,
                    );
                    $fitfab_our_schedule = new WP_Query($args);
                    if ($fitfab_our_schedule->have_posts()) :
                        while ($fitfab_our_schedule->have_posts()) : $fitfab_our_schedule->the_post();
                    if(has_post_thumbnail()):
			$all_posts[] = get_the_ID();
		    ?>
                            <div class="item">
                                <?php if (has_post_thumbnail()) :
                                    $src = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full');
                                ?>
                                <img src="<?php echo fitfab_resize($src, '284', '261'); ?>" alt="<?php echo esc_attr(get_the_title()); ?>" title="<?php echo esc_attr(get_the_title()); ?>"/>
                                <?php endif; ?>
                                <div class="slider-item-caption">
                                    <?php if (!empty($day_arrs)) : ?>
                                        <span><?php echo esc_html($day_arrs); ?></span>
                                    <?php endif; ?>
                                    <h2><?php the_title(); ?></h2>
                                    <a href="<?php the_permalink(); ?>" class="link"><?php esc_html_e('READ MORE', 'fitfab'); ?></a>
                                </div>
                            </div>
                            <?php
			             endif;
                        endwhile;
                        wp_reset_postdata();
                    endif;
                endforeach;
                ?>           
            </div>
        </div>
    </div>
</section>
<?php
endif;
echo $this->endBlockComment('fitfab_our_schedule');
