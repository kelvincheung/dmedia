<?php
/**
 * FitFab - testimonial shortcode
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates
 * @since fitfab 1.0.0
 */
extract(shortcode_atts(array(
    'news_title' => '',
    'order_by' => '',
    'sort_order' => '',
    'posts_per_page' => get_option('posts_per_page'),
    ), $atts));

$newscount = -1;
if( !empty($news_title) || !empty($order_by) || !empty($sort_order)) :

$args = array(
    'post_status' => 'publish',
    'ignore_sticky_posts' => 1,
    'showposts' => $posts_per_page,
    'orderby' => $order_by,
    'order' => $sort_order,
);
$fitfab_latest_news = new WP_Query($args);
?>
<!-- latest-news-home-2 style end here -->
<section class="latest-news-home_two fit-fab-latest-news-layout-two">
    <div class="container">
        <div class="head-global family-oswald">
            <?php if (!empty($news_title)) : ?>
            <h2 class="h2"><?php echo esc_html($news_title); ?></h2><?php endif; ?>
            <a href="#" class="link view-all"> <?php esc_html_e('view all news', 'fitfab'); ?></a>
        </div>
        <div class="row top_gap">
        <?php
	    $i = 1;
            if ($fitfab_latest_news->have_posts()) :
                while ($fitfab_latest_news->have_posts()) : $fitfab_latest_news->the_post();
	    if (has_post_thumbnail()):
		$i++;
                    ?>
                    <div class="col-xs-12 col-sm-4 classes-listing-wrap zoom">
                        <?php
                        if (has_post_thumbnail()):
                            $url = wp_get_attachment_url(get_post_thumbnail_id());
                            ?>   
                            <a href="<?php the_permalink(); ?>">
                                <figure>
                                    <img src="<?php echo esc_url(fitfab_resize($url, '360', '201')); ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" />
                                </figure>
                            </a>
                        <?php endif; ?>
                        <div class="classes-content">
                            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                            <span><a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID'))); ?>"><?php the_author(); ?></a></span>
                            <span><a href="<?php echo esc_url(home_url('/') . get_the_date('Y/m')); ?>"><?php echo get_the_date('F m Y'); ?></a></span>
                        </div>
                        <p>
                            <?php echo fitfab_truncate_content(get_the_content(), 100); ?>   
                        </p>
                        <a href="<?php the_permalink(); ?>" class="link"><?php esc_html_e('read more', 'fitfab'); ?></a>
                    </div>
                    <?php
		    endif;
		    if($i == $newscount) {
			break;
		    }
                endwhile;
                wp_reset_postdata();
            endif;
            ?>

        </div>
    </div>
</section>
<!-- latest-news-home-2 style end here -->
<?php
endif;
echo $this->endBlockComment('fitfab_latest_news');
