<?php
/**
 * FitFab - contact shortcode
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates
 * @since fitfab 1.0.0
 */
extract(shortcode_atts(array(
    'title' => '',
    'image' => '',
    'form' => '',
                ), $atts));
$url = wp_get_attachment_url($image, 'full');
?>
<!-- contact-info start here -->
<section class="contact-info fit-fab-contact-info-laout-one">
    <div class="container">
        <?php if (!empty($title)) : ?>
            <div class="head-global family-oswald">
                <h2 class="h2"><?php echo esc_html($title); ?></h2>
            </div>
        <?php endif; ?>
        <div class="row">
            <div class="col-xs-12 col-sm-7">
                <div class="comment-entry-box">
                    <?php
                    if (!empty($form)):
                        echo do_shortcode('[contact-form-7 id="' . $form . '" title="' . get_the_title($form) . '"]');
                    endif;
                    ?>
                </div>
            </div>
            <?php
            if (!empty($image)):
                $src = wp_get_attachment_url($image, 'full');
                ?>    
                <div class="col-xs-12 col-sm-5 contact-img zoom">
                    <figure><img src="<?php echo esc_url(fitfab_resize($src, '479', '681')); ?>" alt="<?php echo get_the_title($form); ?>" title="<?php echo get_the_title($form); ?>" />
                    </figure>
                </div>
            <?php endif; ?>

        </div>
    </div>
</section>
<!-- contact-info end here -->
<?php
echo $this->endBlockComment('fitfab_contact');
