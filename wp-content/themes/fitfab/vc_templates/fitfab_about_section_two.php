<?php
/**
 * FitFab - about section two
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates.va-map
 * @since fitfab 1.0.0
 */
extract(shortcode_atts(array(
    'background_image' => '',
    'title' => '',
    'sub_title' => '', 
    'about_content' => '', 
    'section_list' => '',
    ), $atts));
if(!empty($title) || !empty($sub_title) || !empty($background_imagen)) :
$image_link = wp_get_attachment_image_src($background_image, "full");
$bg_image = '';
if ($image_link) :
    $bg_image .= 'background-image:url(' . $image_link[0] . ');background-repeat:no-repeat;background-position:right top;';
endif;
$bg_image .= 'background-color:#1e1e28;';
?>
<!-- success_story_wrap start here -->
<section class="success_story_wrap1 fit-fab-feature-layout-one" style="<?php echo esc_attr($bg_image); ?>">
    <div class="container">

        <div class="row">
            <div class="col-xs-12 col-sm-10 col-md-8 col-lg-6">
                <?php if (!empty($title)) : ?>
                    <div class="head-global family-oswald ">
                        <h2 class="h2"><?php echo esc_html($title); ?> <span><?php echo esc_html($sub_title); ?></span></h2>
                    </div>
                <?php endif;
                if (!empty($about_content)) : ?>
                    <p><?php echo esc_html($about_content); ?></p>
                <?php endif;
				 $lists = vc_param_group_parse_atts($atts['section_list']);
				 if($lists > 0) :
				 ?>
                <ul class="list-global pack-list ">
                    <?php
                   
                    foreach ($lists as $list) : ?>
                        <li>
                            <i class="fa fa-check-circle-o"></i><?php echo esc_html($list['list_title']); ?>
                        </li>
                    <?php endforeach; ?>
                </ul>
                <?php endif; ?>

            </div>
        </div>
    </div>
</section>
<!-- success_story_wrap End here -->
<?php
endif;
echo $this->endBlockComment('fitfab_about_section_two');
