<?php
/**
 * FitFab - banner
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates.va-map
 * @since fitfab 1.0.0
 */
extract(shortcode_atts(array(
    'banner_image' => '',
    'title' => '',
   ), $atts));
$url = wp_get_attachment_url($banner_image, 'full');
if (esc_url($url)): ?>
    <section style="background-image: url(<?php echo esc_url($url); ?>)" class="slider-hero inner-banner_info fit-fab-banner-layout-one">
        <?php if(!empty($title)) : ?>
        <div class="container">
            <h1><?php echo esc_html($title); ?></h1>
        </div>
        <?php endif; ?>
    </section>
    <?php
endif;
