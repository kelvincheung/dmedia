<?php
/**
 * FitFab - fitness
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates
 * @since fitfab 1.0.0
 */
extract(shortcode_atts(array(
    'background_image' => '',
    'fitness_title' => '',
    'fitness_sub_title' => '',
    'fitness_description' => '',
    'fitness_heading' => '',
    'fitness_subheading' => '',
    'join_button_text' => '',
    'join_button_link' => '',
   ), $atts));

$image_link = wp_get_attachment_image_src($background_image, "large");
$bg_image = '';
if ($image_link) :
    $bg_image .= 'background-image:url(' . $image_link[0] . ');background-repeat:no-repeat;background-position:bottom right;';
endif;
$bg_image .= 'background-color:#fff;';
?>
<!--facility-list-wrap start here-->
<section class="fitfab-gym fitfab-gym-layout-one">
    <div class = "fitfab-gym-wrap" style="<?php echo esc_attr($bg_image); ?>">
	<div class = "container">
	    <div class = "row">
		<div class = "col-xs-12 col-sm-8">
		    <div class = "head-global">
			<?php if (!empty($fitness_title) || !empty($fitness_sub_title)) : ?>
    			<h2 class = "h2"><?php
				if (!empty($fitness_title)) :
				 echo esc_html($fitness_title);
				endif;
				if (!empty($fitness_sub_title)) :
				    ?> 
				    <span><?php echo esc_html($fitness_sub_title); ?></span>
    			    <?php endif;?>
    			</h2>
			    <?php
			endif;
			if (!empty($fitness_description)) : ?> 
    			<p><?php echo esc_html($fitness_description); ?></p>
			<?php endif; ?>
		    </div>

		    <div class="row top_gap">
			<?php
			if (!empty($atts['fitness_feature'])) :
			    $features = vc_param_group_parse_atts($atts['fitness_feature']);
			    if (isset($features)) :
				foreach ($features as $feature) :
				    ?>
	    			<div class="col-xs-12 col-sm-6 fit_list-block">
					<?php if (!empty($feature['feature_icon'])) : ?>
					   <figure>
						<img class="svg" src="<?php echo esc_url(wp_get_attachment_url($feature['feature_icon'])); ?>" alt="<?php echo esc_html($feature['feature_title']); ?>" />
					   </figure>
					<?php endif; ?>
					<?php if (!empty($feature['feature_title']) || !empty($feature['feature_description'])) : ?>
					    <div class="fit-description">
						<?php if (!empty($feature['feature_title'])) : ?>
		    				<h3> <?php echo esc_html($feature['feature_title']); ?> </h3>
						    <?php
						endif;
						if (!empty($feature['feature_description'])) : ?>
		    				<p><?php echo esc_html($feature['feature_description']); ?></p>
						<?php endif; ?>
					    </div>
					<?php endif; ?>
	    			</div>
				    <?php
				endforeach;
			    endif;
			endif;
			?>
		    </div>
		</div>

		<div class="col-xs-12 col-sm-4 fit_sexy">
		    <?php if (!empty($fitness_heading) || !empty($fitness_subheading)) : ?> 
    		    <h1>
			    <?php
			    if (!empty($fitness_heading)) :
			     echo esc_html($fitness_heading);
			    endif;
			    if (!empty($fitness_subheading)) :
				?>
				<strong><?php echo esc_html($fitness_subheading); ?></strong>
			    <?php endif; ?>
    		    </h1>
		    <?php endif; ?>
		    <?php if (!empty($join_button_text)) : ?>
    		    <a href="<?php echo esc_url((!empty($join_button_link) ? $join_button_link : esc_html__('#', 'fitfab'))); ?>" class="button-btn">
			      <?php echo esc_html($join_button_text); ?>
    		    </a>
		    <?php endif; ?>
		</div>
	   </div>
	</div>
   </div>
</section>
<?php
echo $this->endBlockComment('fitfab_gym_services');
