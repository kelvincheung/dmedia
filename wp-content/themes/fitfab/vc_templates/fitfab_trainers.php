<?php
/**
 * FitFab - trainers shortcode
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates
 * @since fitfab 1.0.0
 */
extract(shortcode_atts(array(
    'background_image' => '',
    'trainer_style' => '',
    'button_text' => '',
    'button_link' => '',
    'trainer_heading' => '',
    'trainer_sub_heading' => '',
    'trainer_title' => '',
    'trainer_sub_title' => '',
    'trainer_description' => '',
    'trainercount' => '',
		), $atts));

$image_link = wp_get_attachment_image_src($background_image, "large");
$bg_image = '';
if ($image_link) :
    $bg_image .= 'background-image:url(' . $image_link[0] . ');background-repeat:no-repeat;background-position:left bottom;';
endif;
$bg_image .= 'background-color:#fff;';

$args = array(
    'post_type' => 'trainer',
    'post_status' => 'publish',
    'posts_per_page' => $trainercount
);
$trainer_query = new WP_Query($args);

if ($trainer_style == 'home' || $trainer_style == '') :
    ?>
    <!-- team-info_wrap start here -->
    <section class="team-info_wrap fit-fab-trainers-layout-one" style="<?php echo esc_attr($bg_image); ?>">
        <div class="container">
    	<div class="row">
    	    <div class="col-xs-12 col-sm-4 team-info">
		    <?php if (!empty($trainer_heading) || !empty($trainer_sub_heading)) : ?> 
			<strong>
			    <?php
			    if (!empty($trainer_heading)) :
				echo esc_html($trainer_heading);
			    endif;
			    if (!empty($trainer_sub_heading)) :
				?>
	    		    <strong><?php echo esc_html($trainer_sub_heading); ?></strong>
			    <?php endif; ?>
			</strong>
			<?php
		    endif;
		    if (!empty($button_text)) :
			$button_link = (!empty($button_link)) ? $button_link : '#'; ?>
			 <a href="<?php echo esc_url($button_link); ?>" class="button-btn"><?php echo esc_html($button_text); ?></a>
		    <?php endif; ?>
    	    </div>
    	    <div class="col-xs-12 col-sm-8">
    		<div class="head-global">
			<?php if (!empty($trainer_title) || !empty($trainer_sub_title)) : ?>
			    <h2 class = "h2"><?php
				if (!empty($trainer_title)) : echo esc_html($trainer_title);
				endif;
				if (!empty($trainer_sub_title)) : ?> 
	    		  <span>
	    			<?php echo esc_html($trainer_sub_title); ?>
				  </span>
				<?php endif; ?>
			    </h2>
			   <?php
			endif;
			if (!empty($trainer_description)) : ?>
			    <p><?php echo esc_html($trainer_description); ?></p>
			<?php endif; ?>
    		</div>
    		<?php if(!empty($trainercount)) : ?>
    		<div id="owl-slider1">
			<?php
			if ($trainer_query->have_posts()) :
			    while ($trainer_query->have_posts()) : $trainer_query->the_post();
				?>
	    		    <div class="item">
				    <?php
				    if (has_post_thumbnail()):
					$url = wp_get_attachment_url(get_post_thumbnail_id());
					?> 
					<img src="<?php echo esc_url(fitfab_resize($url, '360', '260')); ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" >
				    <?php endif; ?>
	    			<div class="slider-content">
	    			    <h3>
					    <?php the_title(); ?>
	    			    </h3>
					<?php
					if (function_exists('fitfab_rwmb_meta')) {
					    $trainer_type = fitfab_rwmb_meta('fitfab_trainer_type', 'type=text');
					}
					if (!empty($trainer_type)) : ?>
					    <span><?php echo esc_html($trainer_type); ?></span>
					<?php endif; ?>
	    			    <a href="<?php the_permalink(); ?>" class="plus-more">+</a>
	    			</div>
	    		   </div>
				<?php
			    endwhile;
			    wp_reset_postdata();
			endif;
			?>
    		</div>
    		<?php endif; ?>
    	  </div>
    	</div>
       </div>
    </section>
    <!-- team-info_wrap End here -->
<?php elseif ($trainer_style == 'home1') : ?>
    <section class="schedule-section trainer-wrap-slide fit-fab-trainers-layout-two">
        <div class="clearfix">
    	<div class="schedule-info">
		<?php if (!empty($trainer_title) || !empty($trainer_sub_title)) : ?> 
		    <h2><?php if (!empty($trainer_title)) : ?><span><?php echo esc_html($trainer_title); ?></span><?php endif; ?> <?php echo esc_html($trainer_sub_title); ?></h2>
		<?php endif; ?>
		<?php if (!empty($trainer_description)) : ?>
		    <p>
			<?php echo esc_html($trainer_description); ?>
		    </p>
		<?php endif; ?>
    	</div>

    	<div class="schedule-slider">
    	    <div id="owl-trainers" class="owl-carousel">
		    <?php
		    if ($trainer_query->have_posts()) :
			while ($trainer_query->have_posts()) : $trainer_query->the_post();
			    ?>
	    		<div class="item">
				<?php
				if (has_post_thumbnail()):
				    $url = wp_get_attachment_url(get_post_thumbnail_id());
				    ?> 
				    <img src="<?php echo esc_url(fitfab_resize($url, '381', '260')); ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>"/>

				<?php endif; ?>
	    		    <div class="slider-content">
	    			<h3>
					<?php the_title(); ?>
	    			</h3>
				    <?php
				    if (function_exists('fitfab_rwmb_meta')) {
					$trainer_type = fitfab_rwmb_meta('fitfab_trainer_type', 'type=text');
				    }
				    if (!empty($trainer_type)) :
					?>
					<span>
					    <?php echo esc_html($trainer_type); ?>
					</span>
				    <?php endif; ?>
	    			<a href="<?php the_permalink(); ?>" class="plus-more open-info">+</a>
	    		    </div>
	    		    <div class="trainer-info-caption">
	    			<h4>
					<?php the_title(); ?>
	    			</h4>
				    <?php
				    if (!empty($trainer_type)) : ?>
					<span><?php echo esc_html($trainer_type); ?></span>
					<?php endif; ?>
	    			<p> <?php echo wp_trim_words(get_the_content(), 20, ''); ?> </p>
	    			<a href="<?php the_permalink(); ?>" class="link"><?php esc_html_e('READ MORE', 'fitfab'); ?></a>
	    			<a href="<?php the_permalink(); ?>" class="plus-more cross"> + </a>
	    		    </div>

	    		</div>
			    <?php
			endwhile;
			wp_reset_postdata();
		    endif;
		    ?>
    	    </div>

    	</div>
        </div>

    </section>
<?php elseif ($trainer_style == 'home2') : ?>
    <section class="exp-trainers bg-trainers-wrap fit-fab-trainers-layout-three">
        <div class="container">
    	<div class="bg-trainers">
		<?php if (!empty($trainer_title) || !empty($trainer_sub_title)) : ?> 
		    <div class="home-three-head extra">
			<h2><?php if (!empty($trainer_title)) : ?><span><?php echo esc_html($trainer_title); ?></span><?php endif; ?> <?php echo esc_html($trainer_sub_title); ?></h2>
		    </div>
		<?php endif; ?>
    	    <div class="row spacer-top">
		    <?php
		    if ($trainer_query->have_posts()) :
			while ($trainer_query->have_posts()) : $trainer_query->the_post();
			    ?>
	    		<div class="col-xs-12 col-sm-4 right-space zoom">
				<?php
				if (has_post_thumbnail()):
				    $url = wp_get_attachment_url(get_post_thumbnail_id());
				    ?> 
				    <figure class="block-img">
					<img src="<?php echo esc_url(fitfab_resize($url, '380', '260')); ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>"/>
				    </figure>
				<?php endif; ?>
	    		    <div class="slider-content">
	    			<h3>
					<?php the_title(); ?>
	    			</h3>
				    <?php
				    if (function_exists('fitfab_rwmb_meta')) {
					$trainer_type = fitfab_rwmb_meta('fitfab_trainer_type', 'type=text');
				    }
				    if (!empty($trainer_type)) :
					?>
					<span><?php echo esc_html($trainer_type); ?></span>
					<?php endif; ?>
	    			<a href="<?php the_permalink(); ?>" class="plus-more open-info">+</a>
	    		   </div>
	    		</div>
			    <?php
			endwhile;
			wp_reset_postdata();
		    endif;
		    ?>
    	    </div>
    	</div>
       </div>
    </section>

<?php elseif ($trainer_style == 'home3') : ?>
    <section class="latest-news-home_two fit-fab-trainers-layout-four">
        <div class="container">
	    <?php if (!empty($trainer_title) || !empty($trainer_sub_title)) : ?> 
		<div class="head-global family-oswald">
		    <h2><?php if (!empty($trainer_title)) : ?><span><?php echo esc_html($trainer_title); ?></span><?php endif; ?> <?php echo esc_html($trainer_sub_title); ?></h2>
		    <?php if(!empty($button_text)): ?>
		    <a class="link view-all" href="<?php echo esc_url($button_link); ?>"><?php echo esc_html($button_text); ?></a>
		<?php endif; ?>
		</div>
	    <?php endif; ?>
    	<div class="row top_gap">
		<?php
		if ($trainer_query->have_posts()) :
		    while ($trainer_query->have_posts()) : $trainer_query->the_post();
			?>
	    	    <div class="col-xs-12 col-sm-4 classes-listing-wrap zoom">
			    <?php
			    if (has_post_thumbnail()):
				$url = wp_get_attachment_url(get_post_thumbnail_id());
				?>
				<figure>
				    <img src="<?php echo esc_url(fitfab_resize($url, '360', '260')); ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>"/>
				</figure>
			    <?php endif; ?>
	    		<div class="classes-content">
	    		    <h3> <?php the_title(); ?> </h3>
				<?php
				if (function_exists('fitfab_rwmb_meta')) {
				    $trainer_type = fitfab_rwmb_meta('fitfab_trainer_type', 'type=text');
				}
				if (!empty($trainer_type)) : ?>
				    <span><?php echo esc_html($trainer_type); ?></span>
				    <?php endif; ?>
	    		    <a href="<?php the_permalink(); ?>" class="plus-more open-info">+</a>
	    		</div>
	    	  </div>
			<?php
		    endwhile;
		    wp_reset_postdata();
		endif;
		?>
    	</div>
        </div>
    </section>
    <?php
endif;
echo $this->endBlockComment('fitfab_trainers');
