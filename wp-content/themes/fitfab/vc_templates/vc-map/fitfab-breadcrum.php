<?php

/**
 * FitFab - pricing breadcrum
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates.vc-map
 * @since fitfab 1.0.0
 */
add_action('vc_before_init', 'fitfab_breadcrum_vc');

function fitfab_breadcrum_vc() {
    vc_map(
            array(
                "name" => esc_html__("Fitfab Breadcrum - Fitfab", 'fitfab'),
                "base" => "fitfab_breadcrum",
                "class" => "",
           "category" => esc_html__("Fit&Fab", 'fitfab'),
                'params' => array(
                  
                    
                )
            )
    );
}
class WPBakeryShortCode_fitfab_breadcrum extends WPBakeryShortCode {
    
}
