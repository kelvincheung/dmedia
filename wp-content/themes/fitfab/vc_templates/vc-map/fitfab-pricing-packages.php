<?php

/**
 * FitFab - pricing packages
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates.vc-map
 * @since fitfab 1.0.0
 */
add_action('vc_before_init', 'home_pricing_packages_vc');

function home_pricing_packages_vc() {
    vc_map(
            array(
                "name" => esc_html__("Pricing Packages - Fitfab", 'fitfab'),
                "base" => "fitfab_pricing_packages",
                "class" => "",
            "category" => esc_html__("Fit&Fab", 'fitfab'),
                'params' => array(
                  
                    
                )
            )
    );
}
class WPBakeryShortCode_fitfab_pricing_packages extends WPBakeryShortCode {
    
}
