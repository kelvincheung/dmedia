<?php

/**
 * FitFab - upcoming events
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates.vc-map
 * @since fitfab 1.0.0
 */
add_action('vc_before_init', 'upcoming_events_vc');

function upcoming_events_vc() {
    vc_map(
            array(
                "name" => esc_html__("Upcoming Events - Fitfab", 'fitfab'),
                "base" => "fitfab_upcoming_events",
                "class" => "",
                "category" => esc_html__("Fit&Fab", 'fitfab'),
                'params' => array(
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Number of Events", 'fitfab'),
                        "param_name" => "number_of_events",
                        "value" => '',
                        "description" => esc_html__("Enter Number of Events Here..", 'fitfab')
                    ),
                    array(
                        'type' => 'textfield',
                        'value' => '',
                        'heading' => esc_html__('Event Title', 'fitfab'),
                        'param_name' => 'event_title',
                        'description' => esc_html__('Enter Event Title Here', 'fitfab')
                    ),
                    array(
                        'type' => 'textfield',
                        'value' => '',
                        'heading' => esc_html__('Event Sub Title', 'fitfab'),
                        'param_name' => 'event_sub_title',
                        'description' => esc_html__('Enter Event Sub Title Here', 'fitfab')
                    ),
                    array(
                        'type' => 'textarea',
                        'value' => '',
                        'heading' => esc_html__('Event Description', 'fitfab'),
                        'param_name' => 'event_description',
                        'description' => esc_html__('Enter Event Description Here', 'fitfab')
                    ),

                )
            )
    );
}

class WPBakeryShortCode_fitfab_upcoming_events extends WPBakeryShortCode {
    
}
