<?php

/**
 * FitFab - about section two 
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates.vc-map
 * @since fitfab 1.0.0
 */
add_action('vc_before_init', 'fitfab_about_section_two_vc');

function fitfab_about_section_two_vc() {
    vc_map(array(
        "name" => esc_html__("About Section Two - Fitfab", 'fitfab'),
        "base" => "fitfab_about_section_two",
        "class" => "",
        "category" => esc_html__("Fit&Fab", 'fitfab'),
        "params" => array(
            array(
                "type"=> "attach_image",
                "class"       => "",
                "heading"     => esc_html__( "Background Image", 'fitfab' ),
                "param_name"  => "background_image",
                "value"       => '',
                "description" => esc_html__( "Select Background Image Here", 'fitfab' )
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("Section Title", 'fitfab'),
                "param_name" => "title",
                "value" => '',
                "description" => esc_html__("Enter section title", 'fitfab')
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("Section Sub Title", 'fitfab'),
                "param_name" => "sub_title",
                "value" => '',
                "description" => esc_html__("Enter Section Sub title", 'fitfab')
            ),
            array(
                "type" => "textarea",
                "class" => "",
                "heading" => esc_html__("Section Content", 'fitfab'),
                 "param_name" => "about_content",
                  "value" => '',
                "description" => esc_html__("Enter section content", 'fitfab')
            ),
            array(
                'type' => 'param_group',
                "class" => "",
                'value' => '',
                'heading' => esc_html__('Section List', 'fitfab'),
                'param_name' => 'section_list',
                'description' => esc_html__('Section List', 'fitfab'),
                'params' => array(
                     array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("List Title", 'fitfab'),
                        "param_name" => "list_title",
                        "value" => '',
                        "description" => esc_html__("Enter List Title Here", 'fitfab')
                    ),
  
                )
            ),
        )
    ));
}

class WPBakeryShortCode_fitfab_about_section_two extends WPBakeryShortCode {
    
}
