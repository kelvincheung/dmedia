<?php

/**
 * FitFab - welcome gym fitness 
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates.vc-map
 * @since fitfab 1.0.0
 */
add_action('vc_before_init', 'fitfab_fitness_program');

function fitfab_fitness_program() {
    global $svg_fonts;
    vc_map(
            array(
                "name" => esc_html__("Fitfab Fitness Program", "fitfab"),
                "base" => "fitfab_fitness_program",
                "class" => "",
                "category" => esc_html__("Fit&Fab", 'fitfab'),
                'params' => array(
                    // params group
                    array(
                        'type' => 'param_group',
                        "class" => "",
                        'value' => '',
                        'heading' => esc_html__('Fitness Program', 'fitfab'),
                        'param_name' => 'fitness_program',
                        'description' => esc_html__('Fitness Program', 'fitfab'),
                        // Note params is mapped inside param-group:
                        'params' => array(
                            array(
                                "type" => "attach_image",
                                "class" => "",
                                "heading" => esc_html__("Image", "fitfab"),
                                "param_name" => "image",
                                "value" => '',
                                "description" => esc_html__("Select Image Here", "fitfab")
                            ),
                            array(
                                "type" => "textfield",
                                "class" => "",
                                "heading" => esc_html__("Title", "fitfab"),
                                "param_name" => "title",
                                "value" => '',
                                "description" => esc_html__("Enter Title Here", "fitfab")
                            ),
                            array(
                                "type" => "textfield",
                                "class" => "",
                                "heading" => esc_html__("Sub Title", "fitfab"),
                                "param_name" => "sub_title",
                                "value" => '',
                                "description" => esc_html__("Enter Sub Title Here", "fitfab")
                            ),
                            array(
                                "type" => "textfield",
                                "class" => "",
                                "heading" => esc_html__("Price", "fitfab"),
                                "param_name" => "price",
                                "value" => '',
                                "description" => esc_html__("Enter Price Here", "fitfab")
                            ),
			    array(
                                "type" => "colorpicker",
                                "class" => "",
                                "heading" => esc_html__("Price Color", "fitfab"),
                                "param_name" => "price_color",
                                "value" => '',
                                "description" => esc_html__("Select Price Color", "fitfab")
                            ),
			    array(
                                "type" => "colorpicker",
                                "class" => "",
                                "heading" => esc_html__("Sub Title Color", "fitfab"),
                                "param_name" => "sub_title_color",
                                "value" => '',
                                "description" => esc_html__("Select Sub Title Color", "fitfab")
                            )
                        )
                    )
                )
            )
    );
}

class WPBakeryShortCode_fitfab_fitness_program extends WPBakeryShortCode {
    
}
