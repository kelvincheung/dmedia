<?php

/**
 * FitFab - packages
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates.vc-map
 * @since fitfab 1.0.0
 */
add_action('vc_before_init', 'home_packages_vc');

function home_packages_vc() {
    vc_map(
            array(
                "name" => esc_html__("Packages - Fitfab", 'fitfab'),
                "base" => "fitfab_packages",
                "class" => "",
                "category" => esc_html__("Fit&Fab", 'fitfab'),
                'params' => array(
                    array(
                        "type" => "dropdown",
                        "class" => "",
                        "heading" => esc_html__("Package Styl1", 'fitfab'),
                        "param_name" => "package_style",
                        "description" => esc_html__("Select Package Style", 'fitfab'),
                        "value" => array(
                            esc_html__('Select', 'fitfab') => '',
                            esc_html__('Home', 'fitfab') => 'home',
                            esc_html__('Home1', 'fitfab') => 'home1',
                            esc_html__('Home2', 'fitfab') => 'home2',
                            esc_html__('Home3', 'fitfab') => 'home3',
                        ),
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Title", 'fitfab'),
                        "param_name" => "title",
                        "value" => '',
                        "description" => esc_html__("Enter Title Here", 'fitfab')
                    ),
                    array(
                        'type' => 'textfield',
                        'value' => '',
                        'heading' => esc_html__('Sub Title', 'fitfab'),
                        'param_name' => 'sub_title',
                        'description' => esc_html__('Enter Sub Title Here', 'fitfab')
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Phone Title", 'fitfab'),
                        "param_name" => "phone_title",
                        "value" => '',
                        "description" => esc_html__("Enter Phone Title", 'fitfab')
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Phone no.", 'fitfab'),
                        "param_name" => "phone_no",
                        "value" => '',
                        "description" => esc_html__("Enter Phone no", 'fitfab')
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Email Title", 'fitfab'),
                        "param_name" => "email_title",
                        "value" => '',
                        "description" => esc_html__("Enter Email Title", 'fitfab')
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Email", 'fitfab'),
                        "param_name" => "email",
                        "value" => '',
                        "description" => esc_html__("Enter Email Here", 'fitfab')
                    ),
                   
                )
            )
    );
}

class WPBakeryShortCode_fitfab_packages extends WPBakeryShortCode {
    
}
