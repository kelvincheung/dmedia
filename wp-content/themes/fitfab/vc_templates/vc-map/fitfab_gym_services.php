<?php

/**
 * FitFab - welcome gym fitness 
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates.vc-map
 * @since fitfab 1.0.0
 */
add_action('vc_before_init', 'fitfab_gym_services');

function fitfab_gym_services() {
    vc_map(
	    array(
		"name" => esc_html__("Fitfab Gym Services ", "fitfab"),
		"base" => "fitfab_gym_services",
		"class" => "",
		"category" => esc_html__("Fit&Fab", 'fitfab'),
		'params' => array(
		    array(
			"type" => "attach_image",
			"class" => "",
			"heading" => esc_html__("Background Image", "fitfab"),
			"param_name" => "background_image",
			"value" => '',
			"description" => esc_html__("Select Background Image Here", "fitfab")
		    ),
		    array(
			'type' => 'textfield',
			'value' => '',
			'heading' => esc_html__('Fitness Title', 'fitfab'),
			'param_name' => 'fitness_title',
			'description' => esc_html__('Enter Fitness Title Here', 'fitfab')
		    ),
		    array(
			'type' => 'textfield',
			'value' => '',
			'heading' => esc_html__('Fitness Sub Title', 'fitfab'),
			'param_name' => 'fitness_sub_title',
			'description' => esc_html__('Enter Fitness Sub Title Here', 'fitfab')
		    ),
		    array(
			'type' => 'textarea',
			'value' => '',
			'heading' => esc_html__('Fitness Description', 'fitfab'),
			'param_name' => 'fitness_description',
			'description' => esc_html__('Enter Fitness Description Here', 'fitfab')
		    ),
		    // params group
		    array(
			'type' => 'param_group',
			"class" => "",
			'value' => '',
			'heading' => esc_html__('Fitness Feature', 'fitfab'),
			'param_name' => 'fitness_feature',
			'description' => esc_html__('Fitness Feature', 'fitfab'),
			// Note params is mapped inside param-group:
			'params' => array(
			    array(
				"type" => "attach_image",
				"class" => "",
				"heading" => esc_html__("Feature Icon", "fitfab"),
				"param_name" => "feature_icon",
				"value" => '',
				"description" => esc_html__("Enter Feature Icon Here", "fitfab")
			    ),
			    array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__("Feature Title", "fitfab"),
				"param_name" => "feature_title",
				"value" => '',
				"description" => esc_html__("Enter Feature Title Here", "fitfab")
			    ),
			    array(
				"type" => "textarea",
				"class" => "",
				"heading" => esc_html__("Feature Description", "fitfab"),
				"param_name" => "feature_description",
				"value" => '',
				"description" => esc_html__("Enter Feature Description Here", "fitfab")
			    ),
			)
		    ),
		    array(
			'type' => 'textfield',
			'value' => '',
			'heading' => esc_html__('Fitness Heading', 'fitfab'),
			'param_name' => 'fitness_heading',
			'description' => esc_html__('Enter Fitness Heading Here', 'fitfab')
		    ),
		    array(
			'type' => 'textfield',
			'value' => '',
			'heading' => esc_html__('Fitness Sub Heading', 'fitfab'),
			'param_name' => 'fitness_subheading',
			'description' => esc_html__('Enter Fitness Sub Heading Here', 'fitfab')
		    ),
		    array(
			"type" => "textfield",
			"class" => "",
			"heading" => esc_html__("Join Button Text", "fitfab"),
			"param_name" => "join_button_text",
			"value" => '',
			"description" => esc_html__("Enter Join Button Text Here", "fitfab")
		    ),
		    array(
			"type" => "textfield",
			"class" => "",
			"heading" => esc_html__("Join Button Link", "fitfab"),
			"param_name" => "join_button_link",
			"value" => '',
			"description" => esc_html__("Enter Join Button Link Here", "fitfab")
		    ),
		)
	    )
    );
}

class WPBakeryShortCode_fitfab_gym_services extends WPBakeryShortCode {
    
}
