<?php

/**
 * FitFab - award 
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates.vc-map
 * @since fitfab 1.0.0
 */
add_action('vc_before_init', 'fitfab_award_vc');

function fitfab_award_vc() {
    vc_map(array(
        "name" => esc_html__("Award - Fitfab", 'fitfab'),
        "base" => "fitfab_award",
        "class" => "",
        "category" => esc_html__("Fit&Fab", 'fitfab'),
        "params" => array(
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("Section Title", 'fitfab'),
                "param_name" => "title",
                "value" => '',
                "description" => esc_html__("Enter Section Title Here", 'fitfab')
            ),
            array(
                'type' => 'param_group',
                "class" => "",
                'value' => '',
                'heading' => esc_html__('Fitfab Award', 'fitfab'),
                'param_name' => 'award',
                'description' => esc_html__('Fitfab Award', 'fitfab'),
                'params' => array(
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Award Title", 'fitfab'),
                        "param_name" => "award_title",
                        "value" => '',
                        "description" => esc_html__("Enter Feature Icon Here", 'fitfab')
                    ),
                    array(
                        "type" => "attach_image",
                        "class" => "",
                        "heading" => esc_html__("Award Image", 'fitfab'),
                        "param_name" => "award_img",
                        "value" => '',
                        "description" => esc_html__("Add Award Image Here", 'fitfab')
                    ),
                )
            ),
        )
    ));
}

class WPBakeryShortCode_fitfab_award extends WPBakeryShortCode {
    
}
