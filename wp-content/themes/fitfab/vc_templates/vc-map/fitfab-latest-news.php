<?php

/**
 * FitFab - latest news 
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates.vc-map
 * @since fitfab 1.0.0
 */
add_action('vc_before_init', 'fitfab_latest_news_vc');

function fitfab_latest_news_vc() {
    vc_map(array(
        "name" => esc_html__("Latest News - Fitfab", 'fitfab'),
        "base" => "fitfab_latest_news",
        "class" => "",
        "category" => esc_html__("Fit&Fab", 'fitfab'),
        "params" => array(
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("News Title", 'fitfab'),
                "param_name" => "news_title",
                "value" => '',
                "description" => esc_html__("Enter News Title Here", 'fitfab')
            ),
	    array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("Posts Per Page", 'fitfab'),
                "param_name" => "posts_per_page",
                "value" => '',
                "description" => esc_html__("Enter News Title Here", 'fitfab')
            ),
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => esc_html__("Order By", 'fitfab'),
                "param_name" => "order_by",
                "value" => array(
                    esc_html__('Select', 'fitfab') => '',
                    esc_html__('Date', 'fitfab') => 'date',
                    esc_html__('Id', 'fitfab') => 'id',
                    esc_html__('Author', 'fitfab') => 'author',
                    esc_html__('Title', 'fitfab') => 'title',
                    esc_html__('Modified', 'fitfab') => 'modified',
                    esc_html__('Random', 'fitfab') => 'random',
                    esc_html__('Menu order', 'fitfab') => 'menu_order',
                ),
                "description" => esc_html__("Enter Order By Here", 'fitfab')
            ),
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => esc_html__("Sort Order", 'fitfab'),
                "param_name" => "sort_order",
                "value" => array(
                    esc_html__('Select', 'fitfab') => 'select',
                    esc_html__('Descending', 'fitfab') => 'descending',
                    esc_html__('Ascending', 'fitfab') => 'ascending',
                ),
                "description" => esc_html__("Enter Sort Order Here", 'fitfab')
            ),
        )
    ));
}

class WPBakeryShortCode_fitfab_latest_news extends WPBakeryShortCode {
    
}
