<?php

/**
 * FitFab - class schedule 
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates.vc-map
 * @since fitfab 1.0.0
 */
add_action('vc_before_init', 'fitfab_class_schedule_vc');

function fitfab_class_schedule_vc() {
    vc_map(array(
        "name" => esc_html__("Class Schedule - Fitfab", 'fitfab'),
        "base" => "fitfab_class_schedule",
        "class" => "",
        "category" => esc_html__("Fit&Fab", 'fitfab'),
        "params" => array(
            array(
                'type' => 'textfield',
                'value' => '',
                'heading' => esc_html__('Class Schedule Title', 'fitfab'),
                'param_name' => 'class_schedule_title',
                'description' => esc_html__('Enter Class Schedule Title Here','fitfab')
            ),
            array(
                'type' => 'textfield',
                'value' => '',
                'heading' => esc_html__('Class Schedule Sub Title', 'fitfab'),
                'param_name' => 'class_schedule_sub_title',
                'description' => esc_html__('Enter Class Schedule Sub Title Here','fitfab')
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("Number of Classes", 'fitfab'),
                "param_name" => "classcount",
                "value" => '',
                "description" => esc_html__("Enter Number of Classes Here", 'fitfab')
            )
        )
    ));
}

class WPBakeryShortCode_fitfab_class_schedule extends WPBakeryShortCode {
    
}
