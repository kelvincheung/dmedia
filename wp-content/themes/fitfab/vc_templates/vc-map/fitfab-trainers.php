<?php

/**
 * FitFab - trainer 
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates.vc-map
 * @since fitfab 1.0.0
 */
add_action('vc_before_init', 'fitfab_trainers_vc');

function fitfab_trainers_vc() {
    vc_map(array(
        "name" => esc_html__("Trainers - Fitfab", 'fitfab'),
        "base" => "fitfab_trainers",
        "class" => "",
        "category" => esc_html__("Fit&Fab", 'fitfab'),
        "params" => array(
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => esc_html__("Trainer Style", 'fitfab'),
                "param_name" => "trainer_style",
                "description" => esc_html__("Select Trainer Style", 'fitfab'),
                "value" => array(
                    esc_html__('Select', 'fitfab') => '',
                    esc_html__('Home', 'fitfab') => 'home',
                    esc_html__('Home1', 'fitfab') => 'home1',
                    esc_html__('Home2', 'fitfab') => 'home2',
                    esc_html__('Home3', 'fitfab') => 'home3',
                ),
            ),
            array(
                "type"=> "attach_image",
                "class"       => "",
                "heading"     => esc_html__( "Background Image", 'fitfab' ),
                "param_name"  => "background_image",
                "value"       => '',
                "description" => esc_html__( "Select Background Image Here", 'fitfab' )
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("Trainer Heding", 'fitfab'),
                "param_name" => "trainer_heading",
                "value" => '',
                "description" => esc_html__("Enter Trainer Heding Here", 'fitfab')
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("Trainer Sub Heding", 'fitfab'),
                "param_name" => "trainer_sub_heading",
                "value" => '',
                "description" => esc_html__("Enter Trainer Sub Heding Here", 'fitfab')
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("Button Text", 'fitfab'),
                "param_name" => "button_text",
                "value" => '',
                "description" => esc_html__("Enter Button Text Here", 'fitfab')
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("Button Link", 'fitfab'),
                "param_name" => "button_link",
                "value" => '',
                "description" => esc_html__("Enter Button Link Here", 'fitfab')
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("Trainer Title", 'fitfab'),
                "param_name" => "trainer_title",
                "value" => '',
                "description" => esc_html__("Enter Trainer Title Here", 'fitfab')
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("Trainer Sub Title", 'fitfab'),
                "param_name" => "trainer_sub_title",
                "value" => '',
                "description" => esc_html__("Enter Trainer Sub Title Here", 'fitfab')
            ),
            array(
                "type" => "textarea",
                "class" => "",
                "heading" => esc_html__("Trainer Description", 'fitfab'),
                "param_name" => "trainer_description",
                "value" => '',
                "description" => esc_html__("Enter Trainer Description Here", 'fitfab')
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("Number of Trainers", 'fitfab'),
                "param_name" => "trainercount",
                "value" => '',
                "description" => esc_html__("Enter Number of Trainers Here", 'fitfab')
            ),
        )
    ));
}

class WPBakeryShortCode_fitfab_trainers extends WPBakeryShortCode {
    
}
