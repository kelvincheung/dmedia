<?php

/**
 * FitFab - popular class
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates.vc-map
 * @since fitfab 1.0.0
 */
add_action('vc_before_init', 'fitfab_popular_class_vc');

function fitfab_popular_class_vc() {
    vc_map(array(
        "name" => esc_html__("Popular Classes - Fitfab", 'fitfab'),
        "base" => "fitfab_popular_class",
        "class" => "",
       "category" => esc_html__("Fit&Fab", 'fitfab'),
        "params" => array(
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => esc_html__("Popular Class Style", 'fitfab'),
                "param_name" => "popular_class_style",
                "description" => esc_html__("Select Popular Class Style", 'fitfab'),
                "value" => array(
                    esc_html__('Select', 'fitfab') => '',
                    esc_html__('Home1', 'fitfab') => 'home1',
                    esc_html__('Home2', 'fitfab') => 'home2',
                    esc_html__('Home3', 'fitfab') => 'home3',
                ),
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("Number of Class", 'fitfab'),
                "param_name" => "classcount",
                "value" => '',
                "description" => esc_html__("Enter Number of Class Here", 'fitfab')
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("Class Heading", 'fitfab'),
                "param_name" => "class_heading",
                "value" => '',
                "description" => esc_html__("Enter Class Heading Here", 'fitfab')
            ),
	     array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("Class Days", "fitfab"),
                "param_name" => "class_days",
                "value" => '',
                "description" => esc_html__("Enter Class Heading Here", "fitfab")
            ),
	    array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("Class Time", "fitfab"),
                "param_name" => "class_time",
                "value" => '',
                "description" => esc_html__("Enter Class Heading Here", "fitfab")
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("Class Sub Heading", 'fitfab'),
                "param_name" => "class_sub_heading",
                "value" => '',
                "description" => esc_html__("Enter Class Sub Heading Here", 'fitfab')
            ),
            array(
                "type" => "attach_image",
                "class" => "",
                "heading" => esc_html__("About Image", 'fitfab'),
                "param_name" => "image",
                "value" => '',
                "description" => esc_html__("Upload about image", 'fitfab')
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("About Heading", 'fitfab'),
                "param_name" => "about_heading",
                "value" => '',
                "description" => esc_html__("Enter About Heading Here", 'fitfab')
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("About Sub Heading", 'fitfab'),
                "param_name" => "about_sub_heading",
                "value" => '',
                "description" => esc_html__("Enter About Sub Heading Here", 'fitfab')
            ),
            array(
                "type" => "textarea",
                "class" => "",
                "heading" => esc_html__("About Title", 'fitfab'),
                "param_name" => "about_title",
                "value" => '',
                "description" => esc_html__("Enter About Title Here", 'fitfab')
            ),
            array(
                "type" => "textarea_html",
                "class" => "",
                "heading" => esc_html__("About Description", 'fitfab'),
                "param_name" => "about_description",
                "value" => '',
                "description" => esc_html__("Enter About Description Here", 'fitfab')
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("More Button Text", 'fitfab'),
                "param_name" => "button_text",
                "value" => '',
                "description" => esc_html__("Enter More Button Text Here", 'fitfab')
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("Button Link", 'fitfab'),
                "param_name" => "button_link",
                "value" => '',
                "description" => esc_html__("Enter Button Link", 'fitfab')
            ),
        )
    ));
}

class WPBakeryShortCode_fitfab_popular_class extends WPBakeryShortCode {
    
}
