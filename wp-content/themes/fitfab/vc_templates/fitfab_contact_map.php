<?php
/**
 * FitFab - contact map
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates
 * @since fitfab 1.0.0
 */
extract(shortcode_atts(array(
    'map_section_title' => '',
    'section_title' => '',
                ), $atts));
global $fitfab_data;
 if (!empty($map_section_title) || !empty($section_title)): 
?>
<!-- finding-map-wrap start here -->

<section class="finding-map-wrap fit-fab-map-layout-one">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-7">
                <?php if (!empty($map_section_title)): ?>
                <div class="head-global family-oswald">
                    <h2 class="h2 h25"><?php echo esc_html($map_section_title); ?></h2>
                </div>
                <?php endif; ?>
                <!-- Map Section -->
                <div class="map-contact">
                    <div id="custom_map"></div>

                </div>
                <!-- Map Section -->
            </div>

            <div class="col-xs-12 col-sm-5">
                <?php if (!empty($section_title)): ?>
                <div class="head-global family-oswald">
                    <h2 class="h2 h25"><?php echo esc_html($section_title); ?></h2>
                </div>
                <?php endif; ?>
                <address class="address-contact">
                    <div class="address-info">
                        <?php if (!empty($fitfab_data['fitfab_address_label'])): ?>
                        <strong><?php echo esc_html($fitfab_data['fitfab_address_label']); ?> </strong>
                        <?php endif; ?>
                        <?php if (!empty($fitfab_data['fitfab_address'])): ?>
                        <span><?php echo esc_html($fitfab_data['fitfab_address']); ?></span>
                        <?php endif; ?>
                    </div>
                    <div class="phone-contact">
                        <?php if (!empty($fitfab_data['fitfab_phone_no_label'])): ?>
                        <strong><?php echo esc_html($fitfab_data['fitfab_phone_no_label']); ?> </strong>
                        <?php endif; ?>
                        <?php if (!empty($fitfab_data['fitfab_phone'])): ?>
                        <a href="<?php esc_attr_e('tel:','fitfab'); ?><?php echo esc_attr($fitfab_data['fitfab_phone']); ?>"><?php echo esc_html($fitfab_data['fitfab_phone']); ?></a>
                        <?php endif; ?>
                    </div>
                    <div class="email-contact">
                        <?php if (!empty($fitfab_data['fitfab_email_label'])): ?>
                        <strong><?php echo esc_html($fitfab_data['fitfab_email_label']); ?> </strong>
                        <?php endif; ?>
                        <?php if (!empty($fitfab_data['fitfab_email'])): ?>
                        <a href="<?php esc_attr_e('mailto:','fitfab'); ?><?php echo esc_attr($fitfab_data['fitfab_email']); ?>"><?php echo esc_html($fitfab_data['fitfab_email']); ?></a>
                        <?php endif; ?>
                    </div>
                </address>
            </div>

        </div>
    </div>
</section>
<!-- finding-map-wrap End here -->
<?php
endif;
echo $this->endBlockComment('fitfab_contact_map');
