<?php
/**
 * FitFab - fitness
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates
 * @since fitfab 1.0.0
 */
extract(shortcode_atts(array(
    'background_image' => '',
    'fitness_title' => '',
    'fitness_style' => '',
    'fitness_sub_title' => '',
    'fitness_description' => '',
    'fitness_heading' => '',
    'fitness_subheading' => '',
    'join_button_text' => '',
    'join_button_link' => '',
    'more_button_text' => '',
    'more_button_link' => '',
      ), $atts));

$image_link = wp_get_attachment_image_src($background_image, "large");
$bg_image = '';
if ($image_link) :
    $bg_image .= 'background-image:url(' . $image_link[0] . ');background-repeat:no-repeat;background-position:bottom right;';
endif;
$bg_image .= 'background-color:#fff;';
if(!empty($background_image) || !empty($fitness_title) || !empty($fitness_sub_title) || !empty($fitness_description)) :

if ($fitness_style == 'home' || $fitness_style == '') :
    ?>
    <!--facility-list-wrap start here-->
    <section class="fitfab-gym fit-fab-fitness-layout-one">
        <!--facility-list-wrap start here-->
        <div class="facility-list-wrap">
            <div class="container">
                <div class="row">
                    <ul class="facility-list clearfix">
                        <?php
                        $class_arr = array('body-building', 'yoga', 'aerobics');
                        $price_arr = array('light-green', 'light-red', 'dark-blue');
                        $i = 0;
                        if (!empty($atts['fitness_program'])) :
                            $packages = vc_param_group_parse_atts($atts['fitness_program']);
                            if (isset($packages)) :
                                foreach ($packages as $package) :
                                    $back_image = '';
                                    if (!empty($package['image'])) :
                                        $package_image = wp_get_attachment_image_src($package['image'], "large");
                                        $back_image = 'background-image:url(' . $package_image[0] . ')';
                                    endif;
                                    ?>
                                    <li class = "<?php echo esc_attr($class_arr[$i]); ?>" style="<?php echo esc_attr($back_image); ?>">
                                        <div class = "build">
                                            <?php if (!empty($package['title'])) : ?>
                                                <h2><?php echo esc_html($package['title']); ?></h2>
                                            <?php endif; ?>
                                            <?php if (!empty($package['sub_title'])) : ?>
                                                <span><?php echo esc_html($package['sub_title']); ?></span>
                                            <?php endif; ?>
                                        </div>
                                        <?php if (!empty($package['price'])) : ?>
                                            <span class = "label-price <?php echo esc_attr($price_arr[$i]); ?>"><?php esc_html_e('$', 'fitfab'); ?><?php echo esc_html($package['price']); ?><?php esc_html_e('*/M', 'fitfab'); ?></span>
                                        <?php endif; ?>
                                    </li>
                                    <?php
                                    $i++;
                                endforeach;
                            endif;
                        endif;
                        ?>
                    </ul>
                </div>

            </div>
        </div>
        <!--facility-list-wrap End here-->
        <div class = "fitfab-gym-wrap" style="<?php echo esc_attr($bg_image); ?>">
            <div class = "container">

                <div class = "row">
                    <div class = "col-xs-12 col-sm-8">
                        <div class = "head-global">
                            <?php if (!empty($fitness_title) || !empty($fitness_sub_title)) : ?>
                                <h2 class = "h2"><?php
                                    if (!empty($fitness_title)) : echo esc_html($fitness_title);
                                    endif;
                                    if (!empty($fitness_sub_title)) :
                                        ?> 
                                        <span><?php
                                            echo esc_html($fitness_sub_title);
                                        endif;
                                        ?>
                                    </span>
                                </h2>
                                <?php
                            endif;
                            if (!empty($fitness_description)) :
                                ?> 
                                <p><?php echo esc_html($fitness_description); ?></p>
                            <?php endif; ?>
                        </div>

                        <div class="row top_gap">
                            <?php
                            if (!empty($atts['fitness_feature'])) :
                                $features = vc_param_group_parse_atts($atts['fitness_feature']);
                                if (isset($features)) :
                                    foreach ($features as $feature) :
                                        ?>
                                        <div class="col-xs-12 col-sm-6 fit_list-block">
                                            <?php if (!empty($feature['feature_icon'])) : ?>
                                                <figure>
                                                    <img class="svg" src="<?php echo esc_url(wp_get_attachment_url($feature['feature_icon'])); ?>" alt="<?php echo esc_html($feature['feature_title']); ?>" />
                                                </figure>
                                            <?php endif; ?>
                                            <?php if (!empty($feature['feature_title']) || !empty($feature['feature_description'])) : ?>
                                                <div class="fit-description">
                                                    <?php if (!empty($feature['feature_title'])) : ?>
                                                        <h3> <?php echo esc_html($feature['feature_title']); ?> </h3>
                                                        <?php
                                                    endif;
                                                    if (!empty($feature['feature_description'])) :
                                                        ?>
                                                        <p>
                                                            <?php echo esc_html($feature['feature_description']); ?>
                                                        </p>
                                                    <?php endif; ?>
                                                </div>
                                            <?php endif; ?>

                                        </div>
                                        <?php
                                    endforeach;
                                endif;
                            endif;
                            ?>
                        </div>

                    </div>

                    <div class="col-xs-12 col-sm-4 fit_sexy">
                        <?php if (!empty($fitness_heading) || !empty($fitness_subheading)) : ?> 
                            <h1>
                                <?php
                                if (!empty($fitness_heading)) : 
                                  echo esc_html($fitness_heading);
                                endif;
                                if (!empty($fitness_subheading)) :
                                    ?>
                                    <strong><?php
                                        echo esc_html($fitness_subheading);
                                        ?></strong>
                                <?php endif; ?>
                            </h1>
                        <?php endif; ?>

                        <?php if (!empty($join_button_text)) : ?>
                            <a href="<?php echo esc_url(!empty($learn_button_link) ? $learn_button_link : '#'); ?>" class="button-btn">
                                <?php echo esc_html($join_button_text); ?>
                            </a>
                        <?php endif; ?>

                    </div>

                </div>
            </div>
        </div>
    </section>
<?php elseif ($fitness_style == 'home1') :
    ?>
    <section class="fitness-center-wrap fit-fab-fitness-layout-two">
        <div class="container">
            <?php if (!empty($fitness_title)) : ?>
                <div class="head-global family-oswald">
                    <h2 class="h2"><?php echo esc_html($fitness_title); ?></h2>
                </div>
            <?php endif; ?>
            <div class="row top_gap">
                <?php
                $features = vc_param_group_parse_atts($atts['fitness_feature']);
                foreach ($features as $feature) :
                    ?>
                    <div class="col-xs-12 col-sm-4 fit_list-block">
                        <?php if (!empty($feature['feature_icon'])) : ?>
                            <figure>
                                <img class="svg" src="<?php echo esc_url(wp_get_attachment_url($feature['feature_icon'])); ?>" alt="<?php echo esc_html($feature['feature_title']); ?>" title="<?php echo esc_html($feature['feature_title']); ?>" />
                            </figure>
                        <?php endif; ?>
                        <div class="fit-description">
                            <?php if (!empty($feature['feature_title'])) : ?>
                                <h3> <?php echo esc_html($feature['feature_title']); ?> </h3>
                                <?php
                            endif;
                            if (!empty($feature['feature_description'])) :
                                ?>
                                <p>
                                    <?php echo esc_html($feature['feature_description']); ?>
                                </p>
                            <?php endif; ?>
                        </div>

                    </div>
                <?php endforeach; ?>
            </div>

        </div>
    </section>
<?php elseif ($fitness_style == 'home2') : ?>
    <!-- fitness center secton start here -->
    <div class="container">
        <section class="fitness-center fit-fab-fitness-layout-three">
            <?php if (!empty($fitness_title) || !empty($fitness_sub_title)) : ?>
                <div class="home-three-head extra 11">
                    <h2><?php if (!empty($fitness_title)) : ?><span><?php echo esc_html($fitness_title); ?></span><?php endif; ?> <?php echo esc_html($fitness_sub_title); ?> </h2>
                </div>
            <?php endif; ?>
            <div class="row top_gap">
                <?php
                $features = vc_param_group_parse_atts($atts['fitness_feature']);
                foreach ($features as $feature) :
                    ?>
                    <div class="col-xs-12 col-sm-4 fit_list-block">
                        <?php if (!empty($feature['feature_icon'])) : ?>
                            <figure>
                                <img class="svg" src="<?php echo esc_url(wp_get_attachment_url($feature['feature_icon'])); ?>" alt="<?php echo esc_html($feature['feature_title']); ?>" title="<?php echo esc_html($feature['feature_title']); ?>">
                            </figure>
                        <?php endif; ?>
                        <div class="fit-description">
                            <?php if (!empty($feature['feature_title'])) : ?>
                                <h3> <?php echo esc_html($feature['feature_title']); ?> </h3>
                                <?php
                            endif;
                            if (!empty($feature['feature_description'])) :
                                ?>
                                <p>
                                    <?php echo esc_html($feature['feature_description']); ?>
                                </p>
                            <?php endif; ?>
                        </div>

                    </div>
                <?php endforeach; ?>

            </div>
            <?php if (!empty($join_button_text) || !empty($more_button_text)) : ?>
                <div class="row">
                    <ul class="btn-list clearfix">
                        <?php if (!empty($more_button_text)) : ?>
                            <li>
                                <a href="<?php echo esc_url($more_button_link); ?>" class="button-btn small-btn"> <?php echo esc_html($more_button_text); ?></a>
                            </li>
                            <?php
                        endif;
                        if (!empty($join_button_text)) :
                            ?>
                            <li>
                                <a href="<?php echo esc_url($join_button_link); ?>" class="button-btn small-btn green-bg"> <?php echo esc_html($join_button_text); ?></a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
            <?php endif; ?>

        </section>
    </div>
    <?php
endif;
endif;
echo $this->endBlockComment('fitfab_fitness');
