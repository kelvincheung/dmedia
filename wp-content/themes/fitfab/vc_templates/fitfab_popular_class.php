<?php
/**
 * FitFab - popular class shortcode
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates
 * @since fitfab 1.0.0
 */
extract(shortcode_atts(array(
    'popular_class_style' => '',
    'class_heading' => '',
    'class_days' => '',
    'class_time' => '',
    'class_sub_heading' => '',
    'image' => '',
    'about_heading' => '',
    'about_sub_heading' => '',
    'about_title' => '',
    'about_description' => '',
    'button_text' => '',
    'button_link' => '',
    'classcount' => '',
     ), $atts));
global $fitfab_data;
if(!empty($popular_class_style) || !empty($class_heading) || !empty($about_title) || !empty($about_heading) || !empty($classcount)) :


if ($popular_class_style == 'home1' || $popular_class_style == '') : ?>
    <!-- populer-classes start here -->
    <section class="populer-classes fit-fab-populer-classes-layout-one">
        <div class="container">
            <?php if (!empty($class_heading)) : ?>
                <div class="head-global family-oswald">
                    <h2 class="h2"><?php echo esc_html($class_heading); ?></h2>
                </div>
            <?php endif; ?>

            <div class="row">
                <div class="col-xs-12 tabing-wrap">
                    <div>
                        <div class="clearfix">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs " role="tablist">
                                <?php
                                $terms = get_terms('class_categories', array('hide_empty' => 1));
                                $i = 1;
                                foreach ($terms as $term) :
                                    $class = '';
                                    if ($i == 1) :
                                        $class = "class=active";
                                    endif;
                                    $i++;
                                    ?>
                                    <li role="presentation" <?php echo esc_attr($class); ?>>
                                        <a href="#<?php echo esc_attr($term->slug); ?>" data-aria-controls="all" data-toggle="tab">
                                            <?php echo esc_html($term->name); ?>
                                        </a>
                                    </li>
                                    <?php
                                    if ($i == 9)
                                        break;
                                endforeach;
                                ?>
                            </ul>
                        </div>
						<?php 	if(!empty($classcount)): ?>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <?php
                            $i = 1;
                            foreach ($terms as $term) :
                                $class = '';
                                if ($i == 1) :
                                    $class = 'tab-pane fade in active';
                                else:
                                    $class = 'tab-pane fade';
                                endif;
                                $i++;
							
                                ?>
                                <div role="tabpanel" class="<?php echo esc_attr($class); ?>" id="<?php echo esc_attr($term->slug); ?>">
                                    <div class="row">
                                        <?php
                                        $args = array(
                                            'post_type' => 'class',
                                            'taxonomy' => 'class_categories',
                                            'term' => $term->slug,
                                            'post_status' => 'publish',
                                            'posts_per_page' => $classcount
                                        );
                                        $fitfab_popular_class = new WP_Query($args);
                                        if ($fitfab_popular_class->have_posts()) :
                                            while ($fitfab_popular_class->have_posts()) : $fitfab_popular_class->the_post();
                                                get_template_part('content/class', 'page');
                                            endwhile;
                                            wp_reset_postdata();
                                        else :
                                            get_template_part('content/', 'none');
                                        endif;
                                        ?>
                                    </div>
                                </div>
                            <?php 
                          
                            endforeach; ?>
                        </div>
                        <?php   endif; ?>
                    </div>

                </div>
            </div>
        </div>
    </section>
<?php elseif ($popular_class_style == 'home2') : ?>

    <!-- populer-classes start here -->
    <section class="populer-classes  ppclass fit-fab-populer-classes-layout-two">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 classes-populer">
                    <div class="home-three-head clearfix">
                        <?php if (!empty($class_heading) || !empty($class_sub_heading)) : ?>
                            <h2><?php if (!empty($class_heading)) : ?><span><?php echo esc_html($class_heading); ?></span> <?php
                                endif;
                                echo esc_html($class_sub_heading); ?> 
                            </h2>
			<span> <span><?php echo esc_html($class_days); ?></span><?php echo esc_html($class_time); ?></span>
                        <?php endif; ?>
                    </div>
				<?php if (!empty($classcount)): ?>
                    <div id="owl-hthree-one" class="owl-carousel">
                        <?php
                        $args = array(
                            'post_type' => 'class',
                            'post_status' => 'publish',
                            'posts_per_page' => $classcount
                        );
                        $fitfab_popular_class = new WP_Query($args);
                        if ($fitfab_popular_class->have_posts()) :
                            while ($fitfab_popular_class->have_posts()) : $fitfab_popular_class->the_post();
                            $trainer = implode(', ', fitfab_rwmb_meta('fitfab_trainer', 'type=checkbox_list'));
                                ?>
                                <div class="item">
                                    <?php
                                    if (has_post_thumbnail()):
                                        $url = wp_get_attachment_url(get_post_thumbnail_id()); ?>
                                        <img src="<?php echo esc_url(fitfab_resize($url, '496', '277')); ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" />
                                    <?php endif; ?>
                                    <div class="slider-cap-info">
                                        <h3><?php the_title(); ?>
                                        <?php if(!empty($trainer)) : ?>
                                        <span><?php echo esc_html($trainer); ?></span>
                                        <?php endif; ?>
                                        </h3>
                                    </div>
                                </div>
                                <?php
                            endwhile;
                            wp_reset_postdata();
                        endif;
                        ?>
                    </div>
                    <?php endif ; ?>
                </div>
                <div class="col-xs-12 col-sm-6 classes-populer about-fit">
		    <?php do_action('fitfab_popular_classes_background',$image); ?>
                    <div class="bg-wrp">
                        <?php if (!empty($about_heading) || !empty($about_sub_heading)) : ?>
                            <div class="home-three-head clearfix">
                                <h2><?php if (!empty($about_heading)) : ?><span><?php echo esc_html($about_heading); ?></span><?php endif; ?> <?php echo esc_html($about_sub_heading); ?> </h2>
                            </div>
                        <?php endif; ?>
                        <div class="conntent-fit-about">
                            <?php if (!empty($about_title)) : ?>
                                <h3><?php echo esc_html($about_title); ?></h3>
                            <?php endif; ?>
                            <?php if (!empty($about_description)) : ?>
                                <p>
                                    <?php echo wp_kses($about_description, wp_kses_allowed_html('post')); ?>
                                </p>
                            <?php endif; ?>
                        </div>
                        <?php
                        if (!empty($button_text)) :
                            $button_link = ($button_link) ? $button_link : '#'; ?>
                            <div class="bottom-link">
                                <a href="<?php echo esc_url($button_link); ?>" class="button-btn"><?php echo esc_html($button_text); ?></a>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- populer-classes End here -->
<?php elseif ($popular_class_style == 'home3') : ?>
    <section class="home_4-populer-class fit-fab-populer-classes-layout-three">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8">
                    <?php if (!empty($class_heading)) : ?>
                        <div class="head-global family-oswald">
                            <h2 class="h2"><?php echo esc_html($class_heading); ?></h2>
                        </div>
                    <?php endif; ?>
                    <?php
                    $args = array(
                        'post_type' => 'class',
                        'post_status' => 'publish',
                        'posts_per_page' => 2
                    );
                    $fitfab_popular_class = new WP_Query($args);
                    if ($fitfab_popular_class->have_posts()) :
                        while ($fitfab_popular_class->have_posts()) : $fitfab_popular_class->the_post();
		    if(function_exists('fitfab_rwmb_meta')){
                            $start_time = date('hA', strtotime(get_post_meta(get_the_id(), 'fitfab_class_start_time', TRUE)));
                            $end_time = date('hA', strtotime(get_post_meta(get_the_id(), 'fitfab_class_end_time', TRUE)));
                            $package_id = fitfab_rwmb_meta('fitfab_package','type=select_advanced');
                            $trainer = implode(', ', fitfab_rwmb_meta('fitfab_trainer', 'type=checkbox_list'));
                            ?>
                            <div class="class-list-wrap">
                                <div class="row spacer-top">
                                    <div class="col-xs-12 col-sm-6 right-space zoom">
                                        <?php
                                        if (has_post_thumbnail()):
                                            $url = wp_get_attachment_url(get_post_thumbnail_id());
                                         ?>
                                            <figure>
                                                <img src="<?php echo esc_url(fitfab_resize($url, '370', '201')); ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" />
                                            </figure>
                                        <?php endif; ?>
                                    </div>
                                    <?php
                                    if (!empty($package_id)) :
                                        $pack_url = wp_get_attachment_url(get_post_thumbnail_id($package_id));
                                        if ($pack_url) :
                                            ?>
                                            <div class="col-xs-12 col-sm-6 left-space zoom">
                                                <figure>
                                                    <img src="<?php echo esc_url(fitfab_resize($pack_url, '370', '201')); ?>" alt="<?php echo get_the_title($package_id); ?>" title="<?php echo get_the_title($package_id); ?>" />
                                                </figure>
                                            </div>
                                            <?php
                                        endif;
                                    endif;
                                    ?>
                                </div>

                                <div class="program-head clearfix">
                                    <div class="classes-content">
                                        <h3><?php the_title(); ?></h3>
                                        <span>
                                            <?php if ($start_time) : ?>
                                                <span><?php echo esc_html($start_time); ?> - <?php echo esc_html($end_time); ?></span>
                                            <?php endif; ?>
                                            <?php
                                            if (!empty($trainer)) :
                                                echo esc_html_e('WITH ', 'fitfab');
                                                echo esc_html($trainer);
                                            endif;
                                            ?>
					</span>
                                    </div>
                                    <?php
                                    if (!empty($package_id)) :
                                        $price_per_month = get_post_meta($package_id, 'fitfab_price_per_month', TRUE);
                                        $join_button_text = get_post_meta($package_id, 'fitfab_join_button_text', TRUE);
                                        $join_button_link = get_post_meta($package_id, 'fitfab_join_button_link', TRUE);
                                        $join_button_link = ($join_button_link) ? $join_button_link : '#';
                                        ?>
                                        <div class="package-price">
                                            <?php if (!empty($price_per_month)) : ?>
                                                <span><?php echo class_exists( 'WooCommerce' )?get_woocommerce_currency_symbol($fitfab_data["fitfab_currency"]):$fitfab_data["fitfab_currency"]; 
                                                echo esc_html($price_per_month); ?><?php esc_html_e('*/M', 'fitfab'); ?></span>
                                            <?php endif; ?>
                                            <?php if (!empty($join_button_text)) : ?>
                                                <a href="<?php echo esc_url($join_button_link); ?>" class="button-btn small-btn green-bg"><?php echo esc_html($join_button_text); ?></a>
                                            <?php endif; ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                                <p>
                                    <?php echo wp_kses(wp_trim_words(get_the_content(), 35, ''), wp_kses_allowed_html('post')); ?>
                                </p>
                            </div> <!-- class-list-wrap -->
                            <?php
		    }
                        endwhile;
                        wp_reset_postdata();
                    endif;
                    ?>
                </div><!-- .col -->

                <div class="col-xs-12 col-sm-4">
                    <?php if (!empty($about_heading)) : ?>
                        <div class="head-global family-oswald">
                            <h2 class="h2"><?php echo esc_html($about_heading); ?></h2>
                        </div>
                    <?php endif; ?>
                    <div class="satisfaction-fit zoom">
                        <?php
                        if (!empty($image)):
                            $src = wp_get_attachment_url($image, 'full');
                            ?>
                            <figure>
                                <img src="<?php echo esc_url(fitfab_resize($src, '360', '291')); ?>" alt="<?php echo esc_attr($about_title); ?>" title="<?php echo esc_attr($about_title); ?>" />
                            </figure>
                        <?php endif; ?>
                        <div class="satisfaction-wrap">
                            <?php if (!empty($about_title)) : ?>
                                <h3><a href="#"><?php echo esc_html($about_title); ?></a></h3>
                            <?php endif; ?>
                            <?php if (!empty($about_description)) : ?>
                                <p>
                                    <?php echo wp_kses($about_description, wp_kses_allowed_html('post')); ?>
                                </p>
                            <?php endif; ?>
                            <?php
                            if (!empty($button_text)) :
                                $button_link = ($button_link) ? $button_link : '#'; ?>
                                <div class="center-btn">
                                    <a href="<?php echo esc_url($button_link); ?>" class="button-btn small-btn"><?php echo esc_html($button_text); ?></a>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div> <!-- .row -->
        </div>
    </section>
    <!-- home_4-populer-class style end here -->
    <?php
endif;
endif;
echo $this->endBlockComment('fitfab_popular_class');
