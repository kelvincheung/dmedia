<?php
/**
 * FitFab - award shortcode
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates.va-map
 * @since fitfab 1.0.0
 */
extract(shortcode_atts(array(
    'title' => '',
    'award' => '',
   ), $atts));
?>
<?php if (!empty($title)|| !empty($award)) : ?>
<!-- award-wrap start here -->
<section class="award-wrap fit-fab-award-layout-one">
    <div class="container">
        <?php if (!empty($title)) : ?>
            <div class="head-global family-oswald text-center ">
                <h2 class="h2"><?php echo esc_html($title); ?></h2>
            </div>
        <?php endif; 
		
		 if ($award >= 0) : ?>
        <div class="row top_gap">
            <?php
            $awards = vc_param_group_parse_atts($atts['award']);
            foreach ($awards as $award) :
                $award_img = wp_get_attachment_image_src($award['award_img'], "large");
                $award_img_style = (!empty($award_img[0])) ? $award_img[0] : '';
                ?>
                <div class="col-xs-12 col-sm-3 award-list">
                    <?php if (!empty($award['award_img'])) : ?>
                        <figure class="rounded-img"><img src="<?php echo esc_url($award_img_style); ?>" alt="<?php echo esc_html($award['award_title']); ?>" title="<?php echo esc_html($award['award_title']); ?>" /></figure>
                    <?php endif; ?>
                    
                    <?php if (!empty($award['award_title'])) : ?>
                        <h4><?php echo esc_html($award['award_title']); ?></h4>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>
        </div>
        <?php endif; ?>
    </div>
</section>
<!-- award-wrap end here -->
<?php
 endif;
echo $this->endBlockComment('fitfab_award');
