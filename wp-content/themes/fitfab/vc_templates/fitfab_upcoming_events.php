<?php
/**
 * FitFab - upcoming events shortcode
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates
 * @since fitfab 1.0.0
 */
extract(shortcode_atts(array(
    'number_of_events' => '',
    'event_title' => '',
    'event_sub_title' => '',
    'event_description' => '',
    ), $atts));
	if(!empty($number_of_events) || !empty($event_title) || !empty($event_sub_title) || !empty($event_description)) :
	

global $post;
?>
<!-- latest-news-home-2 style end here -->

<section class="schedule-section upcome-event fit-fab-upcoming-events-layout-one">
    <div class="clearfix">
        <div class="schedule-info">
            <?php if (!empty($event_title) || !empty($event_sub_title)) : ?>
                <h2><?php if (!empty($event_title)) : ?><span><?php echo esc_html($event_title); ?></span><?php endif; ?> <?php echo esc_html($event_sub_title); ?></h2>
            <?php endif; ?>
            
            <?php if (!empty($event_description)) : ?>
                <p><?php echo esc_html($event_description); ?></p>
            <?php endif; ?>
        </div>
<?php if (!empty($number_of_events)) : ?>
        <div class="schedule-slider">
            <div class="events-wrap clearfix">
                <?php
                $event_args = array(
                    'post_type' => 'event',
                    'post_status' => 'publish',
                    'order' => 'ASC',
                    'showposts' => $number_of_events,
                );
                $i = 0;
                $cls_arr = array('','event-two','event-three','event-four','event-five','event-six'); 
                $event_query = new WP_Query($event_args);
                if ($event_query->have_posts()) :
                    while ($event_query->have_posts()) : $event_query->the_post();
                        $EM_Event = em_get_event($post->ID, 'post_id');
                        $event_cls = $cls_arr[$i];
                        ?>
                        <div class="event-list <?php echo esc_attr($event_cls); ?>">
                            <span><?php echo esc_html($EM_Event->output('#M')); ?> <?php echo esc_html($EM_Event->output('#d')); ?>, <?php echo esc_html($EM_Event->output('#Y')); ?></span>
                            <h2><?php echo wp_trim_words(get_the_title(), 5, ''); ?></h2>
                            <span><?php echo esc_html($EM_Event->output('#_12HSTARTTIME')); ?> - <?php echo esc_html($EM_Event->output('#_12HENDTIME')); ?><span> <?php echo esc_html($EM_Event->output('#_LOCATIONNAME')); ?></span></span>
                            <a href="<?php echo esc_url(get_permalink()); ?>" class="link green-dark"><?php esc_html_e('read more', 'fitfab'); ?></a>
                        </div>
                        <?php $i=$i+1;
                    endwhile;
                    wp_reset_postdata();
                endif;
                ?>
            </div>
        </div>
        <?php endif; ?>
    </div>
</section>
<?php
endif;
echo $this->endBlockComment('fitfab_news_event');
