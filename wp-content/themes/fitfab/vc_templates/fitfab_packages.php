<?php
/**
 * FitFab - packages
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates
 * @since fitfab 1.0.0
 */
extract(shortcode_atts(array(
    'package_style' => '',
    'title' => '',
    'sub_title' => '',
    'phone_title' => '',
    'phone_no' => '',
    'email_title' => '',
    'email' => '',
     ), $atts));
if(!empty($title) || !empty($sub_title) || !empty($phone_title)) :
global $fitfab_data;	
$no_of_item = 3;

$args = array(
    "post_type" => "package",
    "post_status" => "publish",
    'posts_per_page' => $no_of_item
);
$fitfabPackageQuery = new WP_Query($args);
?>
<?php if ($package_style == 'home' || $package_style == 'home1' || $package_style == 'home3' || $package_style == '') : ?>
    <!-- package_wrap start here -->
    <section class="package-wrap fitfab-package-layout-one">
        <div class="container">
            <?php if (!empty($title)) : ?>
                <div class="head-global">
                    <h2 class="h2"><?php echo esc_html($title); ?> <span><?php echo esc_html($sub_title); ?></span></h2>
                </div>
            <?php endif; ?>
            <?php
            if ($fitfabPackageQuery->have_posts()) :
                while ($fitfabPackageQuery->have_posts()) : $fitfabPackageQuery->the_post();
                    get_template_part('content/pricing-packages');
                endwhile;
                wp_reset_postdata();
            else :
                get_template_part('content/', 'none');
            endif;
            ?>
        </div>
    </section>
<?php elseif ($package_style == 'home2') : ?>
    <!-- home-3-pacakage style start -->
    <div class="container">
        <section class="home-3-pacakage fitfab-package-layout-two">
            <?php if (!empty($title)) : ?>
                <div class="home-three-head extra">
                    <h2><span><?php echo esc_html($title); ?></span> <?php echo esc_html($sub_title); ?> </h2>
                </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-xs-12">
                    <ul class="package-list-3">
                        <?php
                        if ($fitfabPackageQuery->have_posts()) :
                            while ($fitfabPackageQuery->have_posts()) : $fitfabPackageQuery->the_post(); ?>
                                <li class="zoom">
                                    <?php if (has_post_thumbnail()): 
                                        $url = wp_get_attachment_url(get_post_thumbnail_id());
                                    ?>
                                        <figure class="block-img ">
                                            <img src="<?php echo esc_url(fitfab_resize($url, '130', '90')); ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" />
                                        </figure>
                                    <?php endif; ?>
                                    <div class="timing-listing">
                                        <h3><?php the_title(); ?></h3>
                                        <?php the_content(); ?>
                                    </div>
                                    <?php
				    if(function_exists('fitfab_rwmb_meta')){
                                    $price_per_month = fitfab_rwmb_meta('fitfab_price_per_month');
                                    $join_button_text = fitfab_rwmb_meta('fitfab_join_button_text');
                                    $join_button_link = fitfab_rwmb_meta('fitfab_join_button_link') ? fitfab_rwmb_meta('fitfab_join_button_link') : '#';
				    }
                                    if (!empty($price_per_month)) : ?>
                                        <div class="pack-price">
                                            <strong><?php echo class_exists( 'WooCommerce' )?get_woocommerce_currency_symbol($fitfab_data["fitfab_currency"]):$fitfab_data["fitfab_currency"]; ?><?php echo esc_html($price_per_month); ?><?php echo esc_html_e('*/M ', 'fitfab'); ?></strong>
                                            <a href="<?php echo esc_url($join_button_link); ?>" class="button-btn small-btn green-bg"><?php echo esc_html($join_button_text); ?></a>
                                        </div>
                                    <?php endif; ?>
                                </li>
                                <?php
                            endwhile;
                            wp_reset_postdata();
                        endif;
                        ?>
                    </ul>
                </div>
            </div>

            <?php if (!empty($phone_no) || !empty($email)) : ?>
                <div class="wrap-wrap clearfix">
                    <div class="row">
                        <?php if (!empty($phone_no)) : ?>
                            <div class="col-xs-12 col-sm-6 contact-ifo bdr-right">
                                <h3><?php echo esc_html($phone_title); ?> </h3>
                                <a href="<?php esc_html_e('tel:', 'fitfab'); ?><?php echo esc_html( str_replace(' ', '', $phone_no)); ?>"  ><?php echo esc_html(str_replace(' ', '', $phone_no)); ?></a>
                                <span class="or-block"> or</span>
                            </div>
                        <?php endif; ?>
                        <?php if (!empty($email)) : ?>
                            <div class="col-xs-12 col-sm-6 contact-ifo">
                                <h3><?php echo esc_html($email_title); ?></h3>
                                <a href="<?php esc_html_e('mailto:', 'fitfab'); ?><?php echo esc_html($email); ?>"  ><?php echo esc_html($email); ?></a>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endif; ?>

        </section>
    </div>
    <!-- home-3-pacakage style End -->
<?php endif; ?>
<!-- package_wrap End here -->
<?php
endif;
echo $this->endBlockComment('fitfab_testimonial');
