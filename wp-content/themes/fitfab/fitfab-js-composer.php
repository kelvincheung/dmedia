<?php
/**
 * Template Name: Visual Composer Template
 * 
 * This is Visual composer page template layout
 * 
 * @package fitfab
 * @subpackage fitfab
 * @since fitfab 1.0.0
 */
get_header();
?>
<!--Content Area Start-->
<div id="content">
    <?php
    if (have_posts()):
        while (have_posts()):
            the_post();
            the_content();
        endwhile;
    endif;
    ?>
</div>
<!--Content Area End-->
<?php
get_footer();
