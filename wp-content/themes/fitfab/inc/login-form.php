<?php
/**
 * FitFab - login form
 * 
 * @package fitfab
 * @subpackage fitfab.inc 
 * @since fitfab 1.0.0
 */


add_action('login_head', 'fitfab_login_logo');
function fitfab_login_logo() {
	global $fitfab_data;
    $site_logo = (!empty($fitfab_data['fitfab_logo']['url'])) ? $fitfab_data['fitfab_logo']['url'] : FITFAB_THEME_URL . "/images/logo.png";
    $color = (!empty($fitfab_data['primary_color'])) ? $fitfab_data['primary_color'] : '#ffffff';
    echo "<style type='text/css'>
       body.login div#login h1 a{ background-image: url('" . esc_url($site_logo) . "') !important; }
        body.login{ background-color: " . esc_attr($color) . "!important; }
    </style>";
}


add_action( 'login_enqueue_scripts', 'fitfab_login_stylesheet' );
function fitfab_login_stylesheet() {
    wp_enqueue_style( 'fitfab-login', FITFAB_THEME_URL . '/assets/css/admin/login-style.css', array(), null, 'all' );
    
}

add_filter('login_headerurl', 'fitfab_wp_login_url');
function fitfab_wp_login_url() {
    return site_url('/');
}

add_filter('login_headertitle', 'fitfab_wp_login_title');
function fitfab_wp_login_title() {
    return get_option('blogname');
}

