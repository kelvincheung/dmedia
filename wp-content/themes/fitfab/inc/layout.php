<?php

/**
 * FitFab - Page Layout settings
 * 
 * @package     fitfab
 * @subpackage fitfab.inc 
 * @since   fitfab  1.0.0
 */
class FitfabPageLayout {

    public function __construct() {
	$this->action();
    }

    /**
     * Layout action list
     */
    function action() {
	add_action("fitfab_header_layout", array(&$this, "fitfab_header"));
	add_action("fitfab_footer_layout", array(&$this, "fitfab_footer"));
	add_action("redux/options/fitfab_data/settings/change", array(&$this, "fitfab_set_home_page"));

	add_action("fitfab_theme_layout_class", array(&$this, "fitfab_theme_class"));
	add_action("fitfab_banner_parallax", array(&$this, "fitfab_banner_parallax"),1,10);

	add_action('fitfab_wrap_class', array(&$this, 'fitfab_wrap_class'));
	add_action('fitfab_blog_layout', array(&$this, 'fitfab_get_blog_layout'));
	add_action('fitfab_single_layout', array(&$this, 'fitfab_get_single_layout'));
	add_action('fitfav_nav_menu', array(&$this, 'fitfab_nav_menu'));
	add_filter('body_class', array(&$this, 'body_class'));
	
	add_filter('the_content_more_link', array(&$this,'the_content_more_link'));
	add_filter('excerpt_more', array(&$this,'the_content_more_link'));
    }

    /**
     * Banner image
     * @param unknown $args
     */
    function fitfab_banner_parallax($args) {
	global $fitfab_data;
	$fitfab_header_banner = (!empty($fitfab_data['fitfab_page_header_bg_img']['url'])) ? 'background:url(' . $fitfab_data['fitfab_page_header_bg_img']['url'] . ') repeat scroll 0 0 / cover' : '';
	if(empty($fitfab_header_banner)) 
	    {	    
	    return;
	    
	    }
	$title = (!empty($args['title']) ? $args['title'] : '')
	?><section class="slider-hero inner-banner_info" style="<?php echo esc_attr($fitfab_header_banner); ?>">
	    <div class="container">
		<h1><?php echo esc_html($title); ?></h1>
	    </div>
	</section><?php
    }

    /**
     * Set theme header class
     */
    function fitfab_theme_class() {
	global $fitfab_data;
	$homeclass = "homepage-2 about_us";
	$class = ($fitfab_data['fitfab_theme_layout']) ? $fitfab_data['fitfab_theme_layout'] : "homepage-1";
	printf("class='" . $class . " " . $homeclass . "'");
    }

    /**
     * Set header layout
     */
    function fitfab_header() {
	global $fitfab_data;
	$layoutOption = $fitfab_data['fitfab_header_layout'];

	switch ($layoutOption) {
	    case "header1":
		$slug = "content/header/one";
		break;
	    case "header3":
		$slug = "content/header/three";
		break;
	    case "header4":
		$slug = "content/header/four";
		break;
	    default:
		$slug = "content/header/two";
		break;
	}
	get_template_part($slug);
	//$this->home_slider();
    }

    /**
     * Set footer layout
     */
    function fitfab_footer() {
	global $fitfab_data;
	$layoutOption = $fitfab_data['fitfab_footer_layout'];

	switch ($layoutOption) {
	    case "footer1":
		$slug = "content/footer/one";
		break;
	    case "footer3":
		$slug = "content/footer/three";
		break;
	    case "footer4":
		$slug = "content/footer/four";
		break;
	    default:
		$slug = "content/footer/two";
		break;
	}
	get_template_part($slug);
	//$this->home_slider();
    }

    function fitfab_home_slider() {
	global $post;
	if (is_object($post)):
	    $slider = get_post_meta($post->ID, 'fitfabcore_slider_slider', true);
	    if (!empty($slider) && has_shortcode($slider, 'rev_slider')) :
		?><section id="slider">
		    <?php echo do_shortcode($slider); ?>
		</section><?php
	    endif;
	endif;
    }

    /**
     * Home page layout
     */
    function fitfab_set_home_page() {
	global $fitfab_data;
	
	$theme = isset($fitfab_data["fitfab_home_layout"]) ? $fitfab_data["fitfab_home_layout"] : 'home1';

	$home1 = isset($fitfab_data["fitfab_home_1"]) ? $fitfab_data["fitfab_home_1"] : '';
	$home2 = isset($fitfab_data["fitfab_home_2"]) ? $fitfab_data["fitfab_home_2"] : '';
	$home3 = isset($fitfab_data["fitfab_home_3"]) ? $fitfab_data["fitfab_home_3"] : '';
	$home4 = isset($fitfab_data["fitfab_home_4"]) ? $fitfab_data["fitfab_home_4"] : '';

	$show_on_front = get_option("show_on_front");
	$page_on_front = get_option("page_on_front");

	$home = "";
	switch ($theme):
	    case "home1": $home = $home1;

		break;
	    case "home2": $home = $home2;
		break;
	    case "home3": $home = $home3;
		break;
	    case "home4": $home = $home4;
		break;
	    case "home5": $home = $home5;
		break;
	    case "home6": $home = $home6;
		break;
	endswitch;
	if ($page_on_front != $home):
	    update_option("show_on_front", "page");
	    update_option('page_on_front', $home);
	endif;
    }

    /**
     * Set wraper class
     */
    function fitfab_wrap_class() {
	global $fitfab_data;
	$layoutOption = (!empty($fitfab_data['fitfab_home'])) ? $fitfab_data['fitfab_home'] : '';
	$wrap_class = ' shop-details shop-list homepage-1';

	if (is_page('homepage-two')) :
	    $wrap_class .= ' homepage-3';

	endif;
	if (is_page('homepage-three')) :
	    $wrap_class .= " homepage-4";
	endif;

	$class = "homepage-2";

	switch ($layoutOption) {
	    case "one":
		$class = "homepage-1";
		break;
	    case "two":
		$class = "homepage-2";
		break;
	    case "three":
		$class = "homepage-3";
		break;
	    case "four":
		$class = "homepage-4";
		break;
	    default:
		$class = "homepage-2";
		break;
	}
	echo "class='" . $class . $wrap_class . "'";
    }

    /**
     * Fitfab : blo layout
     *  
     */
    function fitfab_get_blog_layout() {
	$part = "content/blog/blog";
	get_template_part($part, 'one');
    }

    /**
     * Fitfab : blo layout
     *  
     */
    function fitfab_get_single_layout() {
	$part = "content/single/single";
	get_template_part($part, 'one');
    }

    /**
     * Nav menu
     */
    function fitfab_nav_menu() {
	$locations = get_theme_mod('nav_menu_locations');
	if (empty($locations)) {
	    echo '<nav class="navbar">';
	    wp_nav_menu(array(
		'container' => 'ul',
		'container_id' => 'cssmenu',
		'menu_class' => 'nav navbar-nav',
	    ));
	    echo '</nav>';
	} else {
	    if (isset($locations['primary_navigation'])) {
		if ($locations['primary_navigation'] == 0) :
		    echo '<nav class="navbar">';
		    wp_nav_menu(array(
			'container' => 'ul',
			'container_id' => 'cssmenu',
			'menu_class' => 'nav navbar-nav',
		    ));
		    echo '</nav>';
		else:
		    echo '<nav class="navbar">';
		    wp_nav_menu(array(
			'container' => 'ul',
			'container_id' => 'cssmenu',
			'menu_class' => 'nav navbar-nav',
		    ));
		    echo '</nav>';
		endif;
	    }
	}
    }

    function body_class($classes) {
	global $is_lynx, $is_gecko, $is_IE, $is_opera, $is_NS4, $is_safari, $is_chrome, $is_iphone;
	$browser_version = array();
	if ($is_lynx)
	    $classes[] = 'lynx';
	elseif ($is_gecko)
	    $classes[] = 'gecko';
	elseif ($is_opera)
	    $classes[] = 'opera';
	elseif ($is_NS4)
	    $classes[] = 'ns4';
	elseif ($is_safari)
	    $classes[] = 'safari';
	elseif ($is_chrome)
	    $classes[] = 'chrome';
	elseif ($is_IE) {
	    $classes[] = 'ie';
	    if (preg_match('/MSIE ([0-9]+)([a-zA-Z0-9.]+)/', $_SERVER['HTTP_USER_AGENT'], $browser_version))
		$classes[] = 'ie' . $browser_version[1];
	} else
	    $classes[] = 'unknown';
	if ($is_iphone)
	    $classes[] = 'iphone';
	if (stristr($_SERVER['HTTP_USER_AGENT'], "mac")) {
	    $classes[] = 'osx';
	} elseif (stristr($_SERVER['HTTP_USER_AGENT'], "linux")) {
	    $classes[] = 'linux';
	} elseif (stristr($_SERVER['HTTP_USER_AGENT'], "windows")) {
	    $classes[] = 'windows';
	}

	return $classes;
    }
    
    function the_content_more_link() {
	return '';
    }
}

new FitfabPageLayout();

