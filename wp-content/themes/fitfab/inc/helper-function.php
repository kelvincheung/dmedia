<?php
/**
 * FitFab - Helper Functions
 * 
 * This file has collection of fitfab theme helper functions  
 *
 * @package fitfab
 * @subpackage fitfab.inc
 * @since fitfab 1.0.0
 */

/**
 * Allow SVG upload
 * 
 * @param  $mimes
 * @return $mimes
 */
function fitfab_mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}

add_filter('upload_mimes', 'fitfab_mime_types');
add_filter('mime_types', 'fitfab_mime_types');

/**
 * truncate content
 * 
 * @param content $string
 * @param number $chars
 * @return string
 */
function fitfab_truncate_content($string, $chars = 50) {

    // ----- remove HTML TAGs -----
    $string = preg_replace('/<[^>]*>/', ' ', $string);
    $string = preg_replace('/\[.*?\]/', ' ', $string); //-- remove shortcode
    // ----- remove control characters -----
    $string = str_replace("\r", '', $string);    // --- replace with empty space
    $string = str_replace("\n", ' ', $string);   // --- replace with space
    $string = str_replace("\t", ' ', $string);   // --- replace with space
    // ----- remove multiple spaces -----
    $string = trim(preg_replace('/ {2,}/', ' ', $string));

    $len = strlen($string);

    if ($len > $chars) :
        $string = substr($string, 0, $chars);
    endif;

    return $string;
}

/**
 * Exceprt Length
 * @param  $limit
 * @return mixed
 */
function excerpt($limit) {
    $excerpt = explode(' ', get_the_excerpt(), $limit);
    if (count($excerpt) >= $limit) :
        array_pop($excerpt);
        $excerpt = implode(" ", $excerpt) . '...';
    else:
        $excerpt = implode(" ", $excerpt);
    endif;
    $excerpt = preg_replace('`[[^]]*]`', '', $excerpt);
    return $excerpt;
}

add_filter('get_the_excerpt', 'do_shortcode');

/**
 * Class Posts Array for Metabox
 * @return Ambigous <multitype:, number>
 */
function fitfab_class_posts_dropdown() {
    $args = array(
        'numberposts' => -1,
        'offset' => 0,
        'category' => 0,
        'orderby' => 'post_date',
        'order' => 'ASC',
        'post_type' => 'class',
        'post_status' => 'publish',
        'suppress_filters' => true);

    $fitfab_class_posts = wp_get_recent_posts($args);

    $fitfab_class_post_array = array();
    $fitfab_class_posts_array = array();

    foreach ($fitfab_class_posts as $item):
        $fitfab_class_post_array = array($item["ID"] => $item["post_title"]);
        $fitfab_class_posts_array = $fitfab_class_post_array + $fitfab_class_posts_array;
    endforeach;

    return $fitfab_class_posts_array;
}


/**
 * Fitfab - Class Trainer Dropdown
 * 
 * @package fitfab
 * @since fitfab 1.0.0
 */
function fitfab_trainer_dropdown() {
    $args = array(
        'numberposts' => -1,
        'offset' => 0,
        'category' => 0,
        'orderby' => 'post_date',
        'order' => 'ASC',
        'post_type' => 'trainer',
        'post_status' => 'publish',
        'suppress_filters' => true);

    $fitfab_trainer = wp_get_recent_posts($args);

    $fitfab_trainer_array = array();
    $fitfab_trainers_array = array();

    foreach ($fitfab_trainer as $item):
        $fitfab_trainer_array = array($item["post_title"] => $item["post_title"]);
        $fitfab_trainers_array = $fitfab_trainer_array + $fitfab_trainers_array;
    endforeach;

    return $fitfab_trainers_array;
}

/**
 * Get page id by page template
 * 
 * @param name $page_template
 * @return Ambigous <boolean, unknown>
 */
function fitfab_page_template_id($page_template) {
    $args = array(
        'post_type' => 'page',
        'fields' => 'ids',
        'nopaging' => true,
        'meta_key' => '_wp_page_template',
        'meta_value' => $page_template
    );
    $pages = get_posts($args);
    $page_id = false;
    if (is_array($pages)):
        foreach ($pages as $page):
            $page_id = $page;
            break;
        endforeach;
    endif;
    return $page_id;
}

//Page Loader
function fitfab_page_loader() {
    global $fitfab_data;
    if (!empty($fitfab_data['fitfab_loader_status']) ) :
        ?>
        <div id="loading">
            <div id="loading-center">
                <div id="loading-center-absolute">
                    <div class="object" id="object_one"></div>
                    <div class="object" id="object_two"></div>
                    <div class="object" id="object_three"></div>
                </div>
            </div>
        </div>
        <?php
    endif;
}

//fitfab_header_layout
add_action("wp_footer", "fitfab_page_loader", 8);

add_action('fitfab_popular_classes_background', 'popular_classes_after_image');

function popular_classes_after_image($image) {
    if (empty($image)) {
        return;
    }
    ?>
    <style type="text/css">
        .fit-fab-populer-classes-layout-two .about-fit .bg-wrp:after {
            content: url(<?php echo wp_get_attachment_url($image); ?>);
            bottom: -5px;
            height: auto;
            width: auto;
        }
    </style>
    <?php
}

/**
 * custom css option
 */
/*add_action('wp_head', 'fitfab_custom_css_option', 999999);

function fitfab_custom_css_option() {
    global $fitfab_data;
    echo '<style type="text/css">';
    print($fitfab_data['fitfab_customcss']);
    echo '</style>';
}*/

function fitfab_the_content_more_link() {
    return '';
}

add_filter('the_content_more_link', 'fitfab_the_content_more_link');
add_filter('excerpt_more', 'fitfab_the_content_more_link');

function fitfab_rwmb_meta($key, $args = array(), $post_id = null) {
    if (function_exists('rwmb_meta')) {
        return rwmb_meta($key, $args, $post_id);
    }
}

/**
 * Coming Soon - Template Redirection
 * @global type $fitfab_data
 */
function fitfab_comingsoon_redirect() {
    global $fitfab_data;
    /* if(isset($fitfab_data['fitfab_datepicker']) && !empty($fitfab_data['fitfab_datepicker'])){
      $fitfab_data['fitfab_comming_soon'] = date("F d, Y h:i:s", strtotime($fitfab_data['fitfab_datepicker']));
      } else {
      $fitfab_data['fitfab_comming_soon'] = date("F d, Y h:i:s", strtotime(" + 1 year"));
      } */
    $coming_soon_option = $fitfab_data['fitfab_comming_soon'];
    if ($coming_soon_option && !is_user_logged_in()) {
        fitfab_inclusion('/content/coming-soon-display.php');
        die();
    }
}

add_action('template_redirect', 'fitfab_comingsoon_redirect');

/**
 * Coming Soon - admin notices
 * @global type $fitfab_data
 */
function fitfab_coming_soon_notice() {

    global $fitfab_data;
    $coming_soon_option = $fitfab_data['fitfab_comming_soon'];
    if ($coming_soon_option) {
        $message = esc_html__('Your theme is currently coming soon mod.  ', 'fitfab');
        printf('<div class="error"><p>%s</p></div>', $message);
    }
}

add_action('admin_notices', 'fitfab_coming_soon_notice');

