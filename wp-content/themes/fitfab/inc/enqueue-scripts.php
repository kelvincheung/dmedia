<?php

/**
 * Fitfab - Enqueue Scripts and Styles
 *  
 * @package fitfab
 * @subpackage fitfab.inc
 * @since fitfab 1.0.0
 */
function fitfab_styles_scripts() {

    global $fitfab_data;

    //@styles
    wp_register_style('fitfab-style', get_stylesheet_uri(), array(), null, 'all');
    wp_register_style('bootstrap', FITFAB_THEME_URL . '/assets/css/bootstrap.css', array(), null, 'all');
    wp_register_style('font-awesome.min', FITFAB_THEME_URL . '/assets/css/font-awesome.min.css', array(), null, 'all');
    wp_register_style('simple-line-icons', FITFAB_THEME_URL . '/assets/css/simple-line-icons.css');
    wp_register_style('owl.carousel', FITFAB_THEME_URL . '/assets/css/owl.carousel.css');
    wp_register_style('fitfab-global', FITFAB_THEME_URL . '/assets/css/global.css', array(), null, 'all');
    wp_register_style('fitfab-site', FITFAB_THEME_URL . '/assets/css/site.css', array(), null, 'all');
    wp_register_style('fitfab-responsive', FITFAB_THEME_URL . '/assets/css/responsive.css', array(), null, 'all');
    wp_register_style('fitfab-shopping-cart', FITFAB_THEME_URL . '/assets/css/shopping-cart.css', array(), null, 'all');
    wp_register_style('fitfab-woocommerce-custom-css', FITFAB_THEME_URL . '/assets/css/fitfab-woocommerce-custom.css', array(), null, 'all');
    wp_register_style('fitfab-wp-integration-css', FITFAB_THEME_URL . '/assets/css/wp-integration.css', array(), null, 'all');
    wp_register_style('fitfab-wp-responsive-css', FITFAB_THEME_URL . '/assets/css/wp-responsive.css', array(), null, 'all');

    //theme switcher css
    wp_enqueue_style('bootstrap');
    wp_enqueue_style('font-awesome.min');
    wp_enqueue_style('simple-line-icons');
    wp_enqueue_style('owl.carousel');
    wp_enqueue_style('fitfab-global');
    wp_enqueue_style('fitfab-site');
    wp_enqueue_style('fitfab-responsive');
    wp_enqueue_style('fitfab-wp-integration-css');
    wp_enqueue_style('fitfab-wp-responsive-css');
    wp_enqueue_style('fitfab-style');

    wp_enqueue_style('fitfab-shopping-cart');
    if (function_exists('is_woocommerce')) {
        switch (true) {
            case is_woocommerce():
            case is_cart():
            case is_checkout():
            case is_account_page():
            case is_product():
            case is_product_category():
            case is_shop():
                wp_enqueue_style('fitfab-woocommerce-custom-css');
                break;
        }
    }

    //inline style
    $loader_color = (!empty($fitfab_data['Secondary_theme_color'])) ? $fitfab_data['Secondary_theme_color'] : "#1e73be";

    $custom_css = $fitfab_data['fitfab_customcss'] . ' #loading{ background:' . $loader_color . ';}';
    wp_add_inline_style('fitfab-style', $custom_css);

    //@scripts
     $googleapisArgs = array('sensor' => false);
    $google_map_api_key = (!empty($fitfab_data['fitfab_google_map_api_key'])) ? $fitfab_data['fitfab_google_map_api_key'] : "";
    if(!empty($google_map_api_key)) {
	$googleapisArgs['key'] = $google_map_api_key;
    }
    wp_register_script('google.map', add_query_arg($googleapisArgs, '//maps.googleapis.com/maps/api/js'), array('jquery'), null, true);
    wp_register_script('bootstrap', FITFAB_THEME_URL . '/assets/js/bootstrap.js', array('jquery'), null, true);
    wp_register_script('less', FITFAB_THEME_URL . '/assets/js/less.js', array(), null, true);
    wp_register_script('owl.carousel', FITFAB_THEME_URL . '/assets/js/owl.carousel.js', array(), null, true);
    wp_register_script('fitfab.woocommerce.custom', FITFAB_THEME_URL . '/assets/js/woocommerce.custom.js', array('jquery'), null, true);
    wp_register_script('fitfab-option', FITFAB_THEME_URL . '/assets/theme-option/fitfab-option.js', array('jquery', 'less'), null, true);
    wp_register_script('countdown.jquery.plugin', FITFAB_THEME_URL . '/assets/countdown/jquery.plugin.js', array('jquery'), null, true);
    wp_register_script('jquery.countdown', FITFAB_THEME_URL . '/assets/countdown/jquery.countdown.js', array('jquery'), null, true);
    wp_register_script('fitfab-site-js', FITFAB_THEME_URL . '/assets/js/site.js', array('jquery', 'google.map', 'countdown.jquery.plugin', 'jquery.countdown'), null, true);

    wp_enqueue_script('bootstrap');
    wp_enqueue_script('less');
    wp_enqueue_script('owl.carousel');
    wp_enqueue_script('fitfab.woocommerce.custom');
    wp_enqueue_script('fitfab-option');



    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }

    global $fitfab_data;

    $site_options = array(
        'address' => (!empty($fitfab_data['fitfab_address'])) ? $fitfab_data['fitfab_address'] : '',
        'phone' => (!empty($fitfab_data['fitfab_phone'])) ? $fitfab_data['fitfab_phone'] : '',
        'email' => (!empty($fitfab_data['fitfab_email'])) ? $fitfab_data['fitfab_email'] : '',
        'lat' => (!empty($fitfab_data['fitfab_latitude'])) ? $fitfab_data['fitfab_latitude'] : '',
        'lng' => (!empty($fitfab_data['fitfab_longitude'])) ? $fitfab_data['fitfab_longitude'] : '',
    );

    if (isset($fitfab_data['fitfab_datepicker']) && !empty($fitfab_data['fitfab_datepicker'])) {
        $site_options['countdown'] = date("F d, Y h:i:s", strtotime($fitfab_data['fitfab_datepicker']));
    } else {
        $site_options['countdown'] = date("F d, Y h:i:s", strtotime(" + 1 year"));
    }

    wp_localize_script('fitfab-site-js', 'FITFAB_SITE_OPTION', $site_options);

    wp_enqueue_script('fitfab-site-js');

    /**
     * Fitfab - switcher
     * @since fitfab 1.0.0
     */
    $theme_options = array(
        'themeURL' => FITFAB_THEME_URL,
        'color' => (!empty($fitfab_data['primary_color'])) ? $fitfab_data['primary_color'] : "#8ecc3b",
        'Secondary_color' => (!empty($fitfab_data['Secondary_theme_color'])) ? $fitfab_data['Secondary_theme_color'] : "#1e73be",
        'font' => (!empty($fitfab_data['fitfab_theme_font']['font-family'])) ? $fitfab_data['fitfab_theme_font']['font-family'] : "Roboto",
        'header' => (!empty($fitfab_data['fitfab_stickey_header'])) ? $fitfab_data['fitfab_stickey_header'] : "normal",
        'layout' => (!empty($fitfab_data['fitfab_theme_layout'])) ? $fitfab_data['fitfab_theme_layout'] : "full-width",
        'map_api_key' => (!empty($fitfab_data['fitfab_google_map_api_key'])) ? $fitfab_data['fitfab_google_map_api_key'] : " ",
    );

    wp_localize_script('fitfab-option', 'FITFAB_OPTIONS', $theme_options);
}

add_action('wp_enqueue_scripts', 'fitfab_styles_scripts');



/**
 * Fitfab - Less
 * @since fitfab 1.0.0
 */
add_action("wp_head", 'fitfab_less');

function fitfab_less() {
    ?>
    <link rel="stylesheet/less" href="<?php echo FITFAB_THEME_URL . '/assets/css/skin.less'; ?>">
    <?php
}

add_action('admin_head', 'admin_css');

function admin_css() {

echo '<style  type="text/css">
#setting-error-tgmpa { display: block; clear: both; }
#fitfab_data-fitfab_footer_layout ul li,       
#fitfab_data-fitfab_header_layout ul li { width: 100% !important; }
#fitfab_data-fitfab_footer_layout ul li label.redux-image-select,
#fitfab_data-fitfab_header_layout ul li  label.redux-image-select{ width: 100%; }
</style>';

}
