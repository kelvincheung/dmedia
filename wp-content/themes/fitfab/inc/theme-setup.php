<?php
/**
 * FitFab - theme setup functionality
 *
 * @package  fitfab
 * @subpackage fitfab.inc
 * @since fitfab 1.0.0
 */
if (version_compare($GLOBALS['wp_version'], '4.1', '<')) {
    fitfab_inclusion('inc/back-compat.php');
}
/**
 * fitfab_setup
 */
if (!function_exists('fitfab_setup')) :

    function fitfab_setup() {
        load_theme_textdomain('fitfab', get_template_directory() . '/languages');
        add_theme_support('automatic-feed-links');
        add_theme_support('title-tag');
        add_theme_support('woocommerce');
        // This theme uses wp_nav_menu() in one location.
        register_nav_menus(array(
            'primary_navigation' => esc_html__('Primary Navigation', 'fitfab'),
        ));
        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array('search-form', 'comment-form', 'comment-list', 'gallery', 'caption',));
        // Enable support for Post Formats.
        add_theme_support('post-formats', array('aside', 'image', 'gallery', 'video', 'quote', 'link',));

        add_theme_support('post-thumbnails');
        set_post_thumbnail_size(150, 150, true);
        
        add_image_size('package', 360, 260, true);
        add_image_size('class', 340, 190, true);
        add_image_size('featured', 750, 360, true);
        add_image_size('related-post', 375, 261, true);
        add_image_size('testimonial', 64, 64, true);
        add_image_size('event', 552, 240, true);
        add_image_size('new', 360, 260, true);
        add_image_size('widget', 130, 110, true);
        
        
	    update_option('revslider-valid', 'true');
		delete_transient('_vc_page_welcome_redirect');

		/**
		 * Avoid Theme Support feature
		 * We are using Option
		 */
		
		add_theme_support('custom-header');
		remove_theme_support('custom-header');
		
		add_theme_support('custom-background');
		remove_theme_support('custom-background');
		
		add_editor_style();
		remove_editor_styles();
	
    }

endif;
add_action('after_setup_theme', 'fitfab_setup');

/**
 * @global int $content_width
 */
function fitfab_content_width() {
    $GLOBALS['content_width'] = $content_width = 640;
}

add_action('after_setup_theme', 'fitfab_content_width', 0);

if (!function_exists('_wp_render_title_tag')) :

    /**
     * Filters wp_title to print a neat <title> tag based on what is being viewed.
     *
     * @param string $title Default title text for current view.
     * @param string $sep Optional separator.
     * @return string The filtered title.
     */
    function fitfab_wp_title($title, $sep) {
        if (is_feed()) {
            return $title;
        }

        global $page, $paged;

        // Add the blog name.
        $title .= get_bloginfo('name', 'display');

        // Add the blog description for the home/front page.
        $site_description = get_bloginfo('description', 'display');
        if ($site_description && ( is_home() || is_front_page() )) {
            $title .= " $sep $site_description";
        }

        // Add a page number if necessary.
        if (( $paged >= 2 || $page >= 2 ) && !is_404()) {
            $title .= " $sep " . sprintf(esc_html__('Page %s', 'fitfab'), max($paged, $page));
        }

        return $title;
    }

    add_filter('wp_title', 'fitfab_wp_title', 10, 2);

    /**
     * Title shim for sites older than WordPress 4.1.
     *
     * @todo Remove this function when WordPress 4.3 is released.
     */
    function fitfab_render_title() {
        ?>
        <title><?php wp_title('|', true, 'right'); ?></title>
        <?php
    }

    add_action('wp_head', 'fitfab_render_title');
endif;



/**
 * Add Favicon
 */
add_action("wp_head", "fitfab_favicon");
add_action("admin_head", "fitfab_favicon");

function fitfab_favicon() {
	global $fitfab_data;
	if (!function_exists( 'wp_site_icon' )) {		
		$favicon_url = isset($fitfab_data['fitfab_faviconurl']['url']) ? $fitfab_data['fitfab_faviconurl']['url'] : '';
		if(!empty($favicon_url)):

		$site_icon_id = fitfab_get_attachment_id_from_guid($favicon_url);
		update_option('site_icon', $site_icon_id);
		else:
		add_filter( 'get_site_icon_url',  'fitfab_site_icon', 10, 3);
		endif;
	}
	else{
		$fitfab_theme_url = get_template_directory_uri();
		$favicon_url = isset($fitfab_data['fitfab_faviconurl']['url']) ? $fitfab_data['fitfab_faviconurl']['url'] : '';
		$favicon = (!empty($favicon_url)) ? $favicon_url : $fitfab_theme_url . '/favicon.ico';
		echo '<link rel="shortcut icon" type="image/x-icon" href="' . esc_url($favicon) . '"  />';
	}
}


function fitfab_site_icon($url, $size, $blog_id){
	global $fitfab_data;
	$fitfab_theme_url = get_template_directory_uri();
	$favicon_url = isset($fitfab_data['fitfab_faviconurl']['url']) ? $fitfab_data['fitfab_faviconurl']['url'] : '';
	$favicon = (!empty($favicon_url)) ? $favicon_url : $fitfab_theme_url . '/favicon.ico';
	return $favicon;

}
function fitfab_get_attachment_id_from_guid( $guid ) {
	global $wpdb;
	// Missing $guid return false
	if ( ! $guid ){
		return false;
	}
	// Get the ID
	$id = $wpdb->get_var( $wpdb->prepare("SELECT p.ID FROM ".$wpdb->prefix."posts p  WHERE p.guid = %s AND p.post_type = %s", $guid, 'attachment' ) );
	// ID not found, try getting it the expensive WordPress way
	if ( $id == 0 ){
		$id = url_to_postid( $guid );
	}

	return $id;

}


if (!in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {
    return;
}

delete_transient('_wc_activation_redirect');