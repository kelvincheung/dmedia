<?php

/**
 * FitFab : Typography
 *
 * @package fitfab
 * @subpackage fitfab.inc.options.section
 * @since fitfab 1.0.0
 */
return array(
    'title' => esc_html__('Typography', 'fitfab'),
    'id' => 'color-settings',
    'customizer_width' => '450px',
    'desc' => esc_html__('For full documentation on this field, visit: ', 'fitfab'),
    'icon' => 'el el-brush',
    'fields' => array(
        array(
            'id' => 'primary_color',
            'type' => 'color',
            'title' => esc_html__('Primary Color', 'fitfab'),
            'subtitle' => esc_html__('Pick a primary color for the theme.', 'fitfab'),
            'default' => '#8ecc3b',
            'validate' => 'color',
        ),
	array(
            'id' => 'Secondary_theme_color',
            'type' => 'color',
            'title' => esc_html__('Secondary Color', 'fitfab'),
            'subtitle' => esc_html__('Pick a secondary color for the theme.', 'fitfab'),
            'default' => '#6775de',
            'validate' => 'color',
        ),
        array(
            'id' => 'fitfab_theme_font',
            'type' => 'typography',//'typography',
            'title' => esc_html__('Font', 'fitfab'),
            'subtitle' => 'Google fonts',
            'desc' => '',
            'font-backup' => false,
            'google' => true,
            'font-style' => false,
            'subsets' => false,
            'font-size' => false,
            'line-height' => false,
            'word-spacing' => false,
            'letter-spacing' => false,
            'color' => false,
            'preview' => false,
            'all_styles' => true,
            'text-align' => false,
            'font-weight' => false,
        ),
    ),
);
