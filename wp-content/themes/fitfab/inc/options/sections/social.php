<?php
/**
 * FitFab : Social Icon
 *
 * @package fitfab
 * @subpackage fitfab.inc.options.section
 * @since fitfab 1.0.0
 */

return array(
        'title' => esc_html__( 'Social Links', 'fitfab' ),
        'id'    => 'social-links',
        'desc'  => ' ',
        'customizer_width' => '400px',
        'icon'  => 'el el-twitter',
        'fields'=> array(
            array(
                'id'       => 'facebook',
                'type'     => 'text',
                'validate' => 'url',
                'title'    => esc_html__( 'Facebook', 'fitfab' ),
                'desc'     => esc_html__( 'Enter URL for your Facebook profile.', 'fitfab' ),
                'default'  => '#',
            ),
            array(
                'id'       => 'twitter',
                'type'     => 'text',
                'validate' => 'url',
                'title'    => esc_html__( 'Twitter', 'fitfab' ),
                'desc'     => esc_html__( 'Enter URL for your Twitter profile.', 'fitfab' ),
                'default'  => '#',
            ),
            array(
                'id'       => 'youtube',
                'type'     => 'text',
                'validate' => 'url',
                'title'    => esc_html__( 'YouTube', 'fitfab' ),
                'desc'     => esc_html__( 'Enter URL for your YouTube profile.', 'fitfab' ),
                'default'  => '#',
            ),
            array(
                'id'       => 'instagram',
                'type'     => 'text',
                'validate' => 'url',
                'title'    => esc_html__( 'Instagram', 'fitfab' ),
                'desc'     => esc_html__( 'Enter URL for your Instagram profile.', 'fitfab' ),
                'default'  => '#',
            ),
            array(
                'id'       => 'rss',
                'type'     => 'text',
                'validate' => 'url',
                'title'    => esc_html__( 'Rss', 'fitfab' ),
                'desc'     => esc_html__( 'Enter URL for your RSS profile.', 'fitfab' ),
                'default'  => '#',
            ),
            array(
                'id'       => 'gplus',
                'type'     => 'text',
                'validate' => 'url',
                'title'    => esc_html__( 'Google+', 'fitfab' ),
                'desc'     => esc_html__( 'Enter URL for your Google+ profile.', 'fitfab' ),
                'default'  => '#',
            ),
       		array(
     			'id'       => 'vimeo',
        		'type'     => 'text',
        		'validate' => 'url',
        		'title'    => esc_html__( 'Vimeo', 'fitfab' ),
        		'desc'     => esc_html__( 'Enter URL for your Vimeo profile.', 'fitfab' ),
        		'default'  => '#',
        	),
        	array(
        		'id'       => 'dribble',
        		'type'     => 'text',
        		'validate' => 'url',
        		'title'    => esc_html__( 'Dribble', 'fitfab' ),
        		'desc'     => esc_html__( 'Enter URL for your Dribble profile.', 'fitfab' ),
        		'default'  => '#',
        	),
        	array(
        		'id'       => 'linkedin',
        		'type'     => 'text',
        		'validate' => 'url',
        		'title'    => esc_html__( 'Linkedin', 'fitfab' ),
        		'desc'     => esc_html__( 'Enter URL for your Linkedin profile.', 'fitfab' ),
        		'default'  => '#',
        	),
        	array(
        		'id'       => 'pinterest',
        		'type'     => 'text',
        		'validate' => 'url',
        		'title'    => esc_html__( 'Pinterest', 'fitfab' ),
        		'desc'     => esc_html__( 'Enter URL for your Pinterest.', 'fitfab' ),
        		'default'  => '#',
        	),	
        	array(
        		'id' => 'fitfab_displaysocial',
        		'type' => 'sorter',
        		'title' => 'Show Social Icon On Header',
        		'subtitle' => 'You can choose ICON display here.',
        		'compiler' => 'true',
        		'options' => array(
        			'SHOW' => array(
        				'Facebook' => 'Facebook',
        				'Twitter' => 'Twitter',
        				'Youtube' => 'Youtube',
        				'Rss' => 'Rss',
        				'Google' => 'Google Plus',
                                        'Instagram' => 'Instagram',
        						),
        			'NOT SHOW' => array(
        				'Vimeo' => 'Vimeo',
        				'Dribble' => 'Dribble',
        				'Linkedin' => 'Linkedin',
        				'Pinterest' => 'Pinterest'
        				)
        			)
        		)
        )
    ) ;
