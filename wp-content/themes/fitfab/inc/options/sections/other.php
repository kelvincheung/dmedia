<?php
/**
 * FitFab : comming soon enable/disable
 *
 * @package fitfab
 * @subpackage fitfab.inc.options.section
 * @since fitfab 1.0.0
 */
if(class_exists( 'WooCommerce' )){
$currency_code_options = get_woocommerce_currencies();

		foreach ( $currency_code_options as $code => $name ) {
			$currency_code_options[ $code ] = $name . ' (' . get_woocommerce_currency_symbol( $code ) . ')';
		}
                }
return array(
		'icon'      => 'el el-cogs',
		'title'     => esc_html__('Other', 'fitfab'),
		'fields'    => array(
			array(
				'id'        => 'fitfab_loader_status',
				'type'      => 'switch',
				'title'     => esc_html__('Loader Image', 'fitfab'),
				'subtitle'  => esc_html__('Loader image on/off.', 'fitfab'),
				'default'   => false,
			),
                        array(
				'id'        => 'fitfab_currency',
                                'type' => 'select',
                                'title' => esc_html__('Select Theme Currency', 'fitfab'),
                                'desc' => esc_html__('Select Theme Currency', 'fitfab'),
                                'options' => class_exists( 'WooCommerce' ) ?$currency_code_options:array('$' => 'United States dollar ($)'),
                                'default' => '$',
			),
		)
	);
