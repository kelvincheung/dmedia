<?php

/**
 * FitFab : 404
 *
 * @package fitfab
 * @subpackage fitfab.inc.options.section
 * @since fitfab 1.0.0
 */
return array(
    'title' => esc_html__('404 Setting', 'fitfab'),
    'id' => '404',
    'customizer_width' => '450px',
    'desc' => ' ',
    'icon' => 'el el-error',
    'fields' => array(
        array(
            'id' => 'fitfab_404_image',
            'type' => 'media',
            'title' => esc_html__('404 Image', 'fitfab'),
            'desc' => '',
            'subtitle' => esc_html__('Upload your 404 page image.', 'fitfab'),
        ),
        array(
            'id' => 'fitfab_404_page_title',
            'type' => 'text',
            'title' => esc_html__('404 Page Title', 'fitfab'),
            'desc' => '',
            'subtitle' => esc_html__('404 Page Title', 'fitfab'),
        ),
        array(
            'id' => 'fitfab_404_page_subtitle',
            'type' => 'text',
            'title' => esc_html__('404 Page Sub Title', 'fitfab'),
            'desc' => '',
            'subtitle' => esc_html__('404 Page Sub Title', 'fitfab'),
        ),
        array(
            'id' => 'fitfab_404_page_another_subtitle',
            'type' => 'text',
            'title' => esc_html__('404 Page Another Sub Title', 'fitfab'),
            'desc' => '',
            'subtitle' => esc_html__('404 Page Another Sub Title', 'fitfab'),
        )
    ),
);
