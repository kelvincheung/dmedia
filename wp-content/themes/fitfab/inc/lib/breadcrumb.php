<?php
/**
 * FitFab - breadcrumb
 *
 * @package     fitfab
 * @subpackage fitfab.inc.lib
 * @since fitfab 1.0.0
 * */
//fitfab_banner_parallax

add_action("fitfab_banner_parallax", "fitfab_breadcrum_ui", 11);

function fitfab_breadcrum_ui() {
    global $post;
    ?>
    <div class="breadcrum-sec">
        <div class="container">
            <?php fitfab_breadcrumb(); ?>
        </div>
    </div>


    <?php
}

function fitfab_breadcrumb() {

    $delimiter = ''; // delimiter between crumbs
    $home = esc_html__('Home', 'fitfab'); // text for the 'Home' link
    $showCurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show
    $before = '<li class="active">'; // tag before the current crumb
    $after = '</li>'; // tag after the current crumb

    global $post;
    $homeLink = esc_url(home_url('/'));

    echo '<ol class="clearfix breadcrumb"><li><a href="' . $homeLink . '">' . $home . '</a></li> ' . $delimiter . ' ';

    if (is_category()) {

        $thisCat = get_category(get_query_var('cat'), false);
        if ($thisCat->parent != 0)
            echo get_category_parents($thisCat->parent, TRUE, ' ' . $delimiter . ' ');
        print($before . single_cat_title('', false) . $after);
    } elseif (is_search()) {
        print($before . esc_html__('Search results for "', "fitfab") . get_search_query() . '"' . $after);
    } elseif (is_day()) {
        echo '<li><a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a></li> ' . $delimiter . ' ';
        echo '<li><a href="' . get_month_link(get_the_time('Y'), get_the_time('m')) . '">' . get_the_time('F') . '</a></li> ' . $delimiter . ' ';
        print($before . get_the_time('d') . $after);
    } elseif (is_month()) {
        echo '<li><a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a></li> ' . $delimiter . ' ';
        print($before . get_the_time('F') . $after);
    } elseif (is_year()) {
        print($before . get_the_time('Y') . $after);
    } elseif (is_single() && !is_attachment()) {
        if (get_post_type() != 'post') {
            $post_type = get_post_type_object(get_post_type());
            //print_r($post_type); //$slug['url']
            $slug = $post_type->rewrite;
            echo '<li><a href="' . $homeLink . '/' . $slug['slug'] . '">' . $post_type->labels->singular_name . '</a></li>';
            if ($showCurrent == 1)
                echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
        } else {
            $cat = get_the_category();
            $cat = $cat[0];
            $cats = get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
            if ($showCurrent == 0)
                $cats = preg_replace("#^(.+)\s$delimiter\s$#", "$1", $cats);
            echo '<li>' . $cats . '</li>';
            if ($showCurrent == 1)
                print($before . get_the_title() . $after);
        }
    } elseif (!is_single() && !is_page() && get_post_type() != 'post' && !is_404() && !is_author()) {
        $post_type = get_post_type_object(get_post_type());
        print($before . $post_type->labels->singular_name . $after); //-----post type/taxonomy
    } elseif (is_attachment()) {
        $parent = get_post($post->post_parent);
        $cat = get_the_category($parent->ID);
        $cat = $cat[0];
        echo is_wp_error($cat_parents = get_category_parents($cat, TRUE, '' . $delimiter . '')) ? '' : $cat_parents;
        echo '<li><a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a></li>';
        if ($showCurrent == 1)
            echo '' . $delimiter . ' ' . $before . get_the_title() . $after;
    } elseif (is_page() && !$post->post_parent) {
        if ($showCurrent == 1)
            print($before . get_the_title() . $after);
    } elseif (is_page() && $post->post_parent) {
        $parent_id = $post->post_parent;
        $breadcrumbs = array();
        while ($parent_id) {
            $page = get_page($parent_id);
            $breadcrumbs[] = '<li><a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a></li>';
            $parent_id = $page->post_parent;
        }
        $breadcrumbs = array_reverse($breadcrumbs);
        for ($i = 0; $i < count($breadcrumbs); $i++) {
            print($breadcrumbs[$i]);
            if ($i != count($breadcrumbs) - 1)
                echo ' ' . $delimiter . ' ';
        }
        if ($showCurrent == 1)
            echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
    } elseif (is_tag()) {
        print($before . esc_html__('Posts tagged "', "fitfab") . single_tag_title('', false) . '"' . $after);
    } elseif (is_author()) {
        global $author;
        $userdata = get_userdata($author);
         print($before . esc_html__('Articles posted by ', "fitfab") . $userdata->display_name . $after);
    } elseif (is_404()) {
         print($before . esc_html__('Error 404', "fitfab") . $after);
    }

    echo '</ol>';
}
