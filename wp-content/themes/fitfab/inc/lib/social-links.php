<?php
/**
 * FitFab social link
 *
 * @package     fitfab
 * @subpackage fitfab.inc.lib
 * @since fitfab 1.0.0
 */

add_action("fitfab_social_links", "fitfab_social_link");

function fitfab_social_link($ul_class){
    global $fitfab_data;
    
    $aSocial=array(
        "Facebook" => array("class"=> "facebook","link"=> $fitfab_data['facebook']),
    	"Twitter" => array("class"=> "twitter","link"=> $fitfab_data['twitter']),
    	"Youtube" => array("class"=> "youtube","link"=> $fitfab_data['youtube']),
        "Instagram" => array("class"=> "instagram","link"=> $fitfab_data['instagram']),
    	"Rss" => array("class"=> "rss","link"=> $fitfab_data['rss']),
        "Google Plus" => array("class"=> "google-plus","link"=> $fitfab_data['gplus']),
        "Linkedin" => array("class"=> "linkedin","link"=> $fitfab_data['linkedin']),
        "Pinterest" => array("class"=> "pinterest","link"=> $fitfab_data['pinterest']),
        "Dribble" => array("class"=> "dribble","link"=> $fitfab_data['dribble']),
        "Vimeo" => array("class"=> "vimeo","link"=> $fitfab_data['vimeo']),
    );
    
    $show_socials = $fitfab_data['fitfab_displaysocial'];
    if(isset($show_socials['SHOW'])):
        ?><ul class="<?php echo esc_attr($ul_class); ?>"><?php
        foreach($show_socials['SHOW'] as $val):
            if(array_key_exists($val, $aSocial) && !empty($aSocial[$val]["link"])):
                ?><li><a href="<?php echo esc_url($aSocial[$val]["link"]); ?>" title="<?php echo esc_attr($val); ?>" target="_blank"><i class="fa fa-<?php echo esc_attr($aSocial[$val]["class"]); ?>"></i></a></li><?php
            endif;
        endforeach;
        ?></ul><?php
    endif;
}
