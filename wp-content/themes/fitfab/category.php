<?php
/**
 * Fitfab - Category
 * 
 * This template is contains posts category layout 
 *
 * @package fitfab
 * @subpackage fitfab
 * @since fitfab 1.0.0
 */
get_header();
?> 
<div id="content">
       <section class="main-blog-content">
        <div class="container">
            <div class="heading-wrap">
                <h1 class="h1"> <?php echo sprintf(esc_html__('Category Archives: %s', 'fitfab'), single_cat_title('', false)); ?> </h1>
            </div>
            <div class="row">
                <?php do_action('fitfab_blog_layout'); ?>
            </div>
        </div>
    </section>
</div>
    <?php get_footer(); ?> 
