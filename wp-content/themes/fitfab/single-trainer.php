<?php
/**
 * Fitfab : This template is contains trainer layout 
 *
 * @package fitfab
 * @subpackage fitfab
 * @since fitfab 1.0.0
 */
get_header();
?>
<!--Content Area Start-->
<div id="content">
    <?php do_action("fitfab_banner_parallax", array("title" => get_the_title())); ?>
    <section class="trainer-detail">
        <div class="container">
            <?php
            while (have_posts()) : the_post();
                $part = "content/single/";
                get_template_part($part.'single', 'trainer');
                
               // trainer classes
                get_template_part($part.'trainer', 'class');
                ?>
                
            <?php endwhile; ?>
        </div>
    </section>
</div>
<!--Content Area End-->
<?php
get_footer();
