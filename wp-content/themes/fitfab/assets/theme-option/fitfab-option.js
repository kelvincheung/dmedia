/**
 * Fitfab - Style Switcher
 *
 * @package fitfab
 * @subpackage fitfab.assets.theme-option
 * @since fitfab 1.0.0
 */
(function($) {
    "use strict";
    
    less.modifyVars({
	skinColor: FITFAB_OPTIONS.color,
	fontFamily: FITFAB_OPTIONS.font,
	secondaryColor: FITFAB_OPTIONS.Secondary_color
    });
    // Header style
    var headerHeight = $('#header').outerHeight();
    var st = $(window).scrollTop();
    var stickOnScroll = function() {
	if (FITFAB_OPTIONS.header == "intelligent") {

	    $('#header').removeClass('normal');
	    $('#slider').addClass('top');
	    $('#header').addClass('intelligent');
	    var pos = $(window).scrollTop();

	    if (pos > headerHeight) {
		if (pos > st) {
		    $('#header').addClass('simple')
		    $('#header.simple').removeClass('down')
		    $('#header.simple').addClass('fixed up')

		} else {
		    $('#header.simple').removeClass('up')
		    $('#header.simple').addClass('fixed down')

		}
		st = pos;

	    } else {
		$('#header.simple').removeClass('fixed down up simple')
	    }
	    if (pos == $(document).height() - $(window).height()) {
		$('#header.simple').removeClass('up')
		$('#header.simple').addClass('fixed down')
	    }

	} else if (FITFAB_OPTIONS.header == "fix") {

	    $('#slider').removeClass('top')
	    $('#header').addClass('simple fixed')
	    $('#header').removeClass('down up')
	    $('#header').removeClass('intelligent');
	    $('#wrapper').css({
		paddingTop: 0
	    })
	}
	else {

	    $('#header.simple').removeClass('fixed down up simple')
	    $('#header').addClass('normal');
	    $('#slider').removeClass('top');
	    $('#header').removeClass('intelligent');
	    $('#wrapper').css({
		paddingTop: 0
	    })
	}
    }

    stickOnScroll()
    $(window).scroll(function() {
	stickOnScroll()
    });
    // end for sticky header

    // Theme Layout
    var layoutStyle = function(layout) {
	if (layout == 'full-width') {
	    $('#wrapper').removeClass('boxed')
	} else {
	    $('#wrapper').addClass('boxed')
	}
    }

    layoutStyle(FITFAB_OPTIONS.layout);

})(jQuery);

