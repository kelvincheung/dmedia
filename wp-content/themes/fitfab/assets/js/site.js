(function($) {
    "use strict";

    /**
     * Fitfab - Comment Form
     * @since fitfab 1.0.0
     */
    if ($("#commentform").length > 0) {


	$("#commentform").submit(function() {
	    var $cmtFlag = true;
	    $("#email, #comment ,#author").removeClass('fitfab-required');
	    if ($('#email').length > 0) {
		var $emailRgx = /^([a-zA-Z.0-9])+@([a-zA_Z0-9])+\.([a-zA-Z])/;
		var $cmauthor = $("#author").val();
		var $cmemail = $("#email").val();
		if ($cmauthor == "") {
		    $("#author").addClass('fitfab-required');
		    $cmtFlag = false;
		}
		if ($cmemail == "" || !$emailRgx.test($cmemail)) {
		    $("#email").addClass('fitfab-required');
		    $cmtFlag = false;
		}
	    }
	    var $cmmsg = $("#comment").val();
	    if ($cmmsg == "") {
		$("#comment").addClass('fitfab-required');
		$cmtFlag = false;
	    }
	    return $cmtFlag;
	});


    }

    /**
     *Fitfab - Search Form
     *@since  fitfab 1.0.0
     */

    if ($("form.search-form").length > 0) {
	$("form.search-form").submit(function() {
	    var $serchFlag = true;
	    $("form.search-form input[name=s]").removeClass('fitfab-required');
	    var $serchmsg = $("form.search-form input[name=s]").val();
	    if ($serchmsg == "") {
		$("form.search-form input[name=s]").addClass('fitfab-required');
		$serchFlag = false;
	    }
	    return $serchFlag;
	});
    }


    if ($(".fitfab_widget_search form").length > 0) {
	$(".fitfab_widget_search form").submit(function() {
	    var $serchFlag = true;
	    $(".fitfab_widget_search form input[name=s]").removeClass('fitfab-required');
	    var $serchmsg = $(".fitfab_widget_search form input[name=s]").val();
	    if ($serchmsg == "") {
		$(".fitfab_widget_search form input[name=s]").addClass('fitfab-required');
		$serchFlag = false;
	    }
	    return $serchFlag;
	});
    }
    var induCountdown = new Date(FITFAB_SITE_OPTION.countdown);
    $("#induCountdown").countdown({
        until: induCountdown,
        format: 'DHMS'
    });
    
    
	
//     Remove Blank "<p>"
    $('p').filter(function() {
	return $.trim($(this).text()) === '' && $(this).children().length == 0;
    }).remove();

    $(".homepage-2 .slider-navigation ol").each(function() {
	$(this).find("li").each(function(index) {
	    $(this)
		    .css("list-style-type", "none")
		    .prepend("<span class='listnumber'>" + (index + 1) + "</span>");
	})
    });

    $('.homepage-2 .slider-navigation ol li:first-child').addClass('active');

    if ($('#owl-slider1').length) {

	$("#owl-slider1").owlCarousel({
	    autoPlay: 3000, //Set AutoPlay to 3 seconds

	    items: 2,
	    itemsDesktop: [1199, 2],
	    itemsDesktopSmall: [979, 2],
	    itemsMobile: [600, 1]

	});
    }
    if ($('#owl-slider2').length) {

	$("#owl-slider2").owlCarousel({
	    autoPlay: 3000, //Set AutoPlay to 3 seconds
	    singleItem: true

	});
    }
    if ($('#owl-sucess-story').length) {

	$("#owl-sucess-story").owlCarousel({
	    autoPlay: 3000, //Set AutoPlay to 3 seconds
	    singleItem: true,
	});
    }


    if ($('#owl-slider-hero').length) {

	$("#owl-slider-hero").owlCarousel({
	    navigation: false, // Show next and prev buttons
	    slideSpeed: 800,
	    paginationSpeed: 400,
	    autoPlay: 8000,
	    singleItem: true

	});

	$('.slider-navigation').find('li').on('click', function() {
	    $('.slider-navigation li').removeClass('active')
	    $(this).addClass('active');

	    var a = $(this).index();
	    $("#owl-slider-hero").trigger('owl.goTo', a)
	});

	setInterval(function() {
	    var i = $('#owl-slider-hero .owl-pagination .owl-page.active ').index();
	    $('.slider-navigation li').removeClass('active')
	    $('.slider-navigation li').eq(i).addClass('active');
	}, 1000);

    }
    if ($('#owl-blog').length) {

	$("#owl-blog").owlCarousel({
	    navigation: true, // Show next and prev buttons
	    slideSpeed: 300,
	    paginationSpeed: 400,
	    singleItem: true,
	    pagination: false

	});
    }

    if ($('#owl-schedule').length) {

	$("#owl-schedule").owlCarousel({
	    autoPlay: 3000, //Set AutoPlay to 3 seconds

	    items: 6,
	    itemsDesktop: [1600, 5],
	    itemsDesktopSmall: [1200, 4],
	    itemsMobile: [600, 1]

	});
    }
    if ($('#owl-schedule-two').length) {

	$("#owl-schedule-two").owlCarousel({
	    autoPlay: 3000, //Set AutoPlay to 3 seconds

	    items: 3,
	    itemsDesktop: [1600, 3],
	    itemsDesktopSmall: [1200, 3],
	    itemsMobile: [600, 1]

	});
    }

    if ($('#owl-schedule-home-3').length) {

	$("#owl-schedule-home-3").owlCarousel({
	    autoPlay: 3000, //Set AutoPlay to 3 seconds

	    items: 3,
	    itemsDesktop: [1600, 3],
	    itemsDesktopSmall: [1200, 3],
	    itemsMobile: [600, 1]

	});
    }

    if ($('#owl-trainers').length) {

	$("#owl-trainers").owlCarousel({
	    autoPlay: 3000, //Set AutoPlay to 3 seconds

	    items: 4,
	    itemsDesktop: [1600, 3.5],
	    itemsDesktopSmall: [1200, 3],
	    itemsMobile: [600, 1]

	});
    }

    if ($('#owl-hthree-one').length) {
	$("#owl-hthree-one").owlCarousel({
	    navigation: true, // Show next and prev buttons
	    slideSpeed: 300,
	    paginationSpeed: 400,
	    singleItem: true

	});
    }
    if ($('#owl-blog-details').length) {
	var $owl = $("#owl-blog-details");
	$owl.owlCarousel({
//	    autoPlay: 3000, //Set AutoPlay to 3 seconds
	    navigation: false,
	    pagination: false,
	    items: 2,
	    itemsDesktop: [1600, 2],
	    itemsDesktopSmall: [1200, 2],
	    itemsMobile: [600, 1],
	    afterAction: afterAction

	});
    }
    function afterAction() {
	var $prevTitle = '';
	var $nexTitle = '';
	var $nexHref = '';
	var $previousItemIndex = parseInt(this.owl.currentItem) - 1;
	$previousItemIndex = ($previousItemIndex >= 0) ? $previousItemIndex : -1;

	if ($previousItemIndex != -1) {
	    $prevTitle = $('#owl-blog-details .owl-wrapper').children('.owl-item').eq($previousItemIndex).children('.item').data('title');
	    $('.blog-related-post .prev h3 a').html($prevTitle);
	    $nexHref = $('#owl-blog-details .owl-wrapper').children('.owl-item').eq($previousItemIndex).children('.item').data('href');
	    $('.blog-related-post .prev h3 a').attr('href', $nexHref);
	} else {
	    $('.blog-related-post .prev h3 a').html('');
	    $('.blog-related-post .prev h3 a').attr('href', 'javascript:void(0);');
	}

	var $nextItemInex = parseInt(this.owl.currentItem) + parseInt(this.orignalItems);
	$nextItemInex = ($nextItemInex < this.owl.owlItems.length) ? $nextItemInex : -1;
	console.log('next: ' + $nextItemInex);
	if ($nextItemInex != -1) {
	    $nexTitle = $('#owl-blog-details .owl-wrapper').children('.owl-item').eq($nextItemInex).children('.item').data('title');
	    $('.blog-related-post .next h3 a').html($nexTitle);
	    $nexHref = $('#owl-blog-details .owl-wrapper').children('.owl-item').eq($nextItemInex).children('.item').data('href');
	    $('.blog-related-post .next h3 a').attr('href', $nexHref);
	} else {
	    $('.blog-related-post .next h3 a').html('');
	    $('.blog-related-post .next h3 a').attr('href', 'javascript:void(0);');
	}
    }

    $(".blog-related-post .prev .prev-link").click(function() {
	$owl.trigger('owl.next');
    })
    $(".blog-related-post .next .next-link").click(function() {
	$owl.trigger('owl.prev');
    })

    $('#header .nav ,.pagination li ').find('li').on('click', function() {
	$(this).siblings('li').removeClass('active');
	$(this).addClass('active');
    });

    $('.open-info').on('click', function(e) {
	e.preventDefault();
	$(this).parents('.item').addClass('open_slide');
    })

    $('.cross').on('click', function(e) {
	e.preventDefault();
	$(this).parents('.item').removeClass('open_slide');
    })
    //==============Map Function
    if (jQuery('#custom_map').length) {

	var initialize = function() {
	    //function initialize() {
	    var pos = new google.maps.LatLng(FITFAB_SITE_OPTION.lat, FITFAB_SITE_OPTION.lng);

	    var mapProp = {
		center: pos,
		zoom: 10,
		scrollwheel: false,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	    };

	    var infowindow = new google.maps.InfoWindow({
		position: pos,
		content: '<div id="info">' + '<span class="head">' + FITFAB_SITE_OPTION.address + '</span>' + ' <a href="tel:' + FITFAB_SITE_OPTION.phone + '" >' + FITFAB_SITE_OPTION.phone + '</a>' + '<a href="mailto:' + FITFAB_SITE_OPTION.email + '" class="text-mail">' + FITFAB_SITE_OPTION.email + '</a>' + '</div>'

	    });

	    var map = new google.maps.Map(document.getElementById("custom_map"), mapProp);

	    infowindow.open(map);

	}

	google.maps.event.addDomListener(window, 'load', initialize);

    }
    ;

    $(window).load(function() {
	$('#loading').delay(50).fadeOut();
    });

    //==============Map Function2
    if (jQuery('#custom_mapevent').length) {

	var initialize = function() {
	    //function initialize() {
	    var pos = new google.maps.LatLng(23.001452, 72.574112);

	    var mapProp = {
		center: pos,
		zoom: 10,
		scrollwheel: false,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	    };

	    var marker = new google.maps.Marker({
		position: pos,
		map: map,
		draggable: false,
	    });

	    var map = new google.maps.Map(document.getElementById("custom_mapevent"), mapProp);
	    marker.setMap(map);
	    infowindow.open(map, marker);

	}

	google.maps.event.addDomListener(window, 'load', initialize);

    }
    ;
    $(window).scroll(function() {

	if ($(this).scrollTop() > 10) {
	    $('.header-one').addClass('header-color');
	}
	else {
	    $('.header-one').removeClass('header-color');
	}
    });
    $('.practice-list').parent('.row').parent('.tab-pane').parent('.tab-content').parent('div').addClass('practice-list-main-container');


    $('img.svg').each(function() {
	var $img = $(this);
	var imgID = $img.attr('id');
	var imgClass = $img.attr('class');
	var imgURL = $img.attr('src');

	$.get(imgURL, function(data) {
	    // Get the SVG tag, ignore the rest
	    var $svg = $(data).find('svg');

	    // Add replaced image's ID to the new SVG
	    if (typeof imgID !== 'undefined') {
		$svg = $svg.attr('id', imgID);
	    }
	    // Add replaced image's classes to the new SVG
	    if (typeof imgClass !== 'undefined') {
		$svg = $svg.attr('class', imgClass + ' replaced-svg');
	    }

	    // Remove any invalid XML tags as per http://validator.w3.org
	    $svg = $svg.removeAttr('xmlns:a');

	    // Replace image with new SVG
	    $img.replaceWith($svg);

	}, 'xml');

    });

})(jQuery);

