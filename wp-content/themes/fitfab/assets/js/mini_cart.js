(function($) {
    "use strict";
    
    $('.collapse .navbar-nav').before('<span id="woo-mini-cart" class="icon-basket-loaded"></span>');

	$('#woo-mini-cart').on('click',function(){
		$('.nav_menu_cart').slideToggle();
	});
	
})(jQuery);