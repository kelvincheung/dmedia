/**
 * Fitfab
 * @version fitfab 1.0
 */
(function(a) {

    /**
     * Comment form
     */
    if (a("#commentform").length > 0) {

        a(".blog-program form#commentform").wrap('<div class="comment-entry-box" />');
        a(".blog-program #respond").wrapInner('<div class="comment-wrap-block" />');
        a(".blog-program #commentform p.form-submit #submit-comment").addClass('button-btn submit-btn');
    }
    
    /**
     * Woocommerce js
     */
    a(".woocommerce-ordering").on("change", "select.orderby", function() {
        a(this).closest("form").submit()
    }), a("div.quantity:not(.stock), td.quantity:not(.stock)").addClass("stock clearfix").append('<span class="increment-decrement"><i class="fa fa-plus increment wrap-span "></i> <i class="fa fa-minus decrement wrap-span"></i></span>'), a("input.qty:not(.product-quantity input.qty)").each(function() {
        var b = parseFloat(a(this).attr("min"));
        b && b > 0 && parseFloat(a(this).val()) < b && a(this).val(b)
    }), a(document).on("click", ".fa-plus, .fa-minus", function() {
        var b = a(this).closest(".quantity").find(".qty"), c = parseFloat(b.val()), d = parseFloat(b.attr("max")), e = parseFloat(b.attr("min")), f = b.attr("step");
        c && "" !== c && "NaN" !== c || (c = 0), ("" === d || "NaN" === d) && (d = ""), ("" === e || "NaN" === e) && (e = 0), ("any" === f || "" === f ||
                void 0 === f || "NaN" === parseFloat(f)) && (f = 1), a(this).is(".fa-plus") ? b.val(d && (d == c || c > d) ? d : c + parseFloat(f)) : e && (e == c || e > c) ? b.val(e) : c > 0 && b.val(c - parseFloat(f)), b.trigger("change")
    })
})(jQuery);
