<?php
/**
 * FitFab - header layout
 * 
 * @package fitfab
 * @subpackage fitfab
 * @since fitfab 1.0.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>
        <!--Page Wrapper Start-->
        <div id="wrapper" <?php do_action("fitfab_wrap_class"); ?>>
            <!--header Section Start Here -->
            <?php do_action("fitfab_header_layout"); ?>
            <!--header Section Ends Here -->
<?php 