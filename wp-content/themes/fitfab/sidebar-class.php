<?php 
/**
 * FitFab - sidebar class
 * 
 * @package fitfab
 * @subpackage fitfab
 * @since fitfab 1.0.0
 */
 ?>
<div class="col-xs-12 col-sm-4 class-aside-wrap">
    <?php
    if (is_active_sidebar('fitfab-sidebar-class')) :
        dynamic_sidebar('fitfab-sidebar-class');
    endif;
    ?>
</div>