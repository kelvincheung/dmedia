<?php
/**
 * Fitfab : Template part for displaying event.
 *
 * @package fitfab
 * @subpackage fitfab.content
 * @since fitfab 1.0.0
 */
global $post,$fitfab_single_event;
$EM_Event = em_get_event($post->ID, 'post_id');
?>
<div class="row padding">
    <div class="col-xs-12 col-sm-1">
        <div class="event-event clearfix">
            <span class="event-calender-event"> 
                <?php echo esc_html($EM_Event->output('#d')); ?> 
                <span><?php echo esc_html($EM_Event->output('#M')); ?></span> 
            </span>
        </div>

    </div>
    <?php if (has_post_thumbnail()): ?>
    <div class="col-xs-12 col-sm-6 zoom">
        <figure>
            <?php the_post_thumbnail('event'); ?>
        </figure>

    </div>
    <?php endif; ?>
    <div class="col-xs-12 col-sm-5">
        <div class="event-info-event">
            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
            <div class="time-location-event">
                <?php if ($EM_Event->output('#_12HSTARTTIME') && $EM_Event->output('#_12HENDTIME')) : ?>
                    <span><?php echo esc_html($EM_Event->output('#_12HSTARTTIME')); ?> - <?php echo esc_html($EM_Event->output('#_12HENDTIME')); ?></span>
                <?php endif; ?>
                <?php if ($EM_Event->output('#_LOCATIONNAME')) : ?>
                    <span><?php echo esc_html($EM_Event->output('#_LOCATIONNAME')); ?></span>
                <?php endif; ?>
            </div>
                <p>
                    <?php echo esc_html($post->post_excerpt); ?>
                </p>
                <a href="<?php the_permalink(); ?>" class="read-more"><?php esc_html_e("READ MORE", "fitfab"); ?> </a>

        </div>

    </div>

</div>
<?php
