<?php
/**
 * Fitfab : Template part for displaying pricing packages.
 * 
 * @package fitfab
 * @subpackage fitfab.content
 * @since fitfab 1.0.0
 */
global $count;
$terms = get_the_terms($post->ID, 'trainer');
$on_trainer = '';
if ($terms && !is_wp_error($terms)) :
    $trainer = array();
    foreach ($terms as $term) :
        $trainer[] = $term->name;
    endforeach;

    $on_trainer = join(", ", $trainer);
endif;
if ($count % 3 == 0) :
    ?>
    <div class="row space_pad">
    <?php endif; ?>
    <div class="col-xs-12 col-sm-4 package-list zoom <?php echo esc_attr($count); ?>">

        <?php if (has_post_thumbnail()): ?>
            <figure>
                <?php the_post_thumbnail('package'); ?>
            </figure>
        <?php endif; ?>
        <div class="package-info">
            <div class="head-two">
                <h3><?php the_title(); ?></h3>
                <span><?php
                echo rwmb_meta('fitfab_sub_title');    
		?></span>
            </div>
            <?php the_content(); ?>
        </div>
        <?php
	if(function_exists('fitfab_rwmb_meta')){
        $price_per_month = fitfab_rwmb_meta('fitfab_price_per_month');
        $join_button_text = fitfab_rwmb_meta('fitfab_join_button_text');
        $join_button_link = fitfab_rwmb_meta('fitfab_join_button_link') ? fitfab_rwmb_meta('fitfab_join_button_link') : '#';
        if(!empty($price_per_month)) :
             global $fitfab_data;
        ?>
        <div class="package-price">
            <span><?php echo class_exists( 'WooCommerce' )?get_woocommerce_currency_symbol($fitfab_data["fitfab_currency"]):$fitfab_data["fitfab_currency"]; ?><?php echo esc_html($price_per_month); esc_html_e('*/M','fitfab'); ?></span>
            <?php if (!empty($join_button_text)) : ?>
                <a href="<?php echo esc_url($join_button_link); ?>" class="button-btn"><?php echo esc_html($join_button_text); ?></a>
            <?php endif; ?>
        </div>
        <?php endif; } ?>
    </div>
    <?php
    $count++;
    if ($count % 3 == 0) :
        ?>
    </div>
<?php endif;