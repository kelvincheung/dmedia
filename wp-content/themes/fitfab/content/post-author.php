<?php
/**
 * Fitfab - author
 *
 * @package fitfab
 * @subpackage fitfab.content
 * @since fitfab 1.0.0
 */
?>
<div class="blog-detail-comment fit-fab-author-details">
    <?php if(!is_author()) : ?>
            <h2 class="bh2"><?php esc_html_e('Author post', 'fitfab'); ?></h2>
            <?php endif; ?>
    <div class="author-comment clearfix">
        <figure>
            <?php if (function_exists('get_avatar')) {
            echo get_avatar(get_the_author_meta('email'), '168');
        } ?>
        </figure>
        <div class="comment-info">
            <h3><?php the_author_meta('display_name'); ?> <span><?php the_date() ?></span></h3>
            <p><?php the_author_meta('description') ?></p>
            <span><?php esc_html_e('view all post by','fitfab'); ?> <span><?php the_author_posts_link(); ?></span></span>
        </div>
    </div>
</div>
<?php
