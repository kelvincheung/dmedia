<?php
/**
 * Fitfab : Template part for displaying posts.
 *
 * @package fitfab
 * @subpackage fitfab.content.format
 * @since fitfab 1.0.0
 */
global $fitfab_page;
    ?>
    <div id="post-<?php the_ID(); ?>" <?php post_class('blog-program'); ?>>
        <blockquote>
            <?php the_content(); ?>   
        </blockquote>
        <?php 
        if ($fitfab_page == 'single-page') :

        elseif (is_search()):
          ?><p><?php the_excerpt(); ?></p><?php
        else:
          the_content();
          wp_link_pages();
	  $post_content = get_post_field('post_content', get_the_ID());
            if (1 < strpos($post_content, '<!--more-->') && !is_single()) {
                ?>
                <a href="<?php the_permalink(); ?>" class="blog-sidebar-read-btn"><?php echo esc_html_e('read more', 'fitfab'); ?></a>
            <?php } 
        endif;
        ?>
        <span> <a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID'))); ?>">- <?php the_author(); ?></a> </span>
    </div>

<?php 
