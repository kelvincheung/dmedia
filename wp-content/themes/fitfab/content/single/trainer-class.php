<?php
/**
 * Fitfab : trainer classes
 *
 * @package fitfab
 * @subpackage fitfab.content.single
 * @since fitfab 1.0.0
 */
$trainer_type = get_the_title();
?>
<div class = "row yoga-class-wrap top-space">
    <div class = "col-xs-12">
        <div class = "head-global family-oswald">
            <h2 class = "h2">
                <?php esc_html_e('Classes by', 'fitfab'); ?> 
                <?php if (!empty($trainer_type)) : echo esc_html($trainer_type);
                endif; ?>
            </h2>

        </div>
    </div>
    <?php
    $args = array(
        'meta_key' => 'fitfab_trainer',
        'meta_value' => get_the_title(),
        'post_type' => 'class',
        'post_status' => 'publish',
    );
    $trainer_query = new WP_Query($args);
    while ($trainer_query->have_posts()) : $trainer_query->the_post();
        ?>
        <div class="col-xs-12 col-sm-4 classes-listing-wrap zoom">
                <?php if (has_post_thumbnail()): ?>
                <figure>
                    <?php
                    the_post_thumbnail('class');
                    $start_time = date('hA', strtotime(get_post_meta($post->ID, 'fitfab_class_start_time', TRUE)));
                    $end_time = date('hA', strtotime(get_post_meta($post->ID, 'fitfab_class_end_time', TRUE)));
                    if ($start_time) :
                        ?>
                        <span><?php echo esc_html($start_time); ?> - <?php echo esc_html($end_time); ?></span>
                <?php endif; ?>
                </figure>
    <?php endif; ?>
            <div class="classes-content">
                <h3><?php the_title(); ?></h3>
                <span><?php
                    if (!empty($trainer)) :
                        echo esc_html_e('WITH ', 'fitfab');
                        echo esc_html($trainer);
                    endif;
                    ?>
                </span>
            </div>
            <p>
    <?php echo fitfab_truncate_content(get_the_content(), 85); ?>   
            </p>
            <a href="<?php the_permalink(); ?>" class="link"><?php esc_html_e('read more', 'fitfab'); ?></a>
        </div>
        <?php
    endwhile;
    wp_reset_postdata();
    ?>           
</div>