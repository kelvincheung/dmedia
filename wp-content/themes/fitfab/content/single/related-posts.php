<?php
/**
 * Fitfab : Template part for related post.
 *
 * @package fitfab
 * @subpackage fitfab.content.single
 * @since fitfab 1.0.0
 */
$related_count = get_theme_mod('related_posts_count', '5');
$categories = get_the_category($post->ID);

$args = array();

if ($categories) :
    $category_ids = array();
    foreach ($categories as $individual_category)
	$category_ids [] = $individual_category->term_id;
    $args = array(
	'category__in' => $category_ids,
	'post__not_in' => array(
	    $post->ID
	),
	'posts_per_page' => $related_count,
	'ignore_sticky_posts' => 1
    );
endif;
$fitfab_relatedPost = new wp_query($args);
if ($fitfab_relatedPost->have_posts()) :
    ?>
    <div class="blog-related-post">
        <h2><?php esc_html_e('related posts', 'fitfab'); ?></h2>
        <div id="owl-blog-details">
	    <?php
	    while ($fitfab_relatedPost->have_posts()) :
		$fitfab_relatedPost->the_post();
		if (has_post_thumbnail()):
		    ?>
	    	<div class="item" data-href="<?php the_permalink(); ?>" data-title="<?php the_title(); ?>">
	    	    <div class="post-info">
	    		<a href="<?php the_permalink(); ?>">
	    		    <img alt="blog-detail" src="<?php echo fitfab_resize(wp_get_attachment_url(get_post_thumbnail_id()), 360, 170); ?>">
	    		</a>
	    		<div class="slider-content-blog">
	    		    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
	    		    <span>&#10077;<?php
				    echo esc_html_e('in ', 'fitfab');
				    $categories = get_the_category();
				    $separator = ', ';
				    $output = '';
				    if (!empty($categories)) {
					foreach ($categories as $category) {
					    $output .= esc_html($category->name) . $separator;
					}
					echo trim($output, $separator);
				    }
				    ?>
	    			&#10078;</span>
	    		</div>
	    	    </div>
	    	</div>
		    <?php
		endif;
	    endwhile;
	    ?>
        </div>
        <div class="prev-next-title clearfix">
    	<div class="prev">
	    <a class="prev-link" href="javascript: void(0);"><?php esc_html_e('previous', 'fitfab'); ?></a>
    	    <h3><a href="javascript: void(0);"><?php esc_html_e('heading title here', 'fitfab'); ?></a></h3>
    	</div>
    	<div class="next">
    	    <a class="next-link" href="javascript: void(0);"><?php esc_html_e('next', 'fitfab'); ?></a>
    	    <h3 class="h"><a href="javascript: void(0);"><?php esc_html_e('heading title here', 'fitfab'); ?></a></h3>
    	</div>
        </div>
    </div>
    <?php
endif;
wp_reset_postdata();
