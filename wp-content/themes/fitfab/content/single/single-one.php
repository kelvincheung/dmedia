<?php
/**
 * Fitfab - blog singlelayout
 * 
 * @package fitfab
 * @subpackage fitfab.content.single
 * @since fitfab 1.0.0
 */
global $fitfab_page;
$fitfab_page = 'single-page';
?>
<div class="col-xs-12 col-sm-8">
    <div class="blog-program">
	<?php
	if (have_posts()) :
	    while (have_posts()) : the_post();
		$format = (get_post_format()) ? get_post_format() : 'standard';
		get_template_part('content/format/' . $format);
	    endwhile;
	else :
	    get_template_part('content/none');
	endif;
	// Related Posts
	get_template_part('content/single/related', 'posts');
	?>
        <div class="comment-blog-details">
	    <?php
	    get_template_part('content/post', 'author');
		if (comments_open() || get_comments_number()) :
		    comments_template();
		endif;
	    ?>
	</div>
    </div>
</div>
<?php get_sidebar(); ?>
  