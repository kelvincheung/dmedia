<?php
/**
 * FitFab : Header Layout Four
 *
 * @package fitfab
 * @subpackage fitfab.content.header
 * @since fitfab 1.0.0
 */
global $fitfab_data;
?>
<!--Header Section Start-->
<header id="header" class="header-four">
    <div class="container">
        <div class="row">
            <div class="col-xs-6 col-sm-4">
                <?php
                $fitfab_logo = (!empty($fitfab_data['fitfab_logo']['url'])) ? $fitfab_data['fitfab_logo']['url'] : FITFAB_THEME_URL . '/assets/images/home-2_logo.png';
                $fitfab_logo_text = (!empty($fitfab_data['fitfab_logo_text'])) ? $fitfab_data['fitfab_logo_text'] : '';
                ?>
                <a href="<?php echo esc_url(home_url('/')); ?>" class="logo"> <img src="<?php echo esc_url($fitfab_logo); ?>" alt="<?php bloginfo('name'); ?>" /><span><?php echo esc_html($fitfab_logo_text); ?></span></a>
                <div class="calling-contact">
                    <?php
                    $link_callus = (!empty($fitfab_data['fitfab_phone_link'])) ? $fitfab_data['fitfab_phone_link'] : '#';
                    ?>
                    <?php if(!empty($fitfab_data['fitfab_call_us_text'])) : ?>
                    <span><?php echo esc_html($fitfab_data['fitfab_call_us_text']); ?></span>
                    <?php endif; ?>
                    <?php if(!empty($fitfab_data['fitfab_phone_no'])) : ?>
                    <a href="<?php echo esc_url($link_callus); ?>"><?php echo esc_html($fitfab_data['fitfab_phone_no']); ?></a>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-xs-6 col-sm-8 pull-right mobile-static">
                <nav class="navbar">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only"><?php esc_html_e('Toggle navigation', 'fitfab'); ?></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <?php do_action('fitfav_nav_menu'); do_action('fitfab_nav_menu_cart'); ?>
                    </div><!-- /.navbar-collapse -->
                </nav>
            </div>
        </div>
    </div>
</header>
<!--Header Section End-->
<?php
