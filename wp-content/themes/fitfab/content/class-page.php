<?php
/**
 * Fitfab : Template part for displaying classes.
 *
 * @package fitfab
 * @subpackage fitfab.content
 * @since fitfab 1.0.0
 */
global $class_detail;
$trainer = '';
if (function_exists('fitfab_rwmb_meta')) {
    $fitfab_trainer = fitfab_rwmb_meta('fitfab_trainer', 'type=checkbox_list');
    if (!empty($fitfab_trainer)) {
	$trainer = implode(', ', $fitfab_trainer);
    }
}
if ($class_detail == 'class-detail') :
    if (has_post_thumbnail()):
	$url = wp_get_attachment_url(get_post_thumbnail_id());
	?>
	<div class="zoom">
	    <figure class="class-thumbnail">
		<img src="<?php echo esc_url(fitfab_resize($url, '750', '360')); ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" />
	    </figure>
	</div>
    <?php endif; ?>
    <div class="head-global family-oswald">
        <h2 class="h2"><?php the_title(); ?></h2>
	<?php if (!empty($trainer)) : ?>
	    <span><?php esc_html_e('Trainer', 'fitfab') ?> :  <?php echo esc_html($trainer); ?> </span>
	<?php endif; ?>
    </div>
    <div class="single-class-content">
	<?php
	the_content();
	?>
    </div>
    <div class="btn-media-block">
	<?php
	if (function_exists('fitfab_rwmb_meta')) {
	    $join_button_text = fitfab_rwmb_meta('fitfab_join_button_text');
	    $join_button_link = fitfab_rwmb_meta('fitfab_join_button_link');
	    $join_button_link = ($join_button_link) ? $join_button_link : '#';
	    if (!empty($join_button_text)) :
		?>
	        <a href="<?php echo esc_url($join_button_link); ?>" class="button-btn medium-btn"> <?php echo esc_html($join_button_text); ?></a>
	    <?php
	    endif;
	}
	?>
        <div class="media-class">
    	<strong><?php esc_html_e('SHARE IT', 'fitfab'); ?></strong>
    	<ul class="media-wrap1 clearfix">
		<?php
		$facebook = 'http://www.facebook.com/sharer/sharer.php?u=' . get_permalink() . '&title=' . get_the_title();
		$twitter = 'http://twitter.com/intent/tweet?status=' . get_the_title() . '+' . get_permalink();
		$google = 'https://plus.google.com/share?url=' . get_permalink();
		?>
    	    <li>

    		<a href="<?php echo esc_url($facebook); ?>" class="facebook"><i class="fa fa-facebook"></i></a>
    	    </li>
    	    <li>
    		<a href="<?php echo esc_url($twitter); ?>" class="facebook"><i class="fa fa-twitter"></i></a>
    	    </li>

    	    <li>
    		<a href="<?php echo esc_url($google); ?>" class="facebook"><i class="fa fa-google-plus"></i></a>
    	    </li>

    	</ul>
        </div>
    </div>
    <?php
else :
    ?>
    <div class="col-xs-12 col-sm-4 classes-listing-wrap zoom">
	    <?php if (has_post_thumbnail()): ?>
	    <figure>
		<?php
		the_post_thumbnail('class');
		$start_time = date('hA', strtotime(get_post_meta(get_the_ID(), 'fitfab_class_start_time', TRUE)));
		$end_time = date('hA', strtotime(get_post_meta(get_the_ID(), 'fitfab_class_end_time', TRUE)));
		if ($start_time) :
		    ?>
	    	<span><?php echo esc_html($start_time); ?> - <?php echo esc_html($end_time); ?></span>
	    <?php endif; ?>
	    </figure>
    <?php endif; ?>
        <div class="classes-content">
    	<h3><?php the_title(); ?></h3>
    	<span><?php
		if (!empty($trainer)) :
		    echo esc_html_e('WITH ', 'fitfab');
		    echo esc_html($trainer);
		endif;
		?>
    	</span>
        </div>
        <p>
    <?php echo fitfab_truncate_content(get_the_content(), 85); ?>   
        </p>
        <a href="<?php the_permalink(); ?>" class="link"><?php esc_html_e('read more', 'fitfab'); ?></a>
    </div>
<?php
endif;
