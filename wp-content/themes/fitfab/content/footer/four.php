<?php
/**
 * FitFab : Footer Layout Four
 *
 * @package fitfab
 * @subpackage fitfab.content.footer
 * @since fitfab 1.0.0
 */
?>
<!--Footer Section Start-->
<footer id="footer" class="footer-one footer-four">
    <div class="container">
        <div class="primary-footer">
            <div class="row">
            <?php if (is_active_sidebar('fitfab-footer-1')) :?>
                <div class="col-xs-12 col-sm-6 col-md-3 footer-logo footer-l">
                    <?php
                    global $fitfab_data;
                    $fitfab_logo = (!empty($fitfab_data['fitfab_footer_logo']['url'])) ? $fitfab_data['fitfab_footer_logo']['url'] : FITFAB_THEME_URL . '/assets/images/home-1_logo.png';
                    ?>
                    <a href="<?php echo esc_url(home_url('/')); ?>"> <img alt="footer-logo" src="<?php echo esc_url($fitfab_logo); ?>"></a>
                    <?php dynamic_sidebar('fitfab-footer-1'); ?>
                </div>
             <?php endif;?>

             <?php if (is_active_sidebar('fitfab-footer-2')) : ?>
                <div class="col-xs-12 col-sm-6 col-md-4 class-time footer-l">
                    <?php dynamic_sidebar('fitfab-footer-2'); ?>
                </div>
              <?php endif;?>
              
              <?php if (is_active_sidebar('fitfab-footer-3')) : ?>
                <div class="col-xs-12 col-sm-6 col-md-3 class-time timing-box footer-l">
                    <?php dynamic_sidebar('fitfab-footer-3'); ?>
                </div>
              <?php endif;?>

              <?php if (is_active_sidebar('fitfab-footer-4')) : ?>
                <div class="col-xs-12 col-sm-6 col-md-2 footer-l">
                    <?php $fitfab_boxing_img = (!empty($fitfab_data['fitfab_boxing_img']['url'])) ? 'background:url(' . $fitfab_data['fitfab_boxing_img']['url'] . ') 0 0 no-repeat' : ''; ?>
		        <div class="boxing-inner" style="<?php echo esc_attr($fitfab_boxing_img); ?>">
                        <?php dynamic_sidebar('fitfab-footer-4'); ?>
                    </div>
                </div>
              <?php  endif; ?>

            </div>
        </div>

        <div class="bottom-footer">
            <div class="row">
                <div class="col xs-12 col-sm-6 col-lg-7 copy-right">
                    <p>
                        <?php $copyright = (!empty($fitfab_data['fitfab_copyrighttext'])) ? $fitfab_data['fitfab_copyrighttext'] : ''; ?>
                        <?php if (!empty($fitfab_data['fitfab_copyrighttext'])) : ?>
                            <?php print($fitfab_data['fitfab_copyrighttext']); ?>
                        <?php endif; ?>
                        <?php
                        $designlink = (!empty($fitfab_data['fitfab_themedesignlink'])) ? $fitfab_data['fitfab_themedesignlink'] : '#';
                        if (!empty($fitfab_data['fitfab_themedesign'])) :
                            ?>
                            <?php esc_html_e('Design by', 'fitfab'); ?> <a href="<?php echo esc_url($designlink); ?>" ><?php echo esc_html($fitfab_data['fitfab_themedesign']); ?></a>
                        <?php endif; ?>
                    </p>
                </div>
                <div class="col-xs-12 col-sm-6 col-lg-5">
                    <div class="calling-contact">
                        <?php
                        $link_callus = (!empty($fitfab_data['fitfab_phone_link'])) ? $fitfab_data['fitfab_phone_link'] : '#';
                        ?>
                        <?php if (!empty($fitfab_data['fitfab_call_us_text'])) : ?>
                            <span><?php echo esc_html($fitfab_data['fitfab_call_us_text']); ?></span>
                        <?php endif; ?>
                        <?php if (!empty($fitfab_data['fitfab_phone_no'])) : ?>
                            <a href="<?php echo esc_url($link_callus); ?>"><?php echo esc_html($fitfab_data['fitfab_phone_no']); ?></a>
                        <?php endif; ?>
                    </div>

                    <?php echo do_action('fitfab_social_links', 'media-wrap clearfix'); ?>

                </div>
            </div>
        </div>

    </div>

</footer>
<!--Footer Section End-->
<?php
