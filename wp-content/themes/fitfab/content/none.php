<?php
/**
 * Fitfab - The template part for displaying a message that posts cannot be found.
 *
 * @package fitfab
 * @subpackage fitfab.content
 * @since fitfab 1.0.0
 */
?>
<section class="no-results not-found">
	<header class="page-header">
		<h1 class="page-title"><?php esc_html_e( 'Nothing Found', 'fitfab' ); ?></h1>
	</header>
	<!-- .page-header -->
	<div class="page-content">
		<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>
			<p><?php printf(wp_kses('Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'fitfab'), admin_url('post-new.php')); ?></p>
		<?php elseif ( is_search() ) : ?>
			<p><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'fitfab' ); ?></p>
		<?php else : ?>
			<p><?php esc_html_e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'fitfab' ); ?></p>
			<?php get_search_form(); ?>
		<?php endif; ?>
	</div>
	<!-- .page-content -->
</section>
<?php 
