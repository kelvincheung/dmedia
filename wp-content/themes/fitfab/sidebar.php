<?php
/**
 * FitFab - blog sidebar
 * 
 * @package     fitfab
 * @subpackage fitfab 
 * @since   fitfab  1.0.0
 */
if (!is_active_sidebar('fitfab-sidebar-1')) {
    return;
}
?>
<div class="col-xs-12 col-sm-4">
    <?php dynamic_sidebar('fitfab-sidebar-1'); ?>
</div>
<?php

