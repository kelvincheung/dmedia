<?php

/**
 * FitFab - Functions
 * 
 * This file has collection of fitfab theme required functinality  
 *
 * @package fitfab
 * @subpackage fitfab
 * @since fitfab 1.0.0
 */
define('FITFAB_THEME_DIR', get_template_directory());
define('FITFAB_THEME_URL', get_template_directory_uri());

/**
 * Fitfab - listing of core files
 * @since fitfab 1.0.0 
 */
$fitfab_loading = array(
    'inc/theme-setup.php',
    'vendor/vendor.php',
    'inc/login-form.php',
    'inc/enqueue-scripts.php',
    'inc/helper-function.php',
    'inc/comment-list.php',
    'inc/post-format-hooks.php', // enqueuescript lib
    'inc/layout.php',
    'inc/woocommerce/woocommerce.php',
    'inc/lib/breadcrumb.php',
    'inc/lib/pagination.php',
    'inc/lib/image-resize.php',
    'inc/lib/social-links.php',
    'inc/widgets/widgets.php',
);
fitfab_loader($fitfab_loading);

/**
 * Fitfab Plus - Loader File
 * 
 * @param array $files
 * @return void
 * @since fitfab 1.0.0 
 */
function fitfab_loader($files) {
    foreach ($files as $file) {
        fitfab_inclusion($file);
    }
}

/**
 * FitFab - Visual Composer
 * @since fitfab 1.0.0
 */
if (defined('WPB_VC_VERSION')) {
    fitfab_inclusion('vc_templates/vc-map/vc-map.php');
}
/**
 * FitFab - Category Meta
 * @since fitfab 1.0.0
 */

function fitfab_inclusion($path, $require = true, $once = true, $return=false){
	  $file_path = locate_template($path);
    if(empty($file_path)) {
        $hit_theme = wp_get_theme();
        $html='<h2>'.esc_html__('ERROR:', 'fitfab').' </h2>';
        $html.='<p>"'.$hit_theme->stylesheet.'/'.$path.'" file not exists.</p>';
        wp_die($html, esc_html__('File not exists', 'fitfab')); 
    };
	
	//default: require_once
	if($once){
		if($require){
			require_once $file_path;// trailingslashit( get_template_directory() ).$path;
		}
		else{
			include_once $file_path;// trailingslashit( get_template_directory() ).$path;
		}
	}
	else{
		if($require){
			require $file_path;// trailingslashit( get_template_directory() ).$path;
		}
		else{
			if($return){
				return include $file_path;// trailingslashit( get_template_directory() ).$path;
					
			}
			else{
				include $file_path;// trailingslashit( get_template_directory() ).$path;
			}
				
		}
	}

}