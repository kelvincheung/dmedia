<?php

/**
 * Fitfab - Vendor 
 * 
 * @package fitfab
 * @since fitfab 1.0.0
 */

if (!class_exists('Redux')) :
fitfab_inclusion('vendor/ReduxCore/framework.php');
fitfab_inclusion ('inc/options/ReduxCore.php');
fitfab_inclusion ('inc/options/options.php');
endif;
fitfab_inclusion('vendor/envato_setup/envato_setup.php');

