<?php
/**
 * Envato Theme Setup Wizard Class
 *
 * Takes new users through some basic steps to setup their ThemeForest theme.
 *
 * @author      dtbaker
 * @author      vburlak
 * @package     envato_wizard
 * @version     1.1.7
 *
 * Based off the WooThemes installer.
 *
 */
if (!defined('ABSPATH')) {
    exit;
}
define("WPTM_DIR", __DIR__);
fitfab_inclusion('vendor/envato_setup/tgm/tgm.php');
if (!class_exists('Envato_Theme_Setup_Wizard')) {

    /**
     * Envato_Theme_Setup_Wizard class
     */
    class Envato_Theme_Setup_Wizard {

	/**
	 * The class version number.
	 *
	 * @since 1.1.1
	 * @access private
	 *
	 * @var string
	 */
	protected $version = '1.1.4';

	/** @var string Current theme name, used as namespace in actions. */
	protected $theme_name = 'fitfab';

	/** @var string Current Step */
	protected $step = '';

	/** @var array Steps for the setup wizard */
	protected $steps = array();

	/**
	 * Relative plugin path
	 *
	 * @since 1.1.2
	 *
	 * @var string
	 */
	protected $plugin_path = '';

	/**
	 * Relative plugin url for this plugin folder, used when enquing scripts
	 *
	 * @since 1.1.2
	 *
	 * @var string
	 */
	protected $plugin_url = '';

	/**
	 * The slug name to refer to this menu
	 *
	 * @since 1.1.1
	 *
	 * @var string
	 */
	protected $page_slug;

	/**
	 * TGMPA instance storage
	 *
	 * @var object
	 */
	protected $tgmpa_instance;

	/**
	 * TGMPA Menu slug
	 *
	 * @var string
	 */
	protected $tgmpa_menu_slug = 'tgmpa-install-plugins';

	/**
	 * TGMPA Menu url
	 *
	 * @var string
	 */
	protected $tgmpa_url = 'themes.php?page=tgmpa-install-plugins';

	/**
	 * The slug name for the parent menu
	 *
	 * @since 1.1.2
	 *
	 * @var string
	 */
	protected $page_parent;

	/**
	 * Complete URL to Setup Wizard
	 *
	 * @since 1.1.2
	 *
	 * @var string
	 */
	protected $page_url;

	/**
	 * Holds the current instance of the theme manager
	 *
	 * @since 1.1.3
	 * @var Envato_Theme_Setup_Wizard
	 */
	private static $instance = null;

	/**
	 * @since 1.1.3
	 *
	 * @return Envato_Theme_Setup_Wizard
	 */
	public static function get_instance() {
	    if (!self::$instance) {
		self::$instance = new self;
	    }

	    return self::$instance;
	}

	/**
	 * A dummy constructor to prevent this class from being loaded more than once.
	 *
	 * @see Envato_Theme_Setup_Wizard::instance()
	 *
	 * @since 1.1.1
	 * @access private
	 */
	public function __construct() {
	    $this->_active_theme_option_import();
	    $this->init_globals();
	    $this->init_actions();
	}
	
	

	function _active_theme_option_import() {
	    $oldoption = get_option("fitfab_data");
	    if (empty($oldoption)) {
		$options = $this->_get_json('active-theme-option.json');
		if (!empty($options[0])) {
		     $optionsCurrent = $options[0]; 
		    update_option("fitfab_data", $optionsCurrent);
		}
	    }
	}
	
	/**
	 * Setup the class globals.
	 *
	 * @since 1.1.1
	 * @access private
	 */
	public function init_globals() {
	    $current_theme = wp_get_theme();
	    $this->theme_name = strtolower(preg_replace('#[^a-zA-Z]#', '', $current_theme->get('Name')));
	    $this->page_slug = apply_filters($this->theme_name . '_theme_setup_wizard_page_slug', $this->theme_name . '-setup');
	    $this->parent_slug = apply_filters($this->theme_name . '_theme_setup_wizard_parent_slug', '');

	    //If we have parent slug - set correct url
	    if ($this->parent_slug !== '') {
		$this->page_url = 'admin.php?page=' . $this->page_slug;
	    } else {
		$this->page_url = 'themes.php?page=' . $this->page_slug;
	    }
	    $this->page_url = apply_filters($this->theme_name . '_theme_setup_wizard_page_url', $this->page_url);

	    //set relative plugin path url
	    $this->plugin_path = trailingslashit($this->cleanFilePath(dirname(__FILE__)));
	    $relative_url = str_replace($this->cleanFilePath(get_template_directory()), '', $this->plugin_path);
	    $this->plugin_url = trailingslashit(get_template_directory_uri() . $relative_url);
	}

	/**
	 * Setup the hooks, actions and filters.
	 *
	 * @uses add_action() To add actions.
	 * @uses add_filter() To add filters.
	 *
	 * @since 1.1.1
	 * @access private
	 */
	public function init_actions() {
	    if (apply_filters($this->theme_name . '_enable_setup_wizard', true) && current_user_can('manage_options')) {
		add_action('after_switch_theme', array($this, 'switch_theme'));
		if (class_exists('TGM_Plugin_Activation') && isset($GLOBALS['tgmpa'])) {
		    add_action('init', array($this, 'get_tgmpa_instanse'), 30);
		    add_action('init', array($this, 'set_tgmpa_url'), 40);
		}

		add_action('admin_menu', array($this, 'admin_menus'));
		add_action('admin_enqueue_scripts', array($this, 'enqueue_scripts'));
		add_action('admin_init', array($this, 'admin_redirects'), 30);
		add_action('admin_init', array($this, 'init_wizard_steps'), 30);
		add_action('admin_init', array($this, 'setup_wizard'), 30);
		add_filter('tgmpa_load', array($this, 'tgmpa_load'), 10, 1);
		add_action('wp_ajax_envato_setup_plugins', array($this, 'ajax_plugins'));
		add_action('wp_ajax_envato_setup_content', array($this, 'ajax_content'));
	    }
	}

	public function enqueue_scripts() {
	}

	public function tgmpa_load($status) {
	    return is_admin() || current_user_can('install_themes');
	}

	public function switch_theme() {
	    set_transient('_' . $this->theme_name . '_activation_redirect', 1);
	}

	public function admin_redirects() {
	    ob_start();
	    if (!get_transient('_' . $this->theme_name . '_activation_redirect')) {
		return;
	    }
	    delete_transient('_' . $this->theme_name . '_activation_redirect');
	    wp_safe_redirect(admin_url($this->page_url));
	    exit;
	}

	/**
	 * Get configured TGMPA instance
	 *
	 * @access public
	 * @since 1.1.2
	 */
	public function get_tgmpa_instanse() {
	    $this->tgmpa_instance = call_user_func(array(get_class($GLOBALS['tgmpa']), 'get_instance'));
	}

	/**
	 * Update $tgmpa_menu_slug and $tgmpa_parent_slug from TGMPA instance
	 *
	 * @access public
	 * @since 1.1.2
	 */
	public function set_tgmpa_url() {

	    $this->tgmpa_menu_slug = ( property_exists($this->tgmpa_instance, 'menu') ) ? $this->tgmpa_instance->menu : $this->tgmpa_menu_slug;
	    $this->tgmpa_menu_slug = apply_filters($this->theme_name . '_theme_setup_wizard_tgmpa_menu_slug', $this->tgmpa_menu_slug);

	    $tgmpa_parent_slug = ( property_exists($this->tgmpa_instance, 'parent_slug') && $this->tgmpa_instance->parent_slug !== 'themes.php' ) ? 'admin.php' : 'themes.php';

	    $this->tgmpa_url = apply_filters($this->theme_name . '_theme_setup_wizard_tgmpa_url', $tgmpa_parent_slug . '?page=' . $this->tgmpa_menu_slug);
	}

	/**
	 * Add admin menus/screens.
	 */
	public function admin_menus() {

	    if ($this->is_submenu_page()) {
		//prevent Theme Check warning about "themes should use add_theme_page for adding admin pages"
		$add_subpage_function = 'add_submenu' . '_page';
		$add_subpage_function($this->parent_slug, esc_html__('Setup Wizard', 'fitfab'), esc_html__('Setup Wizard', 'fitfab'), 'manage_options', $this->page_slug, array($this, 'setup_wizard'));
	    } else {
		add_theme_page(__('Setup Wizard', 'fitfab'), esc_html__('Setup Wizard', 'fitfab'), 'manage_options', $this->page_slug, array($this, 'setup_wizard'));
	    }
	}

	/**
	 * Setup steps.
	 *
	 * @since 1.1.1
	 * @access public
	 * @return array
	 */
	public function init_wizard_steps() {

	    $this->steps = array(
		'introduction' => array(
		    'name' => esc_html__('Introduction', 'fitfab'),
		    'view' => array($this, 'envato_setup_introduction'),
		    'handler' => '',
		),
	    );
	    if (class_exists('TGM_Plugin_Activation') && isset($GLOBALS['tgmpa'])) {
		$this->steps['default_plugins'] = array(
		    'name' => esc_html__('Plugins', 'fitfab'),
		    'view' => array($this, 'envato_setup_default_plugins'),
		    'handler' => '',
		);
	    }
	    $this->steps['default_content'] = array(
		'name' => esc_html__('Content', 'fitfab'),
		'view' => array($this, 'envato_setup_default_content'),
		'handler' => '',
	    );
	    $this->steps['design'] = array(
		'name' => esc_html__('Logo & Design', 'fitfab'),
		'view' => array($this, 'envato_setup_logo_design'),
		'handler' => array($this, 'envato_setup_logo_design_save'),
	    );
	    $this->steps['next_steps'] = array(
		'name' => esc_html__('Ready!', 'fitfab'),
		'view' => array($this, 'envato_setup_ready'),
		'handler' => '',
	    );

	    return apply_filters($this->theme_name . '_theme_setup_wizard_steps', $this->steps);
	}

	/**
	 * Show the setup wizard
	 */
	public function setup_wizard() {
	    if (empty($_GET['page']) || $this->page_slug !== $_GET['page']) {
		return;
	    }
	    ob_end_clean();

	    $this->step = isset($_GET['step']) ? sanitize_key($_GET['step']) : current(array_keys($this->steps));

	    wp_register_script('jquery-blockui', $this->plugin_url . '/js/jquery.blockUI.js', array('jquery'), '2.70', true);
	    wp_register_script('envato-setup', $this->plugin_url . '/js/envato-setup.js', array('jquery', 'jquery-blockui'), $this->version);
	    wp_localize_script('envato-setup', 'envato_setup_params', array(
		'tgm_plugin_nonce' => array(
		    'update' => wp_create_nonce('tgmpa-update'),
		    'install' => wp_create_nonce('tgmpa-install'),
		),
		'tgm_bulk_url' => admin_url($this->tgmpa_url),
		'ajaxurl' => admin_url('admin-ajax.php'),
		'wpnonce' => wp_create_nonce('envato_setup_nonce'),
		'verify_text' => esc_html__('...verifying', 'fitfab'),
	    ));

	    //wp_enqueue_style( 'envato_wizard_admin_styles', $this->plugin_url . '/css/admin.css', array(), $this->version );
	    wp_enqueue_style('envato-setup', $this->plugin_url . '/css/envato-setup.css', array('dashicons', 'install'), $this->version);

	    //enqueue style for admin notices
	    wp_enqueue_style('wp-admin');

	    wp_enqueue_media();
	    wp_enqueue_script('media');

	    ob_start();
	    $this->setup_wizard_header();
	    $this->setup_wizard_steps();
	    $show_content = true;
	    echo '<div class="envato-setup-content">';
	    if (!empty($_REQUEST['save_step']) && isset($this->steps[$this->step]['handler'])) {
		$show_content = call_user_func($this->steps[$this->step]['handler']);
	    }
	    if ($show_content) {
		$this->setup_wizard_content();
	    }
	    echo '</div>';
	    $this->setup_wizard_footer();
	    exit;
	}

	public function get_step_link($step) {
	    return add_query_arg('step', $step, admin_url('admin.php?page=' . $this->page_slug));
	}

	public function get_next_step_link() {
	    $keys = array_keys($this->steps);
	    return add_query_arg('step', $keys[array_search($this->step, array_keys($this->steps)) + 1], remove_query_arg('translation_updated'));
	}

	/**
	 * Setup Wizard Header
	 */
	public function setup_wizard_header() {
	    ?><!DOCTYPE html>
	    <html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
	        <head>
	    	<meta name="viewport" content="width=device-width" />
	    	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	    	<?php echo '<tit'.'le>'; esc_html_e('Theme &rsaquo; Setup Wizard', 'fitfab'); echo '</tit'.'le>';?>
		    <?php wp_print_scripts('envato-setup'); ?>
		    <?php do_action('admin_print_styles'); ?>
		    <?php do_action('admin_print_scripts'); ?>
		    <?php do_action('admin_head'); ?>
	        </head>
	        <body class="envato-setup wp-core-ui">
	    	<h1 id="wc-logo">
			<?php
			 $oldoption = get_option("fitfab_data");
				$logo = (!empty($oldoption['fitfab_logo']['url'])) ? $oldoption['fitfab_logo']['url'] : FITFAB_THEME_URL . '/assets/images/home-2_logo.png';
				
			 // = (!empty($fitfab_data['fitfab_logo'] ['url'])) ? $fitfab_data['fitfab_logo'] ['url'] : FITFAB_THEME_URL . '/assets/images/home-2_logo.png';
			
			?>
	    	    <a href="<?php echo esc_url(home_url('/')); ?>" class="logo"><img src="<?php echo esc_url($logo); ?>"  alt="<?php echo get_bloginfo('name'); ?>" title="<?php echo get_bloginfo('name'); ?>"  /></a>
	    	</h1>
		    <?php
		}

		/**
		 * Setup Wizard Footer
		 */
		public function setup_wizard_footer() {
		    ?>
		    <?php if ('next_steps' === $this->step) : ?>
			<a class="wc-return-to-dashboard" href="<?php echo esc_url(admin_url()); ?>"><?php esc_html_e('Return to the WordPress Dashboard', 'fitfab'); ?></a>
		    <?php endif; ?>
	        </body>
		<?php
		@do_action('admin_footer'); // this was spitting out some errors in some admin templates. quick @ fix until I have time to find out what's causing errors.
		do_action('admin_print_footer_scripts');
		?>
	    </html>
	    <?php
	}

	/**
	 * Output the steps
	 */
	public function setup_wizard_steps() {
	    $ouput_steps = $this->steps;
	    array_shift($ouput_steps);
	    ?>
	    <ol class="envato-setup-steps">
		<?php foreach ($ouput_steps as $step_key => $step) : ?>
		    <li class="<?php
		    $show_link = false;
		    if ($step_key === $this->step) {
			echo 'active';
		    } elseif (array_search($this->step, array_keys($this->steps)) > array_search($step_key, array_keys($this->steps))) {
			echo 'done';
			$show_link = true;
		    }
		    ?>"><?php
			    if ($show_link) {
				?>
		    	<a href="<?php echo esc_url($this->get_step_link($step_key)); ?>"><?php echo esc_html($step['name']); ?></a>
			    <?php
			} else {
			    echo esc_html($step['name']);
			}
			?></li>
		<?php endforeach; ?>
	    </ol>
	    <?php
	}

	/**
	 * Output the content for the current step
	 */
	public function setup_wizard_content() {
	    isset($this->steps[$this->step]) ? call_user_func($this->steps[$this->step]['view']) : false;
	}

	/**
	 * Introduction step
	 */
	public function envato_setup_introduction() {
	    if (isset($_REQUEST['export'])) {


		// used for me to export my widget settings.
		$widget_positions = get_option('sidebars_widgets');
		$widget_options = array();
		$my_options = array();
		foreach ($widget_positions as $sidebar_name => $widgets) {
		    if (is_array($widgets)) {
			foreach ($widgets as $widget_name) {
			    $widget_name_strip = preg_replace('#-\d+$#', '', $widget_name);
			    $widget_options[$widget_name_strip] = get_option('widget_' . $widget_name_strip);
			}
		    }
		}
		foreach ($widget_options['nav_menu'] as $key => $menuItem) {
		    if (is_int($key)) {
			$menuObj = wp_get_nav_menu_object($menuItem['nav_menu']);
			$widget_options['nav_menu'][$key]['nav_menu'] = $menuObj->name;
		    }
		}
		// choose which custom options to load into defaults
		$all_options = wp_load_alloptions();
		foreach ($all_options as $name => $value) {
		    if (stristr($name, '_widget_area_manager')) {
			$my_options[$name] = $value;
		    }
		    if (stristr($name, 'wam_')) {
			$my_options[$name] = $value;
		    }
		}
		$my_options = get_option('fitfab_data');
		?>
		<h1>Current Settings:</h1>
		<p>Widget Positions:</p>
		<textarea style="width:100%; height:80px;"><?php echo json_encode($widget_positions); ?></textarea>
		<p>Widget Options:</p>
		<textarea style="width:100%; height:80px;"><?php echo json_encode($widget_options); ?></textarea>
		<p>Custom Options:</p>
		<textarea style="width:100%; height:80px;">[<?php echo json_encode($my_options); ?>]</textarea>
		<p>Copy these values into your PHP code when distributing/updating the theme.</p>
		<?php
	    } else {
		?>
		<h1><?php esc_html_e('Welcome to the setup wizard for Fitfab!', 'fitfab'); ?></h1>
		<p><?php esc_html_e('Thank you for choosing the Fitfab theme. This quick setup wizard will help you configure your new website. This wizard will install the required WordPress plugins, default content, logo and tell you a little about Help &amp; Support options.', 'fitfab'); ?></p>
		<p><?php esc_html_e('It should only take few minutes.','fitfab'); ?></p>
		<p><?php esc_html_e('No time right now? If you don\'t want to go through the wizard, you can skip and return to the WordPress dashboard. Come back anytime if you change your mind!', 'fitfab'); ?></p>
		<p class="envato-setup-actions step">
		    <a href="<?php echo esc_url($this->get_next_step_link()); ?>"
		       class="button-primary button button-large button-next"><?php esc_html_e('Let\'s Go!', 'fitfab'); ?></a>
		    <a href="<?php echo esc_url(wp_get_referer() && !strpos(wp_get_referer(), 'update.php') ? wp_get_referer() : admin_url('') ); ?>"
		       class="button button-large"><?php esc_html_e('Not right now', 'fitfab'); ?></a>
		</p>
		<?php
	    }
	}

	private function _get_plugins() {
	    $instance = call_user_func(array(get_class($GLOBALS['tgmpa']), 'get_instance'));
	    $plugins = array(
		'all' => array(), // Meaning: all plugins which still have open actions.
		'install' => array(),
		'update' => array(),
		'activate' => array(),
	    );

	    foreach ($instance->plugins as $slug => $plugin) {
		if ($instance->is_plugin_active($slug) && false === $instance->does_plugin_have_update($slug)) {
		    // No need to display plugins if they are installed, up-to-date and active.
		    continue;
		} else {
		    $plugins['all'][$slug] = $plugin;

		    if (!$instance->is_plugin_installed($slug)) {
			$plugins['install'][$slug] = $plugin;
		    } else {
			if (false !== $instance->does_plugin_have_update($slug)) {
			    $plugins['update'][$slug] = $plugin;
			}

			if ($instance->can_plugin_activate($slug)) {
			    $plugins['activate'][$slug] = $plugin;
			}
		    }
		}
	    }
	    return $plugins;
	}

	/**
	 * Page setup
	 */
	public function envato_setup_default_plugins() {

	    tgmpa_load_bulk_installer();
	    // install plugins with TGM.
	    if (!class_exists('TGM_Plugin_Activation') || !isset($GLOBALS['tgmpa'])) {
		die('Failed to find TGM');
	    }
	    $url = wp_nonce_url(add_query_arg(array('plugins' => 'go')), 'envato-setup');
	    $plugins = $this->_get_plugins();

	    // copied from TGM

	    $method = ''; // Leave blank so WP_Filesystem can populate it as necessary.
	    $fields = array_keys($_POST); // Extra fields to pass to WP_Filesystem.

	    if (false === ( $creds = request_filesystem_credentials(esc_url_raw($url), $method, false, false, $fields) )) {
		return true; // Stop the normal page form from displaying, credential request form will be shown.
	    }

	    // Now we have some credentials, setup WP_Filesystem.
	    if (!WP_Filesystem($creds)) {
		// Our credentials were no good, ask the user for them again.
		request_filesystem_credentials(esc_url_raw($url), $method, true, false, $fields);

		return true;
	    }

	    /* If we arrive here, we have the filesystem */
	    ?>
	    <h1><?php esc_html_e('Default Plugins', 'fitfab'); ?></h1>
	    <form method="post">

		<?php
		$plugins = $this->_get_plugins();
		if (count($plugins['all'])) {
		    ?>
		    <p><?php esc_html_e('Your website needs a few essential plugins. The following plugins will be installed:', 'fitfab'); ?></p>
		    <ul class="envato-wizard-plugins">
			<?php foreach ($plugins['all'] as $slug => $plugin) { ?>
		    	<li data-slug="<?php echo esc_attr($slug); ?>"><?php echo esc_html($plugin['name']); ?>
		    	    <span>
				    <?php
				    $keys = array();
				    if (isset($plugins['install'][$slug])) {
					$keys[] = 'Installation';
				    }
				    if (isset($plugins['update'][$slug])) {
					$keys[] = 'Update';
				    }
				    if (isset($plugins['activate'][$slug])) {
					$keys[] = 'Activation';
				    }
				    echo implode(' and ', $keys) . ' required';
				    ?>
		    	    </span>
		    	    <div class="spinner"></div>
		    	</li>
			<?php } ?>
		    </ul>
		    <?php
		} else {
		    echo '<p><strong>' . esc_html_e('Good news! All plugins are already installed and up to date. Please continue.', 'fitfab') . '</strong></p>';
		}
		?>

	        <p><?php esc_html_e('You can add and remove plugins later on from within WordPress.', 'fitfab'); ?></p>

	        <p class="envato-setup-actions step">
	    	<a href="<?php echo esc_url($this->get_next_step_link()); ?>" class="button-primary button button-large button-next" data-callback="install_plugins"><?php esc_html_e('Continue', 'fitfab'); ?></a>
	    	<a href="<?php echo esc_url($this->get_next_step_link()); ?>" class="button button-large button-next"><?php esc_html_e('Skip this step', 'fitfab'); ?></a>
		    <?php wp_nonce_field('envato-setup'); ?>
	        </p>
	    </form>
	    <?php
	}

	public function ajax_plugins() {
	    if (!check_ajax_referer('envato_setup_nonce', 'wpnonce') || empty($_POST['slug'])) {
		wp_send_json_error(array('error' => 1, 'message' => esc_html__('No Slug Found', 'fitfab')));
	    }
	    $json = array();
	    // send back some json we use to hit up TGM
	    $plugins = $this->_get_plugins();
	    // what are we doing with this plugin?
	    foreach ($plugins['activate'] as $slug => $plugin) {
		if ($_POST['slug'] == $slug) {
		    $json = array(
			'url' => admin_url($this->tgmpa_url),
			'plugin' => array($slug),
			'tgmpa-page' => $this->tgmpa_menu_slug,
			'plugin_status' => 'all',
			'_wpnonce' => wp_create_nonce('bulk-plugins'),
			'action' => 'tgmpa-bulk-activate',
			'action2' => -1,
			'message' => esc_html__('Activating Plugin', 'fitfab'),
		    );
		    break;
		}
	    }
	    foreach ($plugins['update'] as $slug => $plugin) {
		if ($_POST['slug'] == $slug) {
		    $json = array(
			'url' => admin_url($this->tgmpa_url),
			'plugin' => array($slug),
			'tgmpa-page' => $this->tgmpa_menu_slug,
			'plugin_status' => 'all',
			'_wpnonce' => wp_create_nonce('bulk-plugins'),
			'action' => 'tgmpa-bulk-update',
			'action2' => -1,
			'message' => esc_html__('Updating Plugin', 'fitfab'),
		    );
		    break;
		}
	    }
	    foreach ($plugins['install'] as $slug => $plugin) {
		if ($_POST['slug'] == $slug) {
		    $json = array(
			'url' => admin_url($this->tgmpa_url),
			'plugin' => array($slug),
			'tgmpa-page' => $this->tgmpa_menu_slug,
			'plugin_status' => 'all',
			'_wpnonce' => wp_create_nonce('bulk-plugins'),
			'action' => 'tgmpa-bulk-install',
			'action2' => -1,
			'message' => esc_html__('Installing Plugin', 'fitfab'),
		    );
		    break;
		}
	    }

	    if ($json) {
		$json['hash'] = md5(serialize($json)); // used for checking if duplicates happen, move to next plugin
		wp_send_json($json);
	    } else {
		wp_send_json(array('done' => 1, 'message' => esc_html__('Success', 'fitfab')));
	    }
	    exit;
	}

	private function _content_default_get() {

	    $content = array();

	    $content['pages'] = array(
		'title' => esc_html__('Pages', 'fitfab'),
		'description' => esc_html__('This will create default pages as seen in the demo.', 'fitfab'),
		'pending' => esc_html__('Pending.', 'fitfab'),
		'installing' => esc_html__('Installing Default Pages.', 'fitfab'),
		'success' => esc_html__('Success.', 'fitfab'),
		'install_callback' => array($this, '_content_install_pages'),
	    );
	    $content['widgets'] = array(
		'title' => esc_html__('Widgets', 'fitfab'),
		'description' => esc_html__('Insert default sidebar widgets as seen in the demo.', 'fitfab'),
		'pending' => esc_html__('Pending.', 'fitfab'),
		'installing' => esc_html__('Installing Default Widgets.', 'fitfab'),
		'success' => esc_html__('Success.', 'fitfab'),
		'install_callback' => array($this, '_content_install_widgets'),
	    );
	    $content['sliders'] = array(
		'title' => esc_html__('Sliders', 'fitfab'),
		'description' => esc_html__('Insert default sliders as seen in the demo.', 'fitfab'),
		'pending' => esc_html__('Pending.', 'fitfab'),
		'installing' => esc_html__('Installing Default Sliders.', 'fitfab'),
		'success' => esc_html__('Success.', 'fitfab'),
		'install_callback' => array($this, '_content_install_sliders'),
	    );
	    $content['settings'] = array(
		'title' => esc_html__('Settings', 'fitfab'),
		'description' => esc_html__('Configure default settings.', 'fitfab'),
		'pending' => esc_html__('Pending.', 'fitfab'),
		'installing' => esc_html__('Installing Default Settings.', 'fitfab'),
		'success' => esc_html__('Success.', 'fitfab'),
		'install_callback' => array($this, '_content_install_settings'),
	    );

	    return $content;
	}

	/**
	 * Page setup
	 */
	public function envato_setup_default_content() {
	    ?>
	    <h1><?php esc_html_e('Default Content', 'fitfab'); ?></h1>
	    <form method="post">
	        <p><?php printf(__('It\'s time to insert some default content for your new WordPress website. Choose what you would like inserted below and click Continue.', 'fitfab'), '<a href="' . esc_url(admin_url('edit.php?post_type=page')) . '" target="_blank">', '</a>'); ?></p>
	        <table class="envato-setup-pages" cellspacing="0">
	    	<thead>
	    	    <tr>
	    		<td class="check"> </td>
	    		<th class="item"><?php esc_html_e('Item', 'fitfab'); ?></th>
	    		<th class="description"><?php esc_html_e('Description', 'fitfab'); ?></th>
	    		<th class="status"><?php esc_html_e('Status', 'fitfab'); ?></th>
	    	    </tr>
	    	</thead>
	    	<tbody>
			<?php foreach ($this->_content_default_get() as $slug => $default) { ?>
			    <tr class="envato_default_content" data-content="<?php echo esc_attr($slug); ?>">
				<td>
				    <input type="checkbox" name="default_content[pages]" class="envato_default_content" id="default_content_<?php echo esc_attr($slug); ?>" value="1" checked>
				</td>
				<td><label for="default_content_<?php echo esc_attr($slug); ?>"><?php echo wp_kses_post($default['title']); ?></label></td>
				<td class="description"><?php echo wp_kses_post($default['description']); ?></td>
				<td class="status"> <span><?php echo wp_kses_post($default['pending']); ?></span> <div class="spinner"></div></td>
			    </tr>
			<?php } ?>
	    	</tbody>
	        </table>

	        <p><?php esc_html_e('Once inserted, this content can be managed from the WordPress admin dashboard.', 'fitfab'); ?></p>

	        <p class="envato-setup-actions step">
	    	<a href="<?php echo esc_url($this->get_next_step_link()); ?>" class="button-primary button button-large button-next" data-callback="install_content"><?php esc_html_e('Continue', 'fitfab'); ?></a>
	    	<a href="<?php echo esc_url($this->get_next_step_link()); ?>" class="button button-large button-next"><?php esc_html_e('Skip this step', 'fitfab'); ?></a>
		    <?php wp_nonce_field('envato-setup'); ?>
	        </p>
	    </form>
	    <?php
	}

	public function ajax_content() {
	    $content = $this->_content_default_get();
	    if (!check_ajax_referer('envato_setup_nonce', 'wpnonce') || empty($_POST['content']) && isset($content[$_POST['content']])) {
		wp_send_json_error(array('error' => 1, 'message' => esc_html__('No content Found', 'fitfab')));
	    }

	    $json = false;
	    $this_content = $content[$_POST['content']];

	    if (isset($_POST['proceed'])) {
		// install the content!

		if (!empty($this_content['install_callback'])) {
		    if ($result = call_user_func($this_content['install_callback'])) {
			$json = array(
			    'done' => 1,
			    'message' => $this_content['success'],
			    'debug' => $result,
			);
		    }
		}
	    } else {

		$json = array(
		    'url' => admin_url('admin-ajax.php'),
		    'action' => 'envato_setup_content',
		    'proceed' => 'true',
		    'content' => $_POST['content'],
		    '_wpnonce' => wp_create_nonce('envato_setup_nonce'),
		    'message' => $this_content['installing'],
		);
	    }

	    if ($json) {
		$json['hash'] = md5(serialize($json)); // used for checking if duplicates happen, move to next plugin
		wp_send_json($json);
	    } else {
		wp_send_json(array('error' => 1, 'message' => esc_html__('Error', 'fitfab')));
	    }

	    exit;
	}

	private function _import_wordpress_xml_file($xml_file_path) {
	    global $wpdb;

	    if (!defined('WP_LOAD_IMPORTERS')) {
		define('WP_LOAD_IMPORTERS', true);
	    }

	    // Load Importer API
	    require_once ABSPATH . 'wp-admin/includes/import.php' ;

	    if (!class_exists('WP_Importer')) {
		$class_wp_importer = ABSPATH . 'wp-admin/includes/class-wp-importer.php';
		if (file_exists($class_wp_importer)) {
		    require $class_wp_importer;
		}
	    }

	    if (!class_exists('WP_Import')) {
		$class_wp_importer = __DIR__ . '/importer/wordpress-importer.php';
		if (file_exists($class_wp_importer)) {
		    require $class_wp_importer;
		}
	    }

	    if (class_exists('WP_Import')) {
		require_once __DIR__ . '/importer/envato-content-import.php';
		$wp_import = new envato_content_import();
		$wp_import->fetch_attachments = true;
		ob_start();
		$wp_import->import($xml_file_path);
		$message = ob_get_clean();
		return array($wp_import->check(), $message);
	    }
	    return false;
	}

	private function _content_install_pages() {
	    return $this->_import_wordpress_xml_file(__DIR__ . '/content/all.xml');
	}

	private function _get_menu_ids() {
	    $menus = get_terms('nav_menu');
	    $menu_ids = array();
	    foreach ($menus as $menu) {
		$menu_ids[$menu->name] = $menu->term_id;
	    }
	    return $menu_ids;
	}

	private function _content_install_sliders() {
	    if (class_exists('RevSlider')) :
    		$rvSlider = new RevSlider();
		$rvslider_loc = 'http://theemon.com/plugins-territory/fitfab/sliders/';
		$rvslider_zip = array("home_slider_one.zip","home_slider_two.zip", "home-three.zip", "home-four.zip");
		$tempSlider = array();
                foreach($rvslider_zip as $zip) :
		    $tempDownloadURL = download_url($rvslider_loc.$zip);
		 if ( !is_wp_error( $tempSlider ) ):
		    $tempSlider[] = $tempDownloadURL;
		    ob_start();
                    $rvSlider->importSliderFromPost(true, true, $tempDownloadURL);
		    ob_end_clean();
		  endif;  
                endforeach;
		foreach($tempSlider as $temp){
		    unlink($temp);
		}
		return true;
	    endif;
	    return false;
	}

	private function _content_install_widgets() {
	    // todo: pump these out into the 'content/' folder along with the XML so it's a little nicer to play with
	    $import_widget_positions = $this->_get_json('widget_positions.json');
	    $import_widget_options = $this->_get_json('widget_options.json');
	    $menu_ids = $this->_get_menu_ids();
	    //print_r($menu_ids);
	    // importing.
	    $widget_positions = get_option('sidebars_widgets');
	    // adjust the widget settings to match our menu ID's which we discovered above.
	    if (is_array($import_widget_options) && isset($import_widget_options['nav_menu'])) {
		foreach ($import_widget_options['nav_menu'] as $key => $val) {
		    if(is_int($val)){
		    if (!empty($val['title'])) {
			if (($val['title'] == 'our offer' || $val['title'] == 'our service' || $val['title'] == 'store') && !empty($menu_ids['offers'])) {
			    $import_widget_options['nav_menu'][$key]['nav_menu'] = $menu_ids[$import_widget_options['nav_menu'][$key]['nav_menu']];
			}
		    }
		    }
		}
	    }
	    foreach ($import_widget_options as $widget_name => $widget_option) {
		$existing_options = get_option('widget_' . $widget_name, array());
		$new_options = $existing_options + $widget_option;
		update_option('widget_' . $widget_name, $new_options);
	    }
	    update_option('sidebars_widgets', array_merge($widget_positions, $import_widget_positions));

	    return true;
	}

	function setMenu() {
	    //SET HOME PAGE

	    $locations = array();
	    $menus = wp_get_nav_menus();
	    if ($menus) {
		foreach ($menus as $menu) {
		    if ("primary-navigation" == $menu->slug) {
			$locations["primary_navigation"] = $menu->term_id;
		    }
		    if ("footer-menu" == $menu->slug) {
			$locations["footer_navigation"] = $menu->term_id;
		    }
		}
	    }

	    set_theme_mod('nav_menu_locations', $locations); // set menus to locations    
	}

	private function _content_install_settings() {
	    //Theme Options
	    $options = $this->_get_json('options.json');
	    $remote_url = 'http://theemon.com/f/fit-fab-wp/PlaceHolder';
	    if (!empty($options)) {
		$optionsCurrent = str_replace($remote_url, site_url(), $options[0]);
		update_option("fitfab_data", $optionsCurrent);
	    }
	    //Set Front Page
	    $front_page = get_page_by_path('homepage');
	    update_option("show_on_front", "page");
	    update_option('page_on_front', $front_page->ID);
	    //Set Primary Menu
	    $this->setMenu();
	    return true;
	}

	private function _get_json($file) {
	    if (is_file(__DIR__ . '/content/' . basename($file))) {
		if(!function_exists('WP_Filesystem')){
		    require_once ABSPATH . 'wp-admin/includes/file.php';
		}
		WP_Filesystem();
		global $wp_filesystem;
		$file_name = __DIR__ . '/content/' . basename($file);
		if (file_exists($file_name)) {
		    return json_decode($wp_filesystem->get_contents($file_name), true);
		}
	    }
	    return array();
	}

	/**
	 * Logo & Design
	 */
	public function envato_setup_logo_design() {
	    ?>
	    <h1><?php esc_html_e('Logo &amp; Design', 'fitfab'); ?></h1>
	    <form method="post">
	        <p><?php echo esc_html__('Please add your logo below. For best results, the logo should be a transparent PNG ( 164 by 34 pixels). The logo can be changed at any time from the Appearance > Theme Options area in your dashboard.', 'fitfab'); ?></p>

	        <table>
	    	<tr>
	    	    <td>
	    		<div id="current-logo" style="background-color: #f1f1f1; padding: 6px 4px;">
				<?php
				 $oldoption = get_option("fitfab_data");
				$image_url = (!empty($oldoption['fitfab_logo']['url'])) ? $oldoption['fitfab_logo']['url'] : FITFAB_THEME_URL . '/assets/images/home-2_logo.png';
				if ($image_url) {
				    $image = '<img class="site-logo" src="%s" alt="%s" style="width:%s; height:auto" />';
				    printf(
					    $image, $image_url, get_bloginfo('name'), '200px'
				    );
				}
				?>
	    		</div>
	    	    </td>
	    	    <td>
	    		<a href="#" class="button button-upload"><?php esc_html_e('Upload New Logo', 'fitfab'); ?></a>
	    	    </td>
	    	</tr>
	        </table>

	        <input type="hidden" name="new_logo_id" id="new_logo_id" value="">

	    	<p class="envato-setup-actions step">
	    	    <input type="submit" class="button-primary button button-large button-next" value="<?php esc_attr_e('Continue', 'fitfab'); ?>" name="save_step" />
	    	    <a href="<?php echo esc_url($this->get_next_step_link()); ?>" class="button button-large button-next"><?php esc_html_e('Skip this step', 'fitfab'); ?></a>
			<?php wp_nonce_field('envato-setup'); ?>
	    	</p>
	    </form>
	    <?php
	}

	/**
	 * Save logo & design options
	 */
	public function envato_setup_logo_design_save() {
	    check_admin_referer('envato-setup');

	    $new_logo_id = (int) $_POST['new_logo_id']; 
	    // save this new logo url into the database and calculate the desired height based off the logo width.
	    // copied from dtbaker.theme_options.php
	    if ($new_logo_id) {
		$attr = wp_get_attachment_image_src($new_logo_id, 'full');	
					
		if ($attr) {			
		   $oldoption = get_option("fitfab_data");			
		    $oldoption['fitfab_logo']['url'] = $attr[0];
		    update_option("fitfab_data", $oldoption);	   
		 
		}
	    }
	    wp_redirect(esc_url_raw($this->get_next_step_link()));
	    exit;
	}



	/**
	 * Final step
	 */
	public function envato_setup_ready() {
	    ?>
	    <h1><?php esc_html_e('Your Website is Ready!', 'fitfab'); ?></h1>

	    <p>Congratulations! The theme has been activated and your website is ready. Login to your WordPress dashboard to make changes and modify any of the default content to suit your needs.</p>

	    <div class="envato-setup-next-steps">
	        <div class="envato-setup-next-steps-first">
	    	<h2><?php esc_html_e('Next Steps', 'fitfab'); ?></h2>
	    	<ul>
	    	    <li class="setup-product"><a class="button button-next button-large" href="<?php echo esc_url(home_url()); ?>"><?php esc_html_e('View your new website!', 'fitfab'); ?></a></li>
	    	</ul>
	        </div>
	        <div class="envato-setup-next-steps-last">
	    	<h2><?php esc_html_e('More Resources', 'fitfab'); ?></h2>
	    	<ul>
	    	    <li class="documentation"><a href="<?php echo esc_url('http://theemon.com/f/fit-fab-wp/Documentation/'); ?>" target="_blank"><?php esc_html_e('Read the Theme Documentation', 'fitfab'); ?></a></li>
	    	    <li class="howto"><a href="<?php echo esc_url('https://wordpress.org/support/'); ?>" target="_blank"><?php esc_html_e('Learn how to use WordPress', 'fitfab'); ?></a></li>
	    	</ul>
	        </div>
	    </div>
	    <?php
	}

	/**
	 * Helper function
	 * Take a path and return it clean
	 *
	 * @param string $path
	 *
	 * @since    1.1.2
	 */
	public static function cleanFilePath($path) {
	    $path = str_replace('', '', str_replace(array("\\", "\\\\"), '/', $path));
	    if ($path[strlen($path) - 1] === '/') {
		$path = rtrim($path, '/');
	    }
	    return $path;
	}

	public function is_submenu_page() {
	    return ( $this->parent_slug == '' ) ? false : true;
	}

    }

}// if !class_exists

/**
 * Loads the main instance of Envato_Theme_Setup_Wizard to have
 * ability extend class functionality
 *
 * @since 1.1.1
 * @return object Envato_Theme_Setup_Wizard
 */
add_action('after_setup_theme', 'envato_theme_setup_wizard', 10);
if (!function_exists('envato_theme_setup_wizard')) :

    function envato_theme_setup_wizard() {
	Envato_Theme_Setup_Wizard::get_instance();
    }




endif;