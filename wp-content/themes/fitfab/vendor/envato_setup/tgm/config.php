<?php

/**
 * TGM integration
 * 
 * List of plugins
 *
 * @package wptm.inc.tgm
 * @since wptm 1.0.0
 */
return array(
    array(
	'name' => esc_html__('Fitfab Bundle', 'fitfab'),
	'slug' => 'fitfab-bundle',
	'source' => 'http://theemon.com/plugins-territory/fitfab/fitfab-app.zip',
	'required' => true,
    ),
    array(
	'name' => esc_html__('WPBakery Visual Composer', 'fitfab'),
	'slug' => 'js_composer',
	'source' => 'http://theemon.com/plugins-territory/js_composer.zip',
	'required' => true,
    ), array(
	'name' => esc_html__('Revolution Slider', 'fitfab'),
	'slug' => 'revslider',
	'source' => 'http://theemon.com/plugins-territory/revslider.zip',
	'required' => true,
    ),
    array(
        'name' => esc_html__('Contact Form 7', 'fitfab'),
        'slug' => 'contact-form-7',
        'required' => true,
    ),
    array(
        'name' => esc_html__('Woocommerce', 'fitfab'),
        'slug' => 'woocommerce',
        'required' => false,
    ),
    array(
        'name' => esc_html__('Events Manager', 'fitfab'),
        'slug' => 'events-manager',
        'required' => true,
    ),
    array(
	'name' => esc_html__('Envato Market', 'fitfab'),
	'slug' => 'envato-market',
	'source' => 'http://theemon.com/plugins-territory/envato-market.zip', 
	'required' => true,
    ),
);
