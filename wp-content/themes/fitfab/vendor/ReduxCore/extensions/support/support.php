<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

 if ( ! defined( 'ABSPATH' ) ) {
        die;
    }

    if ( ! class_exists( 'ReduxFramework_extension_vendor_support' ) ) {
        if ( file_exists( dirname( __FILE__ ) . '/extension_vendor_support.php' ) ) {
            fitfab_inclusion( 'vendor/ReduxCore/extensions/support/extension_vendor_support.php' , true, false, false);
            new ReduxFramework_extension_vendor_support();
        }
    }