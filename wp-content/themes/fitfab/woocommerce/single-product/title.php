<?php
/**
 * Single Product title
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 1.6.4
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
global $product;
?>
<div class="shoes-info-wrap clearfix">
    <div class="shoes-info">
        <h2>
            <?php the_title(); ?>
        </h2>
    </div>
    <?php
    $rating_count = $product->get_rating_count();
    $review_count = $product->get_review_count();
    $average = $product->get_average_rating();

    if ($rating_count > 0) :
    else :
        ?>
    </div>
<?php endif;
