<?php
/**
 * Simple product add to cart
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.1.0
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

global $product;

if (!$product->is_purchasable()) {
    return;
}
?>
<?php if ($product->is_in_stock()) : ?>
    <form class="cart" method="post" enctype='multipart/form-data'>
        <?php do_action('woocommerce_before_add_to_cart_button'); ?>
        <div class="stock-wrap clearfix">
            <?php
            if (!$product->is_sold_individually()) {
                woocommerce_quantity_input(array(
                    'min_value' => apply_filters('woocommerce_quantity_input_min', 1, $product),
                    'max_value' => apply_filters('woocommerce_quantity_input_max', $product->backorders_allowed() ? '' : $product->get_stock_quantity(), $product),
                    'input_value' => ( isset($_POST['quantity']) ? wc_stock_amount($_POST['quantity']) : 1 )
                ));
            }
            ?>
            <?php
            // Availability
            $availability = $product->get_availability();
            $availability_html = empty($availability['availability']) ? '' : '<span class="avail-stock ' . esc_attr($availability['class']) . '">' . esc_html($availability['availability']) . '</span>';

            echo apply_filters('woocommerce_stock_html', $availability_html, $availability['availability'], $product);
            ?>
        </div>
        <div class="cart-info">
            <input type="hidden" name="add-to-cart" value="<?php echo esc_attr($product->get_id()); ?>" />

            <button type="submit" class="button-btn"><?php echo esc_html($product->single_add_to_cart_text()); ?><i class="icon-basket-loaded"> </i></button>
        </div>
        <?php do_action('woocommerce_after_add_to_cart_button'); ?>
    </form>

    <?php do_action('woocommerce_after_add_to_cart_form'); ?>

<?php endif; ?>
