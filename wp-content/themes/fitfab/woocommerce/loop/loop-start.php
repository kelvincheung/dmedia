<?php
/**
 * Product Loop Start
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */
?>
<section class="cool-products fitfab-cool-product-layout-one">
    <div class="container">
        <div class="head-global">
            <h2 class="h2"><?php esc_html_e('cool products','fitfab'); ?></h2>
        </div>
        <div class="row">