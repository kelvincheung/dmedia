<?php
/**
 * Login form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.1.0
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

if (is_user_logged_in()) {
    return;
}
?>
<form method="post" class="login" <?php if ($hidden) echo 'style="display:none;"'; ?>>

<?php do_action('woocommerce_login_form_start'); ?>

    <?php if ($message) print($message); ?>
    <div class="form-group  grp-return clearfix">
        <div class="ffname">
            <label for="username"><?php esc_html_e('Username or email', 'fitfab'); ?> <span class="required">*</span></label>
            <input type="text" class="form-control input-text" name="username" id="username" />
        </div>
        <div class="llname">
            <label for="password"><?php esc_html_e('Password', 'fitfab'); ?> <span class="required">*</span></label>
            <input class="form-control input-text" type="password" name="password" id="password" />
        </div>
    </div>
    <div class="clear"></div>

<?php do_action('woocommerce_login_form'); ?>

    <div class="btn-group1 btn-btn-login">
<?php wp_nonce_field('woocommerce-login'); ?>
        <input type="submit" class="button-btn  submit-btn" name="login" value="<?php esc_attr_e('Login', 'fitfab'); ?>" />
        <input type="hidden" name="redirect" value="<?php echo esc_url($redirect) ?>" />
        <label for="rememberme" class="inline">
            <input name="rememberme" type="checkbox" id="rememberme" value="forever" /> <?php esc_html_e('Remember me', 'fitfab'); ?>
        </label>
    </div>
    <p class="lost_password">
        <a href="<?php echo esc_url(wp_lostpassword_url()); ?>"><?php esc_html_e('Lost your password?', 'fitfab'); ?></a>
    </p>

    <div class="clear"></div>

<?php do_action('woocommerce_login_form_end'); ?>

</form>
