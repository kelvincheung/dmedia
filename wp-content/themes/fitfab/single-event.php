<?php
/**
 * Fitfab - event detail
 * 
 * @package fitfab
 * @subpackage fitfab
 * @since fitfab 1.0.0
 */
get_header();
?>
<!--Content Area Start-->
<div id="content">
    <?php do_action("fitfab_banner_parallax", array("title" => get_the_title())); ?>
    <section class="main-event-content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8">
                    <?php
                    while (have_posts()) : the_post();
                        $part = "content/single/single";
                        get_template_part($part, "event");
                    endwhile;
                    wp_reset_postdata();
                    ?>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <?php get_sidebar('event'); ?>
                </div>
            </div>
        </div>
    </section>
</div>
<!--Content Area End-->
<?php
get_footer();
