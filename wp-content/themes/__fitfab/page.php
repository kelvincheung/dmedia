<?php
/**
 * Fitfab - Default page
 * 
 * @package fitfab
 * @subpackage fitfab
 * @since fitfab 1.0.0
 */
get_header();
?>
<!--Content Area Start-->
<div id="content">
    <?php do_action("fitfab_banner_parallax", array("title" => get_the_title())); ?>
    <section class="main-blog-content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8">
                    <?php
                    while (have_posts()) : the_post();
                        $format = 'standard';
                        get_template_part('content/format/' . $format);
                   endwhile;
                    ?>
		    <div class="comment-blog-details">
			    <?php
				if (comments_open() || get_comments_number()) :
				    comments_template();
			       endif;
			?>
		    </div>
            </div>
	    <?php
	    if (!is_page(array('cart', 'checkout'))) :
		get_sidebar();
	    endif;
	    ?>
        </div>
    </section>
</div>
<!--Content Area End-->
<?php
get_footer();
