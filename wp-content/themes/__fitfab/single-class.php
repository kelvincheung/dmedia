<?php
/**
 * FitFab - class detail page
 * 
 * @package fitfab
 * @subpackage fitfab
 * @since fitfab 1.0.0
 */
get_header();
global $class_detail;
$class_detail = 'class-detail';
?>
<!--Content Area Start-->
<div id="content">
    <?php do_action("fitfab_banner_parallax", array("title" => get_the_title())); ?>
    <div class="single-classes-page-wrap classes-page-wrap">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8">
                    <?php
                    if (have_posts()) :
                        while (have_posts()) : the_post();
                            get_template_part('content/class', 'page');
                        endwhile;
                    endif;
                    // Related Classes
                    get_template_part('content/related', 'classes');
                    ?>

                </div>
                <?php get_sidebar('class'); ?>
            </div>
        </div>
    </div>
</div>
<!--Content Area End-->
<?php
get_footer();
