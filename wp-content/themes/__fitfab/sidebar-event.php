<?php
/**
 * FitFab - event sidebar
 *
 * @package fitfab
 * @subpackage fitfab
 * @since fitfab 1.0.0
 */
while (have_posts()) : the_post();
    global $post;
    $EM_Event = em_get_event($post->ID, 'post_id');
    ?>
    <div class="map-event">
        <h3><?php esc_html_e('Events map', 'fitfab'); ?></h3>
        <?php echo $EM_Event->output('#_LOCATIONMAP'); ?>
    </div>

    <div class="address-event">
        <h3> <?php echo $EM_Event->output('#l'); ?>, <?php echo $EM_Event->output('#F'); ?> <?php echo $EM_Event->output('#j'); ?></h3>
        <address class="address-info1">
            <?php echo $EM_Event->output('#_LOCATIONADDRESS'); ?>
            <span><?php echo $EM_Event->output('#_LOCATIONNAME'); ?>, <?php echo esc_html($EM_Event->output('#_LOCATIONPOSTCODE')); ?></span>
        </address>
    </div>
    <?php
endwhile;
if (!is_active_sidebar('fitfab-fevent-widget-section')) {
    dynamic_sidebar('fitfab-event-widget-section');
}