<?php
/**
 * FitFab - footer layout
 * 
 * @package fitfab
 * @subpackage fitfab
 * @since fitfab 1.0.0
 */
?>
<?php do_action('fitfab_footer_layout'); ?>
</div>
<!--Page Wrapper End-->
<?php wp_footer(); ?>
</body>
</html>