<?php
/**
 * FitFab : Assign home page
 *
 * @package fitfab
 * @subpackage fitfab.inc.options.section
 * @since fitfab 1.0.0
 */

return array(
    'icon' => 'el el-th-large',
    'title' => esc_html__('Home Layout', 'fitfab'),
    'fields' => array(
        array(
            'id' => 'fitfab_home_1',
            'type' => 'select',
            'title' => esc_html__('Assign Home Page 1', 'fitfab'),
            'data' => 'pages',
            'desc' => esc_html__('Assign page for home 1', 'fitfab')
        ),
        array(
            'id' => 'fitfab_home_2',
            'type' => 'select',
            'title' => esc_html__('Assign Home Page 2', 'fitfab'),
            'data' => 'pages',
            'desc' => esc_html__('Assign page for home 2', 'fitfab')
        ),
        array(
            'id' => 'fitfab_home_3',
            'type' => 'select',
            'title' => esc_html__('Assign Home Page 3', 'fitfab'),
            'data' => 'pages',
            'desc' => esc_html__('Assign page for home 3', 'fitfab')
        ),
        array(
            'id' => 'fitfab_home_4',
            'type' => 'select',
            'title' => esc_html__('Assign Home Page 4', 'fitfab'),
            'data' => 'pages',
            'desc' => esc_html__('Assign page for home 4', 'fitfab')
        ),
        array(
            'id' => 'fitfab_home_layout',
            'type' => 'image_select',
            'compiler' => true,
            'desc' => esc_html__('Select Home Layout', 'fitfab'),
            'title' => esc_html__('Home Layout', 'fitfab'),
            'options' => array(
                'home1' => array('alt' => 'Home1', 'img' => ReduxFramework::$_url . 'assets/img/theme1.png'),
                'home2' => array('alt' => 'Home2', 'img' => ReduxFramework::$_url . 'assets/img/theme2.png'),
                'home3' => array('alt' => 'Home3', 'img' => ReduxFramework::$_url . 'assets/img/theme3.png'),
                'home4' => array('alt' => 'Home4', 'img' => ReduxFramework::$_url . 'assets/img/theme3.png'),
            ),
            'default' => 'header1'
        ),
    )
);
