<?php 
/**
 * FitFab : Theme Option for Contact Part
 * 
 * @package fitfab
 * @subpackage fitfab.inc.options.section
 * @since fitfab 1.0.0
 */
return array(
    'icon' => 'el-icon-map-marker',
    'title' => esc_html__('Contact', 'fitfab'),
    'fields' => array(
        array(
            'id' => 'fitfab_address_label',
            'type' => 'text',
            'title' => esc_html__('Address Label Text', 'fitfab'),
            'subtitle' => '',
            'desc' => esc_html__('Enter Address Label Text', 'fitfab')
        ),
        array(
            'id' => 'fitfab_address',
            'type' => 'textarea',
            'title' => esc_html__('Address', 'fitfab'),
            'subtitle' => '',
            'desc' => esc_html__('Enter Address Here', 'fitfab')
        ),
        array(
            'id' => 'fitfab_phone_no_label',
            'type' => 'text',
            'title' => esc_html__('Phone No Label Text', 'fitfab'),
            'subtitle' => '',
            'desc' => esc_html__('Enter Phone No Label Text', 'fitfab')
        ),
        array(
            'id' => 'fitfab_phone',
            'type' => 'text',
            'title' => esc_html__('Phone No.', 'fitfab'),
            'subtitle' => '',
            'desc' => esc_html__('Enter phone No.', 'fitfab')
        ),
        array(
            'id' => 'fitfab_email_label',
            'type' => 'text',
            'title' => esc_html__('Email Label Text', 'fitfab'),
            'subtitle' => '',
            'desc' => esc_html__('Enter Email Label Text', 'fitfab')
        ),
        array(
            'id' => 'fitfab_email',
            'type' => 'text',
            'title' => esc_html__('Email', 'fitfab'),
            'subtitle' => '',
            'desc' => esc_html__('Enter Email Here', 'fitfab')
        ),
        array(
            'id' => 'fitfab_latitude',
            'type' => 'text',
            'title' => esc_html__('Map Latitude', 'fitfab'),
            'subtitle' => '',
            'desc' => esc_html__('Map Latitude', 'fitfab')
        ),
        array(
            'id' => 'fitfab_longitude',
            'type' => 'text',
            'title' => esc_html__('Map Longitude', 'fitfab'),
            'subtitle' => '',
            'desc' => esc_html__('Map Longitude', 'fitfab')
        ),

    )
);
