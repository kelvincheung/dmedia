<?php
/**
 * FitFab : Advance
 *
 * @package fitfab
 * @subpackage fitfab.inc.options.section
 * @since fitfab 1.0.0
 */

return array(
    'title' => esc_html__('Advance', 'fitfab'),
    'id' => 'fitfab_advance',
    'desc' => '',
    'customizer_width' => '600px',
    'icon' => 'el-icon-home',
    'fields' => array(
    		array(
            'id' => 'fitfab_customcss',
            'type' => 'ace_editor',
            'mode' => 'css',
            'theme' => 'monokai',
            'title' => esc_html__('Custom CSS', 'fitfab'),
            'subtitle' => esc_html__('Quickly add some CSS to your theme by adding it to this block.', 'fitfab'),
            'desc' => '',
            'validate' => 'css',
            'default' => '',
        )
    )
);
