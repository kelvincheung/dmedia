<?php 
/**
 * FitFab : Theme Option for Footer Setting
 * 
 * @package fitfab
 * @subpackage fitfab.inc.options.section
 * @since fitfab 1.0.0
 */
return array(
    'icon' => 'el-icon-map-marker',
    'title' => esc_html__('Footer', 'fitfab'),
    'fields' => array(
        array(
                'id' => 'fitfab_copyrighttext',
                'type' => 'text',
                'title' => esc_html__('Copyright Text', 'fitfab'),
                'subtitle' => '',
                'desc' => ''
        ),
        array(
                'id' => 'fitfab_themedesign',
                'type' => 'text',
                'title' => esc_html__('Theme Design', 'fitfab'),
                'subtitle' => '',
                'desc' => ''
        ),
        array(
                'id' => 'fitfab_themedesignlink',
                'type' => 'text',
                'title' => esc_html__('Theme Design Link', 'fitfab'),
                'subtitle' => '',
                'desc' => ''
        ),

    )
);
