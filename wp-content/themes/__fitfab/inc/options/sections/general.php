<?php

/**
 * FitFab : General
 *
 * @package fitfab
 * @subpackage fitfab.inc.options.section
 * @since fitfab 1.0.0
 */
return array(
    'title' => esc_html__('General', 'fitfab'),
    'id' => 'general',
    'customizer_width' => '450px',
    'desc' => '',
    'icon' => 'el el-home',
    'fields' => array(
        array(
            'id' => 'fitfab_faviconurl',
            'type' => 'media',
            'preview' => false,
            'title' => esc_html__('Custom Favicon', 'fitfab'),
            'desc' => esc_html__('Upload a 16px x 16px Png/Gif image that will represent your website\'s favicon.', 'fitfab'),
            'subtitle' => '',
        ),
        array(
            'id' => 'fitfab_logo',
            'type' => 'media',
            'url' => true,
            'title' => esc_html__('Logo Image', 'fitfab'),
            'compiler' => 'true',
            'desc' => esc_html__('Basic media uploader with disabled URL input field.', 'fitfab'),
            'subtitle' => esc_html__('Upload any media using the WordPress native uploader', 'fitfab'),
            'default' => '',
        ),
        array(
            'id' => 'fitfab_logo_text',
            'type' => 'text',
            'url' => true,
            'title' => esc_html__('Logo Text', 'fitfab'),
            'compiler' => 'true',
            'desc' => esc_html__('Enter Logo Text', 'fitfab'),
        ),
        array(
            'id' => 'fitfab_opening_hours',
            'type' => 'text',
            'url' => true,
            'title' => esc_html__('Opening Hours', 'fitfab'),
            'compiler' => 'true',
            'desc' => esc_html__('Enter Opening Hours Here', 'fitfab'),
        ),
        array(
            'id' => 'fitfab_join_us_text',
            'type' => 'text',
            'title' => esc_html__('Join Us Text', 'fitfab'),
            'subtitle' => '',
            'desc' => esc_html__('Enter Join Us Text', 'fitfab')
        ),
        array(
            'id' => 'fitfab_join_us_link',
            'type' => 'text',
            'title' => esc_html__('Join Us Link', 'fitfab'),
            'subtitle' => '',
            'desc' => esc_html__('Enter Join Us Link', 'fitfab')
        ),
        array(
            'id' => 'fitfab_call_us_text',
            'type' => 'text',
            'title' => esc_html__('Call Us Text', 'fitfab'),
            'subtitle' => '',
            'desc' => esc_html__('Enter Call Us Text', 'fitfab')
        ),
        array(
            'id' => 'fitfab_phone_no',
            'type' => 'text',
            'title' => esc_html__('Phone No.', 'fitfab'),
            'subtitle' => '',
            'desc' => esc_html__('Eg. : 61 3 8376 6284', 'fitfab')
        ),
        array(
            'id' => 'fitfab_phone_link',
            'type' => 'text',
            'title' => esc_html__('Phone Link', 'fitfab'),
            'subtitle' => '',
            'desc' => esc_html__('Enter Phone Link', 'fitfab')
        ),
        array(
            'id' => 'fitfab_stickey_header',
            'type' => 'select',
            'title' => esc_html__('Stickey Header', 'fitfab'),
            'subtitle' => '',
            'desc' => '',
            'options' => array(
                'normal' => esc_html__('Normal', 'fitfab'),
                'fix' => esc_html__('Fixed', 'fitfab'),
                'intelligent' => esc_html__('Intelligent', 'fitfab'),
            ),
            'default' => 'normal'
        ),
        array(
            'id' => 'fitfab_theme_layout',
            'type' => 'select',
            'title' => esc_html__('Theme Layout', 'fitfab'),
            'subtitle' => '',
            'desc' => '',
            'options' => array(
                'full-width' => esc_html__('Full Width', 'fitfab'),
                'boxed' => esc_html__('Boxed', 'fitfab'),
            ),
            'default' => 'full-width'
        ),
        array(
            'id' => 'fitfab_page_header_bg_img',
            'type' => 'media',
            'title' => esc_html__('Page Banner Image', 'fitfab'),
            'desc' => '',
            'subtitle' => esc_html__('Upload your page banner image.', 'fitfab'),
        ),
        array(
            'id' => 'fitfab_boxing_img',
            'type' => 'media',
            'url' => true,
            'title' => esc_html__('Footer Boxing Image', 'fitfab'),
            'desc' => '',
            'subtitle' => esc_html__('Upload Footer Boxing Image.', 'fitfab'),
        ),
        array(
            'id' => 'fitfab_footer_img',
            'type' => 'media',
            'url' => true,
            'title' => esc_html__('Footer Pattern Image', 'fitfab'),
            'desc' => '',
            'subtitle' => esc_html__('Upload Footer Bg Image.', 'fitfab'),
        ),
    ),
);
