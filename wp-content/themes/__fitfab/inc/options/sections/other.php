<?php
/**
 * FitFab : comming soon enable/disable
 *
 * @package fitfab
 * @subpackage fitfab.inc.options.section
 * @since fitfab 1.0.0
 */

return array(
		'icon'      => 'el el-cogs',
		'title'     => esc_html__('Other', 'fitfab'),
		'fields'    => array(
			array(
				'id'        => 'fitfab_loader_status',
				'type'      => 'switch',
				'title'     => esc_html__('Loader Image', 'fitfab'),
				'subtitle'  => esc_html__('Loader image on/off.', 'fitfab'),
				'default'   => false,
			),
		)
	);
