<?php

/**
 * Fitfab comming soon enable/disable
 *
 * @package fitfab
 * @since fitfab 1.0.0
 */
return array(
    'icon' => 'el el-cogs',
    'title' => esc_html__('Coming Soon', 'fitfab'),
    'fields' => array(
        array(
            'id' => 'fitfab_comming_soon',
            'type' => 'switch',
            'title' => esc_html__('Coming Soon Mode', 'fitfab'),
            'subtitle' => '',
            'default' => false,
        ),
    		array(
            'id' => 'fitfab_comming_logo',
            'type' => 'media',
            'title' => esc_html__('Coming Soon Logo', 'fitfab'),
            'desc' => '',
            'subtitle' => esc_html__('Upload your Logo Image.', 'fitfab'),
        ),
        array(
            'id' => 'coming_soon_page_bg',
            'type' => 'media',
            'title' => esc_html__('Background Image', 'fitfab'),
            'desc' => '',
            'subtitle' => esc_html__('Insert Page Background Image from Here.', 'fitfab'),
        ),
    		array(
    				'id' => 'fitfab_coming_title',
    				'type' => 'text',
    				'title' => esc_html__('Coming soon title', 'fitfab'),
    				'desc' => esc_html__('Enter title of coming soon', 'fitfab')
    		),
    		array(
    				'id' => 'fitfab_coming_content',
    				'type' => 'textarea',
    				'title' => esc_html__('Content of coming soon', 'fitfab'),
    				'desc' => esc_html__('Enter Content of coming soon', 'fitfab')
    		),
    		
        array(
            'id' => 'fitfab_newsletter_comming',
            'type' => 'text',
            'title' => esc_html__('Contact Form Shortcode', 'fitfab'),
            'desc' => esc_html__('Contact Form Shortcode', 'fitfab')
        ),
        array(
            'id' => 'fitfab_copyright_comming',
            'type' => 'text',
            'title' => esc_html__('Copyright Text', 'fitfab'),
            'desc' => esc_html__('Enter Copyright Text for Footer', 'fitfab')
        ),
        array(
            'id' => 'fitfab_datepicker',
            'type' => 'date',
            'title' => esc_html__('Date Option', 'fitfab'),
            'subtitle' => esc_html__('No validation can be done on this field type', 'fitfab'),
            'desc' => esc_html__('Enter the date here.', 'fitfab')
        ),
    )
);
