<?php
/**
 * FitFab : Footer Layout
 *
 * @package fitfab
 * @subpackage fitfab.inc.options.section
 * @since fitfab 1.0.0
 */

return array(
    'icon' => 'el-icon-group',
    'title' => esc_html__('Footer Layout', 'fitfab'),
    'fields' => array(
        array(
            'id' => 'fitfab_footer_layout',
            'type' => 'image_select',
            'compiler' => true,
            'desc' => esc_html__('Select Footer Layout', 'fitfab'),
            'title' => esc_html__('Footer Layout', 'fitfab'),
            'options' => array(
                'footer1' => array('alt' => 'Footer1', 'img' => ReduxFramework::$_url . 'assets/img/footer1.png'),
                'footer2' => array('alt' => 'Footer2', 'img' => ReduxFramework::$_url . 'assets/img/footer2.png'),
                'footer3' => array('alt' => 'Footer3', 'img' => ReduxFramework::$_url . 'assets/img/footer3.png'),
                'footer4' => array('alt' => 'Footer4', 'img' => ReduxFramework::$_url . 'assets/img/footer4.png'),
            ),
            'default' => 'footer1'
        ),
    )
);

