<?php
/**
 * FitFab : Header Layout
 *
 * @package fitfab
 * @subpackage fitfab.inc.options.section
 * @since fitfab 1.0.0
 */

return array(
    'icon' => 'el-icon-indent-right',
    'title' => esc_html__('Header Layout', 'fitfab'),
    'fields' => array(
        array(
            'id' => 'fitfab_header_layout',
            'type' => 'image_select',
            'compiler' => true,
            'desc' => esc_html__('Select Header Layout', 'fitfab'),
            'title' => esc_html__('Header Layout', 'fitfab'),
            'options' => array(
                'header1' => array('alt' => 'Header1', 'img' => ReduxFramework::$_url . 'assets/img/header1.png'),
                'header2' => array('alt' => 'Header2', 'img' => ReduxFramework::$_url . 'assets/img/header2.png'),
                'header3' => array('alt' => 'Header3', 'img' => ReduxFramework::$_url . 'assets/img/header3.png'),
                'header4' => array('alt' => 'Header4', 'img' => ReduxFramework::$_url . 'assets/img/header4.png'),
            ),
            'default' => 'header2'
        ),
    )
);

