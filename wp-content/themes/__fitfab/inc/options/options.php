<?php
/**
 * FitFab - option field section
 *
 * @package fitfab
 * @subpackage fitfab.inc.options
 * @since fitfab 1.0.0
 */

$fitfab_opt_name = "fitfab_data";

add_filter('redux/options/' . $fitfab_opt_name . '/sections', 'fitfab_dynamic_sections');

function fitfab_dynamic_sections($sections) {
    $fitfab_opt_dir = "/inc/options/sections/";
    $args = array(
        "general", 
        "typograph",
        "home-layout",
        "header-layout",
        "footer-layout",
        "contact",
        "advance",
        "social",
    	"coming-soon",
        "footer",
    	"404",
    	"other",
    );

    foreach ($args as $val):
        $sections[] = fitfab_inclusion($fitfab_opt_dir . $val . ".php", false, false, true);
    endforeach;

    return $sections;
}
