<?php
/**
 * FitFab back compat functionality
 *
 * Prevents fitfab from running on WordPress versions prior to 4.1
 *
 * @package fitfab.inc
 * @since fitfab 1.0.0
 */

/**
 * Prevent switching to fitfab on old versions of WordPress.
 */
function fitfab_switch_theme() {
    switch_theme(WP_DEFAULT_THEME, WP_DEFAULT_THEME);
    unset($_GET['activated']);
    add_action('admin_notices', 'fitfab_upgrade_notice');
}

add_action('after_switch_theme', 'fitfab_switch_theme');

/**
 * Add message for unsuccessful theme switch.
 */
function fitfab_upgrade_notice() {
    $message = sprintf(esc_html__('fitfab requires at least WordPress version 4.1 You are running version','fitfab').' %s.'.esc_html__('Please upgrade and try again.', 'fitfab'), $GLOBALS['wp_version']);
    printf('<div class="error"><p>%s</p></div>', $message);
}

/**
 * Prevent the Theme Preview from being loaded on WordPress versions prior to 4.1
 */
function fitfab_preview() {
    if (isset($_GET['preview'])) {
        wp_die(sprintf(esc_html__('fitfab requires at least WordPress version 4.1 You are running version','fitfab').' %s. '.esc_html__('Please upgrade and try again.', 'fitfab'), $GLOBALS['wp_version']));
    }
}

add_action('template_redirect', 'fitfab_preview');
