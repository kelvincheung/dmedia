<?php
/**
 * FitFab -  Free Fitness
 *  
 * @package fitfab
 * @subpackage fitfab.inc.widgets
 * @since fitfab 1.0.0
 */
add_action('widgets_init', 'fitfab_free_fitness');

// Register Widget
function fitfab_free_fitness() {
    register_widget('fitfab_free_fitness_widget');
}

// Widget Class
class fitfab_free_fitness_widget extends WP_Widget {

    function __construct() {
        parent::__construct(
                // Base ID of your widget
                'free-fitness-widget',
                // Widget name will appear in UI
                esc_html__('(Fit & Fab) Free Fitness', 'fitfab'),
                // Widget description
                array('description' => esc_html__('A widget that displays the free fitness text', 'fitfab'),)
        );
    }

    public function widget($args, $instance) {
        $title = $instance['title'];
        $class_id = (!empty( $instance['class'] )) ? $instance['class'] : '';
        $button_text = $instance['button_text'];
        $button_link = (!empty($instance['number']) ) ? $instance['button_link'] : '#';

        $f = new WP_Query(array(
            'p' => $class_id,
            'post_type' => 'class',
            'orderby' => 'date',
            'order' => 'DESC',
            'ignore_sticky_posts' => 1,
        ));

        if ($f->have_posts()) :
            while ($f->have_posts()) : $f->the_post();
                ?>
                <div class="free-class-detail">
                    <?php
                    $url = wp_get_attachment_url(get_post_thumbnail_id());
                    if (esc_url($url)):
                        ?>
                        <figure>
                            <img src="<?php echo esc_url(fitfab_resize($url, '359', '262')); ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>">
                        </figure>
                <?php endif; ?>
                    <div class="classes-info-aside">
                        <?php if(!empty($title)) : ?>
                        <h2><?php echo wp_kses($title, wp_kses_allowed_html('post')); ?></h2>
                        <?php endif; ?>
                        <p><?php echo wp_trim_words(get_the_content(), 15, ''); ?></p>
                        <?php if (!empty($button_text)) : ?>
                            <a href="<?php echo esc_url($button_link); ?>" class="button-btn medium-btn"><?php echo esc_html($button_text); ?></a>
                <?php endif; ?>
                    </div>
                </div>
            <?php
            endwhile;
            wp_reset_postdata();
        endif;
        ?>

        <?php
    }

    // Update the widget
    public function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['title'] = $new_instance['title'];
        $instance['class'] = $new_instance['class'];
        $instance['button_text'] = sanitize_text_field($new_instance['button_text']);
        $button_link = sanitize_text_field($instance['button_link']);
        return $instance;
    }

    //Widget Settings
    public function form($instance) {
        $title = isset($instance['title']) ? $instance['title'] : '';
        $button_text = isset($instance['button_text']) ? $instance['button_text'] : '';
        $button_link = isset($instance['button_link']) ? $instance['button_link'] : '';
        $c = new WP_Query(array(
            'post_type' => 'class',
            'orderby' => 'date',
            'order' => 'DESC',
            'ignore_sticky_posts' => 1,
        ));
        $i = 0;
        $classes = array();
        if ($c->have_posts()) :
            while ($c->have_posts()) : $c->the_post();
                $classes[$i]['title'] = get_the_title();
                $classes[$i]['id'] = get_the_id();
                $i++;
            endwhile;
        endif;
        ?>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title:', 'fitfab') ?></label>
            <input type="text" class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" value="<?php echo esc_attr($title); ?>"/>
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('class')); ?>"><?php esc_html_e('Select Class:', 'fitfab'); ?></label>
            <select id="<?php echo esc_attr($this->get_field_id('class')); ?>" name="<?php echo esc_attr($this->get_field_name('class')); ?>">
                <option value="0"><?php esc_html_e('&mdash; Select &mdash;', 'fitfab'); ?></option>
                <?php foreach ($classes as $class) : ?>
                    <option value="<?php echo esc_attr($class['id']); ?>" <?php selected($instance['class'], $class['id']); ?>>
            <?php echo esc_html($class['title']); ?>
                    </option>
        <?php endforeach; ?>
            </select>
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('button_text')); ?>"><?php esc_html_e('Button Text:', 'fitfab') ?></label>
            <input type="text" class="widefat" id="<?php echo esc_attr($this->get_field_id('button_text')); ?>" name="<?php echo esc_attr($this->get_field_name('button_text')); ?>" value="<?php echo esc_attr($button_text); ?>"/>
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('button_link')); ?>"><?php esc_html_e('Button Link:','fitfab'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('button_link')); ?>" name="<?php echo esc_attr($this->get_field_name('button_link')); ?>" type="text" value="<?php echo esc_attr($button_link); ?>" />
            <?php
        }

    }
    