<?php
/**
 * FitFab -  Package Widgets
 *  
 * @package fitfab
 * @subpackage fitfab.inc.widgets
 * @since fitfab 1.0.0
 */
add_action('widgets_init', 'fitfab_package');

// Register Widget
function fitfab_package() {
    register_widget('fitfab_package_widget');
}

// Widget Class
class fitfab_package_widget extends WP_Widget {

    function __construct() {
        parent::__construct(
                // Base ID of your widget
                'package-widget',
                // Widget name will appear in UI
                esc_html__('(Fit & Fab) Package', 'fitfab'),
                // Widget description
                array('description' => esc_html__('A widget that displays package', 'fitfab'),)
        );
    }

    public function widget($args, $instance) {
        $title = (!empty($instance['title']) ) ? $instance['title'] : esc_html__('YOGA CLASSIC PACKAGE', 'fitfab');
        $package_id = (!empty( $instance['package'] )) ? $instance['package'] : '';
        /**
         * Filter the arguments for the package widget.
         *
         * @since 3.4.0
         *
         * @see WP_Query::get_posts()
         *
         * @param array $args An array of arguments used to retrieve the yoga classic package.
         */
        $p = new WP_Query(array(
            'post_type' => 'package',
            'p' => $package_id,
            'orderby' => 'date',
            'order' => 'DESC',
            'ignore_sticky_posts' => 1,
        ));

        if ($p->have_posts()) :
            ?>
            <div class="package-list">
                <?php if(!empty($title)) : ?>
                    <h3><?php echo wp_kses($title, wp_kses_allowed_html('post')); ?></h3>
                <?php endif;
                ?>
                <?php
                while ($p->have_posts()) : $p->the_post();
                    $terms = get_the_terms(get_the_id(), 'trainer');
                    $on_trainer = '';
                    if ($terms && !is_wp_error($terms)) :
                        $trainer = array();
                        foreach ($terms as $term) :
                            $trainer[] = $term->name;
                        endforeach;

                        $on_trainer = join(", ", $trainer);
                    endif;
                    $url = wp_get_attachment_url(get_post_thumbnail_id());
                    if (esc_url($url)):
                        ?>
                        <div class="zoom">
                            <figure>
                                <img src="<?php echo esc_url(fitfab_resize($url, '360', '260')); ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>">
                            </figure>
                        </div>
                    <?php endif;
                    ?>
                    <div class="package-info">
                        <div class="head-two">
                            <h3><?php the_title(); ?></h3>
                            <span><?php
                                if (!empty($on_trainer)) :
                                    echo esc_html_e('WITH ', 'fitfab');
                                    echo esc_html($on_trainer);
                                endif;
                                ?></span>
                        </div>
                        <?php
                        the_content();
                        ?>
                    </div>
                    <?php
		    if(function_exists('fitfab_rwmb_meta')){
                    $price_per_month = fitfab_rwmb_meta('fitfab_price_per_month');
                    $join_button_text = fitfab_rwmb_meta('fitfab_join_button_text');
                    $join_button_link = fitfab_rwmb_meta('fitfab_join_button_link') ? fitfab_rwmb_meta('fitfab_join_button_link') : '#';
		    }
                    if (!empty($price_per_month)) :
                        ?>
                        <div class="package-price">
                            <span><?php esc_html_e('$','fitfab'); ?><?php echo esc_html($price_per_month); ?><?php esc_html_e('*/M ','fitfab'); ?></span>
                            <?php if (!empty($join_button_text)) : ?>
                                <a href="<?php echo esc_url($join_button_link); ?>" class="button-btn"><?php echo esc_html($join_button_text); ?></a>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                <?php endwhile; ?>
            </div>
            <?php
            // Reset the global $the_post as this query will have stomped on it
            wp_reset_postdata();

        endif;
    }

    /**
     * @param array $new_instance
     * @param array $old_instance
     * @return array
     */
    public function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['title'] =$new_instance['title'];
        $instance['package'] = $new_instance['package'];
        return $instance;
    }

    /**
     * @param array $instance
     */
    public function form($instance) {
        $title = isset($instance['title']) ? $instance['title'] : '';
        $p = new WP_Query(array(
            'post_type' => 'package',
            'orderby' => 'date',
            'order' => 'DESC',
            'ignore_sticky_posts' => 1,
        ));
        $i = 0;
        $packages = array();
        if ($p->have_posts()) :
            while ($p->have_posts()) : $p->the_post();
                $packages[$i]['title'] = get_the_title();
                $packages[$i]['id'] = get_the_id();
                $i++;
            endwhile;
        endif;
        ?>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title:', 'fitfab') ?></label>
            <input type="text" class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" value="<?php echo esc_attr($title); ?>"/>
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('package')); ?>"><?php esc_html_e('Select Package:', 'fitfab'); ?></label>
            <select id="<?php echo esc_attr($this->get_field_id('package')); ?>" name="<?php echo esc_attr($this->get_field_name('package')); ?>">
                <option value="0"><?php esc_html_e('&mdash; Select &mdash;', 'fitfab'); ?></option>
                <?php foreach ($packages as $package) : ?>
                    <option value="<?php echo esc_attr($package['id']); ?>" <?php selected($instance['package'], $package['id']); ?>>
            <?php echo esc_html($package['title']); ?>
                    </option>
        <?php endforeach; ?>
            </select>
        </p>
        <?php
    }

}
