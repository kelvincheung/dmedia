<?php
/**
 * FitFab -  Class Posts Widget
 *  
 * @package fitfab
 * @subpackage fitfab.inc.widgets
 * @since fitfab 1.0.0
 */

add_action( 'widgets_init', 'fitfab_class_posts_widget' );  

// Register Widget
function fitfab_class_posts_widget() {
    register_widget( 'fitfab_class_widget' );
}

// Widget Class
class fitfab_class_widget extends WP_Widget {

    function __construct() {
        parent::__construct(
        // Base ID of your widget
        'class-posts-widget', 

        // Widget name will appear in UI
        esc_html__('(fitfab) Class Posts', 'fitfab'), 

        // Widget description
        array( 'description' => esc_html__( 'A widget that displays the class posts of your blog', 'fitfab' ), ) 
        );
    }
	
	public function widget( $args, $instance ) {
		extract( $args );
		
		//Our variables from the widget settings.
		$title = isset($instance['title']) ? $instance['title'] : '';
		$posts = isset($instance['posts']) ? $instance['posts'] : '';
		?>
		<!-- START WIDGET -->
        <h4><?php echo wp_kses($title, wp_kses_allowed_html('post')); ?></h4>
		<ul class="class-list clearfix">
			<?php
				//query_posts( array('post_type' => 'class', 'orderby' => 'date', 'order' => 'DESC', 'ignore_sticky_posts' => 1, 'showposts' => $posts) );
			$latestQuery = new WP_Query(apply_filters('widget_posts_args', array(
					'post_type' => 'class',
					'orderby' => 'date', 
					'order' => 'DESC', 
					'ignore_sticky_posts' => 1, 
					'post_status' => 'publish',
					'showposts' => $posts
			)));
			if($latestQuery->have_posts()) : while ($latestQuery->have_posts()) : $latestQuery->the_post(); ?>
				<li>
                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                       <?php the_title(); ?>
                    </a>
				</li>
			<?php endwhile; ?>
			<?php endif; ?>
			<?php wp_reset_postdata(); ?>
		</ul>
		<!-- END WIDGET -->
		<?php
	}
	
	// Update the widget
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		
		$instance['title'] = $new_instance['title'];
		$instance['posts'] = $new_instance['posts'];
		return $instance;
	}

	//Widget Settings
	public function form( $instance ) {
		//Set up some default widget settings.
		$defaults = array(
			'title' => esc_html__('Class Posts', 'fitfab'),
			'posts' => 4,
		);
		$instance = wp_parse_args( (array) $instance, $defaults );

		// Widget Title: Text Input
		?>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'title' )); ?>"><?php esc_html_e('Title:', 'fitfab'); ?></label>
			<input id="<?php echo esc_attr($this->get_field_id( 'title' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'title' )); ?>" value="<?php if(!empty($instance['title'])) { echo esc_attr($instance['title']); } ?>" class="widefat" type="text" />
		</p>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'posts' )); ?>"><?php esc_html_e('Number of posts to show:','fitfab'); ?></label>
			<input id="<?php echo esc_attr($this->get_field_id( 'posts' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'posts' )); ?>" value="<?php echo intval( $instance['posts'] ); ?>" class="widefat" type="text" />
		</p>
		<?php
	}
}
?>
