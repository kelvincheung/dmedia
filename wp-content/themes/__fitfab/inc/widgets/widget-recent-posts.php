<?php
/**
 * FitFab -  Recent Posts Widget
 *  
 * @package fitfab
 * @subpackage fitfab.inc.widgets
 * @since fitfab 1.0.0
 */

add_action('widgets_init', 'fitfab_recent_posts_widget');

// Register Widget
function fitfab_recent_posts_widget() {
    register_widget('fitfab_recent_widget');
}

// Widget Class
class fitfab_recent_widget extends WP_Widget {

    function __construct() {
        parent::__construct(
                // Base ID of your widget
                'recent-posts-widget',
                // Widget name will appear in UI
                esc_html__('(Fit & Fab) Recent Posts', 'fitfab'),
                // Widget description
                array('description' => esc_html__('A widget that displays the recent posts of your blog', 'fitfab'),)
        );
    }

    public function widget($args, $instance) {
        extract($args);
        $title = (!empty($instance['title']) ) ? $instance['title'] : esc_html__('Recent Posts', 'fitfab');
        //Our variables from the widget settings.
        $title = apply_filters('widget_title', $instance['title']);
        $posts = isset($instance['posts']) ? $instance['posts'] : '3';
        $show_thumb = isset($instance['show_thumb']) ? (int) $instance['show_thumb'] : '';
        $show_author = isset($instance['show_author']) ? (int) $instance['show_author'] : '';
        $show_date = isset($instance['show_date']) ? (int) $instance['show_date'] : '';
        ?>
        <!-- START WIDGET -->
        <div class="recent-post-blog">
            <?php
            if ($title) : ?>
            <h3><?php echo wp_kses($title, wp_kses_allowed_html('post')); ?></h3>
            <?php endif; ?>
            <ul>
                <?php
                $recentQuery = new WP_Query(apply_filters('widget_posts_args', array(
                		'posts_per_page' => $posts,
                		'no_found_rows' => true,
                		'post_status' => 'publish',
                		'ignore_sticky_posts' => true
                )));
                if ($recentQuery->have_posts()) : while ($recentQuery->have_posts()) : $recentQuery->the_post();
                        ?>
                        <li class="zoom">
                            <?php if ($show_thumb == 1) { ?>
                                <?php if (has_post_thumbnail()): ?>
                                    <figure>
                                        <a class="featured-thumbnail widgetthumb" href='<?php the_permalink(); ?>'>
                                            <?php the_post_thumbnail('widget'); ?>
                                        </a>
                                    </figure>
                                <?php endif; ?>
                            <?php }
                            ?>                    
                            <div  class="recent-info">
                                <h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
                                <span>
                                    <?php if ($show_author == 1) { ?>
                                        <span class="post-author"> <?php the_author_posts_link(); ?></span>
                                    <?php } ?>
                                    <?php if ($show_date == 1) { ?>
                                        <time datetime="<?php echo esc_attr(get_the_date('c')); ?>"> <?php the_time(get_option('date_format')); ?></time>
                                    <?php } ?>
                                </span>
                            </div>
                        </li>
                    <?php endwhile; ?>
                <?php endif; ?>
                <?php wp_reset_postdata(); ?>
            </ul>
        </div>
        <!-- END WIDGET -->
        <?php
    }

    // Update the widget
    public function update($new_instance, $old_instance) {
        $instance = $old_instance;

        $instance['title'] = strip_tags($new_instance['title']);
        $instance['posts'] = $new_instance['posts'];
        $instance['show_thumb'] = intval($new_instance['show_thumb']);
        $instance['show_author'] = intval($new_instance['show_author']);
        $instance['show_date'] = intval($new_instance['show_date']);
        return $instance;
    }

    //Widget Settings
    public function form($instance) {
        //Set up some default widget settings.
        $defaults = array(
            'title' => esc_html__('Recent Posts', 'fitfab'),
            'posts' => 4,
            'show_thumb' => 1,
            'show_author' => 0,
            'show_date' => 1,
        );
        $instance = wp_parse_args((array) $instance, $defaults);
        $show_thumb = isset($instance['show_thumb']) ? $instance['show_thumb'] : 1;
        $show_author = isset($instance['show_author']) ? $instance['show_author'] : 1;
        $show_date = isset($instance['show_date']) ? $instance['show_date'] : 1;

        // Widget Title: Text Input
        ?>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title:', 'fitfab'); ?></label>
            <input id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" value="<?php
            if (!empty($instance['title'])) {
                echo esc_html($instance['title']);
            }
            ?>" class="widefat" type="text" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('posts')); ?>"><?php esc_html_e('Number of posts to show:', 'fitfab'); ?></label>
            <input id="<?php echo esc_attr($this->get_field_id('posts')); ?>" name="<?php echo esc_attr($this->get_field_name('posts')); ?>" value="<?php echo intval($instance['posts']); ?>" class="widefat" type="text" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id("show_thumb")); ?>">
                <input type="checkbox" class="checkbox" id="<?php echo esc_attr($this->get_field_id("show_thumb")); ?>" name="<?php echo esc_attr($this->get_field_name("show_thumb")); ?>" value="1" <?php
                if (isset($instance['show_thumb'])) {
                    checked(1, $instance['show_thumb'], true);
                }
                ?> />
                       <?php esc_html_e('Show Thumbnails', 'fitfab'); ?>
            </label>
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id("show_author")); ?>">
                <input type="checkbox" class="checkbox" id="<?php echo esc_attr($this->get_field_id("show_author")); ?>" name="<?php echo esc_attr($this->get_field_name("show_author")); ?>" value="1" <?php
                if (isset($instance['show_author'])) {
                    checked(1, $instance['show_author'], true);
                }
                ?> />
                       <?php esc_html_e('Show Post Author', 'fitfab'); ?>
            </label>
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id("show_date")); ?>">
                <input type="checkbox" class="checkbox" id="<?php echo esc_attr($this->get_field_id("show_date")); ?>" name="<?php echo esc_attr($this->get_field_name("show_date")); ?>" value="1" <?php
                if (isset($instance['show_date'])) {
                    checked(1, $instance['show_date'], true);
                }
                ?> />
                       <?php esc_html_e('Show Post Date', 'fitfab'); ?>
            </label>
        </p>
        <?php
    }

}
?>
