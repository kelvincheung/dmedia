<?php
/**
 * FitFab -  Footer Address Widget
 *  
 * @package fitfab
 * @subpackage fitfab.inc.widgets
 * @since fitfab 1.0.0
 */

add_action('widgets_init', 'fitfab_footer_address_widget');

// Footer Address Widget
function fitfab_footer_address_widget() {
    register_widget('fitfab_footer_address_widget');
}

class fitfab_footer_address_widget extends WP_Widget {

    function __construct() {
        parent::__construct(
                // Base ID of your widget
                'footer-address-widget',
                // Widget name will appear in UI
                esc_html__('(Fit & Fab) Footer Address', 'fitfab'),
                // Widget description
                array('description' => esc_html__('A widget that displays the footer address and email', 'fitfab'),)
        );
    }

    public function widget($args, $instance) {

        /** This filter is documented in wp-includes/widgets/class-wp-widget-pages.php */
        $email = apply_filters('widget_title', empty($instance['email']) ? '' : $instance['email'], $instance, $this->id_base);

        $widget_text = !empty($instance['text']) ? $instance['text'] : '';

        /**
         * Filter the content of the Text widget.
         *
         * @since 2.3.0
         * @since 4.4.0 Added the `$this` parameter.
         *
         * @param string         $widget_text The widget content.
         * @param array          $instance    Array of settings for the current widget.
         * @param WP_Widget_Text $this        Current Text widget instance.
         */
        $text = apply_filters('widget_text', $widget_text, $instance, $this);
        ?>
        <p>
            <?php echo!empty($instance['filter']) ? $text : $text; ?>
        </p>
        <span class="mail-id"><?php esc_html_e('Email :', 'fitfab') ?>  <a href="mailto:<?php echo esc_attr($email); ?>"><?php echo esc_html($email); ?></a></span>
        <?php
    }

    // Update the widget
    public function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['email'] = sanitize_text_field($new_instance['email']);
        if (current_user_can('unfiltered_html'))
            $instance['text'] = $new_instance['text'];
        else
            $instance['text'] = wp_kses_post(stripslashes($new_instance['text']));
        $instance['filter'] = !empty($new_instance['filter']);
        return $instance;
    }

    //Widget Settings
    public function form($instance) {
        $instance = wp_parse_args((array) $instance, array('email' => '', 'text' => ''));
        $filter = isset($instance['filter']) ? $instance['filter'] : 0;
        $email = sanitize_text_field($instance['email']);
        ?>
        <p><label for="<?php echo esc_attr($this->get_field_id('$email')); ?>"><?php esc_html_e('Email:','fitfab'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('$email')); ?>" name="<?php echo esc_attr($this->get_field_name('email')); ?>" type="text" value="<?php echo esc_attr($email); ?>" /></p>

        <p><label for="<?php echo esc_attr($this->get_field_id('text')); ?>"><?php esc_html_e('Content:','fitfab'); ?></label>
            <textarea class="widefat" rows="16" cols="20" id="<?php echo esc_attr($this->get_field_id('text')); ?>" name="<?php echo esc_attr($this->get_field_name('text')); ?>"><?php echo esc_textarea($instance['text']); ?></textarea></p>

        <p><input id="<?php echo esc_attr($this->get_field_id('filter')); ?>" name="<?php echo esc_attr($this->get_field_name('filter')); ?>" type="checkbox"<?php checked($filter); ?> />&nbsp;<label for="<?php echo esc_attr($this->get_field_id('filter')); ?>"><?php esc_html_e('Automatically add paragraphs','fitfab'); ?></label></p>
        <?php
    }

}
