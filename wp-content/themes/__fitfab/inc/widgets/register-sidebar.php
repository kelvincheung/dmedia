<?php

/**
 * FitFab - Register Sidebar
 * 
 *  Register Sidebar  page sidebar/ footer sibar 
 *
 * @package fitfab
 * @subpackage fitfab.inc.widgets 
 * @since fitfab 1.0.0
 */
function fitfab_widgets_init() {

    register_sidebar(array(
        'name' => esc_html__('FitFab Sidebar', 'fitfab'),
        'id' => 'fitfab-sidebar-1',
        'description' => '',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>'
    ));
    register_sidebar(array(
        'name' => esc_html__('FitFab Sidebar Class', 'fitfab'),
        'id' => 'fitfab-sidebar-class',
        'description' => '',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>'
    ));
    register_sidebar(array(
        'name' => esc_html__('Fitfab Sidebar Shop', 'fitfab'),
        'id' => 'fitfab-sidebar-shop',
        'description' => '',
        'before_widget' => '<div id="%1$s" class="title-wrap shop-title-wrap widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h5>',
        'after_title' => '</h5>'
    ));

    register_sidebar(
            array(
                'name' => esc_html__('FitFab Event Widget Section', 'fitfab'),
                'id' => 'fitfab-event-widget-section',
                'description' => esc_html__('Event widgets Section','fitfab'),
                'class' => '',
                'before_widget' => '<div class="text-widget">',
                'after_widget' => '</div>',
                'before_title' => '<h3>',
                'after_title' => '</h3>'
            )
    );

    $sidebars = array(1, 2, 3, 4);

    foreach ($sidebars as $number) {
        register_sidebar(array(
            'name' => esc_html__('FitFab Footer', 'fitfab') . ' ' . $number,
            'id' => 'fitfab-footer-' . $number,
            'description' => esc_html__('This widget area appears on footer of theme.', 'fitfab'),
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>'
        ));
    }
}

add_action('widgets_init', 'fitfab_widgets_init');

