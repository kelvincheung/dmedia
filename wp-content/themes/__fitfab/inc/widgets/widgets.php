<?php
/**
 * FitFab -  Widgets
 *  
 * Includes widgets/sidebar 
 * @package fitfab
 * @subpackage fitfab.inc.widgets
 * @since fitfab 1.0.0
 */

fitfab_inclusion('inc/widgets/register-sidebar.php');
fitfab_inclusion('inc/widgets/widget-class-posts.php');
fitfab_inclusion('inc/widgets/widget-recent-posts.php');
fitfab_inclusion('inc/widgets/widget-footer-address.php');
fitfab_inclusion('inc/widgets/widget-fitness.php');
fitfab_inclusion('inc/widgets/widget-package.php');
fitfab_inclusion('inc/widgets/fitfab-event-widget.php');
fitfab_inclusion('inc/widgets/search-widget.php');


function fitfab_tag_cloud_widget($args) {
    $args['smallest'] = '14';
    $args['largest'] = '24';
    $args['unit'] = 'px';
    return $args;
}
add_filter('widget_tag_cloud_args', 'fitfab_tag_cloud_widget');

