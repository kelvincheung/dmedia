<?php

/**
 * FitFab -  Search Widget 
 *  
 * @package fitfab
 * @subpackage fitfab.inc.widgets
 * @since fitfab 1.0.0
 */

add_action('widgets_init', 'fitfab_search_widget');

// Footer Address Widget
function fitfab_search_widget() {
	register_widget('Widget_Search');
}

class Widget_Search extends WP_Widget {

	public function __construct() {
		$widget_ops = array('classname' => 'fitfab_widget_search', 'description' => esc_html__( "A search form for your site.", 'fitfab') );
		parent::__construct( 'search', esc_html__( 'FitFab Search', 'fitfab' ), $widget_ops );
	}

	/**
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		/** This filter is documented in wp-includes/default-widgets.php */
		$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );

		print($args['before_widget']);
		if ( $title ) {
			print($args['before_title'] . $title . $args['after_title']);
		}

		// Use current theme search form if it exists
        ?>
        <form accept-charset="utf-8" method="get" action="<?php echo home_url( '/' ); ?>">
          <input type="text" id="search-blog" placeholder="search here" name="s" value="<?php echo get_search_query() ?>">
          <button class="btn sub-search-blog">
           <i class="fa fa-search"></i>
          </button>
         </form>
        <?php
		print($args['after_widget']);
	}

	/**
	 * @param array $instance
	 */
	public function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, array( 'title' => '') );
		$title = $instance['title'];
?>
		<p><label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title:', 'fitfab'); ?> <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></label></p>
<?php
	}

	/**
	 * @param array $new_instance
	 * @param array $old_instance
	 * @return array
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$new_instance = wp_parse_args((array) $new_instance, array( 'title' => ''));
		$instance['title'] = strip_tags($new_instance['title']);
		return $instance;
	}

}


