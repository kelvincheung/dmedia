<?php
/**
 * FitFab - Comment List
 * 
 * This template is comment reply & list walker 
 *
 * @package fitfab
 * @subpackage fitfab.inc
 * @since fitfab 1.0.0
 */
if (!function_exists('fitfab_comment')) {

    /**
     * Comment callback
     * 
     * @param  $comment
     * @param  $args
     * @param  $depth
     */
    function fitfab_comment($comment, $args, $depth) {
        $GLOBALS['comment'] = $comment;
        extract($args, EXTR_SKIP);
        $subcls = '';
        if ($depth > 1) :
            $subcls = 'sub-comment';
        endif;
        ?>
        <div <?php comment_class(empty($args['has_children']) ? 'author-comment reply-author clearfix ' . $subcls : 'parent author-comment reply-author clearfix ' . $subcls) ?> id="comment-<?php comment_ID() ?>">
            <?php if ('div' == $args['style']) : ?>
                <div id="div-comment-<?php comment_ID() ?>" class="comment-body">
                <?php endif; ?>
                <div class="comment-author vcard">
                    <figure>
                        <?php if ($args['avatar_size'] != 0) echo get_avatar($comment->comment_author_email, 120); ?>
                    </figure>
                    <div class="comment-info">
                        <h3><?php echo get_comment_author_link(); ?> <span><?php echo get_comment_date(); ?></span><small><?php echo human_time_diff(get_comment_time('U')); ?> <?php esc_html_e('ago', 'fitfab');?></small></h3>
                        <?php edit_comment_link(esc_html__('(Edit)', 'fitfab'), '  ', ''); ?>

                        <?php if ($comment->comment_approved == '0') : ?>
                            <em class="comment-awaiting-moderation"><?php esc_html_e('Your comment is awaiting moderation.', 'fitfab') ?></em>
                            <br />
                        <?php endif; ?>
                        <?php comment_text() ?> 
                        <?php
                        $myclass = 'button-btn button-small-r';
                        echo preg_replace('/comment-reply-link/', $myclass, get_comment_reply_link(array_merge($args, array('depth' => $depth, 'max_depth' => $args['max_depth'], 'reply_text' => esc_html__('reply', 'fitfab')))), 1);
                        ?>
                    </div>
                </div>
            </div>
            <?php
        }

    }
