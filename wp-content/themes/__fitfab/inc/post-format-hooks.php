<?php

/**
 * FitFab post format hooks
 *
 * @package fitfab
 * @subpackage fitfab.inc 
 * @since fitfab 1.0.0
 */
class FitfabPostFormatHooks {

    public function __construct() {
        $this->action();
    }

    public function action() {
        add_action("fitfab_post_format_attribute", array(&$this, 'fitfabFormatAttribute'));
    }

    function fitfabFormatAttribute() {
       if(get_the_ID()){
        ?>
        <span>
            <a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID'))); ?>"><?php the_author(); ?></a>
            <span><a href="<?php echo esc_url(home_url('/').get_the_date('Y/m')); ?>"><?php echo get_the_date('F d Y',get_the_ID()); ?></a></span> 
	    <?php
	    if(comments_open(get_the_ID())): ?>
            <span><a href="<?php echo esc_url(get_permalink()); ?>#comments"><?php comments_number('<i>(0)</i> Comment', '<i> (1) </i> Comment', '<i>(%)</i> Comments'); ?></a></span>
	    <?php endif; ?>
        </span>  
        <?php
	}
    }

}

new FitfabPostFormatHooks();
