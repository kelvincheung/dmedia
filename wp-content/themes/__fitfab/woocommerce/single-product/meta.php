<?php
/**
 * Single Product Meta
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post, $product;

$cat_count = sizeof( get_the_terms( $post->ID, 'product_cat' ) );
$tag_count = sizeof( get_the_terms( $post->ID, 'product_tag' ) );

?>
<ul class="category-content clearfix">

	<?php do_action( 'woocommerce_product_meta_start' ); ?>

	<?php if ( wc_product_sku_enabled() && ( $product->get_sku() || $product->is_type( 'variable' ) ) ) : ?>

    <span class="sku_wrapper"><li><strong><?php esc_html_e( 'SKU:', 'fitfab' ); ?></strong> <span class="sku" itemprop="sku"><?php echo ( $sku = $product->get_sku() ) ? $sku : esc_html__( 'N/A', 'fitfab' ); ?></span></span></li>

	<?php endif; ?>

	<?php echo $product->get_categories( ', ', '<li>' . _n( '<strong>Category:</strong><span>', '<strong>Categories:</strong> <span>', $cat_count, 'fitfab' ) . ' ', '</span></li>' ); ?>

	<?php echo $product->get_tags( ', ', '<li>' . _n( '<strong>Tag:</strong><span>', '<strong>Tags:</strong> <span>', $tag_count, 'fitfab' ) . ' ', '</span></li>' ); ?>

	<?php do_action( 'woocommerce_product_meta_end' ); ?>

</ul>
