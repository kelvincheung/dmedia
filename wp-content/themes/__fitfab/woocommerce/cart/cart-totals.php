<?php
/**
 * Cart totals
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.6
 */
if (!defined('ABSPATH')) {
    exit;
}
?>
<div class="col-sm-6">
    <?php if (is_cart()) : ?>
        <?php woocommerce_shipping_calculator(); ?>
    <?php endif; ?>
</div>
<div class="col-sm-6">
    <div class="total-cost">
        <div class="cart_totals <?php if (WC()->customer->has_calculated_shipping()) echo 'calculated_shipping'; ?>">

            <?php do_action('woocommerce_before_cart_totals'); ?>

            <h3 class="block-heading"><?php esc_html_e('Cart Totals', 'fitfab'); ?></h3>

            <ul>

                <li>
                    <span><?php esc_html_e('Subtotal', 'fitfab'); ?></span>
                    <?php wc_cart_totals_subtotal_html(); ?>
                </li>

                <?php foreach (WC()->cart->get_coupons() as $code => $coupon) : ?>
                    <li class="cart-discount coupon-<?php echo esc_attr(sanitize_title($code)); ?>">
                        <span><?php wc_cart_totals_coupon_label($coupon); ?></span>
                        <span><?php wc_cart_totals_coupon_html($coupon); ?></span>
                    </li>
                <?php endforeach; ?>

                <?php if (WC()->cart->needs_shipping() && WC()->cart->show_shipping()) : ?>

                    <?php do_action('woocommerce_cart_totals_before_shipping'); ?>

                    <?php wc_cart_totals_shipping_html(); ?>

                    <?php do_action('woocommerce_cart_totals_after_shipping'); ?>

                <?php elseif (WC()->cart->needs_shipping()) : ?>

                    <li class="shipping">
                        <span><?php esc_html_e('Shipping ', 'fitfab'); ?></span>
                        <span><?php woocommerce_shipping_calculator(); ?></span>
                    </li>

                <?php endif; ?>

                <?php foreach (WC()->cart->get_fees() as $fee) : ?>
                    <li class="fee">
                        <span><?php echo esc_html($fee->name); ?></span>
                        <span><?php wc_cart_totals_fee_html($fee); ?></span>
                    </li>
                <?php endforeach; ?>

                <?php if (wc_tax_enabled() && WC()->cart->tax_display_cart == 'excl') : ?>
                    <?php if (get_option('woocommerce_tax_total_display') == 'itemized') : ?>
                        <?php foreach (WC()->cart->get_tax_totals() as $code => $tax) : ?>
                            <li class="tax-rate tax-rate-<?php echo sanitize_title($code); ?>">
                                <span><?php echo esc_html($tax->label); ?></span>
                                <span><?php echo wp_kses_post($tax->formatted_amount); ?></span>
                            </li>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <li class="tax-total">
                            <span><?php echo esc_html(WC()->countries->tax_or_vat()); ?></span>
                            <span><?php wc_cart_totals_taxes_total_html(); ?></td>
                        </li>
                    <?php endif; ?>
                <?php endif; ?>

                <?php do_action('woocommerce_cart_totals_before_order_total'); ?>

                <li class="order-total">
                    <span><?php esc_html_e('Total', 'fitfab'); ?></span>
                    <span><?php wc_cart_totals_order_total_html(); ?></span>
                </li>

                <?php do_action('woocommerce_cart_totals_after_order_total'); ?>

            </ul>

            <?php if (WC()->cart->get_cart_tax()) : ?>
                <p class="wc-cart-shipping-notice"><small><?php
                        $estimated_text = WC()->customer->is_customer_outside_base() && !WC()->customer->has_calculated_shipping() ? sprintf(' ' . esc_html__(' (taxes estimated for %s)', 'fitfab'), WC()->countries->estimated_for_prefix() . (WC()->countries->countries[WC()->countries->get_base_country()])) : '';

                        printf(esc_html__('Note: Shipping and taxes are estimated%s and will be updated during checkout based on your billing and shipping information.', 'fitfab'), $estimated_text);
                        ?></small></p>
            <?php endif; ?>

            <div class="wc-proceed-to-checkout">

                <?php do_action('woocommerce_proceed_to_checkout'); ?>

            </div>

            <?php do_action('woocommerce_after_cart_totals'); ?>

        </div>
    </div>
</div>
