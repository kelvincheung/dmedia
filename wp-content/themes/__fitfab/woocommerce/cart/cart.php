<?php
/**
 * Cart Page
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.3.8
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

wc_print_notices();

do_action('woocommerce_before_cart');
global $woocommerce;
?>
<div class="row">
    <div class="col-sm-12">
        <?php $cart_count = "you have " . $woocommerce->cart->cart_contents_count . " item in your cart"; ?>
        <h2 class="total-cart"><i class="fa fa-shopping-cart"></i> <?php echo esc_html($cart_count); ?></h2>
    </div>
    <form action="<?php echo esc_url(WC()->cart->get_cart_url()); ?>" method="post">

        <?php do_action('woocommerce_before_cart_table'); ?>

        <div class="col-sm-8">
            <div class="cart-wrap">
                <ul class="head-title">
                    <li class="block-1">
                        <?php esc_html_e('Product', 'fitfab'); ?>
                    </li>
                    <li class="block-2">
                        <?php esc_html_e('Price', 'fitfab'); ?>
                    </li>
                    <li class="block-3">
                        <?php esc_html_e('Quantity', 'fitfab'); ?>
                    </li>
                    <li class="block-4">
                        <?php esc_html_e('Total', 'fitfab'); ?>
                    </li>
                    <li class="block-5"></li>
                </ul>
                <?php do_action('woocommerce_before_cart_contents'); ?>
                <?php
                foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
                    $_product = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
                    $product_id = apply_filters('woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key);

                    if ($_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters('woocommerce_cart_item_visible', true, $cart_item, $cart_item_key)) {
                        ?>
                        <ul class="product-cart">
                            <li class="block-1" data-product="product">
                                <div class="product-pic">
                                    <?php
                                    $thumbnail = apply_filters('woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key);

                                    if (!$_product->is_visible()) {
                                        print($thumbnail);
                                    } else {
                                        printf('<a href="%s">%s</a>', esc_url($_product->get_permalink($cart_item)), $thumbnail);
                                    }
                                    ?>
                                </div>
                                <h2 class="product-name">
                                    <?php
                                    if (!$_product->is_visible()) {
                                        echo apply_filters('woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key) . '&nbsp;';
                                    } else {
                                        echo apply_filters('woocommerce_cart_item_name', sprintf('<a href="%s">%s </a>', esc_url($_product->get_permalink($cart_item)), $_product->get_title()), $cart_item, $cart_item_key);
                                    }

                                    // Meta data
                                    echo WC()->cart->get_item_data($cart_item);

                                    // Backorder notification
                                    if ($_product->backorders_require_notification() && $_product->is_on_backorder($cart_item['quantity'])) {
                                        echo '<p class="backorder_notification">' . esc_html__('Available on backorder', 'fitfab') . '</p>';
                                    }
                                    ?>
                                </h2>
                            </li>
                            <li class="block-2" data-price="price">
                                <?php
                                echo apply_filters('woocommerce_cart_item_price', WC()->cart->get_product_price($_product), $cart_item, $cart_item_key);
                                ?>
                            </li>
                            <li class="block-3" data-quantity="quantity">
                                <?php
                                if ($_product->is_sold_individually()) {
                                    $product_quantity = sprintf('1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key);
                                } else {
                                    $product_quantity = woocommerce_quantity_input(array(
                                        'input_name' => "cart[{$cart_item_key}][qty]",
                                        'input_value' => $cart_item['quantity'],
                                        'max_value' => $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(),
                                        'min_value' => '0'
                                            ), $_product, false);
                                }

                                echo apply_filters('woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item);
                                ?>
                            </li>
                            <li class="block-4" data-total="total">
                                <?php
                                echo apply_filters('woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal($_product, $cart_item['quantity']), $cart_item, $cart_item_key);
                                ?>
                            </li>
                            <li class="block-5">
                                <?php
                                echo apply_filters('woocommerce_cart_item_remove_link', sprintf(
                                                '<a href="%s" class="" title="%s" data-product_id="%s" data-product_sku="%s"><i class="fa fa-times-circle-o remove-product"></i></a>', esc_url(WC()->cart->get_remove_url($cart_item_key)), esc_html__('Remove this item', 'fitfab'), esc_attr($product_id), esc_attr($_product->get_sku())
                                        ), $cart_item_key);
                                ?>
                            </li>
                        </ul>
                        <?php
                    }
                }

                do_action('woocommerce_cart_contents');
                ?>
            </div>
        </div>
        <div class="col-sm-4">
            <?php if (WC()->cart->coupons_enabled()) { ?>
                <div class="coupen-code">
                    <h3 class="block-heading"><?php esc_html_e('have a promotional code?', 'fitfab') ?></h3>
                    <div class="cart-button-text">
                        <input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="<?php esc_attr_e('Coupon code', 'fitfab'); ?>" /> 
                        <input type="submit" class="button-btn  coupen-btn apply-cuupon" name="apply_coupon" value="<?php esc_attr_e('Apply Coupon', 'fitfab'); ?>" />
                        <?php do_action('woocommerce_cart_coupon'); ?>           
                    <?php } ?>

                    <input type="submit" class="button-btn coupen-btn shipping-btn" name="update_cart" value="<?php esc_attr_e('Updates Shopping Cart', 'fitfab'); ?>" />

                    <?php do_action('woocommerce_cart_actions'); ?>

                    <?php wp_nonce_field('woocommerce-cart'); ?>
                </div>
            </div>

        </div>


        <?php do_action('woocommerce_after_cart_table'); ?>

    </form>
</div>

<div class="row top-space">

    <?php do_action('woocommerce_cart_collaterals'); ?>

</div>

<?php do_action('woocommerce_after_cart'); ?>