<?php
/**
 * Checkout coupon form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.2
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

if (!WC()->cart->coupons_enabled()) {
    return;
}

$info_message = apply_filters('woocommerce_checkout_coupon_message', esc_html__('Have a coupon?', 'fitfab') . ' <a href="#" class="showcoupon">' . esc_html__('Click here to enter your code', 'fitfab') . '</a>');
wc_print_notice('<h3>' . $info_message . '<h3>', 'notice');
?>

<form class="checkout_coupon" method="post" style="display:none">

    <div class="form-group  grp-return clearfix">
        <input type="text" name="coupon_code" class="input-text form-control" placeholder="<?php esc_attr_e('Coupon code', 'fitfab'); ?>" id="coupon_code" value="" />
    </div>

    <div class="btn-group1 btn-btn-login">
        <input type="submit" class="button-btn  submit-btn" name="apply_coupon" value="<?php esc_attr_e('Apply Coupon', 'fitfab'); ?>" />
    </div>

    <div class="clear"></div>
</form>
