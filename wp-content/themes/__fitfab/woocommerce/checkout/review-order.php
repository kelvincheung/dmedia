<?php
/**
 * Review order table
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */
if (!defined('ABSPATH')) {
    exit;
}
?>
<div class="cart-wrap">
    <ul class="head-title">
        <li class="block-1">
            <?php esc_html_e('Product', 'fitfab'); ?>
        </li>
        <li class="block-2">
            <?php esc_html_e('Price', 'fitfab'); ?>
        </li>
        <li class="block-3">
            <?php esc_html_e('Quantity', 'fitfab'); ?>
        </li>
        <li class="block-4">
            <?php esc_html_e('Total', 'fitfab'); ?>
        </li>
    </ul>
    <?php
    do_action('woocommerce_review_order_before_cart_contents');
    foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
        $_product = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);

        if ($_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters('woocommerce_checkout_cart_item_visible', true, $cart_item, $cart_item_key)) {
            ?>
            <ul class="product-cart">
                <li class="block-1" data-product="product">
                    <h2 class="product-name proname">
                        <?php
                        if (!$_product->is_visible()) {
                            echo apply_filters('woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key) . '&nbsp;';
                        } else {
                            echo apply_filters('woocommerce_cart_item_name', sprintf('<a href="%s">%s </a>', esc_url($_product->get_permalink($cart_item)), $_product->get_title()), $cart_item, $cart_item_key);
                        }

                        // Meta data
                        echo WC()->cart->get_item_data($cart_item);

                        // Backorder notification
                        if ($_product->backorders_require_notification() && $_product->is_on_backorder($cart_item['quantity'])) {
                            echo '<p class="backorder_notification">' . esc_html__('Available on backorder', 'fitfab') . '</p>';
                        }
                        ?>
                    </h2>
                </li>
                <li class="block-2" data-price="price">
                    <?php
                    echo apply_filters('woocommerce_cart_item_price', WC()->cart->get_product_price($_product), $cart_item, $cart_item_key);
                    ?>
                </li>
                <li class="block-3" data-quantity="quantity">
                    <?php echo apply_filters('woocommerce_checkout_cart_item_quantity', ' <strong class="product-quantity">' . sprintf('&times; %s', $cart_item['quantity']) . '</strong>', $cart_item, $cart_item_key); ?>
                </li>
                <li class="block-4" data-total="total">
                    <?php
                    echo apply_filters('woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal($_product, $cart_item['quantity']), $cart_item, $cart_item_key);
                    ?>
                </li>
            </ul>
            <?php
        }
    }

    do_action('woocommerce_review_order_after_cart_contents');
    ?>
</div>   
<div class="col-sm-4">
    
</div>
<div class="col-sm-8">
    <div class="total-cost">
        <ul>
            <li class="cart-subtotal">
                <span><?php esc_html_e('Subtotal', 'fitfab'); ?></span>
                <span><?php wc_cart_totals_subtotal_html(); ?></span>
            </li>

            <?php foreach (WC()->cart->get_coupons() as $code => $coupon) : ?>
                <li class="cart-discount coupon-<?php echo esc_attr($code); ?>">
                <th><?php wc_cart_totals_coupon_label($coupon); ?></th>
                <td><?php wc_cart_totals_coupon_html($coupon); ?></td>
                </li>
            <?php endforeach; ?>

            <?php if (WC()->cart->needs_shipping() && WC()->cart->show_shipping()) : ?>

                <?php do_action('woocommerce_review_order_before_shipping'); ?>

                <?php wc_cart_totals_shipping_html(); ?>

                <?php do_action('woocommerce_review_order_after_shipping'); ?>

            <?php endif; ?>

            <?php foreach (WC()->cart->get_fees() as $fee) : ?>
                <li class="fee">
                    <span><?php echo esc_html($fee->name); ?></span>
                    <span><?php wc_cart_totals_fee_html($fee); ?></span>
                </li>
            <?php endforeach; ?>

            <?php if (WC()->cart->tax_display_cart === 'excl') : ?>
                <?php if (get_option('woocommerce_tax_total_display') === 'itemized') : ?>
                    <?php foreach (WC()->cart->get_tax_totals() as $code => $tax) : ?>
                        <li class="tax-rate tax-rate-<?php echo sanitize_title($code); ?>">
                            <span><?php echo esc_html($tax->label); ?></span>
                            <span><?php echo wp_kses_post($tax->formatted_amount); ?></span>
                        </li>
                    <?php endforeach; ?>
                <?php else : ?>
                    <li class="tax-total">
                        <span><?php echo esc_html(WC()->countries->tax_or_vat()); ?></span>
                        <span><?php echo wc_price(WC()->cart->get_taxes_total()); ?></span>
                    </li>
                <?php endif; ?>
            <?php endif; ?>

            <?php do_action('woocommerce_review_order_before_order_total'); ?>

            <li class="order-total">
                <span><?php esc_html_e('Total', 'fitfab'); ?></span>
                <span><?php wc_cart_totals_order_total_html(); ?></span>
            </li>

            <?php do_action('woocommerce_review_order_after_order_total'); ?>
        </ul>
    </div>
</div>
<div class="clear"></div>



