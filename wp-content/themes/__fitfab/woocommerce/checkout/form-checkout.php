<?php
/**
 * Checkout Form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */
if (!defined('ABSPATH')) {
    exit;
}

wc_print_notices();

// If checkout registration is disabled and not logged in, the user cannot checkout
if (!$checkout->enable_signup && !$checkout->enable_guest_checkout && !is_user_logged_in()) {
    echo apply_filters('woocommerce_checkout_must_be_logged_in_message', esc_html__('You must be logged in to checkout.', 'fitfab'));
    return;
}
// filter hook for include new pages inside the payment method
$get_checkout_url = apply_filters('woocommerce_get_checkout_url', WC()->cart->get_checkout_url());
?>
<div class="col-xs-12 col-sm-6">
    <?php if (WC()->cart->ship_to_billing_address_only() && WC()->cart->needs_shipping()) : ?>
        <h3><?php esc_html_e('Billing &amp; Shipping', 'fitfab'); ?></h3>
    <?php else : ?>
        <h3><?php esc_html_e('Billing Details', 'fitfab'); ?></h3>
    <?php endif; ?>
    <div class="billig-info-wrap">
        <form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url($get_checkout_url); ?>" enctype="multipart/form-data">
            <?php if (sizeof($checkout->checkout_fields) > 0) : ?>
                <?php do_action('woocommerce_checkout_before_customer_details'); ?>
                <div id="customer_details">
                    <?php do_action('woocommerce_checkout_billing'); ?>
                    <?php do_action('woocommerce_checkout_shipping'); ?>
                </div>
            <h3 id="order_review_heading" class="checkbox1"><?php esc_html_e('Your order ', 'fitfab'); ?></h3>
            <?php endif; ?>
            <?php do_action('woocommerce_checkout_before_order_review'); ?>
            <div id="order_review" class="woocommerce-checkout-review-order">
                <?php do_action('woocommerce_checkout_order_review'); ?>
            </div>
            <?php do_action('woocommerce_checkout_after_order_review'); ?>
        </form>
    </div>
</div>
<div class="col-xs-12 col-sm-6">
    <div class="return-info"><?php do_action('woocommerce_before_checkout_form', $checkout); ?></div>
    <div class="return-info  coupan"><?php do_action('woocommerce_after_checkout_form', $checkout); ?></div>
</div>
