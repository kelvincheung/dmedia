<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.5.2
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

get_header('shop');
?>

<?php
/**
 * woocommerce_before_main_content hook
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 */
do_action("fitfab_banner_parallax", array("title" => get_the_title()));
?>
<section class="cool-products">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-9 ">
                <?php do_action('woocommerce_archive_description'); ?>
                <?php if (have_posts()) : ?>
                    <div class="head-global clearfix">
                        <?php
                        /**
                         * woocommerce_before_shop_loop hook
                         *
                         * @hooked woocommerce_result_count - 20
                         * @hooked woocommerce_catalog_ordering - 30
                         */
                        do_action('woocommerce_before_shop_loop');
                        ?>
                    </div><!-- /input-group -->
                    <div  class="main row">
                        <?php while (have_posts()) : the_post(); ?>
                            <div class="col-xs-12 col-sm-4 tshirt main-item zoom">
                                <?php wc_get_template_part('content', 'product'); ?>
                            </div>
                        <?php endwhile; // end of the loop. ?>
                    </div>
                    <?php
                    /**
                     * woocommerce_after_shop_loop hook
                     *
                     * @hooked woocommerce_pagination - 10
                     */
                    do_action('woocommerce_after_shop_loop');
                    ?>
                <?php elseif (!woocommerce_product_subcategories(array('before' => woocommerce_product_loop_start(false), 'after' => woocommerce_product_loop_end(false)))) : ?>
                    <?php wc_get_template('loop/no-products-found.php'); ?>
                <?php endif; ?>

            </div>
            <div class="col-xs-12 col-sm-3">
                <?php
                /**
                 * woocommerce_sidebar hook
                 *
                 * @hooked woocommerce_get_sidebar - 10
                 */
                do_action('woocommerce_sidebar');
                ?>
            </div>
        </div>
    </div>
</section>

<?php
/**
 * woocommerce_after_main_content hook
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
?>

<?php get_footer('shop'); ?>
