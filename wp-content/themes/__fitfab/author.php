<?php
/**
 * Fitfab - author page
 * 
 * @package fitfab
 * @subpackage fitfab
 * @since fitfab 1.0.0
 */
get_header();
?>
<div id="content">
       <section class="main-blog-content">
        <div class="container">
            <div class="heading-wrap">
                <h1 class="h1"> <?php
                    printf('<a class="url fn n" href="%1$s" title="%2$s" rel="author">%3$s</a>', esc_url(get_author_posts_url(get_the_author_meta('ID'))), sprintf(esc_html__('View all posts by %s', 'fitfab'), get_the_author()), get_the_author()
                    );
                    ?> </h1>
            </div>
	    <?php get_template_part('content/post','author'); ?>
            <div class="row">
                <?php do_action('fitfab_blog_layout'); ?>
            </div>
        </div>
    </section>
</div>
<?php get_footer(); 