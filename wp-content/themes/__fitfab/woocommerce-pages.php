<?php
/**
 * Template Name: Woocommerce Page
 * 
 * * This is default page layout
 * @package fitfab
 * @subpackage fitfab
 * @since fitfab 1.0.0
 */
get_header();
?>
<!--Content Area Start-->
<div id="content">
    <?php do_action("fitfab_banner_parallax", array("title" => get_the_title())); ?>
    <section class="shopping-cart billing-details">
        <div class="container">
            <div class="row">
                <div>
                    <?php
                    if (have_posts()) : while (have_posts()) : the_post();
                            the_content();
                        endwhile;
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </section>
</div>
<!--Content Area End-->
<?php
get_footer();
