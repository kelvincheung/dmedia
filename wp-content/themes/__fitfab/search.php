<?php
/**
 * FitFab - search page
 * 
 * @package fitfab
 * @subpackage fitfab
 * @since fitfab 1.0.0
 */
get_header();
?>
<!--Content Area Start-->
<div id="content">
    <section class="main-blog-content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8">
                    <header class="page-header">
                        <h1 class="page-title"><?php printf(esc_html__('Search Results for: %s', 'fitfab'), '<span>' . get_search_query() . '</span>'); ?></h1>
                    </header><!-- .page-header -->
                    <?php if (have_posts()) : ?>
                        <?php
                        while (have_posts()) : the_post();
                           $format = 'standard';
                           get_template_part('content/format/' . $format);
                        endwhile;
                        fitfab_pagenavi();
                    else :
                        get_template_part('content/none');
                    endif;
                    ?>
                </div>
                <?php get_sidebar(); ?>
            </div>
        </div>
    </section>
</div>
<!--Content Area End-->
<?php
get_footer();
