<?php
/**
 * Template Name: Blog Template
 *
 * @package fitfab
 * @subpackage fitfab
 * @since fitfab 1.0.0
 */
get_header();
?>
<!--Content Area Start-->
<div id="content">
    <?php do_action("fitfab_banner_parallax", array("title" => get_the_title())); ?>
    <section class="main-blog-contentt">
        <div class="container">
            <div class="row">
                <?php do_action('fitfab_blog_layout'); ?>
            </div>
        </div>
    </section>
</div>
<!--Content Area End-->
<?php
get_footer();
