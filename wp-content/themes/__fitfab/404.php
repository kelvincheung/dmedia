<?php
/**
 * FitFab - 404 page
 * 
 * @package fitfab
 * @subpackage fitfab
 * @since fitfab 1.0.0
 */
get_header();
global $fitfab_data;
?>
<!--Content Area Start-->
<div id="content">
    <?php do_action("fitfab_banner_parallax", array("title" => get_the_title())); ?>
    <?php $error_image = (!empty($fitfab_data['fitfab_404_image']['url'])) ? 'background: url(' . $fitfab_data['fitfab_404_image']['url'] . ') right bottom no-repeat #ffffff' : ''; ?>	
    <section class="artical-part" style= "<?php echo esc_attr($error_image); ?>">
        <div class="container">	
            <div class="error-content">
                <?php if (!empty($fitfab_data['fitfab_404_page_title'])) : ?>
                    <span class="num-cls-blck"><?php echo wp_kses($fitfab_data['fitfab_404_page_title'], wp_kses_allowed_html('post')); ?></span>
                <?php endif; 
		if (!empty($fitfab_data['fitfab_404_page_title'])) : ?>
                    <span class="oops"><?php echo esc_html($fitfab_data['fitfab_404_page_subtitle']); ?></span>
                <?php endif;
		if (!empty($fitfab_data['fitfab_404_page_title'])) : ?>
                    <span class="lkg"><?php echo esc_html($fitfab_data['fitfab_404_page_another_subtitle']); ?></span>
                <?php endif; ?>
                <a class="button-btn btn-error" href="<?php echo esc_url(home_url('/')); ?>"><?php esc_html_e('Go to Homepage', 'fitfab'); ?></a>
            </div>							
        </div>
    </section>
</div>
<!--Content Area End-->
<?php
get_footer();
