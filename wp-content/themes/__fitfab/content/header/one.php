<?php 
/**
 * Fitfab : FitFab Header Layout One
 *
 * @package fitfab
 * @subpackage fitfab.content.header
 * @since fitfab 1.0.0
 */
?>
<!--Header Section Start-->
<header id="header" class="header-one">
    <div class="container">
        <div class="row">
            <div class="col-xs-6 col-sm-2"><?php
                global $fitfab_data;
                $fitfab_logo = (!empty($fitfab_data['fitfab_logo'] ['url'])) ? $fitfab_data['fitfab_logo'] ['url'] : FITFAB_THEME_URL . '/assets/images/home-1_logo.png';
                ?><a href="<?php echo esc_url(home_url('/')); ?>" class="logo">
                    <img src="<?php echo esc_url($fitfab_logo); ?>"
                         alt="<?php bloginfo('name'); ?>" />                         
                </a>
            </div>
            <div
                class="col-xs-6 col-sm-10 col-md-9 col-lg-8 pull-right mobile-static">
                <nav class="navbar">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed"
                                data-toggle="collapse"
                                data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only"><?php esc_html_e('Toggle navigation', 'fitfab'); ?></span>
                            <span class="icon-bar"></span> <span class="icon-bar"></span> <span
                                class="icon-bar"></span>
                        </button>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse"
                         id="bs-example-navbar-collapse-1">
			 <?php do_action('fitfav_nav_menu'); ?>
		    </div>
                    <!-- /.navbar-collapse -->
                </nav>
            </div>
        </div>
    </div>
</header>
<!--Header Section End-->
<?php
