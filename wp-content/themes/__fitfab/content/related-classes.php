<?php
/**
 * Fitfab : Template part for related classes.
 *
 * @package fitfab
 * @subpackage fitfab.content
 * @since fitfab 1.0.0
 */
$custom_taxterms = wp_get_object_terms(get_the_id(), 'class_categories', array('fields' => 'ids'));
$args = array(
    'post_type' => 'class',
    'post_status' => 'publish',
    'posts_per_page' => 2, // you may edit this number
    'orderby' => 'rand',
    'tax_query' => array(
        array(
            'taxonomy' => 'class_categories',
            'field' => 'id',
            'terms' => $custom_taxterms
        )
    ),
    'post__not_in' => array(get_the_id()),
);
$related_classes = new WP_Query($args);
if ($related_classes->have_posts()) :
    ?>
    <div class="row yoga-class-wrap">
        <div class="col-xs-12">
            <div class="head-global family-oswald">
                <h2 class="h2"><?php esc_html_e('related classes', 'fitfab'); ?></h2>
            </div>
        </div>
        <?php
        while ($related_classes->have_posts()) :
            $related_classes->the_post();
            $start_time = date('hA', strtotime(get_post_meta($post->ID, 'fitfab_class_start_time', TRUE)));
            $end_time = date('hA', strtotime(get_post_meta($post->ID, 'fitfab_class_end_time', TRUE)));
            $post_categories = wp_get_post_categories($post->ID);
            $terms = get_the_terms($post->ID, 'trainer');
            $on_trainer = '';
            if ($terms && !is_wp_error($terms)) :
                $trainer = array();
                foreach ($terms as $term) :
                    $trainer[] = $term->name;
                endforeach;

                $on_trainer = join(", ", $trainer);
            endif;
            ?>
            <div class="col-xs-12 col-sm-6 classes-listing-wrap zoom">
                <?php
                if (has_post_thumbnail()):
                    $url = wp_get_attachment_url(get_post_thumbnail_id());
                    ?>
                    <figure>
                        <img src="<?php echo esc_url(fitfab_resize($url, '360', '201')); ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" />
                        <?php
                        if (!empty($start_time) and !empty($end_time)) :
                            ?>
                            <span><?php echo esc_html($start_time); ?> - <?php echo esc_html($end_time); ?></span>
                        <?php endif; ?>
                    </figure>
                <?php endif; ?>
                <div class="classes-content">
                    <h3><?php the_title(); ?></h3>
                    <span>
                        <?php
			if(function_exists('fitfab_rwmb_meta')){
                        $trainer = implode(', ', fitfab_rwmb_meta('fitfab_trainer', 'type=checkbox_list'));
                        if (!empty($trainer)) :
                            echo esc_html($trainer);
                        endif;
			}
                        ?>
                    </span>
                </div>
                <p>
                    <?php echo fitfab_truncate_content(get_the_content(), 85); ?>
                </p>
                <a href="<?php the_permalink(); ?>" class="link"><?php esc_html_e('read more', 'fitfab'); ?></a>

            </div>
        <?php endwhile; ?>
    </div>
    <?php
endif;
wp_reset_postdata();

