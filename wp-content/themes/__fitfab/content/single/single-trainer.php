<?php
/**
 * Fitfab : tariner detail page
 *
 * @package fitfab
 * @subpackage fitfab.content.single
 * @since fitfab 1.0.0
 */
?>
<div class="row">
    <div class="col-sm-4">
        <?php
        if (has_post_thumbnail()):
            $url = wp_get_attachment_url(get_post_thumbnail_id());
            ?>
            <div class="trainer-pic zoom">
                <figure>
                    <img src="<?php echo esc_url(fitfab_resize($url, '360', '301')); ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>">
                </figure>
            </div>
        <?php endif; ?>
    </div>
    <div class="col-sm-5">
        <div class="trainer-info">
            <?php
            $trainer_type = '';
            $trainer_exp = '';
	    if(function_exists('fitfab_rwmb_meta')){
            $trainer_type = fitfab_rwmb_meta('fitfab_trainer_type', 'type=text');
            $trainer_exp = fitfab_rwmb_meta('fitfab_trainer_exp', 'type=text');
	    }
            ?>
            <div class="trainer-name">
                <h2><?php the_title(); ?></h2><span class="button-btn exp-btn"><?php esc_html('EXP:', 'fitfab'); ?>  <?php echo esc_html($trainer_exp); ?></span>
                <?php
                if (!empty($trainer_type)) :
                    ?>
                    <strong>(<?php echo esc_html($trainer_type); ?>)</strong>
                <?php endif; ?>
            </div>
            <p>
                <?php the_content(); ?>
            </p>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="trainer-contact">
            <i class="fa fa-phone phone-btn"></i>
            <span><?php esc_html_e('Contact', 'fitfab'); ?> <span><?php the_title(); ?></span></span>
            <?php 
            $trainer_contact = '';
	    if(function_exists('fitfab_rwmb_meta')){
	    $trainer_contact = fitfab_rwmb_meta('fitfab_trainer_contact', 'type=text');
	    }
            ?>
            <?php if (!empty($trainer_contact)) : ?>
                <a href="<?php esc_html_e('callto:', 'fitfab'); ?><?php echo esc_html($trainer_contact); ?>"><?php echo esc_html($trainer_contact); ?></a>
            <?php endif; ?>
        </div>
        <?php 
        $trainer_video = '';
	if(function_exists('fitfab_rwmb_meta')){
	    $trainer_video = fitfab_rwmb_meta('fitfab_trainer_video', 'type=textarea');
	}
        ?>
        <?php if ($trainer_video) : ?>
            <div class="video-block">
                <?php print($trainer_video); ?>
            </div>
        <?php endif; ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <?php 
        $trainer_qualification = '';
	if(function_exists('fitfab_rwmb_meta')){
	$trainer_qualification = fitfab_rwmb_meta('fitfab_trainer_qualification', 'type=textarea');
	}
        ?>
        <?php if ($trainer_qualification) : ?>
            <div class="benifit-yoga">
                <h3><?php esc_html_e('qualification', 'fitfab'); ?></h3>
                <p>
                    <?php echo wp_kses($trainer_qualification, wp_kses_allowed_html('post')); ?>
                </p>

            </div>
        <?php endif; ?>
    </div>
    <div class="col-sm-4">
        <?php
        $trainer_experience = '';
	if(function_exists('fitfab_rwmb_meta')){
	    $trainer_experience = fitfab_rwmb_meta('fitfab_trainer_experience', 'type=text');
	}
        if (!empty($trainer_experience) && $trainer_experience != '') :
            ?>
            <div class="benifit-yoga">
                <h3><?php esc_html_e('experience', 'fitfab'); ?></h3>
                <ul class="catgory-list">
                    <?php
                    foreach ($trainer_experience as $experience) :
                        ?>
                        <li>
                            <i class="fa fa-long-arrow-right"></i> <?php echo esc_html($experience); ?>

                        </li>
                    <?php endforeach; ?>

                </ul>

            </div>
        <?php endif; ?>
    </div>

    <div class="col-sm-4">
        <?php
        $trainer_specialization = '';
	if(function_exists('fitfab_rwmb_meta')){
        $trainer_specialization = fitfab_rwmb_meta('fitfab_trainer_specialization', 'type=text');
	}
        if (!empty($trainer_specialization) && $trainer_specialization != '') :
            ?>
            <div class="benifit-yoga">
                <h3><?php esc_html_e('specialization', 'fitfab'); ?></h3>
                <ul class="catgory-list">
                    <?php
                    foreach ($trainer_specialization as $specialization) :
                        ?>
                        <li>
                            <i class="fa fa-long-arrow-right"></i> <?php echo esc_html($specialization); ?>

                        </li>
                    <?php endforeach; ?>

                </ul>

            </div>
        <?php endif; ?>
    </div>

</div>