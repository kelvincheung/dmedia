<?php
/**
 * Fitfab : Template part for displaying single event.
 *
 * @package fitfab
 * @subpackage fitfab.content
 * @since fitfab 1.0.0
 */
global $post;
$EM_Event = em_get_event($post->ID, 'post_id');
?>
    <div class="event-event clearfix">
        <span class="event-calender-event"> <?php echo esc_html($EM_Event->output('#d')); ?> <span><?php echo esc_html($EM_Event->output('#M')); ?></span> </span>
        <div class="event-info-event">
            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
            <div class="time-location-event">
                <?php if ($EM_Event->output('#_12HSTARTTIME') && ($EM_Event->output('#_12HENDTIME'))) : ?>
                <span><?php echo esc_html($EM_Event->output('#_12HSTARTTIME')); ?> - <?php echo esc_html($EM_Event->output('#_12HENDTIME')); ?></span>
                <?php endif; ?>
                <?php if ($EM_Event->output('#_LOCATIONNAME')) : ?>
                <span><?php echo esc_html($EM_Event->output('#_LOCATIONNAME')); ?></span>
                <?php endif; ?>
            </div>

        </div>
    </div>
    <div class="blog-program">
        <?php if (has_post_thumbnail()): ?>
        <div class="zoom">
            <figure>
                <?php the_post_thumbnail('featured'); ?>
            </figure>
        </div>
        <?php endif; ?>

        <div class="para-info">
            <?php the_content(); ?>
        </div>

    </div>
    <?php
