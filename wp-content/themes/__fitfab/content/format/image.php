<?php
/**
 * Fitfab : Template part for displaying posts.
 *
 * @package fitfab
 * @subpackage fitfab.content.format
 * @since fitfab 1.0.0
 */
global $fitfab_page;
    ?>
    <div id="post-<?php the_ID(); ?>" <?php post_class('blog-program'); ?>>
        <?php if (has_post_thumbnail()): ?>   
            <figure>
                <?php the_post_thumbnail('featured'); ?>
            </figure>
        <?php endif; ?>

        <div class="classes-content">
            <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
            <?php do_action('fitfab_post_format_attribute'); ?>
        </div>
        <?php
        if ($fitfab_page == 'single-page') :
          the_content();
          wp_link_pages();
        elseif (is_search()):
            ?><p><?php the_excerpt(); ?></p><?php
        else:
            the_content();
            wp_link_pages();
	    $post_content = get_post_field('post_content', get_the_ID());
            if (1 < strpos($post_content, '<!--more-->') && !is_single()) {
                ?>
                <a href="<?php the_permalink(); ?>" class="blog-sidebar-read-btn"><?php echo esc_html_e('read more', 'fitfab'); ?></a>
            <?php } 
        endif;        ?>   
    </div>

<?php 
