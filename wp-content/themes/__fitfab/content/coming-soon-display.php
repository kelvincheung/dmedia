<?php
/**
 * FitFab - coming soon layout
 * 
 * @package fitfab
 * @since fitfab 1.0.0
 */
?>
<!DOCTYPE html>
<html lang="en" class="under-construction">
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
	<meta name="robots" content="noindex, nofollow" />
        <meta http-equiv='X-UA-Compatible' content='IE=edge' />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
        <?php wp_head(); ?>
    </head>
    <body  <?php body_class('fit-fab-coming-soon-layout'); ?>>
	<?php 
            global $fitfab_data;
            
            $coming_soon_bg = $fitfab_data['coming_soon_page_bg']['url'];  
            
            ?> 
        <div style="background-image:url('<?php echo esc_url($coming_soon_bg); ?>'); background-size: cover; background-repeat: no-repeat; background-position: 0 0;">
            
            <div class="launch clearfix">
            <header id="header">
                <div class="container">
                    <div class="row">
                        <?php $site_logo = (!empty($fitfab_data['fitfab_comming_logo']['url'])) ? $fitfab_data['fitfab_comming_logo']['url'] : FITFAB_THEME_URL. '/assets/images/home-2_logo.png'; ?>
                        <a href="<?php echo esc_url(home_url('/')); ?>" class="col-xs-12 logo" title="<?php bloginfo('name'); ?>"><img src="<?php echo esc_url($site_logo); ?>" alt="<?php bloginfo('name'); ?>"></a>
                    </div>

                </div>

            </header>
            <div class="content">
                <div class="container">
		    <div class="row">
			<div class="col-xs-12 col-md-12 col-lg-12">
			   <h2><?php echo esc_html($fitfab_data['fitfab_coming_title']); ?></h2>
			</div>
		    </div>
                    <div class="row">
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			    <?php echo do_shortcode('<div  class="form-wrapper">'.$fitfab_data['fitfab_newsletter_comming'].'</div>'); ?> 
			</div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 coming-soon-content">
                             <p><?php print($fitfab_data['fitfab_coming_content']); ?></p>
                            <div id="induCountdown" class="counter clearfix"></div>
                        </div>
                    </div>
                </div>

            </div>
            <footer id="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-xs-12" style="padding: 10px 0;">
                            <span><?php print($fitfab_data['fitfab_copyright_comming']); ?></span>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
	</div>
	<?php wp_footer(); ?>	
    </body>
</html>
