<?php
/**
 * Fitfab - blog layout
 * 
 * @package fitfab
 * @subpackage fitfab.content
 * @since fitfab 1.0.0
 */
?>
<div class="col-xs-12 col-sm-8 fit-fab-blog-layout-one">
    <?php
    if(is_page()):
    $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

    $args = array(
        'post_type' => 'post',
        'post_status' => 'publish',
        'paged' => $paged
    );

    $blog_query = new WP_Query($args);

    if ($blog_query->have_posts()) :
        while ($blog_query->have_posts()) : $blog_query->the_post();
            $format = (get_post_format()) ? get_post_format() : 'standard';
            get_template_part('content/format/' . $format);
        endwhile;
        fitfab_pagenavi($blog_query);
    else :
        get_template_part('content/none');
    endif;
    wp_reset_postdata();
    else:
	 if (have_posts()) :
        while (have_posts()) : the_post();
            $format = (get_post_format()) ? get_post_format() : 'standard';
            get_template_part('content/format/' . $format);
        endwhile;
        fitfab_pagenavi();
    else :
        get_template_part('content/none');
    endif;
    endif; 
    ?>
</div><!-- #primary -->
<?php get_sidebar();
