<?php
/**
 * Load Module
 * 
 * Plugin installation using TGM
 * Import demo data using ONECLIK/MANUAL/OFFLINE
 * Export demo data of theme, options
 * 
 * @package wptm.inc
 * @since wptm 1.0.0
 */


fitfab_inclusion( 'vendor/util/inc/tgm/tgm.php' );
fitfab_inclusion( 'vendor/util/inc/oneclick/import.php' );

