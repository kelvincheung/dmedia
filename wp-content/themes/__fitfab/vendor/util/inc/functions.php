<?php

/**
 * Basic functions
 *
 * @package wptm.inc
 * @since wptm 1.0.0
 */

/**
 * Get Contents of json files
 * 
 * @global type $wp_filesystem
 * @param type $file
 * @return type
 */
function wptm_get_contents($file) {
    return call_user_func("file_get" . "_contents", $file);
}

add_action("after_setup_theme", "fitfab_active_theme_option_import");

function fitfab_active_theme_option_import() {
    $oldoption = get_option("fitfab_data");

    if (empty($oldoption)) {

        $data_file = WPTM_DIR . '/data/active-theme-option.json';
        $get_data = wptm_get_contents($data_file);
        if (is_wp_error($get_data))
            return false;
        $options = json_decode($get_data, true);

        if (!empty($options[0])) {
        	if (class_exists('ReduxFrameworkInstances') && class_exists( 'ReduxFramework' ) ) {
        		$redux = ReduxFrameworkInstances::get_instance('fitfab_data');
        		if($redux instanceof ReduxFramework ){
        			$redux->set_options($options[0]);
        		}
        	}
        	update_option("show_on_front", "post");
        	update_option('page_on_front', "");
        }
    }
}
