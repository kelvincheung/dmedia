<?php
/**
 * Import Tab 
 * 
 * @package wptm.admin
 * @since wptm 1.0.0
 */
?><div class="wptm-dashboard-info">

    <p id="wptm-message" class="updated"> <b><span class="dashicons dashicons-hammer"></span> Choose any one installation. </b> </p>
    <table class="wptm-status-table">
        <thead>
            <tr>
                <th>ONE CLICK INSTALLATION</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <ul style="list-style: square">
                        <li>Your machine must be connected to Internet.</li>
                        <li>Posts, Pages, Media, Comments, widgets, Menus, Custom Post Types or any other data will get imported.</li>
                        <li>Images will be downloaded from our server, these images are for demo purpose only.</li>
                        <li>Existing posts, pages, categories, images, custom post types or any other data will be deleted or modified .</li>
                        <li>WordPress settings will be modified after the demo content is imported successfully.</li>
                        <li>Please click import only once and wait, it can take a couple of minutes</li>
                    </ul>
                    <p><a hr<?php ?>ef="admin.php?page=theemon-importer&step=2&install=oneclick" class="button button-primary">START ONE CLICK INSTALLATION </a></p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<?php 
