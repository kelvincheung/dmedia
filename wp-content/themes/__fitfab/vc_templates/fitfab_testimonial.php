<?php
/**
 * FitFab - testimonial shortcode
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates
 * @since fitfab 1.0.0
 */
extract(shortcode_atts(array(
    'background_image' => '',
    'title' => '',
    'sub_title' => '',
    'postscount' => '',
    'testimonial_style' => '',
     ), $atts));

if (!$postscount):
    $postscount = 3;
endif;

$image_link = wp_get_attachment_image_src($background_image, "large");
$bg_image = '';
if ($image_link) :
    $bg_image .= 'background-image:url(' . $image_link[0] . ');background-repeat:no-repeat;background-position:right top;';
endif;
$bg_image .= 'background-color:#1e1e28;';

$args = array(
    'post_type' => 'testimonial',
    'post_status' => 'publish',
    'showposts' => $postscount,
);
$fitfab_testimonial = new WP_Query($args);
if ($testimonial_style == 'home' || $testimonial_style == '') :
    ?>
    <!-- success_story_wrap start here -->
    <section class="success_story_wrap" style="<?php echo esc_attr($bg_image); ?>">
        <div class="container">
            <?php if (!empty($title) || !empty($sub_title)) : ?>
                <div class="head-global">
                    <h2 class="h2"><?php echo esc_html($title); ?> <?php if (!empty($sub_title)) : ?>
                            <span><?php echo esc_html($sub_title); ?></span><?php endif; ?>
                    </h2>
                </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-xs-12 col-sm-10 col-md-8 col-lg-6">
                    <div id="owl-slider2">
                        <?php
                        if ($fitfab_testimonial->have_posts()) :
                            while ($fitfab_testimonial->have_posts()) : $fitfab_testimonial->the_post();
                                ?>
                                <div class="item">
                                    <h3><?php the_title(); ?></h3>
                                    <?php the_content(); ?>
                                </div>
                                <?php
                            endwhile;
                            wp_reset_postdata();
                        endif;
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php elseif ($testimonial_style == 'home1') : ?>
    <!-- success-home-2 style start here -->
    <section class="success-home-two" style="<?php echo esc_attr($bg_image); ?>">
        <div class="container">
            <?php if (!empty($title)) : ?>
                <div class="head-global family-oswald">
                    <h2 class="h2"><?php echo esc_html($title); ?></h2>
                </div>
            <?php endif; ?>
            <div class="row">
                <div id="owl-sucess-story" class="owl-carousel">
                    <?php
                    if ($fitfab_testimonial->have_posts()) :
                        while ($fitfab_testimonial->have_posts()) : $fitfab_testimonial->the_post(); ?>
                            <div class="item">
                                <div class="sucess-cap">
                                    <strong><?php the_title(); ?></strong>
                                    <?php the_content(); ?>
                                </div>
                            </div>
                            <?php
                        endwhile;
                        wp_reset_postdata();
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </section>
    <!-- success-home-2 style end here -->
<?php elseif ($testimonial_style == 'home2') : ?>
    <!-- success home style start here -->
    <div class="container">
        <section class="success-home-two success-home-three" style="<?php echo esc_attr($bg_image); ?>">
            <?php if (!empty($title) || !empty($sub_title)) : ?>
                <div class="head-global family-oswald">
                    <h2 class="h2"><?php if (!empty($title)) : ?>
                            <span><?php echo esc_html($title); ?></span>
                        <?php endif; ?>
                        <?php echo esc_html($sub_title); ?>
                    </h2>
                </div>
            <?php endif; ?>
            <div class="row">
                <div id="owl-sucess-story" class="owl-carousel">
                    <?php
                    if ($fitfab_testimonial->have_posts()) :
                        while ($fitfab_testimonial->have_posts()) : $fitfab_testimonial->the_post();
                            ?>
                            <div class="item">
                                <div class="sucess-cap">
                                    <strong><?php the_title(); ?></strong>
                                    <?php the_content(); ?>
                                    <?php if (has_post_thumbnail()) : ?>
                                        <div class="img-rounded">
                                            <?php the_post_thumbnail('testimonial'); ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <?php
                        endwhile;
                        wp_reset_postdata();
                    endif;
                    ?>
                </div>
            </div>
        </section>
    </div>
    <!-- success home style end here -->
    <?php
endif;
echo $this->endBlockComment('fitfab_testimonial');
