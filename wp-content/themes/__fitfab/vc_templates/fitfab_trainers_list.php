<?php
/**
 * FitFab - Trainers List
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates
 * @since fitfab 1.0.0
 */
extract(shortcode_atts(array(
    'no_of_trainers_list' => '',
    'order_by' => '',
    'sort_order' => '',
		), $atts));
if (!$no_of_trainers_list) {
    $no_of_trainers_list = 6;
}
if (!$order_by) {
    $order_by = 'date';
}
if (!$sort_order) {
    $sort_order = 'asc';
}

$args = array(
    'post_type' => 'trainer',
    'post_status' => 'publish',
    'posts_per_page' => $no_of_trainers_list
);
$trainer_query = new WP_Query($args);
?>
<!-- 	main-trainers-content style start here -->
<div class="fit-fab-trainers-list-layout-one">
    <div class="breadcrum-sec">
	<div class="container">
	    <?php fitfab_breadcrumb(); ?>
	</div>
    </div>
    <section class="trainers-block">
	<div class="container">
	    <!-- latest-news-home_two  style start here -->
	    <div class="latest-news-home_two">
		<?php
		$count = 0;
		if ($trainer_query->have_posts()) :
		    while ($trainer_query->have_posts()) : $trainer_query->the_post();
			?>
			<?php if ($count % 3 == 0) : ?>
	    		<div class="row top_gap">
			    <?php endif; ?>
			    <div class="col-xs-12 col-sm-4 classes-listing-wrap zoom">
				<?php
				if (has_post_thumbnail()):
				    $url = wp_get_attachment_url(get_post_thumbnail_id());
				    ?>
	    			<figure>
	    			    <a href="<?php the_permalink(); ?>">
	    				 <img src="<?php echo esc_url(fitfab_resize($url, '360', '260')); ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>">
	    			    </a>
	    			</figure>
				<?php endif; ?>
				<div class="classes-content">
				    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
				    <?php
				    if (function_exists('fitfab_rwmb_meta')) {
					$trainer_type = fitfab_rwmb_meta('fitfab_trainer_type', 'type=text');
				    }
				    if (!empty($trainer_type)) :
					?>
	    			    <span><?php echo esc_html($trainer_type); ?> </span>
				    <?php endif; ?>
				    <a href="<?php the_permalink(); ?>" class="plus-more open-info">+</a>
				</div>
			   </div>
			    <?php
			    $count++;
			    if ($count % 3 == 0) :
				?></div><?php
			  endif; 
		    endwhile;
		    wp_reset_postdata();
		endif;
		?>
	    </div>

	    <!-- latest-news-home_two  style end here -->
	</div>
    </section>
</div>
<!-- 	main-blog-content style end here -->
<?php
echo $this->endBlockComment('fitfab_trainers_list');
