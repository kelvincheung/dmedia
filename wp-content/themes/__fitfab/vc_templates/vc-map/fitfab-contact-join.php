<?php

/**
 * FitFab - contact join
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates.vc-map
 * @since fitfab 1.0.0
 */
add_action('vc_before_init', 'fitfab_contact_join_vc');

function fitfab_contact_join_vc() {
    vc_map(array(
        "name" => esc_html__("Contact Join - Fitfab", 'fitfab'),
        "base" => "fitfab_contact_join",
        "class" => "",
        "category" => esc_html__("Fitfab", 'fitfab'),
        "params" => array(
            array(
                "type" => "attach_image",
                "class" => "",
                "heading" => esc_html__("Background Image", 'fitfab'),
                "param_name" => "background_image",
                "value" => '',
                "description" => esc_html__("Select Background Image Here", 'fitfab')
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("Section Description", 'fitfab'),
                "param_name" => "dsecription",
                "value" => '',
                "description" => esc_html__("Enter Description", 'fitfab')
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("Section Title", 'fitfab'),
                "param_name" => "title",
                "value" => '',
                "description" => esc_html__("Enter Section Title", 'fitfab')
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("Phone No.", 'fitfab'),
                "param_name" => "phone",
                "value" => '',
                "description" => esc_html__("Enter Phone No.", 'fitfab')
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("Section Text", 'fitfab'),
                "param_name" => "text",
                "value" => '',
                "description" => esc_html__("Enter Section Text", 'fitfab')
            ),
        )
    ));
}

class WPBakeryShortCode_fitfab_contact_join extends WPBakeryShortCode {
    
}
