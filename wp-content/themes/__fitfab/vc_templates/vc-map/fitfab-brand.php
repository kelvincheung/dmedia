<?php

/**
 * FitFab - coolest brand
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates.vc-map
 * @since fitfab 1.0.0
 */
add_action('vc_before_init', 'fitfab_brand_vc');

function fitfab_brand_vc() {
    vc_map(array(
        "name" => esc_html__("Brand - Fitfab", 'fitfab'),
        "base" => "fitfab_brand",
        "class" => "",
        "category" => esc_html__("Fitfab", 'fitfab'),
        "params" => array(
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("Title", 'fitfab'),
                "param_name" => "title",
                "value" => '',
                "description" => esc_html__("Enter Title Here", 'fitfab')
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("Sub Title", 'fitfab'),
                "param_name" => "sub_title",
                "value" => '',
                "description" => esc_html__("Enter Sub Title Here", 'fitfab')
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("Number of Brand", 'fitfab'),
                "param_name" => "no_of_brand",
                "value" => '',
                "description" => esc_html__("Enter Number of Brand Here", 'fitfab')
            ),
        )
    ));
}

class WPBakeryShortCode_fitfab_brand extends WPBakeryShortCode {
    
}
