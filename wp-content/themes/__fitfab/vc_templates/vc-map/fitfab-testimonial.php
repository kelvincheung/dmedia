<?php

/**
 * FitFab - testimonial 
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates.vc-map
 * @since fitfab 1.0.0
 */
add_action('vc_before_init', 'fitfab_testimonial_vc');

function fitfab_testimonial_vc() {
    vc_map(array(
        "name" => esc_html__("Testimonials - Fitfab", 'fitfab'),
        "base" => "fitfab_testimonial",
        "class" => "",
        "category" => esc_html__("Fitfab", 'fitfab'),
        "params" => array(
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => esc_html__("Testimonial Style", 'fitfab'),
                "param_name" => "testimonial_style",
                "description" => esc_html__("Select Testimonial Style", 'fitfab'),
                "value" => array(
                    esc_html__('Select', 'fitfab') => '',
                    esc_html__('Home', 'fitfab') => 'home',
                    esc_html__('Home1', 'fitfab') => 'home1',
                    esc_html__('Home2', 'fitfab') => 'home2',
                ),
            ),
            array(
                "type"=> "attach_image",
                "class"       => "",
                "heading"     => esc_html__( "Background Image", 'fitfab' ),
                "param_name"  => "background_image",
                "value"       => '',
                "description" => esc_html__( "Select Background Image Here", 'fitfab' )
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("Title", 'fitfab'),
                "param_name" => "title",
                "value" => '',
                "description" => esc_html__("Enter Title Here", 'fitfab')
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("Sub Title", 'fitfab'),
                "param_name" => "sub_title",
                "value" => '',
                "description" => esc_html__("Enter Sub Title Here", 'fitfab')
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("Number of Testimonials", 'fitfab'),
                "param_name" => "postscount",
                "value" => '',
                "description" => esc_html__("Enter Number of Testimonials", 'fitfab')
            ),
        )
    ));
}

class WPBakeryShortCode_fitfab_testimonial extends WPBakeryShortCode {
    
}
