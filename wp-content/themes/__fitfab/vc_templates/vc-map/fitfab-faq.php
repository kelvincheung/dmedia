<?php

/**
 * FitFab - award 
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates.vc-map
 * @since fitfab 1.0.0
 */
add_action('vc_before_init', 'fitfab_faq_vc');

function fitfab_faq_vc() {
    vc_map(array(
        "name" => esc_html__("FAQ - Fitfab", 'fitfab'),
        "base" => "fitfab_faq",
        "class" => "",
        "category" => esc_html__("Fitfab", 'fitfab'),
        "params" => array(
            array(
                "type" => "attach_image",
                "class" => "",
                "heading" => esc_html__("Background Image", 'fitfab'),
                "param_name" => "background_image",
                "value" => '',
                "description" => esc_html__("Select Background Image Here", 'fitfab')
            ),
            array(
                "type" => "textfield",
                "holder" => "",
                "class" => "",
                "heading" => esc_html__("Section Title", 'fitfab'),
                "param_name" => "title",
                "value" => '',
                "description" => esc_html__("Enter Section Title", 'fitfab')
            ),
            array(
                "type" => "textarea",
                "holder" => "",
                "class" => "",
                "heading" => esc_html__("Section Content", 'fitfab'),
                "param_name" => "sec_content",
                "value" => '',
                "description" => esc_html__("Enter Section Content Here", 'fitfab')
            ),
            array(
                "type" => "textfield",
                "holder" => "",
                "class" => "",
                "heading" => esc_html__("NO. of items to display", 'fitfab'),
                "param_name" => "no_of_items",
                "value" => '',
                "description" => esc_html__("NO. of items to display", 'fitfab')
            ),
        )
    ));
}

class WPBakeryShortCode_fitfab_faq extends WPBakeryShortCode {
    
}
