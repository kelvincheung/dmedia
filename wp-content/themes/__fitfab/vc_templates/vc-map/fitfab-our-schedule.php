<?php

/**
 * FitFab - our schedule 
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates.vc-map
 * @since fitfab 1.0.0
 */
add_action('vc_before_init', 'fitfab_our_schedule_vc');

function fitfab_our_schedule_vc() {
    vc_map(array(
        "name" => esc_html__("Our Schedule - Fitfab", 'fitfab'),
        "base" => "fitfab_our_schedule",
        "class" => "",
        "category" => esc_html__("Fitfab", 'fitfab'),
        "params" => array(
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("Title", 'fitfab'),
                "param_name" => "title",
                "value" => '',
                "description" => esc_html__("Enter Title Here", 'fitfab')
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("Sub Title", 'fitfab'),
                "param_name" => "sub_title",
                "value" => '',
                "description" => esc_html__("Enter Sub Title Here", 'fitfab')
            ),
            array(
                "type" => "textarea",
                "class" => "",
                "heading" => esc_html__("Description", 'fitfab'),
                "param_name" => "description",
                "value" => '',
                "description" => esc_html__("Enter Description Here", 'fitfab')
            ),
        )
    ));
}

class WPBakeryShortCode_fitfab_our_schedule extends WPBakeryShortCode {
    
}
