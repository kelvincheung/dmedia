<?php
add_action('vc_before_init', 'fitfab_rwo_extend');

function fitfab_rwo_extend() {
vc_map(array(
	'name' => esc_html__( 'Row' , 'fitfab' ),
	'base'=> 'vc_row',
	'is_container' => true,
	'icon' => 'icon-wpb-row',
	'show_settings_on_create' => false,
	'category' => esc_html__( 'Content', 'fitfab' ),
	'description' => esc_html__( 'Place content elements inside the row', 'fitfab' ),
	'params' => array(
		array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Row stretch', 'fitfab' ),
			'param_name' => 'full_width',
			'value' => array(
				esc_html__( 'Default', 'fitfab' ) => '',
				esc_html__( 'Stretch row', 'fitfab' ) => 'stretch_row',
				esc_html__( 'Stretch row and content', 'fitfab' ) => 'stretch_row_content',
				esc_html__( 'Stretch row and content (no paddings)', 'fitfab' ) => 'stretch_row_content_no_spaces',
				esc_html__( 'FitFab Row', 'fitfab' ) => 'fit_fab_row', 
			),
			'description' => esc_html__( 'Select stretching options for row and content (Note: stretched may not work properly if parent container has "overflow: hidden" CSS property).', 'fitfab' ),
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Columns gap', 'fitfab' ),
			'param_name' => 'gap',
			'value' => array(
				'0px' => '0',
				'1px' => '1',
				'2px' => '2',
				'3px' => '3',
				'4px' => '4',
				'5px' => '5',
				'10px' => '10',
				'15px' => '15',
				'20px' => '20',
				'25px' => '25',
				'30px' => '30',
				'35px' => '35',
			),
			'std' => '0',
			'description' => esc_html__( 'Select gap between columns in row.', 'fitfab' ),
		),
		array(
			'type' => 'checkbox',
			'heading' => esc_html__( 'Full height row?', 'fitfab' ),
			'param_name' => 'full_height',
			'description' => esc_html__( 'If checked row will be set to full height.', 'fitfab' ),
			'value' => array( esc_html__( 'Yes', 'fitfab' ) => 'yes' ),
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Columns position', 'fitfab' ),
			'param_name' => 'columns_placement',
			'value' => array(
				esc_html__( 'Middle', 'fitfab' ) => 'middle',
				esc_html__( 'Top', 'fitfab' ) => 'top',
				esc_html__( 'Bottom', 'fitfab' ) => 'bottom',
				esc_html__( 'Stretch', 'fitfab' ) => 'stretch',
			),
			'description' => esc_html__( 'Select columns position within row.', 'fitfab' ),
			'dependency' => array(
				'element' => 'full_height',
				'not_empty' => true,
			),
		),
		array(
			'type' => 'checkbox',
			'heading' => esc_html__( 'Equal height', 'fitfab' ),
			'param_name' => 'equal_height',
			'description' => esc_html__( 'If checked columns will be set to equal height.', 'fitfab' ),
			'value' => array( esc_html__( 'Yes', 'fitfab' ) => 'yes' )
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Content position', 'fitfab' ),
			'param_name' => 'content_placement',
			'value' => array(
				esc_html__( 'Default', 'fitfab' ) => '',
				esc_html__( 'Top', 'fitfab' ) => 'top',
				esc_html__( 'Middle', 'fitfab' ) => 'middle',
				esc_html__( 'Bottom', 'fitfab' ) => 'bottom',
			),
			'description' => esc_html__( 'Select content position within columns.', 'fitfab' ),
		),
		array(
			'type' => 'checkbox',
			'heading' => esc_html__( 'Use video background?', 'fitfab' ),
			'param_name' => 'video_bg',
			'description' => esc_html__( 'If checked, video will be used as row background.', 'fitfab' ),
			'value' => array( esc_html__( 'Yes', 'fitfab' ) => 'yes' ),
		),
		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'YouTube link', 'fitfab' ),
			'param_name' => 'video_bg_url',
			'value' => 'https://www.youtube.com/watch?v=lMJXxhRFO1k',
			// default video url
			'description' => esc_html__( 'Add YouTube link.', 'fitfab' ),
			'dependency' => array(
				'element' => 'video_bg',
				'not_empty' => true,
			),
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Parallax', 'fitfab' ),
			'param_name' => 'video_bg_parallax',
			'value' => array(
				esc_html__( 'None', 'fitfab' ) => '',
				esc_html__( 'Simple', 'fitfab' ) => 'content-moving',
				esc_html__( 'With fade', 'fitfab' ) => 'content-moving-fade',
			),
			'description' => esc_html__( 'Add parallax type background for row.', 'fitfab' ),
			'dependency' => array(
				'element' => 'video_bg',
				'not_empty' => true,
			),
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Parallax', 'fitfab' ),
			'param_name' => 'parallax',
			'value' => array(
				esc_html__( 'None', 'fitfab' ) => '',
				esc_html__( 'Simple', 'fitfab' ) => 'content-moving',
				esc_html__( 'With fade', 'fitfab' ) => 'content-moving-fade',
			),
			'description' => esc_html__( 'Add parallax type background for row (Note: If no image is specified, parallax will use background image from Design Options).', 'fitfab' ),
			'dependency' => array(
				'element' => 'video_bg',
				'is_empty' => true,
			),
		),
		array(
			'type' => 'attach_image',
			'heading' => esc_html__( 'Image', 'fitfab' ),
			'param_name' => 'parallax_image',
			'value' => '',
			'description' => esc_html__( 'Select image from media library.', 'fitfab' ),
			'dependency' => array(
				'element' => 'parallax',
				'not_empty' => true,
			),
		),
		array(
			'type' => 'el_id',
			'heading' => esc_html__( 'Row ID', 'fitfab' ),
			'param_name' => 'el_id',
			'description' => sprintf( esc_html__( 'Enter row ID (Note: make sure it is unique and valid according to <a href="%s" target="_blank">w3c specification</a>).', 'fitfab' ), 'http://www.w3schools.com/tags/att_global_id.asp' ),
		),
		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Extra class name', 'fitfab' ),
			'param_name' => 'el_class',
			'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'fitfab' ),
		),
		array(
			'type' => 'css_editor',
			'heading' => esc_html__( 'CSS box', 'fitfab' ),
			'param_name' => 'css',
			'group' => esc_html__( 'Design Options', 'fitfab' ),
		),
	),
	'js_view' => 'VcRowView',
));
}
