<?php

/**
 * FitFab - about section three 
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates.vc-map
 * @since fitfab 1.0.0
 */
add_action('vc_before_init', 'fitfab_about_section_three_vc');

function fitfab_about_section_three_vc() {
    vc_map(array(
        "name" => esc_html__("About Section Three - Fitfab", 'fitfab'),
        "base" => "fitfab_about_section_three",
        "class" => "",
        "category" => esc_html__("Fitfab", 'fitfab'),
        "params" => array(
            array(
                "type"=> "attach_image",
                "class"       => "",
                "heading"     => esc_html__( "Background Image", 'fitfab' ),
                "param_name"  => "background_image",
                "value"       => '',
                "description" => esc_html__( "Select Background Image Here", 'fitfab' )
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("Section One Title", 'fitfab'),
                "param_name" => "one_title",
                "value" => '',
                "description" => esc_html__("Enter Section One title", 'fitfab')
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("Section One Sub Title", 'fitfab'),
                "param_name" => "one_sub_title",
                "value" => '',
                "description" => esc_html__("Enter Section One Sub title", 'fitfab')
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("Section Button One Link", 'fitfab'),
                "param_name" => "button_one_link",
                "value" => '',
                "description" => esc_html__("Enter Button One Link", 'fitfab')
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("Section Button One Label", 'fitfab'),
                "param_name" => "button_one_text",
                "value" => '',
                "description" => esc_html__("Enter Section Button One Label", 'fitfab')
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("Section Two Title", 'fitfab'),
                "param_name" => "two_title",
                "value" => '',
                "description" => esc_html__("Enter Section Two title", 'fitfab')
            ),
            array(
                "type" => "textarea",
                "class" => "",
                "heading" => esc_html__("Section Two Content", 'fitfab'),
                "param_name" => "two_content",
                "value" => '',
                "description" => esc_html__("Enter Section Two Content", 'fitfab')
            ),
            array(
                'type' => 'param_group',
                "class" => "",
                'value' => '',
                'heading' => esc_html__('Pack List One', 'fitfab'),
                'param_name' => 'pack_list_one',
                'description' => esc_html__('Pack List One', 'fitfab'),
                'params' => array(
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("List Title One", 'fitfab'),
                        "param_name" => "list_title_one",
                        "value" => '',
                        "description" => esc_html__("Enter List Title One Here", 'fitfab')
                    ),
                )
            ),
            array(
                'type' => 'param_group',
                "class" => "",
                'value' => '',
                'heading' => esc_html__('Pack List Two', 'fitfab'),
                'param_name' => 'pack_list_two',
                'description' => esc_html__('Pack List Two', 'fitfab'),
                'params' => array(
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("List Title Two", 'fitfab'),
                        "param_name" => "list_title_two",
                        "value" => '',
                        "description" => esc_html__("Enter List Title Two Here", 'fitfab')
                    ),
                )
            ),
        )
    ));
}

class WPBakeryShortCode_fitfab_about_section_three extends WPBakeryShortCode {
    
}
