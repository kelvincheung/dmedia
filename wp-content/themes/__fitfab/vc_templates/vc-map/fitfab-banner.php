<?php

/**
 * FitFab - banner 
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates.vc-map
 * @since fitfab 1.0.0
 */
add_action('vc_before_init', 'fitfab_banner_vc');

function fitfab_banner_vc() {
    vc_map(array(
        "name" => esc_html__("Banner - Fitfab", 'fitfab'),
        "base" => "fitfab_banner",
        "class" => "",
        "category" => esc_html__('Fitfab Banner', 'fitfab'),
        "params" => array(
            array(
                'type' => 'attach_image',
                'value' => '',
                'heading' => 'Upload Banner Image',
                'param_name' => 'banner_image',
            ),
            array(
                'type' => 'textfield',
                'value' => '',
                'heading' => 'Section Title',
                'param_name' => 'title',
                "description" => esc_html__("Enter Section Title Here", 'fitfab')
            ),
        )
    ));
}

class WPBakeryShortCode_fitfab_banner extends WPBakeryShortCode {
    
}
