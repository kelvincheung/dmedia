<?php

/**
 * FitFab - testimonial 
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates.vc-map
 * @since fitfab 1.0.0
 */
add_action('vc_before_init', 'fitfab_about_vc');

function fitfab_about_vc() {
    global $svg_fonts;
    vc_map(array(
        "name" => esc_html__("About - Fitfab", 'fitfab'),
        "base" => "fitfab_about",
        "class" => "",
        "category" => esc_html__("Fitfab", 'fitfab'),
        "params" => array(
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("Section Title", 'fitfab'),
                "param_name" => "title",
                "value" => '',
                "description" => esc_html__("Enter section title", 'fitfab')
            ),
            array(
                "type" => "textarea",
                "class" => "",
                "heading" => esc_html__("Section sub Title", 'fitfab'),
                "param_name" => "sub_title",
                "value" => '',
                "description" => esc_html__("Enter section sub title", 'fitfab')
            ),
            array(
                "type" => "textarea_html",
                "class" => "",
                "heading" => esc_html__("Section Content", 'fitfab'),
                "param_name" => "main_content",
                "value" => '',
                "description" => esc_html__("Enter section content", 'fitfab')
            ),
            array(
                "type" => "attach_image",
                "class" => "",
                "heading" => esc_html__("Section Image", 'fitfab'),
                "param_name" => "image",
                "value" => '',
                "description" => esc_html__("Upload section image", 'fitfab')
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("Section One Title", 'fitfab'),
                "param_name" => "one_title",
                "value" => '',
                "description" => esc_html__("Enter Section One Title", 'fitfab')
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("Section One Sub Title", 'fitfab'),
                "param_name" => "one_sub_title",
                "value" => '',
                "description" => esc_html__("Enter Section One Sub Title", 'fitfab')
            ),
            array(
                "type" => "textarea",
                "class" => "",
                "heading" => esc_html__("Section One Content", 'fitfab'),
                "param_name" => "one_content",
                "value" => '',
                "description" => esc_html__("Enter Section One Content", 'fitfab')
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("Section Button One Link", 'fitfab'),
                "param_name" => "button_one_link",
                "value" => '',
                "description" => esc_html__("Enter Button One Link", 'fitfab')
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("Section Button One Label", 'fitfab'),
                "param_name" => "button_one_text",
                "value" => '',
                "description" => esc_html__("Enter Section Button One Label", 'fitfab')
            ),
            array(
                'type' => 'param_group',
                "class" => "",
                'value' => '',
                'heading' => esc_html__('Fitness Feature', 'fitfab'),
                'param_name' => 'fitness_feature',
                'description' => esc_html__('Fitness Feature', 'fitfab'),
                'params' => array(
                    array(
                        "type" => "attach_image",
                        "class" => "",
                        "heading" => esc_html__("Feature Icon", 'fitfab'),
                        "param_name" => "feature_icon",
                        "value" => $svg_fonts,
                        "description" => esc_html__("Enter Feature Icon Here", 'fitfab')
                    ),
                     array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Feature Title", 'fitfab'),
                        "param_name" => "feature_title",
                        "value" => '',
                        "description" => esc_html__("Enter Feature Title Here", 'fitfab')
                    ),
                     array(
                        "type" => "textarea",
                        "class" => "",
                        "heading" => esc_html__("Feature Content", 'fitfab'),
                        "param_name" => "feature_content",
                        "value" => '',
                        "description" => esc_html__("Enter Feature Content Here", 'fitfab')
                    ),
                )
            ),
        )
    ));
}

class WPBakeryShortCode_fitfab_about extends WPBakeryShortCode {
    
}
