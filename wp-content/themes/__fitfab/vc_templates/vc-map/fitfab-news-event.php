<?php

/**
 * FitFab - nesw event 
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates.vc-map
 * @since fitfab 1.0.0
 */
add_action('vc_before_init', 'fitfab_news_event_vc');

function fitfab_news_event_vc() {
    vc_map(array(
        "name" => esc_html__("News Event - Fitfab", 'fitfab'),
        "base" => "fitfab_news_event",
        "class" => "",
        "category" => esc_html__("Fitfab", 'fitfab'),
        "params" => array(
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => esc_html__("News Event Style", 'fitfab'),
                "param_name" => "news_event_style",
                "description" => esc_html__("Select News Event Style", 'fitfab'),
                "value" => array(
                    esc_html__('Select', 'fitfab') => '',
                    esc_html__('Home', 'fitfab') => 'home',
                    esc_html__('Home1', 'fitfab') => 'home1',
                ),
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("News Title", 'fitfab'),
                "param_name" => "news_title",
                "value" => '',
                "description" => esc_html__("Enter News Title Here", 'fitfab')
            ),
             array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("Views All News Linlk", 'fitfab'),
                "param_name" => "all_news_link",
                "value" => '',
                "description" => esc_html__("Enter View All News Linlk Here", 'fitfab')
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("Event Title", 'fitfab'),
                "param_name" => "event_title",
                "value" => '',
                "description" => esc_html__("Enter Event Title Here", 'fitfab')
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("View All Event Linlk", 'fitfab'),
                "param_name" => "all_event_link",
                "value" => '',
                "description" => esc_html__("Enter View All Event Linlk Here", 'fitfab')
            ),
        )
    ));
}

class WPBakeryShortCode_fitfab_news_event extends WPBakeryShortCode {
    
}
