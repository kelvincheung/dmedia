<?php

/**
 * FitFab - contact 
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates.vc-map
 * @since fitfab 1.0.0
 */
function fitfab_form() {
    $projects = get_posts(array('post_type' => 'wpcf7_contact_form', 'posts_per_page' => '-1'));
    $projects_titles = array();
    foreach ($projects as $project) {
        $projects_titles[$project->post_title] = $project->ID;
    }
    return $projects_titles;
}
add_action('vc_before_init', 'fitfab_contact_vc');

function fitfab_contact_vc() {
    vc_map(array(
        "name" => esc_html__("Contact - Fitfab", 'fitfab'),
        "base" => "fitfab_contact",
        "class" => "",
        "category" => esc_html__("Fitfab", 'fitfab'),
        "params" => array(
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("Section Title", 'fitfab'),
                "param_name" => "title",
                "value" => '',
                "description" => esc_html__("Enter section title", 'fitfab')
            ),
            array(
                "type" => "attach_image",
                "class" => "",
                "heading" => esc_html__("Section Image", 'fitfab'),
                "param_name" => "image",
                "value" => '',
                "description" => esc_html__("Upload section image", 'fitfab')
            ),
            array(
                "type" => "dropdown",
                "heading" => esc_html__("Select a Form", 'fitfab'),
                "param_name" => "form",
                "value" => fitfab_form(),
                "description" => esc_html__("Select a Form.", 'fitfab')
            ),
        )
    ));
}

class WPBakeryShortCode_fitfab_contact extends WPBakeryShortCode {
    
}
