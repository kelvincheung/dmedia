<?php

/**
 * FitFab - Classes
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates.vc-map
 * @since fitfab 1.0.0
 */
add_action('vc_before_init', 'fitfab_classes_vc');

function fitfab_classes_vc() {
    vc_map(array(
        "name" => esc_html__("Classes - Fitfab", 'fitfab'),
        "base" => "fitfab_classes",
        "class" => "",
        "category" => esc_html__("Fitfab Classes", 'fitfab'),
        "params" => array(
        
            )
    ));
}

class WPBakeryShortCode_fitfab_classes extends WPBakeryShortCode {
    
}
