<?php
/**
 * FitFab - map all short code
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates.vc-map
 * @since fitfab 1.0.0
 */

$vc_files = array(
    'general-helper',
    'fitfab-award',
    'fitfab-class-schedule',
    'fitfab-contact',
    'fitfab-contact-map',
    'fitfab-contact-join',
    'fitfab-faq',
    'fitfab-trainers',
    'fitfab-fitness',
    'fitfab-testimonial',
    'fitfab-news-event',
    'fitfab-latest-news',
    'fitfab-upcoming-events',
    'fitfab-popular-class',
    'fitfab-our-schedule',
    'fitfab-our-schedule-two',
    'fitfab-brand',
    'fitfab-packages',
    'fitfab-about',
    'fitfab-about-section-two',
    'fitfab-about-section-three',
    'fitfab-banner',
    'fitfab-event',
    'fitfab-classes',
    'fitfab-trainers-list',
    'fitfab-pricing-packages',
    'fitfab-breadcrum',
    'fitfab-row-extend',
    'fitfab_fitness_program',
    'fitfab_gym_services', 
);

foreach ($vc_files as $file) {
    fitfab_inclusion('vc_templates/vc-map/'.$file.'.php');
}