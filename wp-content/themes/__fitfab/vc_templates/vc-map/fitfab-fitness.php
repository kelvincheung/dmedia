<?php

/**
 * FitFab - welcome gym fitness 
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates.vc-map
 * @since fitfab 1.0.0
 */
add_action('vc_before_init', 'home_fitness_vc');

function home_fitness_vc() {
    global $svg_fonts;
    vc_map(
            array(
                "name" => esc_html__("Fitness - Fitfab", 'fitfab'),
                "base" => "fitfab_fitness",
                "class" => "",
                "category" => esc_html__("Fitfab", 'fitfab'),
                'params' => array(
                    array(
                        "type" => "dropdown",
                        "class" => "",
                        "heading" => esc_html__("Fitness Style", 'fitfab'),
                        "param_name" => "fitness_style",
                        "description" => esc_html__("Select Fitness Style", 'fitfab'),
                        "value" => array(
                            esc_html__('Select', 'fitfab') => '',
                            esc_html__('Home', 'fitfab') => 'home',
                            esc_html__('Home1', 'fitfab') => 'home1',
                            esc_html__('Home2', 'fitfab') => 'home2',
                        ),
                    ),
                    // params group
                    array(
                        'type' => 'param_group',
                        "class" => "",
                        'value' => '',
                        'heading' => esc_html__('Fitness Program', 'fitfab'),
                        'param_name' => 'fitness_program',
                        'description' => esc_html__('Fitness Program', 'fitfab'),
                        // Note params is mapped inside param-group:
                        'params' => array(
                            array(
                                "type" => "attach_image",
                                "class" => "",
                                "heading" => esc_html__("Image", 'fitfab'),
                                "param_name" => "image",
                                "value" => '',
                                "description" => esc_html__("Select Image Here", 'fitfab')
                            ),
                            array(
                                "type" => "textfield",
                                "class" => "",
                                "heading" => esc_html__("Title", 'fitfab'),
                                "param_name" => "title",
                                "value" => '',
                                "description" => esc_html__("Enter Title Here", 'fitfab')
                            ),
                            array(
                                "type" => "textfield",
                                "class" => "",
                                "heading" => esc_html__("Sub Title", 'fitfab'),
                                "param_name" => "sub_title",
                                "value" => '',
                                "description" => esc_html__("Enter Sub Title Here", 'fitfab')
                            ),
                            array(
                                "type" => "textfield",
                                "class" => "",
                                "heading" => esc_html__("Price", 'fitfab'),
                                "param_name" => "price",
                                "value" => '',
                                "description" => esc_html__("Enter Price Here", 'fitfab')
                            )
                        )
                    ),
                    array(
                        "type" => "attach_image",
                        "class" => "",
                        "heading" => esc_html__("Background Image", 'fitfab'),
                        "param_name" => "background_image",
                        "value" => '',
                        "description" => esc_html__("Select Background Image Here", 'fitfab')
                    ),
                    array(
                        'type' => 'textfield',
                        'value' => '',
                        'heading' => esc_html__('Fitness Title', 'fitfab'),
                        'param_name' => 'fitness_title',
                        'description' => esc_html__('Enter Fitness Title Here', 'fitfab')
                    ),
                    array(
                        'type' => 'textfield',
                        'value' => '',
                        'heading' => esc_html__('Fitness Sub Title', 'fitfab'),
                        'param_name' => 'fitness_sub_title',
                        'description' => esc_html__('Enter Fitness Sub Title Here', 'fitfab')
                    ),
                    array(
                        'type' => 'textarea',
                        'value' => '',
                        'heading' => esc_html__('Fitness Description', 'fitfab'),
                        'param_name' => 'fitness_description',
                        'description' => esc_html__('Enter Fitness Description Here', 'fitfab')
                    ),
                    // params group
                    array(
                        'type' => 'param_group',
                        "class" => "",
                        'value' => '',
                        'heading' => esc_html__('Fitness Feature', 'fitfab'),
                        'param_name' => 'fitness_feature',
                        'description' => esc_html__('Fitness Feature', 'fitfab'),
                        // Note params is mapped inside param-group:
                        'params' => array(
                            array(
                                "type" => "attach_image",
                                "class" => "",
                                "heading" => esc_html__("Feature Icon", 'fitfab'),
                                "param_name" => "feature_icon",
                                "value" => $svg_fonts,
                                "description" => esc_html__("Enter Feature Icon Here", 'fitfab')
                            ),
                            array(
                                "type" => "textfield",
                                "class" => "",
                                "heading" => esc_html__("Feature Title", 'fitfab'),
                                "param_name" => "feature_title",
                                "value" => '',
                                "description" => esc_html__("Enter Feature Title Here", 'fitfab')
                            ),
                            array(
                                "type" => "textarea",
                                "class" => "",
                                "heading" => esc_html__("Feature Description", 'fitfab'),
                                "param_name" => "feature_description",
                                "value" => '',
                                "description" => esc_html__("Enter Feature Description Here", 'fitfab')
                            )
                        )
                    ),
                    array(
                        'type' => 'textfield',
                        'value' => '',
                        'heading' => esc_html__('Fitness Heading', 'fitfab'),
                        'param_name' => 'fitness_heading',
                        'description' => esc_html__('Enter Fitness Heading Here', 'fitfab')
                    ),
                    array(
                        'type' => 'textfield',
                        'value' => '',
                        'heading' => esc_html__('Fitness Sub Heading', 'fitfab'),
                        'param_name' => 'fitness_subheading',
                        'description' => esc_html__('Enter Fitness Sub Heading Here', 'fitfab')
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Join Button Text", 'fitfab'),
                        "param_name" => "join_button_text",
                        "value" => '',
                        "description" => esc_html__("Enter Join Button Text Here", 'fitfab')
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Join Button Link", 'fitfab'),
                        "param_name" => "join_button_link",
                        "value" => '',
                        "description" => esc_html__("Enter Join Button Link Here", 'fitfab')
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("More Button Text", 'fitfab'),
                        "param_name" => "more_button_text",
                        "value" => '',
                        "description" => esc_html__("Enter More Button Text Here", 'fitfab')
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("More Button Link", 'fitfab'),
                        "param_name" => "more_button_link",
                        "value" => '',
                        "description" => esc_html__("Enter More Button Link Here", 'fitfab')
                    ),
                )
            )
    );
}

class WPBakeryShortCode_fitfab_fitness extends WPBakeryShortCode {
    
}
