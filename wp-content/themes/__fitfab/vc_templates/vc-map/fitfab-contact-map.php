<?php

/**
 * FitFab - contact map 
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates.vc-map
 * @since fitfab 1.0.0
 */
add_action('vc_before_init', 'fitfab_contact_map_vc');

function fitfab_contact_map_vc() {
    vc_map(array(
        "name" => esc_html__("Contact Map - Fitfab", 'fitfab'),
        "base" => "fitfab_contact_map",
        "class" => "",
        "category" => esc_html__("Fitfab", 'fitfab'),
        "params" => array(
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("Map Section Title", 'fitfab'),
                "param_name" => "map_section_title",
                "value" => '',
                "description" => esc_html__("Enter Map Section Title", 'fitfab')
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_html__("Address Section Title", 'fitfab'),
                "param_name" => "section_title",
                "value" => '',
                "description" => esc_html__("Enter Address Section Title", 'fitfab')
            ),
        )
    ));
}

class WPBakeryShortCode_fitfab_contact_map extends WPBakeryShortCode {
    
}
