<?php
/**
 * FitFab - fitfab breadcrum
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates
 * @since fitfab 1.0.0
 */
extract(shortcode_atts(array( ), $atts));
?>
<!-- package_wrap start here -->
<div class="breadcrum-sec fit-fab-breadcrum-layout-one">
    <div class="container">
	<?php fitfab_breadcrumb(); ?>
    </div>
</div>
<!-- package_wrap End here -->
<?php
echo $this->endBlockComment('fitfab_breadcrum');
