<?php
/**
 * FitFab - faq shortcode
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates
 * @since fitfab 1.0.0
 */
extract(shortcode_atts(array(
    'background_image' => '',
    'title' => '',
    'sec_content' => '',
    'no_of_items' => '',
     ), $atts));
if (!$no_of_items) {
    $no_of_items = 8;
}
$image_link = wp_get_attachment_image_src($background_image, "large");
$bg_image = '';
if ($image_link) :
    $bg_image .= 'background-image:url(' . $image_link[0] . ');background-repeat:no-repeat;background-position:left 97%;';
endif;
$bg_image .= 'background-color:#fff;';
?>
<div class="fit-fab-faq-layout-one">
<div class="breadcrum-sec">
        <div class="container">
            <?php fitfab_breadcrumb(); ?>
        </div>
    </div>
<section class="artical-faq" style="<?php echo esc_attr($bg_image); ?>">
    <div class="container">
        <div class="faq-content">
            <?php if (!empty($title)) : ?>
                <h2><?php echo esc_html($title); ?></h2>
                <?php
            endif;
            if (!empty($sec_content)) :
                ?>
                <P><?php echo esc_html($sec_content); ?></P>
            <?php endif; ?>
        </div>
        <?php
        $count = 1;
        $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

        $args = array(
            'post_type' => 'faq',
            'post_status' => 'publish',
            'paged' => $paged,
            'posts_per_page' => $no_of_items,
        );
        $sQuestion = (string) filter_input(INPUT_GET, 'q');
        if (!empty($sQuestion)) {
            $args['s'] = $sQuestion;
        }
        $oFaqQuery = new WP_Query($args);
        
        if ($oFaqQuery->have_posts()) :
            while ($oFaqQuery->have_posts()) : $oFaqQuery->the_post();
                ?>
                <div class="faq-qs-sec">
                    <strong><span class="ques"><?php esc_html_e('Q.', 'fitfab'); ?></span><?php the_title(); ?></strong>
                    <p>
                        <span class="ans"><?php esc_html_e('A.', 'fitfab'); ?></span></p><?php the_content(); ?>
                    
                </div>
                <?php
            endwhile;
            fitfab_pagenavi();
            wp_reset_postdata();
        else :
            get_template_part('content/none');
        endif;
        ?>
    </div>
</section>
</div>
<?php
echo $this->endBlockComment('fitfab_faq');
