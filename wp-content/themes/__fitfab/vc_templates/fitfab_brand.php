<?php
/**
 * FitFab - coolest brand shortcode
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates.va-map
 * @since fitfab 1.0.0
 */
extract(shortcode_atts(array(
    'title' => '',
    'sub_title' => '',
    'no_of_brand' => '',
  ), $atts));

if (!$no_of_brand) {
    $no_of_brand = 6;
}
?>
<!-- home-3-pacakage style start -->
<section class="closet-brands fit-fab-brand-layout-one">
    <div class="container">
	<?php if (!empty($title)) : ?>
    	<div class="home-three-head extra">
    	    <h2><span><?php echo esc_html($title); ?></span> <?php echo esc_html($sub_title); ?> </h2>
    	</div>
	<?php endif; ?>

        <div class="row">
            <div class="col-xs-12">
                <ul class="brands-list">
		    <?php
		    $args = array(
				'post_type' => 'brand',
				'post_status' => 'publish',
				'posts_per_page' => $no_of_brand
		    );
		    $fitfab_brand = new WP_Query($args);
		    if ($fitfab_brand->have_posts()) :
			while ($fitfab_brand->have_posts()) : $fitfab_brand->the_post();
			    if (function_exists('fitfab_rwmb_meta')) {
				$logo_link = fitfab_rwmb_meta('fitfab_logo_link');
			    }
			    $logo_link = ($logo_link) ? $logo_link : '#';
			    if (has_post_thumbnail()) :
				?>
	    		    <li>
	    			 <a href="<?php echo esc_url($logo_link); ?>">
					   <?php the_post_thumbnail(); ?>
	    			 </a>
	    		    </li>
				<?php
			    endif;
			endwhile;
			wp_reset_postdata();
		    endif;
		    ?> 
                </ul>
            </div>
        </div>
    </div>
</section>
<?php
echo $this->endBlockComment('fitfab_brand');
