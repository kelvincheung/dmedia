<?php
/**
 * FitFab - fitness
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates
 * @since fitfab 1.0.0
 */
extract(shortcode_atts(array(
    'fitness_program' => '',
		), $atts));
?>
<section class="fitfab-gym fitfab-fitness-programs-layout-one">
    <div class="facility-list-wrap">
	<div class="container">
	    <div class="row">
		<ul class="facility-list clearfix">
		    <?php
		    if (!empty($fitness_program)) :
			$packages = vc_param_group_parse_atts($fitness_program);
			if (isset($packages)) :
			    foreach ($packages as $package) :
				$back_image = '';
				if (!empty($package['image'])) :
				    $package_image = wp_get_attachment_image_src($package['image'], "large");
				    $back_image = 'background-image:url(' . $package_image[0] . ')';
				endif;
				?>
	    		    <li class = "" style="<?php echo esc_attr($back_image); ?>">
	    			<div class = "build">
					<?php if (!empty($package['title'])) : ?>
					    <h2><?php echo esc_html($package['title']); ?></h2>
					<?php endif; ?>
					<?php if (!empty($package['sub_title'])) : ?>
					    <span><?php echo esc_html($package['sub_title']); ?></span>
					<?php endif; ?>
	    			</div>
				    <?php if (!empty($package['price'])) : ?>
					<span style="color: <?php echo esc_attr($package['price_color']); ?>" class="label-price"><?php echo esc_html($package['price']); ?></span>
				    <?php endif; ?>
	    		    </li>
				<?php
			    endforeach;
			endif;
		    endif;
		    ?>
		</ul>
	    </div>
	</div>
    </div>
</section>
<?php
echo $this->endBlockComment('fitfab_fitness_program');
