<?php
/**
 * FitFab - contact join
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates
 * @since fitfab 1.0.0
 */
extract(shortcode_atts(array(
    'background_image' => '',
    'dsecription' => '',
    'title' => '',
    'phone' => '',
    'text' => '',
                ), $atts));

$image_link = wp_get_attachment_image_src($background_image, "large");
$bg_image = '';
if ($image_link) :
    $bg_image .= 'background-image:url(' . $image_link[0] . ');background-repeat:no-repeat;background-position:bottom right;';
endif;
$bg_image .= 'background-color:#fff;';
?>
<!-- join-free class start here -->
<section class="join-free-class fit-fab-join-free-classes-layout-one" style="<?php echo esc_attr($bg_image); ?>">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 free-join-inner">
                <?php if (!empty($dsecription)) : ?>
                    <strong><?php echo esc_html($dsecription); ?></strong>
                <?php endif; ?>
                <?php if (!empty($title)) : ?>
                    <h3><?php echo esc_html($title); ?> </h3>
                <?php endif; ?>
                <?php if (!empty($phone)) : ?>
                    <span><?php echo esc_html($phone); ?> <span><?php echo esc_html($text); ?></span></span>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<!-- join-free class end here -->
<?php
echo $this->endBlockComment('fitfab_contact_section_bottom');
