<?php
/**
 * FitFab - news event shortcode
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates
 * @since fitfab 1.0.00
 */
extract(shortcode_atts(array(
    'news_event_style' => '',
    'news_title' => '',
    'event_title' => '',
    'all_news_link' => '',
    'all_event_link' => '',
    ), $atts));

$newscount = 2;
$eventscount = 2;

$event_args = array(
    'post_type' => 'event',
    'post_status' => 'publish',
    'order' => 'ASC',
    'showposts' => $eventscount,
);
$event_query = new WP_Query($event_args);

$args = array(
    'ignore_sticky_posts' => 1,
    'post_status' => 'publish',
    'showposts' => $newscount,
);
$fitfab_news_events = new WP_Query($args);
?>
<?php if ($news_event_style == 'home' || $news_event_style == '') : ?>
    <!-- news-home start here -->
    <section class="news-home fitfab-news-event-layout-one">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <?php if (!empty($news_title)) : ?>
                        <div class="head-global">
                            <h2 class="h2"><?php echo esc_html($news_title); ?></h2>
                        </div>
                    <?php endif; ?>
                    <?php
                    if ($fitfab_news_events->have_posts()) :
                        while ($fitfab_news_events->have_posts()) : $fitfab_news_events->the_post();
                            ?>
                            <div class="row news-gap">
                                <?php
                                if (has_post_thumbnail()):
                                    $url = wp_get_attachment_url(get_post_thumbnail_id());
                                    ?>   
                                    <div class="col-xs-12 col-sm-4 news-img zoom">
                                        <figure>
                                           <a href="<?php the_permalink(); ?>">
                                              <img src="<?php echo esc_url(fitfab_resize($url, '178', '126')); ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" />
                                           </a>
                                        </figure>
                                    </div>
                                <?php endif; ?>

                                <div class="col-xs-12 col-sm-8 news-content">
                                    <?php if (get_the_title()) : ?>
                                        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                    <?php endif; ?>
                                    <?php if (get_the_content()) : ?>
                                        <p><?php echo wp_trim_words(get_the_content(), 20, ''); ?></p>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <?php
                        endwhile;
                        wp_reset_postdata();
                    endif;
                    ?>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <?php if (!empty($event_title)) : ?>
                        <div class="head-global">
                            <h2 class="h2"><?php echo esc_html($event_title); ?></h2>
                        </div>
                        <?php
                    endif;
                    if ($event_query->have_posts()) :
                        while ($event_query->have_posts()) : $event_query->the_post();
                            global $post, $EM_Event;
                            $EM_Event = em_get_event($post->ID, 'post_id'); ?>
                            <div class="first-event clearfix">
                                <span class="event-calender"> <?php echo esc_html($EM_Event->output('#d')); ?> <span><?php echo esc_html($EM_Event->output('#M')); ?></span> </span>
                                <div class="event-info">
                                    <?php if (get_the_title()) : ?>
                                        <h3><a href="<?php echo esc_url(get_permalink()); ?>"><?php the_title(); ?></a></h3>
                                    <?php endif; ?>
                                    <div class="time-location">
                                        <?php if ($EM_Event->output('#_12HSTARTTIME')) : ?>
                                            <span><?php echo esc_html($EM_Event->output('#_12HSTARTTIME')); ?> - <?php echo esc_html($EM_Event->output('#_12HENDTIME')); ?> </span>
                                        <?php endif; ?>
                                        <?php if ($EM_Event->output('#_LOCATIONNAME')) : ?>
                                            <span><?php echo esc_html($EM_Event->output('#_LOCATIONNAME')); ?>, <?php echo esc_html($EM_Event->output('#_LOCATIONTOWN')); ?></span>
                                        <?php endif; ?>
                                    </div>
                                    <?php if (get_the_content()) : ?>
                                        <p>
                                            <?php echo wp_trim_words(get_the_content(), 20, ''); ?>
                                        </p>
                                    <?php endif; ?>
                                    <a href="<?php echo esc_url(get_permalink()); ?>" class="link"><?php esc_html_e('read more', 'fitfab'); ?></a>
                                </div>
                            </div>
                            <?php
                        endwhile;
                        wp_reset_postdata();
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </section>
    <!-- news-home End here -->
<?php else : ?>
    <!-- latest-news-home-2 style end here -->

    <!-- news-home start here -->
    <section class="populer-classes news-home-3 fitfab-news-event-layout-two">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 classes-populer">
                    <div class="home-three-head extra clearfix">
                        <?php if (!empty($news_title)) : ?>
                            <h2><span><?php esc_html_e('UPCOMING', 'fitfab'); ?></span><?php echo esc_html($news_title); ?></h2>
                        <?php endif; ?>
                         <?php $all_news_link = (!empty($all_news_link)) ? $all_news_link : '#';  ?>
                        <span> <a href="<?php echo esc_url($all_news_link); ?>" class="button-btn small-btn"><?php esc_html_e('view all news', 'fitfab'); ?></a></span>
                    </div>

                    <?php if ($fitfab_news_events->have_posts()) :
                        while ($fitfab_news_events->have_posts()) : $fitfab_news_events->the_post(); ?>
                            <div class="row news-gap">
                                <?php
                                if (has_post_thumbnail()):
                                    $url = wp_get_attachment_url(get_post_thumbnail_id());
                                    ?>   
                                    <div class="col-xs-12 col-sm-4 news-img zoom">
                                            <figure>
                                                <a href="<?php the_permalink(); ?>">
                                                    <img src="<?php echo esc_url(fitfab_resize($url, '178', '126')); ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" />
                                                </a>
                                            </figure>
                                    </div>
                                <?php endif; ?>
                                <div class="col-xs-12 col-sm-8 news-content">
                                    <?php if (get_the_title()) : ?>
				    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                        <?php
                                    endif;
                                    if (get_the_content()) : ?>
                                        <p><?php echo wp_trim_words(get_the_content(), 20, ''); ?></p>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <?php
                        endwhile;
                        wp_reset_postdata();
                    endif;
                    ?>
                </div>
                
                <div class="col-xs-12 col-sm-6 classes-populer about-fit">
                    <div class="home-three-head extra clearfix">
                        <?php if (!empty($event_title)) : ?>
                            <h2><span><?php esc_html_e('UPCOMING', 'fitfab'); ?></span><?php echo esc_html($event_title); ?></h2>
                        <?php endif; ?>
                        <?php $all_event_link = (!empty($all_event_link)) ? $all_event_link : '#';  ?>
                        <span> <a href="<?php echo esc_url($all_event_link); ?>" class="button-btn small-btn green-bg"><?php esc_html_e('view all events', 'fitfab'); ?></a></span>
                    </div>
                    <?php
                    $i = 1;
                    $event_query = new WP_Query($event_args);
                    if ($event_query->have_posts()) :
                        while ($event_query->have_posts()) : $event_query->the_post();
                            global $post, $EM_Event;
                            $EM_Event = em_get_event($post->ID, 'post_id');
                            if ($i == 1) :
                                $class = 'first-3-event';
                            endif;
                            $i++;
                            ?>
                            <div class="first-event <?php echo esc_attr($class); ?>">
                                <span class="event-calender"> <?php echo esc_html($EM_Event->output('#d')); ?> <span><?php echo esc_html($EM_Event->output('#M')); ?></span> </span>
                                <div class="event-info">
                                    <?php if (get_the_title()) : ?>
                                        <h3><a href="<?php echo esc_url(get_permalink()); ?>"><?php the_title(); ?></a></h3>
                                    <?php endif; ?>
                                    <div class="time-location">
                                        <?php if ($EM_Event->output('#_12HSTARTTIME')) : ?>
                                            <span><?php echo $EM_Event->output('#_12HSTARTTIME'); ?> - <?php echo $EM_Event->output('#_12HENDTIME'); ?> </span>
                                        <?php endif; ?>
                                        <?php if ($EM_Event->output('#_LOCATIONNAME')) : ?>
                                            <span><?php echo $EM_Event->output('#_LOCATIONNAME'); ?>, <?php echo $EM_Event->output('#_LOCATIONTOWN'); ?></span>
                                        <?php endif; ?>
                                    </div>
                                    <?php if (get_the_content()) : ?>
                                       <p><?php echo wp_trim_words(get_the_content(), 20, ''); ?></p>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <?php
                        endwhile;
                        wp_reset_postdata();
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </section>
    <!-- news-home End here -->

<?php
endif;
echo $this->endBlockComment('fitfab_news_event');
