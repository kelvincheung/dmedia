<?php
/**
 * FitFab - about section three
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates.va-map
 * @since fitfab 1.0.0
 */
extract(shortcode_atts(array(
    'background_image' => '',
    'one_title' => '',
    'one_sub_title' => '',
    'button_one_link' => '',
    'button_one_text' => '',
    'two_title' => '',
    'two_content' => '',
    'pack_list_one' => '',
    'pack_list_two' => '',
                ), $atts));

$image_link = wp_get_attachment_image_src($background_image, "large");
$bg_image = '';
if ($image_link) :
    $bg_image .= 'background-image:url(' . $image_link[0] . ');background-repeat:no-repeat;background-position:bottom left;';
endif;
$bg_image .= 'background-color:#fff;';
?>
<!-- about-us-sexy-wrap start here -->
<section class="about-us-sexy-wrap fit-fab-about-us-info-layout-one" style="<?php echo esc_attr($bg_image); ?>">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-4 fit_sexy-about">
                <?php if (!empty($one_title)) : ?>
                    <strong><?php echo esc_html($one_title); ?><strong><?php echo esc_html($one_sub_title); ?></strong></strong>
                <?php endif; ?>
                <?php if (!empty($button_one_link)) : ?>
                    <a href="<?php echo esc_url($button_one_link); ?>" class="button-btn"><?php echo esc_html($button_one_text); ?></a>
                <?php endif; ?>
            </div>
            <div class="col-xs-12 col-sm-8 founded-wrap">
                <?php if (!empty($two_title)) : ?>
                    <h3> <?php echo esc_html($two_title); ?></h3>
                    <?php
                endif;
                if (!empty($two_content)) : ?>
                    <p><?php echo esc_html($two_content); ?></p>
                <?php endif; ?>
                <div class="row top_gap">
                    <div class="col-xs-12 col-sm-6">
                        <ul class="list-global pack-list ">
                          <?php
                            $pack_lists = vc_param_group_parse_atts($atts['pack_list_one']);
                            foreach ($pack_lists as $pack_list) :
                          ?>
                                <li>
                                    <i class="fa fa-check-circle-o"></i><?php echo esc_html($pack_list['list_title_one']); ?>
                                </li>
                          <?php endforeach; ?>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <ul class="list-global pack-list ">
                            <?php
                            $pack_lists = vc_param_group_parse_atts($atts['pack_list_two']);
                            foreach ($pack_lists as $pack_list) :
                                ?>
                                <li>
                                    <i class="fa fa-check-circle-o"></i><?php echo esc_html($pack_list['list_title_two']); ?>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>

                </div>

            </div>

        </div>
    </div>
</section>
<!-- about-us-sexy-wrap end here -->
<?php
echo $this->endBlockComment('fitfab_about_section_three');
