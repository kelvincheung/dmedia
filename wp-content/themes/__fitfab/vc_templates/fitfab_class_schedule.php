<?php
/**
 * FitFab - class schedule shortcode
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates
 * @since fitfab 1.0.0
 */
extract(shortcode_atts(array(
    'class_schedule_title' => '',
    'class_schedule_sub_title' => '',
    'classcount' => '',
   ), $atts));

if (!$classcount):
    $classcount = 8;
endif;
?>
<!--facility-list-wrap start here-->
<section class="class-schedule fitfab-schedule-fitfab-layout-one">
    <div class="container">
        <div class="head-global">
            <?php if (!empty($class_schedule_title) || !empty($class_schedule_sub_title)) : ?>
                <h2 class = "h2"><?php
                    if (!empty($class_schedule_title)) : echo esc_html($class_schedule_title);
                    endif;
                    if (!empty($class_schedule_sub_title)) :
                        ?> 
                        <span><?php
                            echo esc_html($class_schedule_sub_title);
                        endif;
                        ?>
                    </span>
                </h2>
            <?php endif; ?>
        </div>
        <div class="row">
            <div class="col-xs-12 tabing-wrap">
                <div class="practice-list-main-container">
                    <?php $day_arr = array('monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'); ?>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs " role="tablist">
                        <?php
                        $i = 1;
                        foreach ($day_arr as $day_arrs) :
                            $class = '';
                            if ($i == 1) :
                                $class = 'active';
                            endif;
                            $i++;
                            ?>
                            <li role="presentation" class="<?php echo esc_attr($class); ?>">
                                <a href="#<?php print($day_arrs); ?>" aria-controls="<?php echo esc_attr($day_arrs); ?>" role="tab" data-toggle="tab"> <?php echo esc_html(strtoupper($day_arrs)); ?> </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <?php
                        $i = 1;
                        foreach ($day_arr as $day_arrs) :
                            $class = '';
                            if ($i == 1) :
                                $class = 'tab-pane fade in active';
                            else:
                                $class = 'tab-pane fade';
                            endif;
                            $i++;
                            ?>
                            <div role="tabpanel" class="<?php echo esc_attr($class); ?>" id="<?php echo esc_attr($day_arrs); ?>">
                                <div class="row">
                                    <?php
                                    $i=1;
                                    $args = array(
                                        'meta_key' => 'fitfab_class_day',
                                        'meta_value' => $day_arrs,
                                        'post_type' => 'class',
                                        'post_status' => 'publish',
                                        'showposts' => $classcount,
                                    );
                                    $fitfab_class_schedule = new WP_Query($args);
                                    if ($fitfab_class_schedule->have_posts()) :
                                        while ($fitfab_class_schedule->have_posts()) : $fitfab_class_schedule->the_post();
				    if(function_exists('fitfab_rwmb_meta')){
                                            $fitfab_class_start = fitfab_rwmb_meta('fitfab_class_start_time', $args = array('type' => 'datetime'), get_the_ID());
                                            $fitfab_class_end = fitfab_rwmb_meta('fitfab_class_end_time', $args = array('type' => 'datetime'), get_the_ID());
                                            $fitfab_class_icon = fitfab_rwmb_meta('fitfab_class_icons', $args = array('type' => 'select'), get_the_ID());
                                            $fitfab_class_start_day = date('l', strtotime($fitfab_class_start));
                                            $fitfab_class_start_time = date('h:iA', strtotime($fitfab_class_start)) ? date('h:iA', strtotime($fitfab_class_start)) : '';
                                            $fitfab_class_end_time = date('h:iA', strtotime($fitfab_class_end)) ? date('h:iA', strtotime($fitfab_class_end)) : '';
                                            $trainer_name = fitfab_rwmb_meta('fitfab_trainer', $args = array('type=checkbox'), get_the_ID());
                                            $rightcls = '';
                                            $topcls='';
                                            if($i%4==0) :
                                                $rightcls = 'right-line_hide';
                                            endif;
                                            if($i>4) :
                                                $topcls = 'top-spacer';
                                            endif;
                                            $i++;
                                            ?>
                                            <div class="col-xs-12 col-sm-3 practice-list <?php echo esc_attr($rightcls); ?> <?php echo esc_attr($topcls); ?>">
                                                <?php if (!empty($fitfab_class_icon)) : ?>
                                                    <figure>
                                                        <img class="svg" src="<?php echo esc_url(wp_get_attachment_url($fitfab_class_icon)); ?>" alt="<?php the_title(); ?>"/>
                                                    </figure>
                                                <?php endif; ?>
                                                <span><?php echo esc_html($fitfab_class_start_time); ?>-<?php echo esc_html($fitfab_class_end_time); ?></span>
                                                <h3><?php the_title(); ?></h3>
                                                <strong><?php echo esc_html($trainer_name); ?></strong>
                                            </div>
                                            <?php
				               }
                                        endwhile;
                                        wp_reset_postdata();
                                    endif;
                                    ?>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--facility-list-wrap End here-->
<?php
echo $this->endBlockComment('fitfab_class_schedule');
