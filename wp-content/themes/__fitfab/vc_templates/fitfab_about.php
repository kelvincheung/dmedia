<?php
/**
 * FitFab - about us
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates.va-map
 * @since fitfab 1.0.0
 */
extract(shortcode_atts(array(
    'title' => esc_html__('About us', 'fitfab'),
    'sub_title' => esc_html__('Our Short Story', 'fitfab'),
    'main_content' => '',
    'image' => '',
    'one_title' => '',
    'one_sub_title' => '',
    'one_content' => '',
    'button_one_link' => '',
    'button_one_text' => '',
    'fitness_feature' => '',
     ), $atts));

?>
<section class="who_wrap about-wrap fit-fab-who-we-are-layout-one">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <div class="head-global family-oswald">
                    <?php if (!empty($title)) : ?>
                        <h2 class="h2"><?php echo esc_html($title); ?></h2>
                    <?php endif;
                    if (!empty($sub_title)) : ?>
                        <p><?php echo esc_html($sub_title); ?></p>
                    <?php endif; ?>
                </div>

                <div class="who-info">
                    <p><?php echo wp_kses($main_content, wp_kses_allowed_html('post')); ?></p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 zoom">
                <?php
                if (!empty($image)):
                    $src = wp_get_attachment_url($image, 'full');
                    ?>
                    <figure class="img-mobile">
                        <img src="<?php echo esc_url(fitfab_resize($src, '549', '320')); ?>" alt="<?php echo esc_attr($title); ?>" title="<?php echo esc_attr($title); ?>" />
                    </figure>
                <?php endif; ?>
            </div>

        </div>
        <div class="row">
            <div class="col-xs-12 looking-fit ">
                <div class="head-global family-oswald clearfix">
                    <?php if (!empty($one_title)) : ?>
                        <h2 class="h2"><?php echo esc_html($one_title); ?><span><?php echo esc_html($one_sub_title); ?></span></h2>
                        <?php
                    endif;
                    if (!empty($button_one_link)) :
                        ?>
                        <a href="<?php echo esc_url($button_one_link); ?>" class="button-btn small-btn green-bg"><?php echo esc_html($button_one_text); ?></a>
                    <?php endif; ?>
                </div>
                <?php if (!empty($one_content)) : ?>
                    <p><?php echo esc_html($one_content); ?></p>
                <?php endif; ?>
            </div>
        </div>

        <div class="row top_gap">
            <?php
            $features = vc_param_group_parse_atts($atts['fitness_feature']);
            foreach ($features as $feature) :
                ?>
                <div class="col-xs-12 col-sm-3 fit_list-block1">
                    <?php if (!empty($feature['feature_icon'])) : ?>
                        <figure>
                            <img class="svg" src="<?php echo esc_url(wp_get_attachment_url($feature['feature_icon'])); ?>" alt="<?php echo esc_attr($feature['feature_title']); ?>" title="<?php echo esc_attr($feature['feature_title']); ?>">
                        </figure>
                    <?php endif; ?>
                    <div class="fit-description1">
                        <?php if (!empty($feature['feature_title'])) : ?>
                            <h3> <?php echo esc_html($feature['feature_title']); ?> </h3>
                        <?php
                        endif;
                        if (!empty($feature['feature_content'])) : ?>
                            <p><?php echo esc_html($feature['feature_content']); ?></p>
                        <?php endif; ?>
                    </div>

                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>
<?php
echo $this->endBlockComment('fitfab_about');
