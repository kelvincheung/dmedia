<?php
/**
 * FitFab - classes
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates
 * @since fitfab 1.0.0
 */
extract(shortcode_atts(array(
                ), $atts));
?>
<!-- 	main-event-content style start here -->

<section class="populer-classes classes-page-class fit-fab-calsses-layout-one">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 tabing-wrap">
                <div>
                    <div class="clearfix">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs " role="tablist">
                            <?php
                            $terms = get_terms('class_categories', array('hide_empty' => 1));
                            $i = 1;
                            foreach ($terms as $term) :
                                $class = '';
                                if ($i == 1) :
                                    $class = "class=active";
                                endif;
                                $i++;
                                ?>
                                <li role="presentation" <?php echo esc_attr($class); ?>>
                                    <a href="#<?php print($term->slug); ?>" data-aria-controls="all" data-toggle="tab">
                                        <?php echo esc_html($term->name); ?>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                    <div class="tab-content">
                        <?php
                        $i = 1;
                        foreach ($terms as $term) :
                            $class = '';
                            if ($i == 1) :
                                $class = 'tab-pane fade in active';
                            else:
                                $class = 'tab-pane fade';
                            endif;
                            $i++;
                            ?>
                            <div role="tabpanel" class="<?php echo esc_attr($class); ?>" id="<?php echo esc_attr($term->slug); ?>">
                                <div class="row">
                                    <?php
                                    $args = array(
                                        'post_type' => 'class',
                                        'taxonomy' => 'class_categories',
                                        'term' => $term->slug,
                                        'post_status' => 'publish',
                                        'posts_per_page' => -1
                                    );
                                    $fitfab_classes = new WP_Query($args);
                                    if ($fitfab_classes->have_posts()) :
                                        while ($fitfab_classes->have_posts()) : $fitfab_classes->the_post();
                                            get_template_part('content/class', 'page');
                                        endwhile;
                                    else :
                                        get_template_part('content/', 'none');
                                    endif;
                                    wp_reset_postdata();
                                    ?>
                                </div>
                            </div>
                            <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- 	main-blog-content style end here -->
<?php
echo $this->endBlockComment('fitfab_classes');
