<?php
/**
 * FitFab - pricing packages
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates
 * @since fitfab 1.0.0
 */
extract(shortcode_atts(array(
 ), $atts));

$no_of_item = -1;

$args = array(
    "post_type" => "package",
    "post_status" => "publish",
    'posts_per_page' => $no_of_item
);
$fitfabPackageQuery = new WP_Query($args);
?>
<div class="fit-fab-package-pricing-layout-one">
<div class="breadcrum-sec">
        <div class="container">
            <?php fitfab_breadcrumb(); ?>
        </div>
    </div>
    <!-- package_wrap start here -->
    <section class="package-wrap pricing-package-wrap">
        <div class="container">
            <?php
            if ($fitfabPackageQuery->have_posts()) :
                while ($fitfabPackageQuery->have_posts()) : $fitfabPackageQuery->the_post();
                    get_template_part('content/pricing-packages');
                endwhile;
                wp_reset_postdata();
            else :
                get_template_part('content/', 'none');
            endif;
            ?>
        </div>
    </section>
</div>
<!-- package_wrap End here -->
<?php
echo $this->endBlockComment('fitfab_pricing_packages');
