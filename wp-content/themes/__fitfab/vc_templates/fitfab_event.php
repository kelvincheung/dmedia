<?php
/**
 * FitFab - about us
 *
 * @package fitfab
 * @subpackage fitfab.vc_templates
 * @since fitfab 1.0.0
 */
extract(shortcode_atts(array(
    'no_of_event' => '',
    'order_by' => '',
    'sort_order' => '',
    ), $atts));

if (!$no_of_event) {
    $no_of_event = 6;
}
if (!$order_by) {
    $order_by = 'date';
}
if (!$sort_order) {
    $sort_order = 'asc';
}
?>
<div class="fit-fab-events-layout-one">
    <div class="breadcrum-sec">
        <div class="container">
            <?php fitfab_breadcrumb(); ?>
        </div>
    </div>
<!-- 	main-event-content style start here -->
<section class="main-event-content">
    <div class="container">
        <?php
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $args = array(
            "post_type" => "event",
            "post_status" => "publish",
            "paged" => $paged,
            "posts_per_page" => $no_of_event,
            'orderby' => $order_by,
            'order' => $sort_order,
        );
        $fitfabQuery = new WP_Query($args);
        if ($fitfabQuery->have_posts()):
            while ($fitfabQuery->have_posts()):
                $fitfabQuery->the_post();
                get_template_part("content/event","page");
            endwhile;
            fitfab_pagenavi($fitfabQuery);
        else :
            get_template_part("content/none");
        endif;
        wp_reset_postdata();
        ?> 

    </div>
</section>
</div>
<!-- 	main-blog-content style end here -->
<?php
echo $this->endBlockComment('fitfab_event');
