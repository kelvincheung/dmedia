<?php
/**
 * FitFab - sidebar shop
 * 
 * @package fitfab
 * @subpackage fitfab
 * @since fitfab 1.0.0
 */
if (!is_active_sidebar('fitfab-sidebar-shop')) {
    return;
}
dynamic_sidebar('fitfab-sidebar-shop');
