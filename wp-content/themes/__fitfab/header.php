<?php
/**
 * FitFab - header layout
 * 
 * @package fitfab
 * @subpackage fitfab
 * @since fitfab 1.0.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>

<!-- Google Tag Manager -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-61716329-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- End Google Tag Manager -->

        <!--Page Wrapper Start-->
        <div id="wrapper" <?php do_action("fitfab_wrap_class"); ?>>
             <!--header Section Start Here -->
             <?php do_action("fitfab_header_layout"); ?>
             <!--header Section Ends Here -->
<?php 