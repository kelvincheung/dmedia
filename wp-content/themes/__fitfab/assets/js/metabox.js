jQuery(document).ready(function($) {
    var selectedcheck = $('input[type=radio][name=post_format]:checked').val();
    $('#gallery_format').css('display', 'none');
    $('#video_format').css('display', 'none');
    $('#image_format').css('display', 'none');
    $('#aside_format').css('display', 'none');
    $('#link_format').css('display', 'none');
    $('#quote_format').css('display', 'none');
    if (selectedcheck == 'video')
    {
        $('#video_format').css('display', 'block');
    }
    else if (selectedcheck == 'quote')
    {
        $('#quote_format').css('display', 'block');
    }
    else if (selectedcheck == 'aside')
    {
        $('#aside_format').css('display', 'block');
    }
    else if (selectedcheck == 'gallery')
    {
        $('#gallery_format').css('display', 'block');
    }
    else if (selectedcheck == 'link')
    {
        $('#link_format').css('display', 'block');
    }
    else if (selectedcheck == 'image')
    {
        $('#image_format').css('display', 'block');
    }
    $('input[type=radio][name=post_format]').change(function() {
        var selectedcheck = $(this).val();
        $('#gallery_format').css('display', 'none');
        $('#video_format').css('display', 'none');
        $('#image_format').css('display', 'none');
        $('#aside_format').css('display', 'none');
        $('#link_format').css('display', 'none');
        $('#quote_format').css('display', 'none');
        if (selectedcheck == 'video')
        {
            $('#video_format').css('display', 'block');
        }
        else if (selectedcheck == 'quote')
        {
            $('#quote_format').css('display', 'block');
        }
        else if (selectedcheck == 'aside')
        {
            $('#aside_format').css('display', 'block');
        }
        else if (selectedcheck == 'gallery')
        {
            $('#gallery_format').css('display', 'block');
        }
        else if (selectedcheck == 'link')
        {
            $('#link_format').css('display', 'block');
        }
        else if (selectedcheck == 'image')
        {
            $('#image_format').css('display', 'block');
        }
    });
});