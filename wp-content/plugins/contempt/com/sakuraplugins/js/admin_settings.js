jQuery(document).ready(function(){    
    var ctmpt = new ContemptSettings();
    ctmpt.init();
});

function ContemptSettings(){
	this.init = function(){
		//colors
        /*
        jQuery('#thumbs_hover_background, #thumbs_hover_text_color, #ctp_nav_hover').ColorPicker({
            onSubmit: function(hsb, hex, rgb, el) {
                jQuery(el).val(hex);
                jQuery(el).ColorPickerHide();
            },
            onBeforeShow: function () {
                jQuery(this).ColorPickerSetColor(this.value);
            }            
        }).bind('keyup', function(){
            jQuery(this).ColorPickerSetColor(this.value);
        });
*/

    jQuery('#thumbs_hover_background').colpick({
      layout:'hex',
      submit:0,
      colorScheme:'dark',
      color: jQuery('#thumbs_hover_background').val(),
      onChange:function(hsb,hex,rgb,el,bySetColor) {
        jQuery(el).css('border-color','#'+hex);
        // Fill the text box just if the color was set using the picker, and not the colpickSetColor function.
        if(!bySetColor) jQuery(el).val(hex);
      }
    }).keyup(function(){
      jQuery(this).colpickSetColor(this.value);
    }); 

    jQuery('#thumbs_hover_text_color').colpick({
      layout:'hex',
      submit:0,
      colorScheme:'dark',
      color: jQuery('#thumbs_hover_text_color').val(),
      onChange:function(hsb,hex,rgb,el,bySetColor) {
        jQuery(el).css('border-color','#'+hex);
        // Fill the text box just if the color was set using the picker, and not the colpickSetColor function.
        if(!bySetColor) jQuery(el).val(hex);
      }
    }).keyup(function(){
      jQuery(this).colpickSetColor(this.value);
    }); 

    jQuery('#ctp_nav_hover').colpick({
      layout:'hex',
      submit:0,
      colorScheme:'dark',
      color: jQuery('#ctp_nav_hover').val(),
      onChange:function(hsb,hex,rgb,el,bySetColor) {
        jQuery(el).css('border-color','#'+hex);
        // Fill the text box just if the color was set using the picker, and not the colpickSetColor function.
        if(!bySetColor) jQuery(el).val(hex);
      }
    }).keyup(function(){
      jQuery(this).colpickSetColor(this.value);
    });         



        jQuery('#resetContemptColorsBTN').click(function(e){
            e.preventDefault();
            jQuery('#thumbs_hover_background').val(jQuery('#thumbs_hover_background').attr('data-default'));
            jQuery('#thumbs_hover_text_color').val(jQuery('#thumbs_hover_text_color').attr('data-default'));
            jQuery('#ctp_nav_hover').val(jQuery('#ctp_nav_hover').attr('data-default'));
        });		
	}
}