jQuery(document).ready(function(){    
    var mmAdmin = new MMAdmin();
    mmAdmin.init();
});

function MMAdmin(){
	var post_meta = jQuery('#featured_images_rx_portfolio').attr('data-post_meta');
	this.init = function(){
		initSortableFeaturedImages();
		handleImagesUpload();
    handleSpecialWidget();
	}

    function handleSpecialWidget(){
        var wgtUI = jQuery('#specialWidgetUI');
        jQuery('#widgetCB').change(function(){
          if(this.checked){              
              wgtUI.css('display', 'block');
          }else{
              wgtUI.removeClass('showWidgetSP');
              wgtUI.css('display', 'none');
          }
        });
    }

   function initSortableFeaturedImages(){
       jQuery("#featuredImagesUI").sortable({
            placeholder: "ui-state-highlight",
            stop: function( event, ui ){     
            }
        });
        jQuery("#featuredImagesUI").disableSelection(); 
        jQuery("#featuredImagesUI").children().each(function(indx){
            thumbsHoverAction(jQuery(this).find('.featuredThumbOverlay'));
            jQuery(this).find('.thumbOverlayRemove').click(function(e){
                e.preventDefault();
                if(confirm('Are you sure you want to remove this image?')){
                    jQuery(this).parent().parent().parent().remove();                    
                    checkChildrens();  
                }                                
            });

            jQuery(this).find('.thumbOverlayEdit').click(function(e){
                e.preventDefault();
           		var iconUI = jQuery(this).parent().parent().parent();
           		openEditBox(iconUI, iconUI.find('.imgSizeUI').val());                                                                          
            });            

        });       
   }	

   function checkChildrens(){       
       var dummyData = '<input id="dummyFeaturedImagesDATA" type="hidden" name="'+post_meta+'[featuredImages]" value="" />';
       var children = jQuery("#featuredImagesUI").children();   
       if(children.length==0){
           removeDummyFeaturedData();
           jQuery(dummyData).appendTo(jQuery('#featuresThumbsContainer'));
       }else{
           removeDummyFeaturedData();
       }
   } 
   
   function removeDummyFeaturedData(){
       try{
           jQuery('#dummyFeaturedImagesDATA').remove();
       }catch(e){}
   }    

	function handleImagesUpload(){

       jQuery('#removeAllFeaturedImagesBTN').click(function(e){
           e.preventDefault();
           try{
               var children = jQuery("#featuredImagesUI").children();               
               if(children.length==0){
                   alert('There are no featured images');
                   return;
               }
               if(confirm('Are you sure you want to remove all images?')){
                   jQuery("#featuredImagesUI").empty();
                   checkChildrens();
               }
           }catch(e){}
       });

       jQuery('#addFeaturedImagesBTN').click(function(e){
              e.preventDefault();
              var send_attachment_bkp = wp.media.editor.send.attachment;
                    var frame = wp.media({
                        title: "Select Images",
                        multiple: true,
                        library: { type: 'image' },
                        button : { text : 'add image' }
                    });
                    
                    frame.on('close',function() {                        
                        var selection = frame.state().get('selection');
                        selection.each(function(attachment) {                               
                               var iconUrl = 'http://placehold.it/150x150';
                               if(attachment.attributes.sizes.thumbnail!=undefined){
                                   iconUrl = (attachment.attributes.sizes.thumbnail.url!='')?attachment.attributes.sizes.thumbnail.url:iconUrl;
                               }                              
                               featuredImageHTML = '<li class="ui-state-default"><div class="thumbBoxImage">';
                               featuredImageHTML += '<div class="featuredThumb"><img src="'+iconUrl+'" /></div>';
                               featuredImageHTML += '<input type="hidden" name="'+post_meta+'[featuredImages][]" value="'+attachment.id+'" />';
                               featuredImageHTML += '<input class="imgSizeUI" type="hidden" name="'+post_meta+'[imagesSize][]" value="50" />';
                               
                               featuredImageHTML += '<div class="featuredThumbOverlay">';
                               featuredImageHTML +='<div class="thumbOverlayMove"></div>';
                               featuredImageHTML +='<div class="thumbOverlayRemove"></div>';                                                          
                               featuredImageHTML +='</div>';
                               featuredImageHTML += '</div></li>';
                               var featuredImageJq = jQuery(featuredImageHTML);
                               featuredImageJq.appendTo("#featuredImagesUI");
                               jQuery("#featuredImagesUI").sortable("refresh"); 
                               
                               checkChildrens(); 
                               
                               thumbsHoverAction(featuredImageJq.find('.featuredThumbOverlay'));
                               featuredImageJq.find('.thumbOverlayRemove').click(function(e){
                                    e.preventDefault();
                                    if(confirm('Are you sure you want to remove this image?')){
                                        jQuery(this).parent().parent().parent().remove();
                                        checkChildrens();   
                                    }
                               });                                
                        });
                         wp.media.editor.send.attachment = send_attachment_bkp;
                    });                                  
                            
                    frame.open();
             return false;            
       });		
	}

   function thumbsHoverAction(el){
       el.css('opacity', 0);       
       el.hover(function(e){
           TweenMax.to(jQuery(this), .2, {css:{opacity:1}, ease:Power4.EaseIn});
       }, function(e){
           TweenMax.to(jQuery(this), .2, {css:{opacity:0}, ease:Power4.EaseIn});
       });
   }

   initFeaturedImageSettings();
   //featured image size
   function initFeaturedImageSettings(){
      var featuredSizesCB = jQuery('#featuredSizeUI');      
      jQuery('#featuredSizeUI option').each(function(indx){
        if(jQuery(this).val()==jQuery('#featuredValUI').val()){
          jQuery(this).attr('selected', 'selected');
        }
      });
      featuredSizesCB.change(function(e){        
        jQuery('#featuredValUI').val(jQuery('#featuredSizeUI option:selected').val());
      }); 

    jQuery('#preview_text_color').colpick({
      layout:'hex',
      submit:0,
      colorScheme:'dark',
      color: jQuery('#preview_text_color').val(),
      onChange:function(hsb,hex,rgb,el,bySetColor) {
        jQuery(el).css('border-color','#'+hex);
        // Fill the text box just if the color was set using the picker, and not the colpickSetColor function.
        if(!bySetColor) jQuery(el).val(hex);
      }
    }).keyup(function(){
      jQuery(this).colpickSetColor(this.value);
    });      

      //text color
      /*
        jQuery('#preview_text_color').ColorPicker({
            onSubmit: function(hsb, hex, rgb, el) {
                jQuery(el).val(hex);
                jQuery(el).ColorPickerHide();
            },
            onBeforeShow: function () {
                jQuery(this).ColorPickerSetColor(this.value);
            }            
        }).bind('keyup', function(){
            jQuery(this).ColorPickerSetColor(this.value);
        }); 
*/

      //video code
      jQuery('#useVideoCB').change(function(){
        if(this.checked){
          console.log('is video');
          jQuery('#redirect_video_content_ui').removeClass('sk_modal_hide');          
          //jQuery('#useBackgroundCB').attr('checked', false);
          //jQuery('#back_fill_color').addClass('sk_modal_hide');
        }else{
          jQuery('#redirect_video_content_ui').addClass('sk_modal_hide');
        }
      });

      //use background
      jQuery('#useBackgroundCB').change(function(){
        if(this.checked){
          jQuery('#back_fill_color').removeClass('sk_modal_hide');
          jQuery('#useVideoCB').attr('checked', false);
          jQuery('#video_code_edit').addClass('sk_modal_hide');
        }else{
          jQuery('#back_fill_color').addClass('sk_modal_hide');
        }
      });    

    jQuery('#back_fill_color_input').colpick({
      layout:'hex',
      submit:0,
      colorScheme:'dark',
      color: jQuery('#back_fill_color_input').val(),
      onChange:function(hsb,hex,rgb,el,bySetColor) {
        jQuery(el).css('border-color','#'+hex);
        // Fill the text box just if the color was set using the picker, and not the colpickSetColor function.
        if(!bySetColor) jQuery(el).val(hex);
      }
    }).keyup(function(){
      jQuery(this).colpickSetColor(this.value);
    });

        /*
        jQuery('#back_fill_color_input').ColorPicker({
            onSubmit: function(hsb, hex, rgb, el) {
                jQuery(el).val(hex);
                jQuery(el).ColorPickerHide();
            },
            onBeforeShow: function () {
                jQuery(this).ColorPickerSetColor(this.value);
            }            
        }).bind('keyup', function(){
            jQuery(this).ColorPickerSetColor(this.value);
        });  
*/

      //use redirect
      jQuery('#useRedirectCB').change(function(){
        if(this.checked){
          jQuery('#redirect_content_ui').removeClass('sk_modal_hide');
        }else{
          jQuery('#redirect_content_ui').addClass('sk_modal_hide');
        }
      });    

   }

   function openEditBox(originalEl, originalWdth){  

   		var currentWDT = originalWdth;	
   		var lgtbox_ui = jQuery('<div class="edit_img_ui"><div class="sk_modal_ui"></div></div>');
   		var modal_ui = lgtbox_ui.find('.sk_modal_ui');
   		jQuery('<div class="sk_modal_title">Image settings</div><div class="sk_modal_close"></div>').appendTo(modal_ui);
   		
   		//modal content
   		var modal_content = jQuery('<div class="sk_modal_content"></div>');
   		modal_content.appendTo(modal_ui);

   		//START SELECT WIDTH
   		jQuery('<p>Select image width and caption(optional):</p>').appendTo(modal_content);

   		var select = '';
   		select += '<select id="selectUI">';
      select += '<option value="10">Width 10%</option>';
      select += '<option value="15">Width 15%</option>';
      select += '<option value="20">Width 20%</option>';
   		select += '<option value="25">Width 25%</option>';
   		select += '<option value="30">Width 30%</option>';      
   		select += '<option value="33">Width 33%</option>';
      select += '<option value="35">Width 35%</option>';
      select += '<option value="40">Width 40%</option>';
   		select += '<option value="50">Width 50%</option>';
      select += '<option value="60">Width 60%</option>';
   		select += '<option value="70">Width 70%</option>';
   		select += '<option value="75">Width 75%</option>';
      select += '<option value="80">Width 80%</option>';
      select += '<option value="90">Width 90%</option>';
   		select += '<option value="100">Width 100%</option>';   		
   		select += '</select>';
   		var selectUI = jQuery(select);

   		//select change - get val
   		selectUI.change(function(e){   			
   			currentWDT = jQuery('#selectUI option:selected').val();
   		});   		
 			
 		//select original value
 		selectUI.children().each(function(indx){ 			
 			if(jQuery(this).val()==currentWDT){
 				jQuery(this).attr('selected', 'selected');
 			}
 		});

		selectUI.appendTo(modal_content);

    //image caption
    jQuery('<input id="gr_captionInput" value="'+originalEl.find('.imgCaption').val()+'" placeholder="Thumb caption" type="text" />').appendTo(modal_content);

    //open link when click on image    
    var isUseLink = (originalEl.find('.imageLink').val()=="true")?true:false;
    jQuery('<div class="sk_modal_space2"></div>').appendTo(modal_content);
    var isLinkCB = jQuery('<label><input type="checkbox" '+((isUseLink)?'checked':'')+' />Open custom link when click on image</label>');
    isLinkCB.appendTo(modal_content);  

    var modal_link_container = jQuery('<div class="sk_modal_space2"></div><div><input id="imageCustomLinkURL" class="sk_modal_input_medium" type="text" value="'+originalEl.find('.imageLinkURL').val()+'" /><div>');    
    modal_link_container.appendTo(modal_content);

    (isUseLink)?modal_link_container.removeClass('sk_modal_hide'):modal_link_container.addClass('sk_modal_hide');
    isLinkCB.find('input').change(function(e){      
      if(this.checked){
          isUseLink = true;
      }else{
          isUseLink = false;
      }
      (isUseLink)?modal_link_container.removeClass('sk_modal_hide'):modal_link_container.addClass('sk_modal_hide');
    });   

    jQuery('<div class="hlineModal"></div>').appendTo(modal_content);

    //is video    
    var isUseVideo =(originalEl.find('.isVideo').val()=="true")?true:false;
    jQuery('<div class="sk_modal_space2"></div>').appendTo(modal_content);
    var isVideoCB = jQuery('<label><input type="checkbox" '+((isUseVideo)?'checked':'')+' />Is video (Show video within the lightbox - Add embedded video code)</label>');
    isVideoCB.appendTo(modal_content);

    var modal_video_code_ui = jQuery('<div><textarea id="modal_video_code">'+originalEl.find('.videoCode').text()+'</textarea></div>');
    jQuery('<div class="sk_modal_space2"></div>').appendTo(modal_content);
    modal_video_code_ui.appendTo(modal_content);
    (isUseVideo)?modal_video_code_ui.removeClass('sk_modal_hide'):modal_video_code_ui.addClass('sk_modal_hide');
    isVideoCB.find('input').change(function(e){      
      if(this.checked){
          isUseVideo = true;
          isSoundcloudCB.find('input').attr('checked', false);
          modal_soundcloud_code_ui.addClass('sk_modal_hide');
          isUseSoundcloud = false;          
      }else{
          isUseVideo = false;
      }
      (isUseVideo)?modal_video_code_ui.removeClass('sk_modal_hide'):modal_video_code_ui.addClass('sk_modal_hide');
      (isUseVideo)?teaserRow.removeClass('sk_modal_hide'):teaserRow.addClass('sk_modal_hide');      
    });




    jQuery('<div class="hlineModal"></div>').appendTo(modal_content);

    //is soundcloud      
    var isUseSoundcloud =(originalEl.find('.isSoundcloud').val()=="true")?true:false;
    var isSoundcloudCB = jQuery('<label><input type="checkbox" '+((isUseSoundcloud)?'checked':'')+' />Is SoundCloud (Add embedded SoundCloud code)</label>');
    isSoundcloudCB.appendTo(modal_content); 

    var modal_soundcloud_code_ui = jQuery('<div><textarea id="modal_soundcloud_code">'+originalEl.find('.soundCloudCode').text()+'</textarea></div>');
    jQuery('<div class="sk_modal_space2"></div>').appendTo(modal_content);
    modal_soundcloud_code_ui.appendTo(modal_content);
    (isUseSoundcloud)?modal_soundcloud_code_ui.removeClass('sk_modal_hide'):modal_soundcloud_code_ui.addClass('sk_modal_hide');

    isSoundcloudCB.find('input').change(function(e){      
      if(this.checked){
          isUseSoundcloud = true;
          isVideoCB.find('input').attr('checked', false);
          modal_video_code_ui.addClass('sk_modal_hide');
          isUseVideo = false;
          //(isUseVideo)?teaserRow.removeClass('sk_modal_hide'):teaserRow.addClass('sk_modal_hide');
          (isUseSoundcloud)?modal_soundcloud_code_ui.removeClass('sk_modal_hide'):modal_soundcloud_code_ui.addClass('sk_modal_hide'); 
      }else{
          isUseSoundcloud = false;
          (isUseSoundcloud)?modal_soundcloud_code_ui.removeClass('sk_modal_hide'):modal_soundcloud_code_ui.addClass('sk_modal_hide');
      }
      (isUseSoundcloud)?modal_soundcloud_code_ui.removeClass('sk_modal_hide'):modal_soundcloud_code_ui.addClass('sk_modal_hide');
    });  

    //use as Metro UI Thumb         
    jQuery('<div class="hlineModal"></div>').appendTo(modal_content);
    var isUseMetroStyle =(originalEl.find('.isMetroStyle').val()=="true")?true:false;
    var isMetroCB = jQuery('<label><input type="checkbox" '+((isUseMetroStyle)?'checked':'')+' />Use as Metro UI Style (Choose background color, title and icon)</label>');
    isMetroCB.appendTo(modal_content); 
    jQuery('<div class="sk_modal_space2"></div>').appendTo(modal_content);

    //metro container
    var metroUIModalContent = jQuery('<div class="metroUIModalContent"></div>');
    metroUIModalContent.appendTo(modal_content); 

    isMetroCB.find('input').change(function(e){      
      if(this.checked){
          isUseMetroStyle = true;
      }else{
          isUseMetroStyle = false;
      }
      (isUseMetroStyle)?metroUIModalContent.removeClass('sk_modal_hide'):metroUIModalContent.addClass('sk_modal_hide');
    }); 
    (isUseMetroStyle)?metroUIModalContent.removeClass('sk_modal_hide'):metroUIModalContent.addClass('sk_modal_hide');    


    var firstRow = jQuery('<div class="sk_admin_span3"></div>');
    var secondRow = jQuery('<div class="sk_admin_span3"></div>');
    var thirdRow = jQuery('<div class="sk_admin_span3"></div>');
    var clearfx = jQuery('<div class="sk_clear_fx"></div>');
    firstRow.appendTo(metroUIModalContent);secondRow.appendTo(metroUIModalContent);thirdRow.appendTo(metroUIModalContent);clearfx.appendTo(metroUIModalContent);
    
    //color picker      
    var metroBackgroundColor = originalEl.find('.metroBackgroundColor').val();
    var lightboxInputColor = jQuery('<input id="irisInput" type="text" style="width: 80px; margin-right: 10px;" value="'+metroBackgroundColor+'" />');
    lightboxInputColor.appendTo(firstRow);    
    lightboxInputColor.iris({hide: false, width: 150, change: function(e, ui){
        metroBackgroundColor = ui.color.toString();
    }});

    //upload image
    var metroThumbID = originalEl.find('.metroThumb').val();
    var gridIconContainerUIBackground = jQuery('<div class="gridIconContainerUIBackground"></div>');
    var gridIconContainerUI = jQuery('<img class="metroIcon" src="" alt="" />');
    var uploadGridIconBTN = jQuery('<button id="uploadLogoBTN" class="btn" type="button">Upload Metro Icon</button>');
    var metroIconInput = jQuery('<input id="logoAttachemetID" class="inputText" type="hidden" name="" value="'+metroThumbID+'" />');

    gridIconContainerUIBackground.appendTo(secondRow);
    gridIconContainerUI.appendTo(gridIconContainerUIBackground);
    uploadGridIconBTN.appendTo(secondRow);
    metroIconInput.appendTo(secondRow);    
    (metroThumbID!="")?getMetroIconURL(metroThumbID, displayMetroIcon):null; 

    var metroImageSelectedCallback;
    handleLogoMetroIcon();           

    //HANDLE UPLOAD
    function handleLogoMetroIcon(){
        uploadGridIconBTN.click(function(e){                         
            e.preventDefault();
            metroImageSelectedCallback = loadingLogoCallback;    
            tb_show('Upload image', 'media-upload.php?post_id=0&type=image&TB_iframe=true', false);  
            return false;        
        });
        var loadingLogoCallback = function(originalImage, thumbImage, imageID){
            //gridIconContainerUI.attr('src', thumbImage);
            getMetroIconURL(imageID, displayMetroIcon);
            metroIconInput.val(imageID);       
        }     
    }
    function displayMetroIcon(data){        
        var obj = JSON.parse(data);
        gridIconContainerUI.attr('src', obj.url);
    }
    function getMetroIconURL(id, callback){
        var data = {
          action: 'get_metro_thumb',
          metroThumbID: id
        };
        // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
        jQuery.post(ajaxurl, data, function(response) {
            callback(response);
        });
    }

    //utils - upload
    window.send_to_editor = function(html) {
        var originalImage = jQuery(html).attr('href');
        var thumbImage = jQuery(html).find('img').attr('src');
        var imgid = jQuery(html).find('img').attr('class');
        var imageID = imgid.slice( ( imgid.search(/wp-image-/) + 9 ), imgid.length );
        metroImageSelectedCallback(originalImage, thumbImage, imageID);
        tb_remove();  
    }       

    //metro text
    var metroTitleInput = jQuery('<input class="metroTitleInput" type="text" name="" value="'+originalEl.find('.metroTitle').val()+'" />');
    metroTitleInput.appendTo(thirdRow);


		//bottom
    jQuery('<div class="sk_modal_space2"></div>').appendTo(modal_content);
		jQuery('<div class="hlineModal"></div>').appendTo(modal_content);
		var saveChangesLgBTN = jQuery('<input type="submit" value="Save changes" class="button-primary right" />');
		saveChangesLgBTN.appendTo(modal_content);
		var closeLghtbBTN = jQuery('<input type="submit" value="Cancel" class="button-secondary right buttonSpaceRight" />');	
		closeLghtbBTN.appendTo(modal_content);	
		jQuery('<div class="sk_clear_fx"></div>').appendTo(modal_content);
				


		
		saveChangesLgBTN.bind('click', function(e){
			e.preventDefault();
			//save
			originalEl.find('.imgSizeUI').val(currentWDT);
      originalEl.find('.imgCaption').val(jQuery('#gr_captionInput').val());      
      originalEl.find('.imageLink').val(isUseLink);
      originalEl.find('.imageLinkURL').val(modal_content.find('#imageCustomLinkURL').val()); 
      originalEl.find('.isVideo').val(isUseVideo);       
      

      originalEl.find('.videoCode').text(document.getElementById('modal_video_code').value);
      originalEl.find('.isSoundcloud').val(isUseSoundcloud);                    
      originalEl.find('.soundCloudCode').text(document.getElementById('modal_soundcloud_code').value);
      originalEl.find('.isMetroStyle').val(isUseMetroStyle);
      originalEl.find('.metroBackgroundColor').val(jQuery('#irisInput').val());
      originalEl.find('.metroThumb').val(metroIconInput.val());
      originalEl.find('.metroTitle').val(metroTitleInput.val());           

			closeLghtb();			
		});
		closeLghtbBTN.bind('click', function(e){
			e.preventDefault();			
			closeLghtb();			
		});		

   		
   		//close modal X
   		modal_ui.find('.sk_modal_close').bind('click', function(e){
   			e.preventDefault();
   			closeLghtb();
   		});
      modal_ui.find('.sk_modal_close').bind('mouseenter', function(e){
          TweenMax.to(jQuery(this), .15, {css:{scale: .9, rotation: 45}, ease:Power3.EaseIn}); 
      });
      modal_ui.find('.sk_modal_close').bind('mouseleave', function(e){
          TweenMax.to(jQuery(this), .15, {css:{scale: 1, rotation: 0}, ease:Power3.EaseIn});
      });            
      

   		lgtbox_ui.appendTo('body');

   		function closeLghtb(){
   			modal_ui.find('.sk_modal_close').unbind();
   			saveChangesLgBTN.unbind();
   			closeLghtbBTN.unbind();
   			lgtbox_ui.remove();
   			lgtbox_ui = null;
   		}
   }


}