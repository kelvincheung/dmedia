<?php

/**
* plugin options
*/

class CTClass_PluginOptions
{

	private $options;
	function __construct()
	{
		$this->options = get_option(CT_PORTFOLIO_OPTION_GROUP);
	}

	static private $instance;
	static public function getInstance(){
		if(!isset(self::$instance)){
			self::$instance = new CTClass_PluginOptions();
		}
		return self::$instance;
	}

	//get re-write slug
	public function getReWriteSlug(){
		return (isset($this->options['mmReWriteSlug']))?$this->options['mmReWriteSlug']:CT_PORTFOLIO_REWRITE;	
	}

	//hover col for thumb background
	public function getThumbBackgroundHoverCol(){
		return (isset($this->options['thumbs_hover_background']))?$this->options['thumbs_hover_background']:'3fd9cd';		
	}	

	//hover col for thumb text
	public function getThumbTextHoverCol(){
		return (isset($this->options['thumbs_hover_text_color']))?$this->options['thumbs_hover_text_color']:'FFFFFF';		
	}

	//return nav hover color
	public function getNavHoverColor(){
		return (isset($this->options['ctp_nav_hover']))?$this->options['ctp_nav_hover']:'3fd9cd';		
	}


	//custom CSS
	public function getCustomCSS(){	
		return (isset($this->options['ctCustomCSS']))?$this->options['ctCustomCSS']:'';
	}
}

?>