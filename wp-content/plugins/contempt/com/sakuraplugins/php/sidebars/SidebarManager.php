<?php

/**
 * manage sidebars
 */
class CTClass_SidebarManager{
	
	function __construct($argument='') {
		$this->sidebars = array();
	}
	static private $instance;
	static public function getInstance(){
		if(!isset(self::$instance)){
			self::$instance = new CTClass_SidebarManager();
		}
		return self::$instance;
	}
	public function registerSidebar($args, $sidebar_FILE){
		if(function_exists('register_sidebar')){
			register_sidebar($args);
			array_push($this->sidebars, array('id'=>$args['id'], 'name'=>$args['name'], 'sidebar_FILE'=>$sidebar_FILE));
		}
	}
	//get sidebar name based on ID
	public function getSidebarName($id){
		$name = '';
		for ($i=0; $i < sizeof($this->sidebars); $i++) { 
			if(isset($this->sidebars[$i])){
				if($this->sidebars[$i]['id']==$id){
					$name = $this->sidebars[$i]['name'];
					break;
				}
			}
		}
		return $name;
	}
	//get sidebar filename based on ID
	public function getSidebarFILE($id){
		$filename = '';
		for ($i=0; $i < sizeof($this->sidebars); $i++) { 
			if(isset($this->sidebars[$i])){
				if($this->sidebars[$i]['id']==$id){
					$filename = $this->sidebars[$i]['sidebar_FILE'];
					break;
				}
			}
		}
		return $filename;
	}	
	
	//TBD
	public function registerSidebars(){}
}

?>