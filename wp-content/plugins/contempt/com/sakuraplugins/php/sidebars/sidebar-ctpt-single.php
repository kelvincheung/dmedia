<?php
require_once(CT_CLASS_PATH.'/com/sakuraplugins/php/sidebars/SidebarManager.php');
?>
<?php if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar(CTClass_SidebarManager::getInstance()->getSidebarName('ct-portfolio-single-sidebar')) ) : ?>
	
	<p class="no_sidebar"><?php _e('no widgets added to the this sidebar', 'default_textdomain')?></p>
<?php endif; ?>