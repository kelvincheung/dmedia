<?php
require_once(CT_CLASS_PATH.'/com/sakuraplugins/php/sidebars/SidebarManager.php');

/**
 * sidebars
 */
class CTClass_SKSidebars{
	
	public function init(){
		
		//blog-single sidebar
		CTClass_SidebarManager::getInstance()->registerSidebar(array(
			'name' => __( 'Contempt Portfolio - Sidebar', 'default_textdomain' ),
			'id' => 'ct-portfolio-single-sidebar',
			'description' => 'Contempt Portfolio Sidebar, shows within the single portfolio pages (if enabled from post).',
			'before_widget' => '<div id="%1$s" class="ct_WidgetUI widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<div class="ctWidgetTitleUI">',
			'after_title' => '</div>'			
		), 'ctpt-single');			

						
	}
}


?>