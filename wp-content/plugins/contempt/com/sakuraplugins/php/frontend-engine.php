<?php
require_once(CT_CLASS_PATH.'/com/sakuraplugins/php/libs/resize_helper.php');
/**
* helper
*/
class CTClass_FrontEngine
{
	private $customPostOptions;
	private $postID;
	private $ids;
	function __construct($postID)
	{
		$this->postID = $postID;
		$this->customPostOptions = get_post_meta($postID, CT_POST_CUSTOM_META, false);						
	}

	public function initIDS(){
		$this->ids = array();
		$terms = wp_get_post_terms($this->postID, CT_PORTFOLIO_CATEGORIES);
		if(!is_array($terms))
			return;
		if(sizeof($terms)==0)
			return;
		$term = $terms[0];
		//term_id
		$args = array(
			'posts_per_page'=>-1,
			'post_type'=>CT_PORTFOLIO_SLUG,
			'tax_query' => array(
				array(
					'taxonomy' => CT_PORTFOLIO_CATEGORIES,
					'field' => 'id',
					'terms' => $term->term_id
				)
			)
		);
		$the_query = new WP_Query($args);		
		if($the_query->have_posts()){			
			while($the_query->have_posts()){				
				$the_query->the_post();
				array_push($this->ids, get_the_ID());				
			}			
		}		
		wp_reset_postdata();
	}

	//get featured image
	public function getFeaturedImage($w, $h){		
		$imageURL = "";
		$res = wp_get_attachment_image_src(get_post_thumbnail_id($this->postID), 'full');
		if($res){
			$imageURL = $res[0];
			$resizedImage = CTClass_ResizeHelper::resize($imageURL, $w, $h, true);	
			if($resizedImage){
				$imageURL = $resizedImage;
			}								
		}
		return $imageURL;		
	}

	//get subtitle
	public function getThumbSubtitle(){
		return (isset($this->customPostOptions[0]['subtitle']))?$this->customPostOptions[0]['subtitle']:'';
	}


	//next post in same 
	public function getNextPostID(){		
		$returnID = false;
		if(sizeof($this->ids)==0)
			return false;		
		$currentIndx = array_search($this->postID, $this->ids);
		if(($currentIndx+1)<sizeof($this->ids)){			
			$nextid = $this->ids[$currentIndx+1];
			if(!empty($nextid)){
				$returnID = $nextid;
			}	
		}	
		return $returnID;
	}

	public function getPrevPostID(){		
		$returnID = false;
		if(sizeof($this->ids)==0)
			return false;
		$currentIndx = array_search($this->postID, $this->ids);
		if(($currentIndx-1)>-1){
			$previd = $this->ids[$currentIndx-1];
			if(!empty($previd)){
				$returnID = $previd;
			}
		}
		return $returnID;
	}	

	//get permalink
	public function getPermalink($isCategory=false){
		if($isCategory){
			return add_query_arg('ct', '1', get_permalink($this->postID));
		}else{
			return get_permalink($this->postID);		
		}									
	}	

	
	public function getPostMeta(){
		return $this->customPostOptions;
	}	
	
	public function getNavUI($direction="prev", $isCategory=false){
		$thumbURL = $this->getFeaturedImage(600, 350);
		$title = get_the_title($this->postID);
		$subtitle = $this->getThumbSubtitle();
		$permalink = $this->getPermalink($isCategory);
		return $this->buildThumbUI($thumbURL, $title, $subtitle, $permalink);
	}	

	public function buildThumbUI($thumbURL, $title, $subtitle, $permalink){
		$pluginOptions = CTClass_PluginOptions::getInstance();		
		$out = '<div class="contemptThumbUISingle" data-thumburl="'.$thumbURL.'" data-texthover="#'.$pluginOptions->getThumbTextHoverCol().'">';
			$out .= '<a class="ct_thumbIMG" href="'.$permalink.'"><img class="ct-thumb-img" src="'.$thumbURL.'" alt="" /></a>';										
			$out .= '<div class="ct_hover_overlay"><div class="ct-plus"><img src="'.CT_TEMPPATH.'/img/plus-thin.png" alt="" /></div></div>';		
			$out .= '<div class="ct_thumbTitleUI">';
				$out .= '<p class="ct_thumbTitle">'.$title.'</p>';
				$out .= '<p class="ct_thumbSubtitle">'.$subtitle.'</p>';				
			$out .= '</div>';
		$out .= '</div>';
		return $out;
	}	


}

?>