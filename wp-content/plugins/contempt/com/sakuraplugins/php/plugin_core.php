<?php

require_once(CT_CLASS_PATH.'com/sakuraplugins/php/customposts/SkCPT.php');
require_once(CT_CLASS_PATH.'com/sakuraplugins/php/customposts/utils/CPTHelper.php');
require_once(CT_CLASS_PATH.'com/sakuraplugins/php/script_manager/ScriptManager.php');

require_once(CT_CLASS_PATH.'com/sakuraplugins/php/gr_shortcodes.php');

require_once(CT_CLASS_PATH.'/com/sakuraplugins/php/libs/resize_helper.php');
require_once(CT_CLASS_PATH.'/com/sakuraplugins/php/plugin-options.php');
require_once(CT_CLASS_PATH.'/com/sakuraplugins/php/admin_pages/admin-option-page.php');
require_once(CT_CLASS_PATH.'com/sakuraplugins/php/libs/sk_utils.php');

/**
 * base class
 */
class CTClass_PluginBase {

	/************* BASE PLUGIN EVENTS ***********/
	//init handler
	public function initializeHandler(){
		$this->addCPT();
		$this->addTaxonomy();
		$this->registerShortcodes();
		//init sidebars
		$this->initSidebars();		
	}
		
	
	//register shortcodes
	public function registerShortcodes(){
		$this->initShortcodes();
	}	
	
	//admin init handler
	public function adminInitHandler(){
		$this->rxCPT->addMetaBox(__('Photos', CT_PLUGIN_TEXTDOMAIN), 'meta_box_gallery_023648', 'meta_box_gallery');
		$this->rxCPT->addMetaBox(__('Settings', CT_PLUGIN_TEXTDOMAIN), 'meta_box_featured_image_023648', 'meta_box_featured_image');		

	}	

	/**
	 * SAVE POST EXTRA DATA
	 */
	 public function savePostHandler(){
		global $post;						
		if( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ){
			return $post_id;
		}
		if(!current_user_can('edit_posts') || !current_user_can('publish_posts')){
			return;
		}
			//save portfolio data
		if(isset($this->rxCPT) && isset($_POST['post_type'])){
			if($this->rxCPT->getPostSlug() == $_POST['post_type']){									
				if(current_user_can( 'edit_posts', $post->ID ) && isset($_POST[$this->rxCPT->getPostCustomMeta()])){							
					update_post_meta($post->ID, $this->rxCPT->getPostCustomMeta(), $_POST[$this->rxCPT->getPostCustomMeta()]);
				}		 
			}						
		}																
	 }	
	
	//admin enqueue scripts handler
	public function adminEnqueueScriptsHandler(){
		if(!isset($this->rxCPT)){
			return;
		}
		$current_screen = get_current_screen();
		if($current_screen->post_type==$this->rxCPT->getPostSlug()){
			//default styles
			$this->enqueAdminCommonStyles();			
			//default JS
			wp_enqueue_script('jquery');
			
			CTClass_ScriptManager::enqueJqueryUI();
			CTClass_ScriptManager::enqueTweenmax();
			CTClass_ScriptManager::enqueueThickbox();
			CTClass_ScriptManager::enqueColorPicker();
			wp_enqueue_script('iris');
			wp_enqueue_media();			
			wp_register_script('mm_admin_js', CT_TEMPPATH.'/com/sakuraplugins/js/admin.js', array('jquery'));
			wp_enqueue_script('mm_admin_js');				
		}
		$screenID = $current_screen->id;
		if($screenID==CT_PORTFOLIO_SLUG.'_page_ct_portfolio_sett'){
			$this->enqueAdminCommonStyles();
			CTClass_ScriptManager::enqueColorPicker();
			wp_register_script('mm_admin_settings_js', CT_TEMPPATH.'/com/sakuraplugins/js/admin_settings.js', array('jquery'));
			wp_enqueue_script('mm_admin_settings_js');			
		}					
	}	
	private function enqueAdminCommonStyles(){
			wp_register_style('skgrid_admin_style', CT_TEMPPATH.'/com/sakuraplugins/css/admin.css');
			wp_enqueue_style('skgrid_admin_style');	
			wp_register_style('skgrid_admin_style_opts', CT_TEMPPATH.'/com/sakuraplugins/css/admin_options.css');
			wp_enqueue_style('skgrid_admin_style_opts');
	}	

	//admin menu handler
	public function adminMenuHandler(){
		$sk_options_page = new CTClass_OptionPage(CT_PORTFOLIO_OPTION_GROUP);
		add_submenu_page('edit.php?post_type='.CT_PORTFOLIO_SLUG, 'Options', 'Options', 'manage_options', 'ct_portfolio_sett', array($sk_options_page, 'settings_page'));		
	}
	
	
	//WP Enqueue scripts handler
	public function WPEnqueueScriptsHandler(){
		wp_enqueue_script('jquery');
		CTClass_ScriptManager::enqueTweenmax("1.11.8");
		$this->enqueFonts();

		wp_register_style('sk_contempt', CT_TEMPPATH.'/css/sk_contempt.css');
		wp_enqueue_style('sk_contempt');

		$pluginOptions = CTClass_PluginOptions::getInstance();
		wp_add_inline_style('sk_contempt', $this->getCustomStyles().$pluginOptions->getCustomCSS());
	

		//isotope
		wp_register_script('sk_isotope', CT_TEMPPATH.'/js/external/isotope.min.js', array('jquery'), FALSE, TRUE);
		wp_enqueue_script('sk_isotope');		

		wp_register_script('sk_contempt', CT_TEMPPATH.'/js/sk_contempt.js', array('jquery'), FALSE, TRUE);
		wp_enqueue_script('sk_contempt');

		//add custom css													

		if(is_single()){
			//single scripts
			if(CT_PORTFOLIO_SLUG==get_post_type()){				
				wp_register_style('ct_bootstrap', CT_TEMPPATH.'/css/bootstrap/css/bootstrap.min.css');
				wp_enqueue_style('ct_bootstrap');
				wp_register_script('js_bootstrap', CT_TEMPPATH.'/css/bootstrap/js/bootstrap.min.js', array('jquery'));
				wp_enqueue_script('js_bootstrap');
								
				wp_register_style('contempt_single', CT_TEMPPATH.'/css/contempt_single.css');
				wp_enqueue_style('contempt_single');
				wp_register_script('sk_contempt_single', CT_TEMPPATH.'/js/sk_contempt_single.js', array('jquery'));
				wp_enqueue_script('sk_contempt_single');																								
				//add custom CSS							
			}
		}																		
	}	


	private function getCustomStyles(){
		$pluginOptions = CTClass_PluginOptions::getInstance();
		$backCol = $pluginOptions->getThumbBackgroundHoverCol();	
		return '
.ct_hover_overlay{
	background-color: #'.$backCol.';
}		
		';
	}

	//google fonts
	public function getGoogleFontsCSS(){
		return '		  			
		  @import url(http://fonts.googleapis.com/css?family=Open+Sans:400,600,300);		  	
		';
	}	

	//enque required fonts
	public function enqueFonts(){
	    $protocol = is_ssl() ? 'https' : 'http';
	    wp_enqueue_style( 'sk-opensans', $protocol."://fonts.googleapis.com/css?family=Open+Sans:400,800,300,600");
		wp_enqueue_style( 'sk-opensans-condensed', $protocol."://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700");
		wp_enqueue_style( 'sk-roboto', $protocol."://fonts.googleapis.com/css?family=Roboto:400,300");
		wp_enqueue_style( 'sk-montserrat', $protocol."://fonts.googleapis.com/css?family=Montserrat");			
	}				
	
	//remove support
	public function removeSupport($postTypeSlug, $val){
		//remove_post_type_support($postTypeSlug, $val);
	}
	
	//add thumb size/support
	private function addThumbSize($postTypes=NULL){
		if($postTypes==NULL)
			return;
		if(function_exists('add_theme_support')){
			add_theme_support('post-thumbnails');			
		}
	}

	//theme setup event
	public function after_theme_setup(){
		$this->addThumbSize(array(CT_PORTFOLIO_SLUG));
	}	

	//custom query var - used for single nav through the same category
	public function add_query_vars_filter($vars){
	  $vars[] = "ct";
	 return $vars;
	}
	
	protected $sk_sidebars;
	//sidebars implementation
	protected function initSidebars(){
		require_once(CT_CLASS_PATH.'/com/sakuraplugins/php/sidebars/sidebars.php');
		$this->sk_sidebars = new CTClass_SKSidebars();
		$this->sk_sidebars->init();			
	}	
	
	//init listeners
	public function start($opts){
		add_action('init', array($this, 'initializeHandler'));
		add_action('admin_init', array($this, 'adminInitHandler'));
		add_action('save_post', array($this, 'savePostHandler'));		
		add_action('admin_enqueue_scripts', array($this, 'adminEnqueueScriptsHandler'));				
		add_action('admin_menu', array($this, 'adminMenuHandler'));		
		add_action("wp_enqueue_scripts", array($this, 'WPEnqueueScriptsHandler'), 999999);	
		add_action("wp_ajax_get_metro_thumb", array($this, 'get_metro_thumb'));	
		add_action('after_setup_theme', array($this, 'after_theme_setup'));	
		add_filter("single_template", array($this, 'sk_plugin_single'));
		register_deactivation_hook($opts['PLUGIN_FILE'], array($this, 'plugin_deactivate'));
		add_action("wp_before_admin_bar_render", array($this, 'adminBarCustom'));
		//widgets
		require_once(CT_CLASS_PATH.'/com/sakuraplugins/php/widgets/widgets.php');
		//Add custom query vars
		add_filter( 'query_vars', array($this, 'add_query_vars_filter'));				
	}

	//admin bar custom
	public function adminBarCustom(){
		if(function_exists('get_current_screen')){
			$current_screen = get_current_screen();		
			if($current_screen->post_type==CT_PORTFOLIO_SLUG){			
				require_once(CT_CLASS_PATH.'com/sakuraplugins/php/admin_pages/banner.php');
			}
		}
	}	

	//plugin deactivate
	public function plugin_deactivate()
	{
		update_option('ct_is_rewrite', false);
		flush_rewrite_rules();		
	}	


	//ajax callback - admin thumbs
	public function get_metro_thumb(){
		$metroThumbID = (isset($_POST['metroThumbID'])?$_POST['metroThumbID']:'0');
		$response = new StdClass;
		$res = wp_get_attachment_image_src($metroThumbID, 'thumbnail');
		$iconUrl = ($res[0])?$res[0]:"http://placehold.it/100x100";
		$response->url = $iconUrl;		
		echo json_encode($response);
		die();
	}	

	//rx single template
	public function sk_plugin_single($single_template){
		global $post;		
		if($post->post_type==CT_PORTFOLIO_SLUG){			
			$single_template = dirname( __FILE__ ) . '/ct-single.php';										
		}
		return $single_template;
	}		

	/************* END BASE PLUGIN EVENTS ***********/	

	//add taxonomy
	private function addTaxonomy(){
		//portfolio taxonomy
		if(isset($this->rxCPT)){
			register_taxonomy(CT_PORTFOLIO_CATEGORIES, $this->rxCPT->getPostSlug(), array('label'=>'Contempt Categories', 'hierarchical'=>true));
		}				
	}	

	private $rxCPT;
	/*
	 * create youtube CPT
	 */
	public function addCPT(){
		$pluginOptions = CTClass_PluginOptions::getInstance();		
		$settings = array('post_custom_meta_data'=>CT_POST_CUSTOM_META, 'post_type' => CT_PORTFOLIO_SLUG, 'name' => __('Contempt Portfolio', CT_PLUGIN_TEXTDOMAIN), 'menu_icon' => CT_TEMPPATH.'/com/sakuraplugins/images/icons/images-flickr.png',
		'singular_name' => __('Contempt Portfolio', CT_PLUGIN_TEXTDOMAIN), 'rewrite' => $pluginOptions->getReWriteSlug(), 'add_new' => __('New item', CT_PLUGIN_TEXTDOMAIN),
		'edit_item' => __('Edit', CT_PLUGIN_TEXTDOMAIN), 'new_item' => __('New item', CT_PLUGIN_TEXTDOMAIN), 'view_item' => __('View item', CT_PLUGIN_TEXTDOMAIN), 'search_items' => __('Search items', CT_PLUGIN_TEXTDOMAIN),
		'not_found' => __('No item found', CT_PLUGIN_TEXTDOMAIN), 'not_found_in_trash' => __('Item not found in trash', CT_PLUGIN_TEXTDOMAIN), 
		'supports' => array('title', 'thumbnail', 'editor', 'comments'));
		
		$cptHelper = new CTClass_CPTHelper($settings);
		$this->rxCPT = new CTClass_CPT();
		$this->rxCPT->create($cptHelper, $settings);

		//re-write once
		$isReWrite = get_option('ct_is_rewrite');		
		if($isReWrite!=true){
			flush_rewrite_rules();			
			update_option('ct_is_rewrite', true);					
		}					
	}

	/////////////// BEGIN SHORTCODES ///////////////
	private function initShortcodes(){
		$shortcodes = new CTClass_Shortcodes();
		$shortcodes->register();
	}






}


?>