<?php
require_once(CT_CLASS_PATH.'/com/sakuraplugins/php/customposts/GenericPostType.php');
require_once(CT_CLASS_PATH.'/com/sakuraplugins/php/libs/resize_helper.php');
require_once(CT_CLASS_PATH.'/com/sakuraplugins/php/plugin-options.php');

/**
 * Rx CPT
 */
class CTClass_CPT extends CTClass_GenericPostType {

	//metabox featured
	public function meta_box_featured_image(){
		global $post;
		if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
			return $post_id;
		$customPostOptions = get_post_meta($post->ID, $this->getPostCustomMeta(), false);	
		$post_custom_meta_data = $this->getPostCustomMeta();				
		?>

		<!--featured image width-->
		<h4>Featured Image:</h4>
		<div class="sectionTitleUnderline"></div>
		<p class="sk_notice"><b>NOTE! </b>Feature image is mandatory even if you use a video. Use the right side panel in order to add a featured image. Recommendation: The featured image should be landscape.</p>
		

		<!--/featured image width-->		

		<div class="sk_clear_fx"></div>
		<div class="hlineSection"></div>

		<!--video-->
		<h4>Use video:</h4>
		<div class="sectionTitleUnderline"></div>		
		<?php
			$useVideoCB = (isset($customPostOptions[0]['useVideoCB']))?$customPostOptions[0]['useVideoCB']:'';
			$useVideoChecked = ($useVideoCB=="ON")?'checked':'';
			$video_code = (isset($customPostOptions[0]['video_code']))?$customPostOptions[0]['video_code']:'';								
			$redirect_video_class = ($useVideoCB!="ON")?' sk_modal_hide':'';	
		?>		
		<label class="checkbox"><input id="useVideoCB" type="checkbox" name="<?php echo $post_custom_meta_data;?>[useVideoCB]" value="ON" <?php echo $useVideoChecked;?>> Use video instead of images (for single page)</label>
		<div class="sk_clear_fx"></div>		
		<div id="redirect_video_content_ui" class="<?php echo $redirect_video_class;?>">
			<p class="sk_notice"><b>NOTE! </b>Add embedded video code below.</p>
			<textarea id="video_code_sk" name="<?php echo $post_custom_meta_data;?>[video_code]" style="width: 80%; min-height: 80px;"><?php echo $video_code;?></textarea>			
		</div>				
		<div class="sk_modal_space2"></div>	
		<!--/video-->	

		

		<!--subtitle-->
		<h4>Preview subtitle:</h4>
		<div class="sectionTitleUnderline"></div>
		<p class="sk_notice"><b>NOTE! </b>Preview subtitle will be shown on preview thumbs bellow the title.</p>		
		<?php
			$subtitle = (isset($customPostOptions[0]['subtitle']))?$customPostOptions[0]['subtitle']:'';
		?>
		<input type="text" name="<?php echo $post_custom_meta_data;?>[subtitle]" value="<?php echo wptexturize($subtitle);?>" />

			
		<div class="sk_modal_space2"></div>	
		<!--/subtitle-->


		<!--custom url-->
		<h4>Custom redirect:</h4>
		<div class="sectionTitleUnderline"></div>		
		<?php
			$useRedirect = (isset($customPostOptions[0]['useRedirect']))?$customPostOptions[0]['useRedirect']:'';
			$useRedirectChecked = ($useRedirect=="ON")?'checked':'';
			$redirect_url = (isset($customPostOptions[0]['redirect_url']))?$customPostOptions[0]['redirect_url']:'';								
			$redirect_class = ($useRedirect!="ON")?' sk_modal_hide':'';	
		?>		
		<label class="checkbox"><input id="useRedirectCB" type="checkbox" name="<?php echo $post_custom_meta_data;?>[useRedirect]" value="ON" <?php echo $useRedirectChecked;?>> Use custom URL redirect</label>
		<div class="sk_clear_fx"></div>		
		<div id="redirect_content_ui" class="<?php echo $redirect_class;?>">
			<p class="sk_notice"><b>NOTE! </b>Add custom URL below, will open portfolio to a custom URL instead of portfolio single.</p>
			<input id="redirect_input" type="text" style="width: 300px;max-width:300px;" name="<?php echo $post_custom_meta_data;?>[redirect_url]" value="<?php echo $redirect_url;?>" />
		</div>				
		<div class="sk_modal_space2"></div>	
		<!--/custom url-->


		<!--sidebar-->
		<h4>Sidebar:</h4>
		<div class="sectionTitleUnderline"></div>		
		<?php
			$isSidebar = (isset($customPostOptions[0]['isSidebar']))?$customPostOptions[0]['isSidebar']:'';
			$isSidebarChecked = ($isSidebar=="ON")?'checked':'';
		?>		
		<p class="sk_notice"><b>NOTE! </b>You can add "Contempt:Recent Projects" widget to the Contempt: Sidebar.</p>
		<label class="checkbox"><input type="checkbox" name="<?php echo $post_custom_meta_data;?>[isSidebar]" value="ON" <?php echo $isSidebarChecked;?>> Enable Sidebar for this item (single page)</label>
		<div class="sk_clear_fx"></div>				
		<div class="sk_modal_space2"></div>	
		<!--/sidebar-->	


		<!--featured-->
		<h4>Featured project:</h4>
		<div class="sectionTitleUnderline"></div>		
		<?php
			$isFeatured = (isset($customPostOptions[0]['isFeatured']))?$customPostOptions[0]['isFeatured']:'';
			$isFeaturedChecked = ($isFeatured=="ON")?'checked':'';
		?>		
		<p class="sk_notice"><b>NOTE! </b>Featured projects are being shown inside "Contempt: Featured Projects" widget.</p>
		<label class="checkbox"><input type="checkbox" name="<?php echo $post_custom_meta_data;?>[isFeatured]" value="ON" <?php echo $isFeaturedChecked;?>> Is project featured</label>
		<div class="sk_clear_fx"></div>				
		<div class="sk_modal_space2"></div>	
		<!--/featured-->


		<!--special widget-->
		<h4>Special widget:</h4>
		<div class="sectionTitleUnderline"></div>		
		<?php
			$isWidget = (isset($customPostOptions[0]['isWidget']))?$customPostOptions[0]['isWidget']:'';
			$isWidgetChecked = ($isWidget=="ON")?'checked':'';
			$showWidget = ($isWidget=="ON")?' showWidgetSP':'';

			$prevLabel = (isset($customPostOptions[0]['previewLabel']))?$customPostOptions[0]['previewLabel']:'Preview';
			$previewURL = ((isset($customPostOptions[0]['previewURL']))?$customPostOptions[0]['previewURL']:'');
			$prevTarget = (isset($customPostOptions[0]['prevTarget']))?$customPostOptions[0]['prevTarget']:'_blank';

			$buyLabel = (isset($customPostOptions[0]['buyLabel']))?$customPostOptions[0]['buyLabel']:'Buy';
			$buyURL = ((isset($customPostOptions[0]['buyURL']))?$customPostOptions[0]['buyURL']:'');			
			$buyTarget = (isset($customPostOptions[0]['buyTarget']))?$customPostOptions[0]['buyTarget']:'_blank';

			$otherLabel = (isset($customPostOptions[0]['otherLabel']))?$customPostOptions[0]['otherLabel']:'Other';
			$otherURL = ((isset($customPostOptions[0]['otherURL']))?$customPostOptions[0]['otherURL']:'');			
			$otherTarget = (isset($customPostOptions[0]['otherTarget']))?$customPostOptions[0]['otherTarget']:'_blank';			

			$custom_editor_content = (isset($customPostOptions[0]['aditionalInfo']))?$customPostOptions[0]['aditionalInfo']:'';			
		?>		
		<p class="sk_notice"><b>NOTE! </b>Special widget allows you to add a specific project widget for single pages (the widget includes three buttons and a text area). If buttons link are empty the button won't display.</p>
		<label class="checkbox"><input id="widgetCB" type="checkbox" name="<?php echo $post_custom_meta_data;?>[isWidget]" value="ON" <?php echo $isWidgetChecked;?>> Is special widget</label>
		<div class="sk_clear_fx"></div>	

		<div id="specialWidgetUI" class="specialWidgetUI<?php echo $showWidget;?>">
			<div class="widgetRow">
				<label>Button label:<input class="widgetTxtInput" type="text" name="<?php echo $post_custom_meta_data;?>[previewLabel]" value="<?php echo $prevLabel;?>" /></label>			
				<label>URL:<input class="widgetTxtInput" type="text" placeholder="http://" name="<?php echo $post_custom_meta_data;?>[previewURL]" value="<?php echo $previewURL;?>" /></label>			
				<label>Target<input class="widgetTxtInput" type="text" name="<?php echo $post_custom_meta_data;?>[prevTarget]" value="<?php echo $prevTarget;?>" /></label>			
			</div>
			<div class="widgetRow">
				<label>Button label:<input class="widgetTxtInput" type="text" name="<?php echo $post_custom_meta_data;?>[buyLabel]" value="<?php echo $buyLabel;?>" /></label>			
				<label>URL:<input class="widgetTxtInput" type="text" placeholder="http://" name="<?php echo $post_custom_meta_data;?>[buyURL]" value="<?php echo $buyURL;?>" /></label>			
				<label>Target<input class="widgetTxtInput" type="text" name="<?php echo $post_custom_meta_data;?>[buyTarget]" value="<?php echo $buyTarget;?>" /></label>			
			</div>
			<div class="widgetRow">
				<label>Button label:<input class="widgetTxtInput" type="text" name="<?php echo $post_custom_meta_data;?>[otherLabel]" value="<?php echo $otherLabel;?>" /></label>			
				<label>URL:<input class="widgetTxtInput" type="text" placeholder="http://" name="<?php echo $post_custom_meta_data;?>[otherURL]" value="<?php echo $otherURL;?>" /></label>			
				<label>Target<input class="widgetTxtInput" type="text" name="<?php echo $post_custom_meta_data;?>[otherTarget]" value="<?php echo $otherTarget;?>" /></label>			
			</div>						
			<div class="widgetRow">
				<?php
					wp_editor($custom_editor_content, "custom_editor", array("textarea_name"=>$post_custom_meta_data.'[aditionalInfo]', "wpautop"=>true));
				?>
			</div>
		</div>			

		<div class="sk_modal_space2"></div>	
		<!--/special widget-->									

		<?php		
	}

	public function meta_box_gallery(){
		global $post;
		if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
			return $post_id;
															
		?>	
			<input id="removeAllFeaturedImagesBTN" type='submit' value='Remove all' class='button-secondary right' />		
			<input id="addFeaturedImagesBTN" type='submit' value='Add images' class='button-primary right buttonSpaceRight' />			
			<div class="sk_clear_fx"></div>
			<div id="featured_images_rx_portfolio" class="optionBox" data-post_meta="<?php echo CT_POST_CUSTOM_META;?>">
				<?php
					$customPostOptions = get_post_meta($post->ID, $this->getPostCustomMeta(), false);
					$featuredImagesAC = (isset($customPostOptions[0]['featuredImages']))?$customPostOptions[0]['featuredImages']:array();
					
				?>
				<div id="featuresThumbsContainer">
					<ul class="sortableThumbs" id="featuredImagesUI">
						<?php if(!empty($featuredImagesAC)):?>
									<?php								
									for ($i=0; $i < sizeof($featuredImagesAC); $i++) {
										$res = wp_get_attachment_image_src($featuredImagesAC[$i], 'medium');
										$iconUrl = 'http://placehold.it/150x150';
										if($res){
											$resizeRes = CTClass_ResizeHelper::resize($res[0], 150, 150, true);
											$iconUrl = ($resizeRes)?$resizeRes:$iconUrl;
										}
										$iconHTML = '<li class="ui-state-default"><div class="thumbBoxImage">';
	                               		$iconHTML .= '<div class="featuredThumb"><img src="'.$iconUrl.'" /></div>';
	                               		$iconHTML .= '<input type="hidden" name="'.$this->getPostCustomMeta().'[featuredImages][]" value="'.$featuredImagesAC[$i].'" />';	                               			                               		

	                               			                               			                               			                               		

										$iconHTML .= '<div class="featuredThumbOverlay">
										<div class="thumbOverlayMove"></div>
										<div class="thumbOverlayRemove"></div>																			
										</div>';
										$iconHTML .= '</div></li>';
										echo $iconHTML;
									}
									?>
						<?php endif;?>					
					</ul>
					<div class="sk_clear_fx"></div>	
				</div>				
			</div>			
			<p class="sk_notice"><b>NOTE! </b>You can change the image's order by drag and drop. Recommended size for images is at least 900px wide for single that has a sidebar and at least 1200 wide for sigle that doesn't have a sidebar (full width single). In order order to obtain a better looking images slider, images should have same size. Performance recommendation: Before you upload the images make sure you save it as JPEG - around quality 8 (images should not exceed 300KB in filesize).</p>

		
		<?php
	}
		
		
}


?>