<?php


/**
 * Contempt Recent projects widget
 */

class ContemptRecentProjects extends WP_Widget {
	public function __construct(){
		parent::__construct(
			'ctmpt_recent_widget',
			'Contempt: Recent Projects',
			array('description'=>'Shows recent portfolio items')
		);		
	}	
	public function form($instance){
		$title = (isset($instance['title' ])) ? $instance['title'] : 'Recent projects';
		$postNo = (isset($instance['postNo' ])) ? $instance['postNo'] : 3;		
		?>
			<p>
				<input id="<?php echo $this->get_field_id( 'title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
			    <label for="<?php echo $this->get_field_id('title');?>">Title</label>				
			    <br />
			    <input id="<?php echo $this->get_field_id( 'postNo'); ?>" name="<?php echo $this->get_field_name('postNo'); ?>" type="text" value="<?php echo esc_attr($postNo); ?>" />
			    <label for="<?php echo $this->get_field_id('postNo');?>">Posts number</label>								
			</p>		
		<?php					
	}	
	public function update($new_instance, $old_instance){
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['postNo'] = strip_tags($new_instance['postNo']);
		return $instance;		
	}	
	public function widget($args, $instance){
		extract($args);
		$title = apply_filters('widget_title', $instance['title']);
		$posts_no = isset($instance['postNo'])?$instance['postNo']:3;				
		
		echo $before_widget;
		require_once(CT_CLASS_PATH.'/com/sakuraplugins/php/post-options.php');
		//get posts			
		$posts_array = get_posts(array('posts_per_page'=>$posts_no, 'post_type'=>CT_PORTFOLIO_SLUG)); 	
		?>
			<?php echo $before_title.$title.$after_title;?>				
			<ul class="ctptRecentPosts">
				<?php global $post; $post_no=-1;?>
				<?php foreach( $posts_array as $post ) : setup_postdata($post);?>
				<?php 
					$postOptions = new CTClass_PostOptions($post->ID);
					$post_no++;
					$permalink = get_permalink($post->ID);
				?>
				<li>
					<div class="ctpRecentProjectUI" data-href="<?php echo $permalink;?>">
						<a class="cptRecentImg" href="<?php echo $permalink;?>"><img src="<?php echo $postOptions->getFeaturedImageThumb();?>" alt="" /></a>
						<div class="cptRecentTitleUI">
							<a class="cptRecentTitle" href="<?php echo $permalink;?>"><?php echo get_the_title($post->ID);?></a>
							<p class="cptRecentSubtitle"><?php echo $postOptions->getSubtitle();?></p>
						</div>
						<?php if($post_no<sizeof($posts_array)-1):?>
							<div class="ctpRecentBottomLine"></div>
						<?php endif;?>
						<div class="clear-fx"></div>
					</div>					
				</li>
				<?php endforeach; ?>
			</ul>
			
		<?php		
		//end get posts
		
		
		echo $after_widget;
	}
}



/**
 * Contempt Featured projects widget
 */

class ContemptFeaturedProjects extends WP_Widget {
	public function __construct(){
		parent::__construct(
			'ctmpt_featured_widget',
			'Contempt: Featured Projects',
			array('description'=>'Shows featured portfolio items')
		);		
	}	
	public function form($instance){
		$title = (isset($instance['title' ])) ? $instance['title'] : 'Featured projects';
		$postNo = (isset($instance['postNo' ])) ? $instance['postNo'] : 2;		
		?>
			<p>
				<input id="<?php echo $this->get_field_id( 'title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
			    <label for="<?php echo $this->get_field_id('title');?>">Title</label>				
			    <br />
			    <input id="<?php echo $this->get_field_id( 'postNo'); ?>" name="<?php echo $this->get_field_name('postNo'); ?>" type="text" value="<?php echo esc_attr($postNo); ?>" />
			    <label for="<?php echo $this->get_field_id('postNo');?>">Posts number</label>								
			</p>		
		<?php					
	}	
	public function update($new_instance, $old_instance){
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['postNo'] = strip_tags($new_instance['postNo']);
		return $instance;		
	}	
	public function widget($args, $instance){
		extract($args);
		$title = apply_filters('widget_title', $instance['title']);
		$posts_no = isset($instance['postNo'])?$instance['postNo']:3;				
		
		echo $before_widget;
		require_once(CT_CLASS_PATH.'/com/sakuraplugins/php/post-options.php');
		//get posts			
		$posts_array = get_posts(array('posts_per_page'=>'-1', 'post_type'=>CT_PORTFOLIO_SLUG)); 	
		?>
			<?php echo $before_title.$title.$after_title;?>				
			<ul class="ctptFeaturedPosts">
				<?php global $post; $post_count=-1;?>
				<?php foreach( $posts_array as $post ) : setup_postdata($post);?>
				<?php 
					$postOptions = new CTClass_PostOptions($post->ID);
				?>
					<?php if($postOptions->isFeatured()):?>
					<?php
						$post_count++;											
					?>
					<?php if($post_count<(int)$posts_no):?>
						<?php $postOptions->_eFeatured();?>
					<?php endif;?>
					<?php endif;?>
				<?php endforeach; ?>
			</ul>
			
		<?php		
		//end get posts
		
		
		echo $after_widget;
	}
}




/**
 * register widgets
 */
function ct_plugins_register_widgets(){		
	register_widget('ContemptRecentProjects');
	register_widget('ContemptFeaturedProjects');	
}
add_action('widgets_init', 'ct_plugins_register_widgets');
 
 
?>