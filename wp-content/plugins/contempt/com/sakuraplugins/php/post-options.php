<?php
require_once(CT_CLASS_PATH.'/com/sakuraplugins/php/libs/resize_helper.php');

class CTClass_PostOptions
{
	private $customPostOptions;
	private $ID;
	function __construct($ID)
	{
		$this->ID = $ID;
		$this->customPostOptions = get_post_meta($ID, CT_POST_CUSTOM_META, false);
	}

	//get featured image
	public function getFeaturedImage($w, $h=null){
		//calculate h (4:3) - portrait
		$h = (75*$w)/100;//remove this line for custom H		
		$imageURL = "http://placehold.it/".$w."x".$h."/e8117f/FFFFFF/&text=Could not resize the image!";		
		$res = wp_get_attachment_image_src(get_post_thumbnail_id($this->ID), 'full');
		if($res){
			$thumb_temp_url = CTClass_ResizeHelper::resize($res[0], $w, $h, true);			
			$imageURL = ($thumb_temp_url)?$thumb_temp_url:$imageURL;
		}
		return $imageURL;
	}

	//get featured image full
	public function getFeaturedImageFull($w=900){
		$imageURL = false;
		$res = wp_get_attachment_image_src(get_post_thumbnail_id($this->ID), 'full');
		if($res){
			$thumb_temp_url = CTClass_ResizeHelper::resize($res[0], $w);			
			$imageURL = ($thumb_temp_url)?$thumb_temp_url:$res[0];
		}
		return $imageURL;
	}

	//get featured image thumb
	public function getFeaturedImageThumb(){
		$imageURL = false;
		$res = wp_get_attachment_image_src(get_post_thumbnail_id($this->ID), 'thumbnail');
		if($res){			
			$imageURL = $res[0];
		}
		return $imageURL;
	}

	//get featured image featured
	public function getFeaturedImageFeatured($w, $h){		
		$imageURL = "";
		$res = wp_get_attachment_image_src(get_post_thumbnail_id($this->ID), 'full');
		if($res){
			$imageURL = $res[0];
			$resizedImage = CTClass_ResizeHelper::resize($imageURL, $w, $h, true);	
			if($resizedImage){
				$imageURL = $resizedImage;
			}								
		}
		return $imageURL;		
	}			

	//get image generic
	public function getImageGeneric($id, $w=900){		
		$res = wp_get_attachment_image_src($id, 'full');
		$iconUrl = 'http://placehold.it/150x150';
		if($res){
			$resizeRes = CTClass_ResizeHelper::resize($res[0], $w);
			$iconUrl = ($resizeRes)?$resizeRes:$res[0];
		}
		return $iconUrl;
	}		

	//check if use video
	public function isUseVideo(){
		$useVideoCB = (isset($this->customPostOptions[0]['useVideoCB']))?$this->customPostOptions[0]['useVideoCB']:'';
		return ($useVideoCB=="ON")?true:false;		
	}

	//get featured images
	public function getFeaturedImages(){
		$featuredImagesAC = (isset($this->customPostOptions[0]['featuredImages']))?$this->customPostOptions[0]['featuredImages']:array();
		return (sizeof($featuredImagesAC)!=0)?$featuredImagesAC:false;
	}

	//get subtitle
	public function getSubtitle(){
		return (isset($this->customPostOptions[0]['subtitle']))?$this->customPostOptions[0]['subtitle']:'';
	}

	//get permalink
	public function getPermalink($isCategory=false){
		$useRedirect = (isset($this->customPostOptions[0]['useRedirect']))?$this->customPostOptions[0]['useRedirect']:'';
		$useRedirect = ($useRedirect=="ON")?true:false;
		if(!$useRedirect){
			if($isCategory){
				return add_query_arg('ct', '1', get_permalink($this->ID));
			}else{
				return get_permalink($this->ID);		
			}			
		}			
		return (isset($this->customPostOptions[0]['redirect_url']))?$this->customPostOptions[0]['redirect_url']:'';											
	}

	//check if single has a sidebar
	public function isSidebar(){
		$isSidebar = (isset($this->customPostOptions[0]['isSidebar']))?$this->customPostOptions[0]['isSidebar']:'';
		return ($isSidebar=="ON")?true:false;		
	}

	//get categories - to be used for filetrable class
	public function getFilterClasses(){
		$terms = get_the_terms($this->ID, CT_PORTFOLIO_CATEGORIES);
		$filterClasses = '';
		if($terms && !is_wp_error($terms)){			
			foreach ( $terms as $term ) {								
				$filterClasses .= (' '.$term->slug);
			}			
		}
		return $filterClasses;	
	}

	//get video code
	public function getVideoCode(){
		return (isset($this->customPostOptions[0]['video_code']))?$this->customPostOptions[0]['video_code']:'';										
	}

	//check if project is featured
	public function isFeatured(){
		$isFeatured = (isset($this->customPostOptions[0]['isFeatured']))?$this->customPostOptions[0]['isFeatured']:'';
		return ($isFeatured=="ON")?true:false;
	}


	//echo featured project
	public function _eFeatured(){
		$thumbURL = $this->getFeaturedImageFeatured(600, 350);
		$title = get_the_title($this->ID);
		$subtitle = $this->getSubtitle();
		$permalink = $this->getPermalink(false);
		echo $this->buildThumbUIFeatured($thumbURL, $title, $subtitle, $permalink);
	}

	public function buildThumbUIFeatured($thumbURL, $title, $subtitle, $permalink){
		$pluginOptions = CTClass_PluginOptions::getInstance();		
		$out = '<li class="contemptThumbUISingle contemptFeaturedWidgetUI" data-thumburl="'.$thumbURL.'" data-texthover="#'.$pluginOptions->getThumbTextHoverCol().'">';
			$out .= '<a class="ct_thumbIMG" href="'.$permalink.'"><img class="ct-thumb-img" src="'.$thumbURL.'" alt="" /></a>';										
			$out .= '<div class="ct_hover_overlay"><div class="ct-plus"><img src="'.CT_TEMPPATH.'/img/plus-thin.png" alt="" /></div></div>';		
			$out .= '<div class="ct_thumbTitleUI">';
				$out .= '<p class="ct_thumbTitle">'.$title.'</p>';
				$out .= '<p class="ct_thumbSubtitle">'.$subtitle.'</p>';				
			$out .= '</div>';
		$out .= '</li>';
		return $out;
	}	

	/* GET BOOTSTRAP CAROUSEL - GENERIC
	================================================== */
	public function getBootstrapCarousel($carouselImagesAC, $w){
		$carousel_id = uniqid('carousel_');
		$active = 'active';
		$carouselItems = '';
		$carouselIndicators = '';
		for ($i=0; $i < sizeof($carouselImagesAC); $i++) { 
			$imgUrl = $this->getImageGeneric($carouselImagesAC[$i], $w);
			//echo $imgUrl;
			$carouselItems .= '<div class="'.$active.' item"><img src="'.$imgUrl.'" alt="" /></div>';			
			$carouselIndicators .= '<li data-target="#'.$carousel_id.'" data-slide-to="'.$i.'" class="'.$active.'"></li>';
			$active = '';
		}
		$carouselControlsStyle = (sizeof($carouselImagesAC)==1)?$carouselControlsStyle='display: none;':$carouselControlsStyle='';		
		
		$carouselHTML = '
		<!--image carousel-->
		<div id="'.$carousel_id.'" class="carousel slide">
			<ol class="carousel-indicators" style="'.$carouselControlsStyle.'">
				'.$carouselIndicators.'
			</ol>		
			<!-- Carousel items -->
				<div class="carousel-inner">
					'.$carouselItems.'						
				</div>						
			<!-- /Carousel items -->
			<!-- Carousel nav -->
 <a class="left carousel-control" href="#'.$carousel_id.'" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left">&lsaquo;</span>
  </a>
  <a class="right carousel-control" href="#'.$carousel_id.'" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right">&rsaquo;</span>
  </a>  											
			<!-- Carousel nav -->												
		</div>		
		<!--/image carousel-->
		';
		
		return $carouselHTML;
	}


	//custom widget
	public function isCustomWidget(){
		$isWidget = (isset($this->customPostOptions[0]['isWidget']))?$this->customPostOptions[0]['isWidget']:'';
		return ($isWidget=="ON")?true:false;		
	}

	public function _eCustomWidgetContent(){
		echo wpautop((isset($this->customPostOptions[0]['aditionalInfo']))?$this->customPostOptions[0]['aditionalInfo']:'', true);			
	}

	public function getWgtButtons(){
		$prevLabel = (isset($this->customPostOptions[0]['previewLabel']))?$this->customPostOptions[0]['previewLabel']:'Preview';
		$previewURL = ((isset($this->customPostOptions[0]['previewURL']))?$this->customPostOptions[0]['previewURL']:'');
		$prevTarget = (isset($this->customPostOptions[0]['prevTarget']))?$this->customPostOptions[0]['prevTarget']:'_blank';

		$buyLabel = (isset($this->customPostOptions[0]['buyLabel']))?$this->customPostOptions[0]['buyLabel']:'Buy';
		$buyURL = ((isset($this->customPostOptions[0]['buyURL']))?$this->customPostOptions[0]['buyURL']:'');
		$buyTarget = (isset($this->customPostOptions[0]['buyTarget']))?$this->customPostOptions[0]['buyTarget']:'_blank';

		$otherLabel = (isset($this->customPostOptions[0]['otherLabel']))?$this->customPostOptions[0]['otherLabel']:'Other';
		$otherURL = ((isset($this->customPostOptions[0]['otherURL']))?$this->customPostOptions[0]['otherURL']:'');
		$otherTarget = (isset($this->customPostOptions[0]['otherTarget']))?$this->customPostOptions[0]['otherTarget']:'_blank';		

		return array('prevLabel'=>$prevLabel, 'previewURL'=>$previewURL, 'prevTarget'=>$prevTarget, 'buyLabel'=>$buyLabel, 'buyURL'=>$buyURL, 'buyTarget'=>$buyTarget, 
			'otherLabel'=>$otherLabel, 'otherURL'=>$otherURL, 'otherTarget'=>$otherTarget);

	}

}

?>