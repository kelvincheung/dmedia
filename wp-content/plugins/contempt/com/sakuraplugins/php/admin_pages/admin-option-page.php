<?php

/**
 *  generic settings
 */
class CTClass_GenericSettingsPage{
	
	private $optionsGroup;
	function __construct($optsGroup) {
		$this->optionsGroup = $optsGroup;
		add_action('admin_init', array($this, 'registerSettingsGroups'));
	}
	
	//register settings group
	public function registerSettingsGroups(){
		register_setting($this->optionsGroup, $this->optionsGroup);
	}	
	
	//get option group
	protected function getOptionGroup(){
		return $this->optionsGroup;
	}	
}


/**
 * RXOptionPage
 */
class CTClass_OptionPage extends CTClass_GenericSettingsPage {
	
	public function settings_page(){
		$options = get_option($this->getOptionGroup());							
		?>
		<div class="spacer10"></div>
		<form method="post" action="options.php">
			<?php settings_fields($this->getOptionGroup()); ?>				
		  
		  <!--options wrapper-->
		  <div id="optionsWrapper">	
		  	<h1 class="optionsMainTitle">Contempt Portfolio Options</h1>

		  	<!--re-write-->
		  	<div class="whiteOptionBox">
		  		<h2 class="optionsSecondTitle">Re-Write slug</h2>
		  		<div class="hLineTitle"></div>
		  		<p class="sk_notice"><strong>NOTE!</strong> The re-write slug will affect the way permalinks look, ex: http://website.com/slug/item-name. If you change the slug, in order the changes to take effect, go to Admin > Settings > Permalinks and click "Save".
		  			Do not add spaces within the slug! Make sure you do not have the same slug as the name of a static page.
		  		</p>		  		
		  		<?php
		  			$mmReWriteSlug = (isset($options['mmReWriteSlug']))?$options['mmReWriteSlug']:CT_PORTFOLIO_REWRITE;
		  		?>
		  		<input type="text" name="<?php echo $this->getOptionGroup();?>[mmReWriteSlug]" value="<?php echo $mmReWriteSlug;?>" />
		  	</div>
		  	<!--/re-write-->
		  	
			<p class="submit">
				<input type="submit" class="button-primary" value="<?php _e('Save Changes', CT_PLUGIN_TEXTDOMAIN) ?>" />
	        </p>


		  	<!--shortcodes-->
		  	<div class="whiteOptionBox whiteOptionBoxTopSpace">
		  		<h2 class="optionsSecondTitle">Shortcodes</h2>
		  		<div class="hLineTitle"></div>
		  		<p class="sk_notice"><strong>NOTE!</strong> If you want to change the order of portfolio items you can use <a href="https://wordpress.org/plugins/post-types-order/" target="_blank">Post Types Order Plugin</a>.</p>
		  		<p>Display all portfolio items with pagination (three columns).</p>		  		
				<div class="shortcode_display">[contempt_all cols="three" items_per_page="12"]</div>
				<div class="small_gap"></div>
		  		<p>Display all portfolio items with pagination two columns.</p>		  		
				<div class="shortcode_display">[contempt_all cols="two" items_per_page="12"]</div>
				<div class="small_gap"></div>

		  		<p>Display all posts from a specific category with pagination (three columns). Add category slug</p>
				<div class="shortcode_display">[contempt_category cols="three" category="web-design" items_per_page="12"]</div>
				<div class="small_gap"></div>
		  		<p>Display all posts from a specific category with pagination (Two columns). Add category slug</p>
				<div class="shortcode_display">[contempt_category cols="two" category="web-design" items_per_page="12"]</div>
				<div class="small_gap"></div>				


		  		<p>Display all posts filterable from categories. Add categories slug separated by commas. There is no pagination for this shortcode. </p>
				<div class="shortcode_display">[contempt_filterable categories="web-design, graphics" label_all="All"]</div>				
				<div class="sk_clear_fx"></div>
		  	</div>
		  	<!--/shortcodes-->	


		  	<!--single page settings-->
		  	<div class="whiteOptionBox whiteOptionBoxTopSpace">
		  		<h2 class="optionsSecondTitle">Colors</h2>
		  		<div class="hLineTitle"></div>
		  						

		  		<div class="sk_admin_row">
		  			<?php
		  				$thumbs_hover_background = (isset($options['thumbs_hover_background']))?$options['thumbs_hover_background']:'3fd9cd';
		  				$thumbs_hover_text_color = (isset($options['thumbs_hover_text_color']))?$options['thumbs_hover_text_color']:'FFFFFF';
		  				$ctp_nav_hover = (isset($options['ctp_nav_hover']))?$options['ctp_nav_hover']:'3fd9cd';
		  			?>
		  			<div class="sk_admin_span3">
		  				<p><b>Thumbs hover background color</b></p>
		  				<input id="thumbs_hover_background" data-default="3fd9cd" type="text" style="max-width: 100px;" name="<?php echo $this->getOptionGroup();?>[thumbs_hover_background]" value="<?php echo $thumbs_hover_background;?>" />
		  			</div>

		  			<div class="sk_admin_span3">
		  				<p><b>Thumbs hover text color</b></p>
		  				<input id="thumbs_hover_text_color" data-default="FFFFFF" type="text" style="max-width: 100px;" name="<?php echo $this->getOptionGroup();?>[thumbs_hover_text_color]" value="<?php echo $thumbs_hover_text_color;?>" />
		  			</div>

		  			<div class="sk_admin_span3">
		  				<p><b>Filterable nav hover color</b></p>
		  				<input id="ctp_nav_hover" data-default="3fd9cd" type="text" style="max-width: 100px;" name="<?php echo $this->getOptionGroup();?>[ctp_nav_hover]" value="<?php echo $ctp_nav_hover;?>" />
		  			</div>
		  			<div class="sk_clear_fx"></div>	
				    <p>
				    	<button id="resetContemptColorsBTN" class="btn" type="button">Reset to default</button>
				    </p>		  			
		  		</div>



		  	</div>
		  	<!--/single page settings--> 	

			<p class="submit">
				<input type="submit" class="button-primary" value="<?php _e('Save Changes', CT_PLUGIN_TEXTDOMAIN) ?>" />
	        </p>	


		  	<!--custom CSS-->
		  	<div class="whiteOptionBox whiteOptionBoxTopSpace">
		  		<h2 class="optionsSecondTitle">Custom CSS</h2>
		  		<div class="hLineTitle"></div>
		  		<p class="sk_notice"><strong>NOTE!</strong> Add custom CSS below.</p>
		  		<?php
		  			$ctCustomCSS = (isset($options['ctCustomCSS']))?$options['ctCustomCSS']:'';
		  		?>
		  		<textarea class="customCSSBox" name="<?php echo $this->getOptionGroup();?>[ctCustomCSS]"><?php echo $ctCustomCSS;?></textarea>
		  	</div>
		  	<!--/custom CSS-->	


			<p class="submit">
				<input type="submit" class="button-primary" value="<?php _e('Save Changes', CT_PLUGIN_TEXTDOMAIN) ?>" />
	        </p>	        		  		        	  	

		</form>		
		
		<?php
	}





}


?>