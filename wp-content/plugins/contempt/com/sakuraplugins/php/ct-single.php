<?php
require_once(CT_CLASS_PATH.'/com/sakuraplugins/php/post-options.php');
require_once(CT_CLASS_PATH.'/com/sakuraplugins/php/frontend-engine.php');
$id = get_the_ID();
$title = get_the_title($id);
$postOptions = new CTClass_PostOptions($id);
$subtitle = $postOptions->getSubtitle();
$isCategoryCT = (get_query_var('ct'))?true:false;
$featured_image = ($isCategoryCT)?$postOptions->getFeaturedImageFull(1200):$postOptions->getFeaturedImageFull(900);
$isFeaturedVideo = $postOptions->isUseVideo();
$featured_images = $postOptions->getFeaturedImages();

?>
<?php get_header(); ?>

<!--wrapper-->
<div id="ctGridWrapper">
    <!--container-->
  <div class="container">
    <!--row-->
    <div class="row">
        <!--single wrapper-->
        <div class="ctpSingleWrapper">
        
        <?php if($postOptions->isSidebar()):?>          
            <!--sidebar single-->
            <?php require_once(CT_CLASS_PATH.'/com/sakuraplugins/php/single-sidebar.php');?>
            <!--/sidebar single-->
        <?php endif;?>        
        
        <?php if(!$postOptions->isSidebar()):?>
            <!--no sidebar single-->
          <?php require_once(CT_CLASS_PATH.'/com/sakuraplugins/php/single-nosidebar.php');?>
          <!--/no sidebar single-->
        <?php endif;?> 

        </div>     
        <!--/single wrapper-->  

    </div>
    <!--/row-->
  </div>
  <!--/container-->

</div>
<!--/wrapper-->
<?php get_footer(); ?>
