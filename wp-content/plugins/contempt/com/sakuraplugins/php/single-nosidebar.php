<!--full width-->
<div class="col-md-12">


	<?php if (have_posts()) :  while (have_posts()) : the_post(); ?>
	<div class="singleSidebarTitleUI">
		<h1 class="singleSdbTitle"><?php echo $title;?></h1>
		<div class="sigleTitleLine sigleTitleLineTop"></div>
		<?php if($subtitle!=""):?>
		<h2 class="singleSdbSubtitle"><?php echo $subtitle;?></h2>
		<div class="clear-fx"></div>
		<div class="sigleTitleLine sigleTitleLineBottom"></div>
		<?php endif;?>		
	</div>
			<!--is featured-->
			<?php if(($featured_images) || ($isFeaturedVideo) || ($featured_image)):?>
			<div class="featuredContainerUI">	
				<!--images slider-->
				<?php if(!$isFeaturedVideo && ($featured_images)):?>
					<?php echo $postOptions->getBootstrapCarousel($featured_images, 1200);?>
				<?php endif;?>
				<!--/images slider-->
				
				<!--featured video-->		
				<?php if($isFeaturedVideo):?>
					<div class="flex-video">
					<?php echo $postOptions->getVideoCode();?>
					</div>
				<?php endif;?>
				<!--/featured video-->
				
				<!--default featured image-->
				<?php if(!$isFeaturedVideo && !$featured_images && $featured_image):?>
					<img class="ctDefaultFeaturedImage" src="<?php echo $featured_image;?>" alt="" />
				<?php endif;?>
				<!--/default featured image-->
			</div>	
			<?php endif;?>
			<!--/is featured-->

			<!--content-->
			<div class="ctGenericContent">
				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<?php the_content();?>
				</div>
			</div>
			<!--/content-->

			<!--nav-->
			<div class="ctNav">				
				<!--navigate through same category-->
				<?php if($isCategoryCT):?>					
					<?php
						$fEngine = new CTClass_FrontEngine(get_the_ID());
						$fEngine->initIDS();
						$prevPostID = $fEngine->getPrevPostID();
						$nextPostID = $fEngine->getNextPostID();										
					?>
					<?php if($prevPostID):?>
						<?php
							$fEngine = new CTClass_FrontEngine($prevPostID);
							$prevHTML = $fEngine->getNavUI('prev', $isCategoryCT);						
						?>
						<div class="ctNavUI ctNavPrev">
							<?php echo $prevHTML;?>
						</div>
					<?php endif;?>
					<?php if($nextPostID):?>
						<?php
							$fEngine = new CTClass_FrontEngine($nextPostID);
							$nextHTML = $fEngine->getNavUI('next', $isCategoryCT);					
						?>
						<div class="ctNavUI ctNavNext">
							<?php echo $nextHTML;?>
						</div>						
					<?php endif;?>
					<div class="clear-fx"></div>						
				<?php endif;?>

				<!--navigate through all items-->
				<?php if(!$isCategoryCT):?>
					<?php
						$prev_post = get_adjacent_post(false, '', true);
						$next_post = get_adjacent_post(false, '', false);																	
					?>				
					<?php if(!empty($prev_post)):?>
						<?php
							$fEngine = new CTClass_FrontEngine($prev_post->ID);
							$prevHTML = $fEngine->getNavUI('prev', $isCategoryCT);						
						?>
						<div class="ctNavUI ctNavPrev">
							<?php echo $prevHTML;?>
						</div>
					<?php endif;?>
					<?php if(!empty($next_post)):?>
						<?php
							$fEngine = new CTClass_FrontEngine($next_post->ID);
							$nextHTML = $fEngine->getNavUI('next', $isCategoryCT);					
						?>
						<div class="ctNavUI ctNavNext">
							<?php echo $nextHTML;?>
						</div>						
					<?php endif;?>
					<div class="clear-fx"></div>						
				<?php endif;?>				
			</div>
			<!--/nav-->	

			<!--comments-->			
			<?php comments_template(); ?> 
			<!--/comments-->


	<?php endwhile; else: ?>
	<p><?php _e('No posts were found. Sorry!', 'default_textdomain');?></p>
	<?php endif; wp_reset_query();?>

</div>
<!--/full width-->