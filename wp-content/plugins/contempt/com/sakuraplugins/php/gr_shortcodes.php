<?php
require_once(CT_CLASS_PATH.'/com/sakuraplugins/php/libs/sk_utils.php');
require_once(CT_CLASS_PATH.'/com/sakuraplugins/php/frontend-engine.php');
require_once(CT_CLASS_PATH.'/com/sakuraplugins/php/plugin-options.php');
require_once(CT_CLASS_PATH.'/com/sakuraplugins/php/post-options.php');
require_once(CT_CLASS_PATH.'/com/sakuraplugins/php/libs/blog-utils.php');
/**
* shortcodes for the grid gallery
*/
class CTClass_Shortcodes
{	
	public function register(){		
		add_shortcode('contempt_all', array($this, 'contempt_all'));
		add_shortcode('contempt_category', array($this, 'contempt_category'));			
		add_shortcode('contempt_filterable', array($this, 'contempt_filterable'));	

		add_shortcode('rx_row', array($this, 'rx_row'));
		add_shortcode('one_third', array($this, 'one_third'));		
		add_shortcode('two_thirds', array($this, 'two_thirds'));
		add_shortcode('one_half', array($this, 'one_half'));	
		//button	
		add_shortcode('rx_button', array($this, 'rx_button'));
		//button	
		add_shortcode('rx_round_button', array($this, 'rx_round_button'));														
		//rx_buttonbox
		add_shortcode('rx_buttonbox', array($this, 'rx_buttonbox'));
		//rx_box_button
		add_shortcode('rx_box_button', array($this, 'rx_box_button'));

		//rx_small_title
		add_shortcode('rx_small_title', array($this, 'rx_small_title'));	

		//rx_title_center
		add_shortcode('rx_title_center', array($this, 'rx_title_center'));						
	}

	//contempt category
	public function contempt_filterable($atts, $content = null){							
		extract(shortcode_atts(array('categories' => '', 'label_all' => 'All'), $atts));
		$out = "";		
		if($categories=="")
			return "Categories attribute is empty, please add at least one category slug!!!";

		$galleriesSlugsAC = explode(",", $categories);
		$validCategories = $this->validateCategoriesTerms($galleriesSlugsAC);
		if(!$validCategories)
			return "Some categories slugs are not valid!";

		$args = array(
			'posts_per_page'=>'-1',
			'post_type'=>CT_PORTFOLIO_SLUG,			
			'tax_query' => array(
				array(
					'taxonomy' => CT_PORTFOLIO_CATEGORIES,
					'field' => 'id',
					'terms' => $validCategories['termsISs']
				)
			)
		);	
		$pluginOptions = CTClass_PluginOptions::getInstance();
		$query = new WP_Query($args);
		//if has posts
		if($query->have_posts()){
			$out .= '<div class="contempt_sk_filter_container">';
			$out .= '<ul class="ctFilterMenu">';
				$out .= '<li><a data-filter="*" data-hover="#'.$pluginOptions->getNavHoverColor().'" href="#" class="ctMenuBTN">'.$label_all.'</a></li>';
				for ($i=0; $i < sizeof($validCategories['validCategories']); $i++) { 					
					$out .= '<li><a data-filter="'.$validCategories['validCategories'][$i]->slug.'" data-hover="#'.$pluginOptions->getNavHoverColor().'" href="#" class="ctMenuBTN">'.$validCategories['validCategories'][$i]->name.'</a></li>';						
				}							
			$out .= '</ul>';			
			$out .= '<div class="contempt_sk_filter_ui">';
			$out .= '<div class="ctLoader"><img src="'.CT_TEMPPATH.'/img/preloader.gif" alt="" /></div>';
			while($query->have_posts()){
				$query->the_post();	
				$id = get_the_ID();
				$title = get_the_title($id);
				$postOptions = new CTClass_PostOptions($id);
				$subtitle = $postOptions->getSubtitle();
				$permalink = $postOptions->getPermalink();
				
				$filetrableClasses = $postOptions->getFilterClasses();								

				//three cols
				$featuredImage = $postOptions->getFeaturedImage(500);
				$out .= '<div class="ctItemThreeFiletrable'.$filetrableClasses.'">';
				$out .= $this->buildThumbUI($featuredImage, $title, $subtitle, $permalink);
				$out .= '</div>';

											
			}
			$out .= '</div>';
			$out .= '</div>';											
		}else{
			$out .= 'No posts found!';
		}
		wp_reset_query();
		//end if has posts				


		return $out;		
	}	




	//contempt category
	public function contempt_category($atts, $content = null){						
		if ( get_query_var('paged') ) {
		    $paged = get_query_var('paged');
		} elseif ( get_query_var('page') ) {
		    $paged = get_query_var('page');
		} else {
		    $paged = 1;
		}		
		extract(shortcode_atts(array('category' => '', 'items_per_page' => '12', 'cols' => 'three'), $atts));
		$out = "";		
		if($category=="")
			return "The category does not exist, please add the category slug!!!";

		$term = term_exists($category);
		if($term==0 || $term==null){			
			return "Invalid category slug!!!";
		}

		$args = array(
			'posts_per_page'=>$items_per_page,
			'post_type'=>CT_PORTFOLIO_SLUG,
			'paged'=>$paged,
			'tax_query' => array(
				array(
					'taxonomy' => CT_PORTFOLIO_CATEGORIES,
					'field' => 'id',
					'terms' => array($term)
				)
			)
		);

		$query = new WP_Query($args);
		//if has posts
		if($query->have_posts()){
			$out = '<div id="contempt_sk">';
			$out .= '<div class="ctLoader"><img src="'.CT_TEMPPATH.'/img/preloader.gif" alt="" /></div>';
			while($query->have_posts()){
				$query->the_post();	
				$id = get_the_ID();
				$title = get_the_title($id);
				$postOptions = new CTClass_PostOptions($id);
				$subtitle = $postOptions->getSubtitle();
				$permalink = $postOptions->getPermalink(true);
				
				if($cols=="three"){
					//three cols
					$featuredImage = $postOptions->getFeaturedImage(500);
					$out .= '<div class="ctItemThree">';
					$out .= $this->buildThumbUI($featuredImage, $title, $subtitle, $permalink);
					$out .= '</div>';
				}else{
					//two cols
					$featuredImage = $postOptions->getFeaturedImage(700);
					$out .= '<div class="ctItemTwo">';
					$out .= $this->buildThumbUI($featuredImage, $title, $subtitle, $permalink);
					$out .= '</div>';
				}				
				//echo $featuredImage;								
			}
			$out .= '</div>';
			$pagination = new CTClass_BlogPagination();
			$pagination_html = $pagination->kriesi_pagination($query->max_num_pages, 2);
			$out .= '<div class="paginationUI">';	
			$out .= $pagination_html;	
			$out .= '<div class="clear-fx"></div>';
			$out .= '</div>';											
		}else{
			$out .= 'No posts found!';
		}
		wp_reset_query();
		//end if has posts		

		return $out;
	}		

	//contempt all
	public function contempt_all($atts, $content = null){	
		$out = "";
		if ( get_query_var('paged') ) {
		    $paged = get_query_var('paged');
		} elseif ( get_query_var('page') ) {
		    $paged = get_query_var('page');
		} else {
		    $paged = 1;
		}				
		extract(shortcode_atts(array('items_per_page' => '12', 'cols' => 'three'), $atts));
		$rx_query = array('post_type' => CT_PORTFOLIO_SLUG, 'paged'=>$paged, 'posts_per_page' =>$items_per_page);													
		

		$query = new WP_Query($rx_query);
		//if has posts
		if($query->have_posts()){
			$out = '<div id="contempt_sk">';
			$out .= '<div class="ctLoader"><img src="'.CT_TEMPPATH.'/img/preloader.gif" alt="" /></div>';
			while($query->have_posts()){
				$query->the_post();	
				$id = get_the_ID();
				$title = get_the_title($id);
				$postOptions = new CTClass_PostOptions($id);
				$subtitle = $postOptions->getSubtitle();
				$permalink = $postOptions->getPermalink();
				
				if($cols=="three"){
					//three cols
					$featuredImage = $postOptions->getFeaturedImage(500);
					$out .= '<div class="ctItemThree">';
					$out .= $this->buildThumbUI($featuredImage, $title, $subtitle, $permalink);
					$out .= '</div>';
				}else{
					//two cols
					$featuredImage = $postOptions->getFeaturedImage(700);
					$out .= '<div class="ctItemTwo">';
					$out .= $this->buildThumbUI($featuredImage, $title, $subtitle, $permalink);
					$out .= '</div>';
				}				
				//echo $featuredImage;								
			}
			$out .= '</div>';
			$pagination = new CTClass_BlogPagination();
			$pagination_html = $pagination->kriesi_pagination($query->max_num_pages, 2);
			$out .= '<div class="paginationUI">';	
			$out .= $pagination_html;	
			$out .= '<div class="clear-fx"></div>';
			$out .= '</div>';											
		}else{
			$out .= 'No posts found!';
		}
		wp_reset_query();
		//end if has posts
		
		return $out;
	}

	public function buildThumbUI($thumbURL, $title, $subtitle, $permalink){
		$pluginOptions = CTClass_PluginOptions::getInstance();		
		$out = '<div class="contemptThumbUI" data-thumburl="'.$thumbURL.'" data-texthover="#'.$pluginOptions->getThumbTextHoverCol().'">';
			$out .= '<a class="ct_thumbIMG" href="'.$permalink.'"></a>';										
			$out .= '<div class="ct_hover_overlay"><div class="ct-plus"><img src="'.CT_TEMPPATH.'/img/plus-thin.png" alt="" /></div></div>';		
			$out .= '<div class="ct_thumbTitleUI">';
				$out .= '<p class="ct_thumbTitle">'.$title.'</p>';
				$out .= '<p class="ct_thumbSubtitle">'.$subtitle.'</p>';				
			$out .= '</div>';
		$out .= '</div>';
		return $out;
	}


	//extract categories data
	private function validateCategoriesTerms($galleriesSlugsAC){
		$categoriesIDs = array();
		$validCategories = array();
		for ($i=0; $i < sizeof($galleriesSlugsAC); $i++) { 
			$term = term_exists($galleriesSlugsAC[$i]);
			if($term!==0 && $term!==null){
				array_push($categoriesIDs, $term);							
				array_push($validCategories, get_term($term, CT_PORTFOLIO_CATEGORIES));
			}else{
				return false;
			}
		}		
		if(sizeof($categoriesIDs)==0)
			return false;
		return array('termsISs'=>$categoriesIDs, 'validCategories'=>$validCategories);
	}


/* sk row
================================================== */	
function rx_row($atts, $content = null){
	return '<div class="row">'.do_shortcode($content).'</div>';
}
/* one third
================================================== */	
function one_third($atts, $content = null){
	return '<div class="col-md-4">'.do_shortcode($content).'</div>';
}

/* two third
================================================== */	
function two_thirds($atts, $content = null){
	return '<div class="col-md-8">'.do_shortcode($content).'</div>';
}

/* one half
================================================== */	
function one_half($atts, $content = null){
	return '<div class="col-md-6">'.do_shortcode($content).'</div>';
}

/* rx_button
================================================== */	
function rx_button($atts, $content = null){
	extract(shortcode_atts(array("color" => "FFFFFF", "background_color"=>'49e2d6', 'background_hover'=>'fc7171', 'href'=>'#', 'target'=>'_self', 'align'=>'left'), $atts));									

	if($align=="left"){
		return '
			<a class="skPluginBTN skPluginBTNMetro" data-secondcolor="#'.$background_hover.'" target="'.$target.'" href="'.$href.'" style="float: left; color:#'.$color.'; background-color:#'.$background_color.'">'.$content.'</a>
			<div class="clearsk"></div>
		';
	}
	if($align=="right"){
		return '
			<a class="skPluginBTN skPluginBTNMetro" data-secondcolor="#'.$background_hover.'" target="'.$target.'" href="'.$href.'" style="float: right; color:#'.$color.'; background-color:#'.$background_color.'">'.$content.'</a>
			<div class="clearsk"></div>
		';
	}	
	if($align=="center"){
		return '
			<div align="center"><a class="skPluginBTN skPluginBTNMetro" data-secondcolor="#'.$background_hover.'" target="'.$target.'" href="'.$href.'" style="position: relative; margin-left:50%;margin-right: 50%; color:#'.$color.'; background-color:#'.$background_color.'">'.$content.'</a></div>				
		';
	}			
}

/* rx_round_button
================================================== */	
function rx_round_button($atts, $content = null){
	extract(shortcode_atts(array("color" => "FFFFFF", "background_color"=>'49e2d6', 'background_hover'=>'fc7171', 'href'=>'#', 'target'=>'_self', 'align'=>'left'), $atts));									

	if($align=="left"){
		return '
			<a class="skRoundBTN skPluginBTNMetro" data-secondcolor="#'.$background_hover.'" target="'.$target.'" href="'.$href.'" style="float: left; color:#'.$color.'; background-color:#'.$background_color.'">'.$content.'</a>
			<div class="clearsk"></div>
		';
	}
	if($align=="right"){
		return '
			<a class="skRoundBTN skPluginBTNMetro" data-secondcolor="#'.$background_hover.'" target="'.$target.'" href="'.$href.'" style="float: right; color:#'.$color.'; background-color:#'.$background_color.'">'.$content.'</a>
			<div class="clearsk"></div>
		';
	}	
	if($align=="center"){
		return '
			<a class="skRoundBTN skPluginBTNMetro skRoundBTNCenter" data-secondcolor="#'.$background_hover.'" target="'.$target.'" href="'.$href.'" style="position: relative; margin-left:50%;margin-right: 50%; color:#'.$color.'; background-color:#'.$background_color.'">'.$content.'</a>				
		';
	}			
}

/* rx_buttonbox
================================================== */	
function rx_buttonbox($atts, $content = null){		
	$return_val = '<ul class="rx_buttonbox">'.do_shortcode($content).'</ul>';
	return $return_val;		
}
function rx_box_button($atts, $content = null){
	extract(shortcode_atts(array("color" => "FFFFFF", "background_color"=>'49e2d6', 'background_hover'=>'fc7171', 'href'=>'#', 'target'=>'_self'), $atts));				
	$return_val = '<li class="rx_box_button"><a class="skRoundBTN skPluginBTNMetro" data-secondcolor="#'.$background_hover.'" target="'.$target.'" href="'.$href.'" style="float: left; color:#'.$color.'; background-color:#'.$background_color.'">'.$content.'</a></li>';
	return $return_val;		
}

/* rx_small_title
================================================== */	
function rx_small_title($atts, $content = null){		
	extract(shortcode_atts(array("color" => "000000"), $atts));				
	$return_val = '<h3 style="color: #'.$color.';" class="rx_small_title">'.$content.'</h3>';
	return $return_val;		
}

/* rx_title_center
================================================== */	
function rx_title_center($atts, $content = null){		
	extract(shortcode_atts(array("color" => "000000"), $atts));				
	$return_val = '<div class="rx_title_center_ui"><h2 style="color: #'.$color.';" class="rx_title_center">'.$content.'</h2></div>';
	return $return_val;		
}




}


?>