var sk_contempt = new ContemptSK();

jQuery(document).ready(function(){        
    sk_contempt.init();                               	    	
});  

function ContemptSK(){

	this.init = function(){       
        
        if(jQuery('#contempt_sk').length!=0){
            if(jQuery('.ctItemThree').length!=0)            
                initContemptThreeCols(jQuery('#contempt_sk'));                            
            if(jQuery('.ctItemTwo').length!=0)
                initContemptTwoCols(jQuery('#contempt_sk'));                   
        }  
                                
        if(jQuery('.contempt_sk_filter_container').length!=0){
            jQuery('.contempt_sk_filter_container').each(function(i){
                initfilterableThreeCols(jQuery(this));
            });
        }      
    }

    //filterable three cols
    function initfilterableThreeCols(mainContainer){           
        var containerUI =  mainContainer.find('.contempt_sk_filter_ui');            
        var thumbsAC = [];
        var currentFilter = "*";
                
        containerUI.find('.ctItemThreeFiletrable').each(function(indx){            
            jQuery(this).css('opacity', 0);
            thumbsAC.push({thumbUI: jQuery(this), loaded: false, indx: indx});
            var imgUI = jQuery('<img class="ct-thumb-img" src="'+jQuery(this).find('.contemptThumbUI').attr('data-thumburl')+'" alt="" />');
            imgUI.bind('load', function(){                
                thumbsAC[indx].loaded = true;
                if(checkAllLoaded()){
                    containerUI.find('.ctLoader').remove();                                        
                    initMenu();
                    initIsotope(currentFilter, 0);
                    addWindowResizeEvent();
                    containerUI.find('.contemptThumbUI').css('opacity', 1);
                    TweenMax.to(containerUI.find('.ctItemThreeFiletrable'), .2, {css:{opacity:1}, ease:Power4.EaseIn});
                }
            });
            imgUI.appendTo(jQuery(this).find('.ct_thumbIMG')); 
            jQuery(this).find('.ct_hover_overlay').css('opacity', 0);
            jQuery(this).find('.ct_hover_overlay').css('display', 'block');
            var ititialTitleCol = jQuery(this).find('.ct_thumbTitle').css('color');
            var ititialSubtitleCol = jQuery(this).find('.ct_thumbSubtitle').css('color');
            jQuery(this).hover(function(e){
                jQuery(this).find('.ct-plus').css('top', jQuery(this).find('.ct-thumb-img').height()/2-70/2+'px');
                TweenMax.to(jQuery(this).find('.ct_hover_overlay'), .2, {css:{opacity:1}, ease:Power4.EaseIn});                
                TweenMax.to(jQuery(this).find('.ct_thumbTitle'), .1, {css:{color:jQuery(this).find('.contemptThumbUI').attr('data-texthover')}, ease:Power4.EaseIn});                
                TweenMax.to(jQuery(this).find('.ct_thumbSubtitle'), .1, {css:{color:jQuery(this).find('.contemptThumbUI').attr('data-texthover')}, ease:Power4.EaseIn});                
            }, function(e){
                TweenMax.to(jQuery(this).find('.ct_hover_overlay'), .2, {css:{opacity:0}, ease:Power4.EaseIn});
                TweenMax.to(jQuery(this).find('.ct_thumbTitle'), .1, {css:{color:ititialTitleCol}, ease:Power4.EaseIn});                
                TweenMax.to(jQuery(this).find('.ct_thumbSubtitle'), .1, {css:{color:ititialSubtitleCol}, ease:Power4.EaseIn});                
            }); 
            jQuery(this).click(function(e){
                e.preventDefault();
                window.location = jQuery(this).find('.ct_thumbIMG').attr('href');
            });          
        });

        function checkAllLoaded(){
            for (var i = 0; i < thumbsAC.length; i++) {
                if(!thumbsAC[i].loaded){
                    return false;
                }
            };
            return true;
        }

        var menu = [];
        var menuIitialCol;
        function initMenu(){
            mainContainer.find('.ctMenuBTN').each(function(indx){
                menuIitialCol = jQuery(this).css('color');
                menu.push(jQuery(this))
                jQuery(this).click(function(e){
                    e.preventDefault();
                    if(currentMenuIndx==indx)
                        return;
                    currentFilter = (jQuery(this).attr('data-filter')=="*")?"*":'.'+jQuery(this).attr('data-filter');
                    initIsotope(currentFilter, indx);
                });

                jQuery(this).hover(function(e){
                    if(currentMenuIndx==indx)
                        return;             
                    TweenMax.to(jQuery(this), .1, {css:{color: jQuery(this).attr('data-hover')}, ease:Power4.EaseIn});       
                    TweenMax.to(jQuery(this), .1, {css:{borderColor: jQuery(this).attr('data-hover')}, ease:Power4.EaseIn});       
                }, function(e){
                    if(currentMenuIndx==indx)
                        return;
                    TweenMax.to(jQuery(this), .1, {css:{color: menuIitialCol}, ease:Power4.EaseIn});
                    TweenMax.to(jQuery(this), .1, {css:{borderColor: menuIitialCol}, ease:Power4.EaseIn});              
                });

            });
        }
        
        var currentMenuIndx = 0;
        function selectMenu(indx){
            currentMenuIndx = indx;
            for (var i = 0; i < menu.length; i++) {
                menu[i].removeClass('ct-selected');
                menu[i].css('color', menuIitialCol);
                menu[i].css('border-color', menuIitialCol);
                if(i==indx){
                    menu[i].addClass('ct-selected');
                    menu[i].css('color', menu[i].attr('data-hover'));
                    menu[i].css('border-color', menu[i].attr('data-hover'));                    
                    //menuChangeColor(menu[i]);
                }
            };
        }

        var container = containerUI;
        function initIsotope(filter, indx){   
            selectMenu(indx);
            container.isotope({                                          
              filter: filter,
              itemSelector: '.ctItemThreeFiletrable',
              layoutMode: 'fitRows'              
            });
        }
        function addWindowResizeEvent(){
          jQuery(window).resize(function(){
              initIsotope(currentFilter);
          }); 
        }        
    }

    //contempt three cols
    function initContemptThreeCols(containerUI){
        var thumbsAC = [];
        jQuery('.ctItemThree').each(function(indx){
            jQuery(this).css('opacity', 0);
            thumbsAC.push({thumbUI: jQuery(this), loaded: false, indx: indx});
            var imgUI = jQuery('<img class="ct-thumb-img" src="'+jQuery(this).find('.contemptThumbUI').attr('data-thumburl')+'" alt="" />');
            imgUI.bind('load', function(){                
                thumbsAC[indx].loaded = true;
                if(checkAllLoaded()){
                    jQuery('.ctLoader').remove();
                    jQuery('.paginationUI').css('display', 'block');
                    jQuery('.ctItemThree').css('display', 'block');
                    initIsotope();
                    addWindowResizeEvent();
                    containerUI.find('.contemptThumbUI').css('opacity', 1);
                    TweenMax.to(jQuery('.ctItemThree'), .2, {css:{opacity:1}, ease:Power4.EaseIn});
                }
            });
            imgUI.appendTo(jQuery(this).find('.ct_thumbIMG')); 
            jQuery(this).find('.ct_hover_overlay').css('opacity', 0);
            jQuery(this).find('.ct_hover_overlay').css('display', 'block');
            var ititialTitleCol = jQuery(this).find('.ct_thumbTitle').css('color');
            var ititialSubtitleCol = jQuery(this).find('.ct_thumbSubtitle').css('color');
            jQuery(this).hover(function(e){
                jQuery(this).find('.ct-plus').css('top', jQuery(this).find('.ct-thumb-img').height()/2-70/2+'px');
                TweenMax.to(jQuery(this).find('.ct_hover_overlay'), .2, {css:{opacity:1}, ease:Power4.EaseIn});                
                TweenMax.to(jQuery(this).find('.ct_thumbTitle'), .1, {css:{color:jQuery(this).find('.contemptThumbUI').attr('data-texthover')}, ease:Power4.EaseIn});                
                TweenMax.to(jQuery(this).find('.ct_thumbSubtitle'), .1, {css:{color:jQuery(this).find('.contemptThumbUI').attr('data-texthover')}, ease:Power4.EaseIn});                
            }, function(e){
                TweenMax.to(jQuery(this).find('.ct_hover_overlay'), .2, {css:{opacity:0}, ease:Power4.EaseIn});
                TweenMax.to(jQuery(this).find('.ct_thumbTitle'), .1, {css:{color:ititialTitleCol}, ease:Power4.EaseIn});                
                TweenMax.to(jQuery(this).find('.ct_thumbSubtitle'), .1, {css:{color:ititialSubtitleCol}, ease:Power4.EaseIn});                
            }); 
            jQuery(this).click(function(e){
                e.preventDefault();
                window.location = jQuery(this).find('.ct_thumbIMG').attr('href');
            });          
        });

        function checkAllLoaded(){
            for (var i = 0; i < thumbsAC.length; i++) {
                if(!thumbsAC[i].loaded){
                    return false;
                }
            };
            return true;
        }

        var container = containerUI;

        function initIsotope(){
            container.isotope({
              // options
              itemSelector: '.ctItemThree',
              layoutMode: 'fitRows'
            });
        }
        function addWindowResizeEvent(){
          jQuery(window).resize(function(){
              initIsotope();
          }); 
        }                                 
    }



    function initContemptTwoCols(containerUI){
        var thumbsAC = [];
        jQuery('.ctItemTwo').each(function(indx){
            jQuery(this).css('opacity', 0);
            thumbsAC.push({thumbUI: jQuery(this), loaded: false, indx: indx});
            var imgUI = jQuery('<img class="ct-thumb-img" src="'+jQuery(this).find('.contemptThumbUI').attr('data-thumburl')+'" alt="" />');
            imgUI.bind('load', function(){                
                thumbsAC[indx].loaded = true;
                if(checkAllLoaded()){                    
                    jQuery('.ctLoader').remove();
                    jQuery('.paginationUI').css('display', 'block');
                    jQuery('.ctItemTwo').css('display', 'block');
                    initIsotope();
                    addWindowResizeEvent();
                    containerUI.find('.contemptThumbUI').css('opacity', 1);
                    TweenMax.to(jQuery('.ctItemTwo'), .2, {css:{opacity:1}, ease:Power4.EaseIn});
                }
            });
            imgUI.appendTo(jQuery(this).find('.ct_thumbIMG')); 
            jQuery(this).find('.ct_hover_overlay').css('opacity', 0);
            jQuery(this).find('.ct_hover_overlay').css('display', 'block');
            var ititialTitleCol = jQuery(this).find('.ct_thumbTitle').css('color');
            var ititialSubtitleCol = jQuery(this).find('.ct_thumbSubtitle').css('color');
            jQuery(this).hover(function(e){
                jQuery(this).find('.ct-plus').css('top', jQuery(this).find('.ct-thumb-img').height()/2-70/2+'px');
                TweenMax.to(jQuery(this).find('.ct_hover_overlay'), .2, {css:{opacity:1}, ease:Power4.EaseIn});                
                TweenMax.to(jQuery(this).find('.ct_thumbTitle'), .1, {css:{color:jQuery(this).find('.contemptThumbUI').attr('data-texthover')}, ease:Power4.EaseIn});                
                TweenMax.to(jQuery(this).find('.ct_thumbSubtitle'), .1, {css:{color:jQuery(this).find('.contemptThumbUI').attr('data-texthover')}, ease:Power4.EaseIn});                
            }, function(e){
                TweenMax.to(jQuery(this).find('.ct_hover_overlay'), .2, {css:{opacity:0}, ease:Power4.EaseIn});
                TweenMax.to(jQuery(this).find('.ct_thumbTitle'), .1, {css:{color:ititialTitleCol}, ease:Power4.EaseIn});                
                TweenMax.to(jQuery(this).find('.ct_thumbSubtitle'), .1, {css:{color:ititialSubtitleCol}, ease:Power4.EaseIn});                
            }); 
            jQuery(this).click(function(e){
                e.preventDefault();
                window.location = jQuery(this).find('.ct_thumbIMG').attr('href');
            });          
        });

        function checkAllLoaded(){
            for (var i = 0; i < thumbsAC.length; i++) {
                if(!thumbsAC[i].loaded){
                    return false;
                }
            };
            return true;
        }

        var container = containerUI;

        function initIsotope(){
            container.isotope({
              // options
              itemSelector: '.ctItemTwo',
              layoutMode: 'fitRows'
            });
        }
        function addWindowResizeEvent(){
          jQuery(window).resize(function(){
              initIsotope();
          }); 
        }                                 
    }    

 




    function uniqueid(){
        // always start with a letter (for DOM friendlyness)
        var idstr=String.fromCharCode(Math.floor((Math.random()*25)+65));
        do {                
            // between numbers and characters (48 is 0 and 90 is Z (42-48 = 90)
            var ascicode=Math.floor((Math.random()*42)+48);
            if (ascicode<58 || ascicode>64){
                // exclude all chars between : (58) and @ (64)
                idstr+=String.fromCharCode(ascicode);    
            }                
        } while (idstr.length<32);

        return (idstr);
    }

}//end main class

