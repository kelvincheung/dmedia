jQuery(document).ready(function(){        
    var ctSingle = new ContemptSingle();
    ctSingle.init();                              	    	
}); 

function ContemptSingle(){
	this.init = function(){
		jQuery('.contemptThumbUISingle').each(function(indx){
            var ititialTitleCol = jQuery(this).find('.ct_thumbTitle').css('color');
            var ititialSubtitleCol = jQuery(this).find('.ct_thumbSubtitle').css('color');  
            jQuery(this).find('.ct_hover_overlay').css('opacity', 0);
            jQuery(this).find('.ct_hover_overlay').css('display', 'block');                      			
            jQuery(this).hover(function(e){            	                	    	
                jQuery(this).find('.ct-plus').css('top', jQuery(this).find('.ct-thumb-img').height()/2-70/2+'px');
                TweenMax.to(jQuery(this).find('.ct_hover_overlay'), .2, {css:{opacity:1}, ease:Power4.EaseIn});                
                TweenMax.to(jQuery(this).find('.ct_thumbTitle'), .1, {css:{color:jQuery(this).attr('data-texthover')}, ease:Power4.EaseIn});                
                TweenMax.to(jQuery(this).find('.ct_thumbSubtitle'), .1, {css:{color:jQuery(this).attr('data-texthover')}, ease:Power4.EaseIn});                
            }, function(e){
                TweenMax.to(jQuery(this).find('.ct_hover_overlay'), .2, {css:{opacity:0}, ease:Power4.EaseIn});
                TweenMax.to(jQuery(this).find('.ct_thumbTitle'), .1, {css:{color:ititialTitleCol}, ease:Power4.EaseIn});                
                TweenMax.to(jQuery(this).find('.ct_thumbSubtitle'), .1, {css:{color:ititialSubtitleCol}, ease:Power4.EaseIn});                
            }); 
            jQuery(this).click(function(e){
                e.preventDefault();
                window.location = jQuery(this).find('.ct_thumbIMG').attr('href');
            });            			
		});
        
        //widget - recent 
        jQuery('.ctpRecentProjectUI').each(function(indx){            
            jQuery(this).click(function(e){
                e.preventDefault();
                window.location = jQuery(this).attr('data-href');
            });
        });

        //buttons shortcodes
            jQuery('.skPluginBTNMetro').each(function(indx){            
                var originalCol = jQuery(this).css('background-color');
                jQuery(this).hover(function(e){                                
                    TweenMax.to(jQuery(this), .2, {css:{backgroundColor: jQuery(this).attr('data-secondcolor')}, ease:Power4.EaseIn});
                }, function(e){
                    TweenMax.to(jQuery(this), .2, {css:{backgroundColor: originalCol}, ease:Power4.EaseIn});
                });
            }); 
                    
	}
}
