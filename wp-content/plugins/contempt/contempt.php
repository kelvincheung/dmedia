<?php
/*
Plugin Name: Contempt - WordPress Portfolio
Plugin URI: http://sakuraplugins.com/
Description: Contempt, a WordPress Plugin that shows your portfolio in an interactive way.
Author: SakuraPlugins
Version: 1.4.0
Author URI: http://sakuraplugins.com/
*/
define('CT_TEMPPATH', plugins_url('', __FILE__));
define('CT_JS_ADMIN', CT_TEMPPATH.'/com/sakuraplugins/js');
define('CT_JS', CT_TEMPPATH.'/js');
define('CT_CLASS_PATH', plugin_dir_path(__FILE__));
define('CT_PLUGIN_TEXTDOMAIN', 'ct_portfolio');
define('CT_PORTFOLIO_SLUG', 'ct_grid');
define('CT_POST_CUSTOM_META', 'ct_portfolio_post_options');
define('CT_PORTFOLIO_OPTION_GROUP', 'ct_portfolio_option_group');
define('CT_PORTFOLIO_REWRITE', 'contempt');
define('CT_FILE', __FILE__);
define('CT_PORTFOLIO_CATEGORIES', 'pct_portfolio_categories');


require_once(CT_CLASS_PATH.'/com/sakuraplugins/php/plugin_core.php');
$gr_plugin_core = new CTClass_PluginBase();
$gr_plugin_core->start(array('PLUGIN_FILE'=>__FILE__));

?>