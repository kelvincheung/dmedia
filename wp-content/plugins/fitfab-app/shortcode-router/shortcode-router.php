<?php

/**
 * Plugin Name:       ShortCode Router
 * Plugin URI:        http://theemon.com
 * Description:       This plugin is resposible for routing shortcode section on theemon
 * Version:           1.0.0
 * Author:            Theemon WordPress Team
 * Author URI:        http://theemon.com
 * Text Domain:       shortcode-router
 * Domain Path:       /languages
 * License:           
 * License URI:       
 */

define("ShortcodeRouter_PATH", plugin_dir_path( __FILE__ ));
define("ShortcodeRouter_URL", plugin_dir_url( __FILE__ ));

class Shortcode_Router{
    public function __construct() {
       
        $this->define_public_hooks();
    }
    
    public function  define_public_hooks(){
        $this->add_shortcode('ul');
        $this->add_shortcode('li');
        $this->add_shortcode('button');
    }
    public function add_shortcode($shortcode){
        add_shortcode($shortcode,array(&$this, $shortcode));
    }
   
    public function __call($name, $args) {
        $atts=$args[0];
        $content=$args[1];
        $file_name=ShortcodeRouter_PATH."shortcode/".$name.'.php';
        
        if(file_exists($file_name)):
            ob_start();
            include($file_name);
            return ob_get_clean();
        endif;
        return false;
    }
    
}

new Shortcode_Router();