<ul class="<?php echo esc_attr($atts['class_name']); ?>">
    <?php
    echo do_shortcode($content);
    ?>
</ul>
