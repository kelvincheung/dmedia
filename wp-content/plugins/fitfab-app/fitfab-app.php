<?php
/*
Plugin Name: FitFab app
Plugin URI: http://theemon.com
Description: This plugin create for fitfab plugin territory functionality.
Version: 1.0.0
Author: Theemon WP Team
Author URI: http://theemon.com/
*/

define('PLUGIN_PATH', plugin_dir_url( __FILE__ ) );
define('PLUGIN_PATH_DIR', plugin_dir_path( __FILE__ ) );

require_once 'cpt/cpt.php';
include_once 'shortcode-router/shortcode-router.php';
require_once ('meta-box/meta-box.php');
add_action("init", "fitfab_theme_metaboxes_config");
function fitfab_theme_metaboxes_config(){
    include_once ('meta-box/meta-boxes.php');
}
