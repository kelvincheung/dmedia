<?php

/**
 * Fitfab - Registering meta boxes
 *
 * @package fitfab.vc_templates
 * @since fitfab 1.0.0
 */

 if(!function_exists('fitfab_package_dropdown')) {
  return false;
 }

add_filter('rwmb_meta_boxes', 'bpxl_register_meta_boxes');

/**
 * Register meta boxes
 *
 * @return void
 */
function bpxl_register_meta_boxes($meta_boxes) {
    // enque metabox.js 
    wp_register_script('metaboxes_options', get_template_directory_uri() . '/assets/js/metabox.js', array('jquery'));
    wp_enqueue_script('metaboxes_options');
    /**
     * Prefix of meta keys (optional)
     * Use underscore (_) at the beginning to make keys hidden
     * Alt.: You also can make prefix empty to disable it
     */
    // Better has an underscore as last sign
    $prefix = 'fitfab_';

    $meta_boxes[] = array(
        'id' => 'gallery_format',
        'title' => esc_html__('Gallery', 'fitfab'),
        'pages' => array('post'),
        'context' => 'normal',
        'priority' => 'core',
        'autosave' => true,
        // List of meta fields
        'fields' => array(
            array(
                'name' => '',
                'desc' => esc_html__('Upload gallery images', 'fitfab'),
                'id' => "{$prefix}gallery_image",
                'type' => 'file_input',
                'clone' => true,
                'max_clone' => '4',
            ),
        ),
    );
    // Video Format            
    $meta_boxes[] = array(
        'id' => 'video_format',
        'title' => esc_html__('Video', 'fitfab'),
        'pages' => array('post'),
        'context' => 'normal',
        'priority' => 'core',
        'autosave' => true,
        // List of meta fields
        'fields' => array(
            array(
                'name' => esc_html__('', 'fitfab'),
                'desc' => esc_html__('Embed video from services like Youtube, Vimeo, or Hulu. ', 'fitfab'),
                'id' => "{$prefix}video",
                'type' => 'textarea',
                'cols' => 20,
                'rows' => 8,
            ),
        ),
    );
    // Image Format            
    $meta_boxes[] = array(
        'id' => 'image_format',
        'title' => esc_html__('Image', 'fitfab'),
        'pages' => array('post'),
        'context' => 'normal',
        'priority' => 'core',
        'autosave' => true,
        // List of meta fields
        'fields' => array(
            array(
                'name' => '',
                'desc' => esc_html__('Upload Image. ', 'fitfab'),
                'id' => "{$prefix}image",
                'type' => 'image_advanced',
            ),
        ),
    );
    // Quote Format            
    $meta_boxes[] = array(
        'id' => 'quote_format',
        'title' => esc_html__('Quote', 'fitfab'),
        'pages' => array('post'),
        'context' => 'normal',
        'priority' => 'core',
        'autosave' => true,
        // List of meta fields
        'fields' => array(
            array(
                'name' => esc_html__('Author Name', 'fitfab'),
                'desc' => '',
                'id' => "{$prefix}quote_author",
                'type' => 'text',
            ),
        ),
    );
    // Aside Format            
    $meta_boxes[] = array(
        'id' => 'aside_format',
        'title' => esc_html__('Aside', 'fitfab'),
        'pages' => array('post'),
        'context' => 'normal',
        'priority' => 'core',
        'autosave' => true,
        // List of meta fields
        'fields' => array(
            array(
                'name' => esc_html__('', 'fitfab'),
                'desc' => '',
                'id' => "{$prefix}aside",
                'type' => 'textarea',
                'cols' => 20,
                'rows' => 8,
            ),
        ),
    );
    // Link Format            
    $meta_boxes[] = array(
        'id' => 'link_format',
        'title' => esc_html__('Link', 'fitfab'),
        'pages' => array('post'),
        'context' => 'normal',
        'priority' => 'core',
        'autosave' => true,
        // List of meta fields
        'fields' => array(
            array(
                'name' => '',
                'desc' => esc_html__('URL description', 'fitfab'),
                'id' => "{$prefix}link",
                'type' => 'url',
                'std' => 'http://google.com',
                'size' => 90
            ),
        ),
    );
    // Class Schedule Box
    $meta_boxes[] = array(
        'id' => 'class_schedule_box',
        'title' => esc_html__('Class Schedule', 'fitfab'),
        'pages' => array('class'),
        'context' => 'normal',
        'priority' => 'core',
        'autosave' => true,
        // List of meta fields
        'fields' => array(
            // SELECT BOX
            array(
                'name' => esc_html__('Class Start Time', 'fitfab'),
                'desc' => esc_html__('Select start time and date for class', 'fitfab'),
                'id' => "{$prefix}class_start_time",
                'type' => 'datetime',
            ),
            array(
                'name' => esc_html__('Class End Time', 'fitfab'),
                'desc' => esc_html__('Select end time and date for class', 'fitfab'),
                'id' => "{$prefix}class_end_time",
                'type' => 'datetime',
                'std' => '',
            ),
            array(
                'name' => esc_html__('Class Day', 'fitfab'),
                'desc' => esc_html__('Select Day for class', 'fitfab'),
                'id' => "{$prefix}class_day",
                'type' => 'checkbox_list',
                'options' => array(
                    'monday' => esc_html__('Monday', 'fitfab'),
                    'tuesday' => esc_html__('Tuesday', 'fitfab'),
                    'wednesday' => esc_html__('Wednesday', 'fitfab'),
                    'thursday' => esc_html__('Thursday', 'fitfab'),
                    'friday' => esc_html__('Friday', 'fitfab'),
                    'saturday' => esc_html__('Saturday', 'fitfab'),
                    'sunday' => esc_html__('Sunday', 'fitfab'),
                ),
            ),
        ),
    );
    $meta_boxes[] = array(
        'id' => 'join_button_text',
        'title' => esc_html__('Join Button Text', 'fitfab'),
        'pages' => array('class'),
        'context' => 'normal',
        'priority' => 'core',
        'autosave' => true,
        // List of meta fields
        'fields' => array(
            // SELECT BOX
            array(
                'name' => '',
                'desc' => esc_html__('Enter Join Button Text', 'fitfab'),
                'id' => "{$prefix}join_button_text",
                'type' => 'text',
            ),
        ),
    );
    $meta_boxes[] = array(
        'id' => 'join_button_link',
        'title' => esc_html__('Join Button Link', 'fitfab'),
        'pages' => array('class'),
        'context' => 'normal',
        'priority' => 'core',
        'autosave' => true,
        // List of meta fields
        'fields' => array(
            // SELECT BOX
            array(
                'name' => '',
                'desc' => esc_html__('Enter Join Button Link', 'fitfab'),
                'id' => "{$prefix}join_button_link",
                'type' => 'text',
            ),
        ),
    );

    // Package of classes
    $meta_boxes[] = array(
        'id' => 'page_box',
        'title' => esc_html__('Class Package', 'fitfab'),
        'pages' => array('class'),
        'context' => 'normal',
        'priority' => 'core',
        'autosave' => true,
        // List of meta fields
        'fields' => array(
            array(
                'name' => esc_html__('Package', 'fitfab'),
                'id' => "{$prefix}package",
                'type' => 'select_advanced',
                // Array of 'value' => 'Label' pairs for select box
                'options' => fitfab_package_dropdown(),
                // Select multiple values, optional. Default is false.
                'multiple' => false,
                // 'std'         => 'value2', // Default value, optional
                'placeholder' => esc_html__('Select a Package', 'fitfab'),
            ),
        ),
    );
		
    $meta_boxes[] = array(
        'id' => 'sub_title',
        'title' => __('Sub Title', 'fitfab'),
        'pages' => array('package'),
        'context' => 'normal',
        'priority' => 'core',
        'autosave' => true,
        // List of meta fields
        'fields' => array(
	    array(
                'name' => __('', 'fitfab'),
                'desc' => __('Enter Sub Title', 'fitfab'),
                'id' => "{$prefix}sub_title",
                'type' => 'text',
            ),
        ),
    );
		
    // Ftifab Price
    $meta_boxes[] = array(
        'id' => 'price_per_month',
        'title' => esc_html__('Price Per Month', 'fitfab'),
        'pages' => array('package'),
        'context' => 'normal',
        'priority' => 'core',
        'autosave' => true,
        // List of meta fields
        'fields' => array(
            // SELECT BOX
            array(
                'name' => '',
                'desc' => esc_html__('Enter Price Here', 'fitfab'),
                'id' => "{$prefix}price_per_month",
                'type' => 'text',
            ),
        ),
    );

    $meta_boxes[] = array(
        'id' => 'join_button_text',
        'title' => esc_html__('Join Button Text', 'fitfab'),
        'pages' => array('package'),
        'context' => 'normal',
        'priority' => 'core',
        'autosave' => true,
        // List of meta fields
        'fields' => array(
            array(
                'name' => '',
                'desc' => esc_html__('Enter Join Button Text', 'fitfab'),
                'id' => "{$prefix}join_button_text",
                'type' => 'text',
            ),
        ),
    );

    $meta_boxes[] = array(
        'id' => 'join_button_link',
        'title' => esc_html__('Join Button Link', 'fitfab'),
        'pages' => array('package'),
        'context' => 'normal',
        'priority' => 'core',
        'autosave' => true,
        // List of meta fields
        'fields' => array(
            // SELECT BOX
            array(
                'name' => '',
                'desc' => esc_html__('Enter Join Button Link', 'fitfab'),
                'id' => "{$prefix}join_button_link",
                'type' => 'text',
            ),
        ),
    );

    // Standard meta box
    $meta_boxes[] = array(
        'id' => 'class_icon_box',
        'title' => esc_html__('Class Icon', 'fitfab'),
        'pages' => array('class'),
        'context' => 'normal',
        'priority' => 'core',
        'autosave' => true,
        // List of meta fields
        'fields' => array(
            // SELECT BOX
            array(
                'name' => esc_html__('Class Icon', 'fitfab'),
                'desc' => esc_html__('Select icon for this post', 'fitfab'),
                'id' => "{$prefix}class_icons",
                'type' => 'image_advanced',
            ),
        ),
    );
    /**
     *  Fitfab - class trainer
     */
    $meta_boxes[] = array(
        'id' => 'class_trainer',
        'title' => esc_html__('Trainer', 'fitfab'),
        'pages' => array('class','package'),
        'context' => 'normal',
        'priority' => 'core',
        'autosave' => true,
        // List of meta fields
        'fields' => array(
            array(
                'name' => esc_html__('Trainer', 'fitfab'),
                'id' => "{$prefix}trainer",
                'type' => 'checkbox_list',
                'options' => fitfab_trainer_dropdown(),
            ),
        ),
    );

    // Page meta box
    $meta_boxes[] = array(
        'id' => 'page_box',
        'title' => esc_html__('Schedule Options', 'fitfab'),
        'pages' => array('schedule_post'),
        'context' => 'normal',
        'priority' => 'core',
        'autosave' => true,
        // List of meta fields
        'fields' => array(
            array(
                'name' => esc_html__('Monday', 'fitfab'),
                'id' => "{$prefix}select_class_monday",
                'type' => 'select_advanced',
                // Array of 'value' => 'Label' pairs for select box
                'options' => fitfab_class_posts_dropdown(),
                // Select multiple values, optional. Default is false.
                'multiple' => false,
                // 'std'         => 'value2', // Default value, optional
                'placeholder' => esc_html__('Select a Class', 'fitfab'),
            ),
            array(
                'name' => esc_html__('Tuesday', 'fitfab'),
                'id' => "{$prefix}select_class_tuesday",
                'type' => 'select_advanced',
                // Array of 'value' => 'Label' pairs for select box
                'options' => fitfab_class_posts_dropdown(),
                // Select multiple values, optional. Default is false.
                'multiple' => false,
                // 'std'         => 'value2', // Default value, optional
                'placeholder' => esc_html__('Select a Class', 'fitfab'),
            ),
            array(
                'name' => esc_html__('Wednesday', 'fitfab'),
                'id' => "{$prefix}select_class_wednesday",
                'type' => 'select_advanced',
                // Array of 'value' => 'Label' pairs for select box
                'options' => fitfab_class_posts_dropdown(),
                // Select multiple values, optional. Default is false.
                'multiple' => false,
                // 'std'         => 'value2', // Default value, optional
                'placeholder' => esc_html__('Select a Class', 'fitfab'),
            ),
            array(
                'name' => esc_html__('Thursday', 'fitfab'),
                'id' => "{$prefix}select_class_thursday",
                'type' => 'select_advanced',
                // Array of 'value' => 'Label' pairs for select box
                'options' => fitfab_class_posts_dropdown(),
                // Select multiple values, optional. Default is false.
                'multiple' => false,
                // 'std'         => 'value2', // Default value, optional
                'placeholder' => esc_html__('Select a Class', 'fitfab'),
            ),
            array(
                'name' => esc_html__('Friday', 'fitfab'),
                'id' => "{$prefix}select_class_friday",
                'type' => 'select_advanced',
                // Array of 'value' => 'Label' pairs for select box
                'options' => fitfab_class_posts_dropdown(),
                // Select multiple values, optional. Default is false.
                'multiple' => false,
                // 'std'         => 'value2', // Default value, optional
                'placeholder' => esc_html__('Select a Class', 'fitfab'),
            ),
            array(
                'name' => esc_html__('Saturday', 'fitfab'),
                'id' => "{$prefix}select_class_saturday",
                'type' => 'select_advanced',
                // Array of 'value' => 'Label' pairs for select box
                'options' => fitfab_class_posts_dropdown(),
                // Select multiple values, optional. Default is false.
                'multiple' => false,
                // 'std'         => 'value2', // Default value, optional
                'placeholder' => esc_html__('Select a Class', 'fitfab'),
            ),
            array(
                'name' => esc_html__('Sunday', 'fitfab'),
                'id' => "{$prefix}select_class_sunday",
                'type' => 'select_advanced',
                // Array of 'value' => 'Label' pairs for select box
                'options' => fitfab_class_posts_dropdown(),
                // Select multiple values, optional. Default is false.
                'multiple' => false,
                // 'std'         => 'value2', // Default value, optional
                'placeholder' => esc_html__('Select a Class', 'fitfab'),
            ),
        ),
    );

    $meta_boxes[] = array(
        'id' => 'logo_link',
        'title' => esc_html__('Logo Link', 'fitfab'),
        'pages' => array('brand'),
        'context' => 'normal',
        'priority' => 'core',
        'autosave' => true,
        // List of meta fields
        'fields' => array(
            array(
                'name' => '',
                'desc' => esc_html__('Enter Logo Link Here', 'fitfab'),
                'id' => "{$prefix}logo_link",
                'type' => 'text',
            ),
        ),
    );

    $meta_boxes[] = array(
        'id' => 'slider_title',
        'title' => esc_html__('Slidr Title', 'fitfab'),
        'pages' => array('slider'),
        'context' => 'normal',
        'priority' => 'core',
        'autosave' => true,
        // List of meta fields
        'fields' => array(
            array(
                'name' => '',
                'desc' => esc_html__('Enter Slider Title Here', 'fitfab'),
                'id' => "{$prefix}slider_title",
                'type' => 'text',
            ),
        ),
    );

    $meta_boxes[] = array(
        'id' => 'slider_sub_title',
        'title' => esc_html__('Slidr Sub Title', 'fitfab'),
        'pages' => array('slider'),
        'context' => 'normal',
        'priority' => 'core',
        'autosave' => true,
        // List of meta fields
        'fields' => array(
            array(
                'name' => '',
                'desc' => esc_html__('Enter Slider Sub Title Here', 'fitfab'),
                'id' => "{$prefix}slider_sub_title",
                'type' => 'text',
            ),
        ),
    );

    $meta_boxes[] = array(
        'id' => 'starts_text',
        'title' => esc_html__('Starts Text', 'fitfab'),
        'pages' => array('slider'),
        'context' => 'normal',
        'priority' => 'core',
        'autosave' => true,
        // List of meta fields
        'fields' => array(
            array(
                'name' => '',
                'desc' => esc_html__('Enter Starts Text Here', 'fitfab'),
                'id' => "{$prefix}starts_text",
                'type' => 'text',
            ),
        ),
    );

    $meta_boxes[] = array(
        'id' => 'join_button_text',
        'title' => esc_html__('Join Button Text', 'fitfab'),
        'pages' => array('slider'),
        'context' => 'normal',
        'priority' => 'core',
        'autosave' => true,
        // List of meta fields
        'fields' => array(
            array(
                'name' => '',
                'desc' => esc_html__('Enter Join Button Text', 'fitfab'),
                'id' => "{$prefix}join_button_text",
                'type' => 'text',
            ),
        ),
    );


    $meta_boxes[] = array(
        'id' => 'join_button_link',
        'title' => esc_html__('Join Button Link', 'fitfab'),
        'pages' => array('slider'),
        'context' => 'normal',
        'priority' => 'core',
        'autosave' => true,
        // List of meta fields
        'fields' => array(
            array(
                'name' => '',
                'desc' => esc_html__('Enter Join Button Link', 'fitfab'),
                'id' => "{$prefix}join_button_link",
                'type' => 'text',
            ),
        ),
    );

    /**
     * FitFab - trainer metaboxes
     * @since fitfab 1.0.0
     */
    
    /**
     * Fitfab - trainer type
     */
    $meta_boxes[] = array(
        'id' => 'trainer_exp',
        'title' => esc_html__('Trainer Experience', 'fitfab'),
        'pages' => array('trainer'),
        'context' => 'normal',
        'priority' => 'core',
        'autosave' => true,
        // List of meta fields
        'fields' => array(
            array(
                'name' => '',
                'desc' => esc_html__('Enter Trainer Experience', 'fitfab'),
                'id' => "{$prefix}trainer_exp",
                'type' => 'text',
                'type' => 'text',
                'size' => 70,
            ),
        ),
    );
                
    /**
     * Fitfab - trainer type
     */
    $meta_boxes[] = array(
        'id' => 'trainer_type',
        'title' => esc_html__('Trainer Type', 'fitfab'),
        'pages' => array('trainer'),
        'context' => 'normal',
        'priority' => 'core',
        'autosave' => true,
        // List of meta fields
        'fields' => array(
            array(
                'name' => '',
                'desc' => esc_html__('Enter Trainer Type', 'fitfab'),
                'id' => "{$prefix}trainer_type",
                'type' => 'text',
                'type' => 'text',
                'size' => 70,
            ),
        ),
    );
    
    /**
     * Fitfab - trainer video
     */
    $meta_boxes[] = array(
        'id' => 'trainer_video',
        'title' => esc_html__('Trainer Video', 'fitfab'),
        'pages' => array('trainer'),
        'context' => 'normal',
        'priority' => 'core',
        'autosave' => true,
        // List of meta fields
        'fields' => array(
            array(
                'name' => '',
                'desc' => esc_html__('Enter Trainer Video', 'fitfab'),
                'id' => "{$prefix}trainer_video",
                'type' => 'textarea',
            ),
        ),
    );            
    
    
    /**
     * Fitfab - trainer contact
     */
    $meta_boxes[] = array(
        'id' => 'trainer_contact',
        'title' => esc_html__('Trainer Contact', 'fitfab'),
        'pages' => array('trainer'),
        'context' => 'normal',
        'priority' => 'core',
        'autosave' => true,
        // List of meta fields
        'fields' => array(
            array(
                'name' => '',
                'desc' => esc_html__('Enter Your Contact', 'fitfab'),
                'id' => "{$prefix}trainer_contact",
                'type' => 'text',
                'size' => 70,
            ),
        ),
    );

    /**
     * Fitfab - qualification
     */
    $meta_boxes[] = array(
        'id' => 'trainer_qualification',
        'title' => esc_html__('Qualification', 'fitfab'),
        'pages' => array('trainer'),
        'context' => 'normal',
        'priority' => 'core',
        'autosave' => true,
        // List of meta fields
        'fields' => array(
            array(
                'name' => '',
                'desc' => esc_html__('Enter your qualification', 'fitfab'),
                'id' => "{$prefix}trainer_qualification",
                'type' => 'textarea',
            ),
        ),
    );

    /**
     * Fitfab - experience
     */
    $meta_boxes[] = array(
        'id' => 'trainer_experience',
        'title' => esc_html__('Experience', 'fitfab'),
        'pages' => array('trainer'),
        'context' => 'normal',
        'priority' => 'core',
        'autosave' => true,
        // List of meta fields
        'fields' => array(
            array(
                'name' => '',
                'desc' => esc_html__('Enter Your Experience', 'fitfab'),
                'id' => "{$prefix}trainer_experience",
                'type' => 'text',
                'class' => 'text_cls',
                'size' => 70,
                'clone' => true,
            ),
        ),
    );

    /**
     * Fitfab - experience
     */
    $meta_boxes[] = array(
        'id' => 'trainer_specialization',
        'title' => esc_html__('Specialization', 'fitfab'),
        'pages' => array('trainer'),
        'context' => 'normal',
        'priority' => 'core',
        'autosave' => true,
        // List of meta fields
        'fields' => array(
            array(
                'name' => '',
                'desc' => esc_html__('Enter Your Specialization', 'fitfab'),
                'id' => "{$prefix}trainer_specialization",
                'type' => 'text',
                'class' => 'text_cls',
                'size' => 70,
                'clone' => true,
            ),
        ),
    );

    return $meta_boxes;
}
