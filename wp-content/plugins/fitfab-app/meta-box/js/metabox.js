jQuery(document).ready(function($) {
    var selectedcheck = $('input[type=radio][name=post_format]:checked').val();
    $('#setting_post_video_code').css('display', 'none');
    $('#setting_post_quote').css('display', 'none');
    $('#setting_post_aside').css('display', 'none');
    $('#setting_post_slider_shortcode').css('display', 'none');
    $('#setting_v_url').css('display', 'none');
    $('#setting_a_url').css('display', 'none');
    $('#post-format-gallery').css('display', 'none');
    if (selectedcheck == 'slider')
    {
       $('#setting_post_slider_shortcode').css('display', 'block');
    }
    else if (selectedcheck == 'video')
    {
        $('#setting_post_video_code').css('display', 'block');
        $('#setting_v_url').css('display', 'block');
    }
    else if (selectedcheck == 'quote')
    {
       $('#setting_post_quote').css('display', 'block');
    }
     else if (selectedcheck == 'aside')
    {
        $('#setting_post_aside').css('display', 'block');
    }
    else if (selectedcheck == 'gallery')
    {
        $('#post-format-gallery').css('display', 'block');
    }
    else if (selectedcheck == 'audio')
    {
        $('#setting_a_url').css('display', 'block');
    }
    $('input[type=radio][name=post_format]').change(function() {
        $('#setting_post_video_code').css('display', 'none');
        $('#setting_post_quote').css('display', 'none');
        $('#setting_post_aside').css('display', 'none');
        $('#setting_post_slider_shortcode').css('display', 'none');
        $('#setting_v_url').css('display', 'none');
        $('#setting_a_url').css('display', 'none');
        $('#setting_gallery_images').css('display', 'none');
        if (this.value == 'slider') {
            $('#setting_post_slider_shortcode').css('display', 'block');
        }
        else if (this.value == 'video'){
            $('#setting_post_video_code').css('display', 'block');
            $('#setting_v_url').css('display', 'block');
        }
        else if (this.value == 'quote'){
            $('#setting_post_quote').css('display', 'block');
        }
        else if (this.value == 'aside'){
            $('#setting_post_aside').css('display', 'block');
        }
        else if (this.value == 'gallery'){
            $('#setting_gallery_images').css('display', 'block');
        }
        else if (this.value == 'audio')
        {
            $('#setting_a_url').css('display', 'block');
        }
    });
});