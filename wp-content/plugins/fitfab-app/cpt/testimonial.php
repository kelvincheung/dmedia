<?php 
/**
 * FitFab App - CPT 
 * 
 * register_post_type('testimonial', $args);
 * 
 * @package fitfab-app
 * @subpackage fitfab-app.cpt
 * @since fitfab-app 1.0.0 
 */
  function fitfab_testimonial_post_type() {

        $labels = array(
            'name'                => esc_html_x( 'Testimonials', 'Post Type General Name', 'fitfab' ),
            'singular_name'       => esc_html_x( 'Testimonial', 'Post Type Singular Name', 'fitfab' ),
            'menu_name'           => esc_html__( 'Testimonial', 'fitfab' ),
            'name_admin_bar'      => esc_html__( 'Testimonial', 'fitfab' ),
            'parent_item_colon'   => esc_html__( 'Parent Testimonial:', 'fitfab' ),
            'all_items'           => esc_html__( 'All Testimonials', 'fitfab' ),
            'add_new_item'        => esc_html__( 'Add New Testimonial', 'fitfab' ),
            'add_new'             => esc_html__( 'Add New', 'fitfab' ),
            'new_item'            => esc_html__( 'New Testimonial', 'fitfab' ),
            'edit_item'           => esc_html__( 'Edit Testimonial', 'fitfab' ),
            'update_item'         => esc_html__( 'Update Testimonial', 'fitfab' ),
            'view_item'           => esc_html__( 'View Testimonial', 'fitfab' ),
            'search_items'        => esc_html__( 'Search Testimonial', 'fitfab' ),
            'not_found'           => esc_html__( 'Not found', 'fitfab' ),
            'not_found_in_trash'  => esc_html__( 'Not found in Trash', 'fitfab' ),
        );
        $args = array(
            'label'               => esc_html__( 'Testimonial', 'fitfab' ),
            'description'         => esc_html__( 'Testimonial Description', 'fitfab' ),
            'labels'              => $labels,
            'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', ),
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'menu_icon'           => 'dashicons-testimonial',
            'show_in_admin_bar'   => true,
            'show_in_nav_menus'   => true,
            'can_export'          => true,
            'has_archive'         => true,		
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'capability_type'     => 'page',
        );
        register_post_type( 'testimonial', $args );
    }
    add_action( 'init', 'fitfab_testimonial_post_type', 0 );

