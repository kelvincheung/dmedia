<?php

/**
 * FitFab App - CPT 
 * 
 * register_post_type('class', $args);
 * register_taxonomy('trainer', $args);
 * 
 * @package fitfab
 * @since fitfab 1.0.0 
 */
function fitfab_classes_post_type() {

    $labels = array(
        'name' => esc_html_x('Class', 'Post Type General Name', 'fitfab'),
        'singular_name' => esc_html_x('Class', 'Post Type Singular Name', 'fitfab'),
        'menu_name' => esc_html__('Classes', 'fitfab'),
        'name_admin_bar' => esc_html__('Post Type', 'fitfab'),
        'parent_item_colon' => esc_html__('Parent Class', 'fitfab'),
        'all_items' => esc_html__('All Classes', 'fitfab'),
        'add_new_item' => esc_html__('Add New Class', 'fitfab'),
        'add_new' => esc_html__('Add New', 'fitfab'),
        'new_item' => esc_html__('New Class', 'fitfab'),
        'edit_item' => esc_html__('Edit Class', 'fitfab'),
        'update_item' => esc_html__('Update Class', 'fitfab'),
        'view_item' => esc_html__('View Class', 'fitfab'),
        'search_items' => esc_html__('Search Class', 'fitfab'),
        'not_found' => esc_html__('Not found', 'fitfab'),
        'not_found_in_trash' => esc_html__('Not found in Trash', 'fitfab'),
    );
    $args = array(
        'label' => esc_html__('class', 'fitfab'),
        'description' => esc_html__('Post Type Description', 'fitfab'),
        'labels' => $labels,
        'supports' => array('title', 'editor', 'excerpt', 'thumbnail'),
        'taxonomies' => array('class_categories'),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_icon' => 'dashicons-welcome-learn-more',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'page',
    );
    register_post_type('class', $args);
}

add_action('init', 'fitfab_classes_post_type', 0);

function fitfab_classes_taxonomy() {
    $labels = array(
        'name' => _x('Categories', 'prefix_video', 'fitfab'),
        'singular_name' => _x('Category', 'prefix_video', 'fitfab'),
        'menu_name' => esc_html__('Categories', 'fitfab'),
        'all_items' => esc_html__('All Categories', 'fitfab'),
        'edit_item' => esc_html__('Edit Category', 'fitfab'),
        'view_item' => esc_html__('View Category', 'fitfab'),
        'update_item' => esc_html__('Update Category', 'fitfab'),
        'add_new_item' => esc_html__('Add New Category', 'fitfab'),
        'new_item_name' => esc_html__('New Category Name', 'fitfab'),
        'parent_item' => esc_html__('Parent Category', 'fitfab'),
        'parent_item_colon' => esc_html__('Parent Category:', 'fitfab'),
        'search_items' => esc_html__('Search Categories', 'fitfab'),
    );
    $args = array(
        'labels' => $labels,
        'hierarchical' => true,
        'public' => true,
        'show_ui' => true,
        'show_admin_column' => true,
        'show_in_nav_menus' => true,
        'show_tagcloud' => true,
    );
    register_taxonomy('class_categories', array('class'), $args);
}

add_action('init', 'fitfab_classes_taxonomy', 0);