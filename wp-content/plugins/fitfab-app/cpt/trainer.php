<?php
/**
 * FitFab App - CPT 
 * 
 * register_post_type('trainer', $args);
 * @package fitfab
 * @since fitfab 1.0.0 
 */
function fitfab_trainer_post_type() {

    $labels = array(
        'name' => esc_html_x('Trainer', 'Post Type General Name', 'fitfab'),
        'singular_name' => esc_html_x('Trainer', 'Post Type Singular Name', 'fitfab'),
        'menu_name' => esc_html__('Trainer', 'fitfab'),
        'name_admin_bar' => esc_html__('Post Type', 'fitfab'),
        'parent_item_colon' => esc_html__('Parent Class', 'fitfab'),
        'all_items' => esc_html__('All Trainer', 'fitfab'),
        'add_new_item' => esc_html__('Add New Trainer', 'fitfab'),
        'add_new' => esc_html__('Add New', 'fitfab'),
        'new_item' => esc_html__('New Trainer', 'fitfab'),
        'edit_item' => esc_html__('Edit Trainer', 'fitfab'),
        'update_item' => esc_html__('Update Trainer', 'fitfab'),
        'view_item' => esc_html__('View Trainer', 'fitfab'),
        'search_items' => esc_html__('Search Trainer', 'fitfab'),
        'not_found' => esc_html__('Not found', 'fitfab'),
        'not_found_in_trash' => esc_html__('Not found in Trash', 'fitfab'),
    );
    $args = array(
        'label' => esc_html__('trainer', 'fitfab'),
        'description' => esc_html__('Post Type Description', 'fitfab'),
        'labels' => $labels,
        'supports' => array('title', 'editor', 'thumbnail'),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_icon' => 'dashicons-businessman',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'page',
    );
    register_post_type('trainer', $args);
}

add_action('init', 'fitfab_trainer_post_type', 0);
