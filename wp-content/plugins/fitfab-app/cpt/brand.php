<?php

/**
 * FitFab App - CPT 
 * 
 * register_post_type('brand', $args);
 * 
 * @package fitfab-app
 * @subpackage fitfab-app.cpt
 * @since fitfab-app 1.0.0 
 */
function fitfab_brand_post_type() {

    $labels = array(
        'name' => esc_html_x('Brand', 'Post Type General Name', 'fitfab'),
        'singular_name' => esc_html_x('Brand', 'Post Type Singular Name', 'fitfab'),
        'menu_name' => esc_html__('Brands', 'fitfab'),
        'name_admin_bar' => esc_html__('Post Type', 'fitfab'),
        'parent_item_colon' => esc_html__('Parent Class', 'fitfab'),
        'all_items' => esc_html__('All Brands', 'fitfab'),
        'add_new_item' => esc_html__('Add New Brand', 'fitfab'),
        'add_new' => esc_html__('Add New', 'fitfab'),
        'new_item' => esc_html__('New Brand', 'fitfab'),
        'edit_item' => esc_html__('Edit Brand', 'fitfab'),
        'update_item' => esc_html__('Update Brand', 'fitfab'),
        'view_item' => esc_html__('View Brand', 'fitfab'),
        'search_items' => esc_html__('Search Brand', 'fitfab'),
        'not_found' => esc_html__('Not found', 'fitfab'),
        'not_found_in_trash' => esc_html__('Not found in Trash', 'fitfab'),
    );
    $args = array(
        'label' => esc_html__('brand', 'fitfab'),
        'description' => esc_html__('Post Type Description', 'fitfab'),
        'labels' => $labels,
        'supports' => array('title','thumbnail'),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_icon' => 'dashicons-index-card',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'page',
    );
    register_post_type('brand', $args);
}

add_action('init', 'fitfab_brand_post_type', 0);
