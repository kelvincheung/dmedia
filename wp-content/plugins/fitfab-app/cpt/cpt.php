<?php
/**
 * FitFab App - CPT 
 *
 * @package fitfab-app
 * @subpackage fitfab-app.cpt
 * @since fitfab-app 1.0.0 
 */
require_once "classes.php";
require_once "trainer.php";
require_once "packages.php";
require_once "testimonial.php";
require_once "brand.php";
require_once "faq.php";

add_action("admin_head", "fitfab_cpt_icon_color");

function fitfab_cpt_icon_color() {
    global $fitfab_data;

    if (!empty($fitfab_data['primary_color'])):
        ?>
        <style>
            #adminmenu .menu-icon-class div.wp-menu-image:before ,
            #adminmenu .menu-icon-testimonial div.wp-menu-image:before ,
            #adminmenu .menu-icon-brand div.wp-menu-image:before ,
            #adminmenu .menu-icon-trainer div.wp-menu-image:before,
            #adminmenu .menu-icon-package div.wp-menu-image:before,
            #adminmenu .menu-icon-faq div.wp-menu-image:before {  color: <?php print($fitfab_data['primary_color']); ?>; }

        </style>
        <?php
    endif;
}
