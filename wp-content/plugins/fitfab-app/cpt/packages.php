<?php
/**
 * FitFab - packages 
 * 
 * @package fitfab
 * @since fitfab 1.0.0 
 */
function fitfab_packages_post_type() {

    $labels = array(
        'name' => esc_html_x('Package', 'Post Type General Name', 'fitfab'),
        'singular_name' => esc_html_x('Package', 'Post Type Singular Name', 'fitfab'),
        'menu_name' => esc_html__('Packages', 'fitfab'),
        'name_admin_bar' => esc_html__('Post Type', 'fitfab'),
        'parent_item_colon' => esc_html__('Parent Package', 'fitfab'),
        'all_items' => esc_html__('All Packages', 'fitfab'),
        'add_new_item' => esc_html__('Add New Package', 'fitfab'),
        'add_new' => esc_html__('Add New', 'fitfab'),
        'new_item' => esc_html__('New Package', 'fitfab'),
        'edit_item' => esc_html__('Edit Package', 'fitfab'),
        'update_item' => esc_html__('Update Package', 'fitfab'),
        'view_item' => esc_html__('View Package', 'fitfab'),
        'search_items' => esc_html__('Search Package', 'fitfab'),
        'not_found' => esc_html__('Not found', 'fitfab'),
        'not_found_in_trash' => esc_html__('Not found in Trash', 'fitfab'),
    );
    $args = array(
        'label' => esc_html__('class', 'fitfab'),
        'description' => esc_html__('Post Type Description', 'fitfab'),
        'labels' => $labels,
        'supports' => array('title', 'editor', 'excerpt','thumbnail',),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_icon' => 'dashicons-controls-back',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'page',
    );
    register_post_type('package', $args);
}

add_action('init', 'fitfab_packages_post_type', 0);


