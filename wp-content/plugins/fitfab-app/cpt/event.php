<?php 
/**
 * FitFab App - CPT 
 * 
 * register_post_type( 'event', $args );
 * 
 * @package fitfab-app
 * @subpackage fitfab-app.cpt
 * @since fitfab-app 1.0.0 
 */
 
function event_post_type() {

	$labels = array(
		'name'                => esc_html_x( 'Events', 'Post Type General Name', 'fitfab' ),
		'singular_name'       => esc_html_x( 'Event', 'Post Type Singular Name', 'fitfab' ),
		'menu_name'           => esc_html__( 'Event', 'fitfab' ),
		'name_admin_bar'      => esc_html__( 'Event', 'fitfab' ),
		'parent_item_colon'   => esc_html__( 'Parent Event:', 'fitfab' ),
		'all_items'           => esc_html__( 'All Events', 'fitfab' ),
		'add_new_item'        => esc_html__( 'Add New Event', 'fitfab' ),
		'add_new'             => esc_html__( 'Add New', 'fitfab' ),
		'new_item'            => esc_html__( 'New Event', 'fitfab' ),
		'edit_item'           => esc_html__( 'Edit Event', 'fitfab' ),
		'update_item'         => esc_html__( 'Update Event', 'fitfab' ),
		'view_item'           => esc_html__( 'View Event', 'fitfab' ),
		'search_items'        => esc_html__( 'Search Event', 'fitfab' ),
		'not_found'           => esc_html__( 'Not found', 'fitfab' ),
		'not_found_in_trash'  => esc_html__( 'Not found in Trash', 'fitfab' ),
	);
	$args = array(
		'label'               => esc_html__( 'Event', 'fitfab' ),
		'description'         => esc_html__( 'Event Description', 'fitfab' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'trackbacks', 'revisions', 'custom-fields', 'page-attributes', 'post-formats', ),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'rewrite'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-calendar',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,		
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'event', $args );

}
add_action( 'init', 'event_post_type', 0 );


