<?php
/**
 * FitFab App - FAQ 
 * @package fitfab
 * @since fitfab 1.0.0
 */
add_action('init', 'register_cpt_faq');

function register_cpt_faq() {
    $labels = array(
        'name' => esc_html_x('FAQ', 'fitfab'),
        'singular_name' => esc_html_x('FAQ', 'fitfab'),
        'add_new' => esc_html_x('Add New FAQ', 'fitfab'),
        'add_new_item' => esc_html_x('Add New FAQ', 'fitfab'),
        'edit_item' => esc_html_x('Edit FAQ', 'fitfab'),
        'new_item' => esc_html_x('New FAQ', 'fitfab'),
        'view_item' => esc_html_x('View faq', 'fitfab'),
        'search_items' => esc_html_x('Search faq', 'fitfab'),
        'not_found' => esc_html_x('No faq found', 'fitfab'),
        'not_found_in_trash' => esc_html_x('No faq found in Trash', 'fitfab'),
        'parent_item_colon' => esc_html_x('Parent faq:', 'fitfab'),
        'menu_name' => esc_html_x('FAQ', 'fitfab'),
    );
    $args = array(
        'labels' => $labels,
        'hierarchical' => true,
        'supports' => array('title', 'editor', 'author', 'thumbnail'),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => true,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'menu_icon' => 'dashicons-format-chat',
        'capability_type' => 'post'
    );
    register_post_type('faq', $args);
}
